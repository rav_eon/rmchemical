<?php
namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
Use App\User;
Use Session;
use Redirect;
use Carbon\Carbon;
use File;
use DateTime;
use Illuminate\Support\Facades\DB; 
    
// class Database {
  // protected $userName;		
  // protected $password;
  // protected $dbName;

  // public function __construct ( $UserName, $Password, $DbName ) {
    // $this->userName = $UserName;
    // $this->password = $Password;
    // $this->dbName = $DbName;
  // }
// }

// and you would use this as:
// $db = new Database ( 'user_name', 'password', 'database_name' );

class Controller extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
    // function getData(){
        // $data =  DB::table('machine')->get();
        // if($data > 0){
            // return view('welcome', $data);
        // }else{
            // return view('welcome');
        // }
    // }
	
	// public function __construct($db){
		// $host = Session::get('host');
		// $dbname = Session::get('dbname');
		// $user = Session::get('user');
		// $pass = Session::get('pass');
		// $db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		// $this->db => $db; 
	// }

    function display_info(Request $req){
        $regno = $req->regno;  
        return view('display_info', ['regno' => $regno]);
    }
	
	
    function display_info_fetch(Request $req){
        $txtStartDate = $req->txtStartDate; 
        $regno = $req->regno;
        $shift_hours = $req->shift_hours;    
        $machine_select = $req->machine_select; 
       
        return view('display_info_fetch', [ 'regno'=>$regno,'machine_select'=> $machine_select, 'txtStartDate' => $txtStartDate, 'shift_hours' => $shift_hours]);
    }


    function login(Request $req){
		$user_name = $req->username;
		$user_pass = $req->password;
		//$this->database($db) ;
		//dd($db);
		$host = "voyagereon.com";
		$dbname="eon_rmchemicals";
		// $host = "localhost";
		// $dbname="db_rmchemicals";
		$user="postgres";
		$pass="eon@c180";		
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");		
		
		$check_db = pg_query($db, "SELECT * FROM rm_users WHERE login_name='$user_name' AND pwd='$user_pass'"); 
		//$check_db = DB::table('rm_users'); 
		// echo "<pre>";
		// print_r($check_db);
		// echo "<pre>";
		// dd();
		
		Session::put('host',$host);
		Session::put('dbname',$dbname);
		Session::put('user',$user);
		Session::put('pass',$pass);
		
		
		if(pg_num_rows($check_db)>0){
		// if(($check_db)>0){
			while($rows = pg_fetch_array($check_db)){				
				$firstname = $rows['login_name'];
				$email = $rows['email_id'];
				$id = $rows['id'];
				$designation_id = $rows['user_type_id'];
				$req->session()->put('firstname', $firstname);
				$req->session()->put('id', $id);
				$req->session()->put('email', $email);
				$req->session()->put('designation_id', $designation_id);
				
				session()->put('designation_id', $designation_id);
				
				$session_firstname = $req->session()->get('firstname');
				$session_email = $req->session()->get('email');
				$session_designation_id =  $req->session()->get('designation_id');
				$session_id = $req->session()->get('id');
				
				if($designation_id == 1){
					return redirect('/');					
				}else if($designation_id == 2){
					return redirect('/manager');
				}else if($designation_id == 3){
					return redirect('/executive');					
				}else if($designation_id == 4){
					return redirect('/shift_officer');
				}
			}
			
		}else{
			$req->session()->put('message', 'Username or Password Incorrect');
			$session_message = $req->session()->get('message');
			return redirect('/login')->with('session_message',$session_message);
		}
    }

	function register(Request $req){
		$firstname = $req->firstname;
		$lastname = $req->lastname;
		$email = $req->email;  
		$mobileno = $req->mobile;      
		$unit_id = $req->unit_id;
		$user_id = $req->user_id;
		$plant_id = $req->plant_id;
		$machine_id = $req->machine_id;
		$designation_id = $req->designation_id;
		$shift_id = $req->shift_id;
		$password = $req->password;
		$confirm_password = $req->confirm_password;
		
		$session_user_id = $req->session()->put('id', $user_id);	
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
      
        $register_user = pg_query($db, "INSERT INTO users(firstname, lastname, email, mobileno, shift_id, unit_id, plant_id, machine_id, designation_id, password) VALUES('$firstname','$lastname','$email','$mobileno','$shift_id','$unit_id','$plant_id','$machine_id','$designation_id','$password')");

        if($register_user){
			Session::flash('message', 'User has been created Successfully'); 
			return redirect('/');
        }else{
            echo "Member not registered successfully";
        }
    }
	
	public function operatorinfo(Request $req){
		//dd($req);
		$operator_name = $req->operator_name;
		$operator_pass = $req->operator_pass;
				
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
			
		$operatorinfo = pg_query($db, "SELECT * FROM rm_users WHERE login_name = '".$operator_name."' AND pwd = '$operator_pass' ");		
		if(pg_num_rows($operatorinfo)>0){
			while($operator_row = pg_fetch_array($operatorinfo)){
				// echo "<pre>";
				// print_r($operator_row['id']);
				// echo "</pre>";
				$op_id = $operator_row['id'];
				Session::put('op_id', $op_id);
			}
			//echo Session::get('op_id');
			Session::put('operator_name',$operator_name);
			//dd();
			return view('operatorsubmit');			
		}else{
			echo "SORRY!!! Your username or password is incorrect. Please try again. ";
		}		
	}
	
	public function operatorlog(Request $req){
		// dd($req);
		$operator_name = $req->operator_name;
		$shift_duration = $req->shift_duration;
		// dd(Session::all());
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		// dd(Session::all());
		// dd($operator_name);
		$operator_query = pg_query($db, "SELECT * FROM rm_users WHERE login_name = '".$operator_name."'");	
		
		if(empty(pg_num_rows($operator_query))){
			// return redirect();
			$status = "INCORRECT OPERATOR NAME / ऑपरेटर का नाम गलत है";
			return view('operatorcode')->withErrors($status);		
		}
		
		$flag = 0;
		//echo Session::get('flag');
		if(Session::get('flag') != 1){
			// dd('1');
			if(pg_num_rows($operator_query)>0){
				$flag = 1; 
				// dd('111');
				// print_r($operator_row = pg_fetch_array($operator_query));
				// dd($operator_row);
				while($operator_row = pg_fetch_array($operator_query)){										
					$operator_id = $operator_row['id'];
					$user_type_id = $operator_row['user_type_id'];
					$op_name = $operator_row['login_name'];
					$first_name = $operator_row['first_name'];
					$status = $operator_row['status'];
					// dd($operator_id);
					// dd($operator_row);
					Session::put('operator_id',$operator_id);
					Session::put('op_name',$op_name);
					Session::put('first_name',$first_name);
					Session::put('shift_duration',$shift_duration);
					if($user_type_id == 5){
						//echo "operator";				
					}else{
						$usertype = "You are not Operator";
						//dd($op_name);
						return Redirect::back()->withErrors($usertype);	
					}
					
					if($status == 't'){
						//echo "TRUE";
					}else{
						$status = "You STATUS is not ACTIVE";
						return Redirect::back()->withErrors($status);	
					}					
					Session::put('flag', $flag);
					return view('operatorsubmit')->with('operator_id', $operator_id);				
				}
			}else{	
				// dd('00');
				$status = "INCORRECT OPERATOR NAME / ऑपरेटर का नाम गलत है";
				return view('operatorcode')->withErrors($status);				
				// return redirect('/operatorlog');
			}
		}else{
			// dd('0000');
			// print_r($operator_row = pg_fetch_array($operator_query));
			while($operator_row = pg_fetch_array($operator_query)){	
				$operator_id = $operator_row['id'];
				$user_type_id = $operator_row['user_type_id'];
				$op_name = $operator_row['login_name'];
				$first_name = $operator_row['first_name'];
				$status = $operator_row['status'];
				// dd($operator_id);
				Session::put('operator_id',$operator_id);				
				Session::put('op_name',$op_name);
				Session::put('first_name',$first_name);
				Session::put('shift_duration',$shift_duration);
			}
		}
		
		//dd($operator_name);
		$operator_name = $req->operator_name;
		$unit_id = $req->unit_id;
		$plant_id = $req->plant_id;	
		$cascade_id = $req->cascade_id;
		$shift_code = $req->currentshift_id;
		//dd($shift_code);
		$operator_id = Session::get('operator_id');	
		$machine_id = Session::get('machine_id');
		// dd($operator_id);
		// "machine id===".$machine_id;
		$machine_name = $req->machine_name;	
		$shift_duration = $req->shift_duration;	
		$designation_id = 5;
	
		Session::put('operator_name', $operator_name);
		Session::put('unit_id', $unit_id);
		Session::put('plant_id', $plant_id);
		Session::put('cascade_id', $cascade_id);
		Session::put('machine_name', $machine_name);
		Session::put('shift_duration', $shift_duration);
		Session::put('designation_id', $designation_id);
	
		// date_default_timezone_set('Asia/Kolkata');
		// $date = new \DateTime();
		// $logintime = date_format($date, 'H:i:s');
		$logintime = date('Y-m-d H:i:s');
		// Display value of variable
		// echo $variable;
		// dd($variable);  
		$log = 1;	
		
		$operator_query22 = pg_query($db, "SELECT * FROM rm_users WHERE login_name = '".$operator_name."'");
		$operatorrowid = pg_fetch_array($operator_query22);		
		$checkoperator_id = $operatorrowid['id'];
		
		$operatorexist = pg_query($db, "SELECT * FROM rm_oplogins WHERE operator_id = $checkoperator_id AND loggedin = '1'");
		if(pg_num_rows($operatorexist)>0){						
			return redirect('operatorexist')->withErrors('Operator Already Loggedin');			
		}
		
		$operator_log = pg_query($db, "INSERT INTO rm_oplogins(operator_id, machine_id, logintime, shift_code, shift_duration, loggedin) VALUES('$operator_id','$machine_id','$logintime','$shift_code','$shift_duration','1')");
		
		$machine_regno = Session::get('machine_regno');
		$machineregnovaluechecked = pg_query($db, "UPDATE public.rm_machines SET loggedin = 1 WHERE machine_regno='$machine_regno'");
			
		if($operator_log){
			return redirect('/operator');
		}else{
			echo "SORRY!!! Technical Error!";
		}
	}
	
	function fetch(Request $req){
        $start_date = $req->txtStartDate;
        $deviceid = $req->device_id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$shift_timing_query = pg_query($db, "SELECT * FROM rm_shifts");
		
		$shift_time = $req->shift_hours;
		$machine_name = $req->machine_select;
		$deviceid = 1224;
		while($shift_row = pg_fetch_array($shift_timing_query)){
			$shift = $shift_row['shift_name'];
			if($shift_time == 'morning' && $shift == 'morning'){				
				$shift_start_time = $shift_row['shift_start_at'];
				$shift_end_time = $shift_row['shift_end_at'];
				
				$start_date_time_mix = $start_date." ".$shift_start_time;
				$end_date_time_mix = $start_date." ".$shift_end_time;				
			}else if($shift_time == 'evening' && $shift == 'evening'){
				$shift_start_time = $shift_row['shift_start_at'];
				$shift_end_time = $shift_row['shift_end_at'];
				
				$start_date_time_mix = $start_date." ".$shift_start_time;
				$end_date_time_mix = $start_date." ".$shift_end_time;
				
			}else if($shift_time == 'night'  && $shift == 'night'){
				$shift_start_time = $shift_row['shift_start_at'];
				$shift_end_time = $shift_row['shift_end_at'];
				
				$start_date_time_mix = $start_date." ".$shift_start_time;
				$end_date_time_mix = $start_date." ".$shift_end_time;
			}
		}		
		
        return view('fetch', ['shift_time'=>$shift_time , 'start_date'=>$start_date,'start_date_time_mix' => $start_date_time_mix, 'end_date_time_mix' => $end_date_time_mix, 'shift_start_time'=>$shift_start_time, 'shift_end_time'=>$shift_end_time, 'machine_name'=> $machine_name, 'deviceid' => $deviceid]);
    }
	
	public function opskudata(Request $req){
		// dd($req);
		//dd(Session::all());
		$sku_row_id = $req->sku_row;
		// $sku_rpm = $req->sku_rpm;
		$sku_rpm = $req->sku_rpm2;
		$remarks = $req->remarks;
		$operator_id = Session::get('operator_id');
		$machine_id = Session::get('machine_id');
		// date_default_timezone_set('Asia/Kolkata');
		$date = new \DateTime();
		$now = date_format($date, 'H:i:s');
		Session::put('sku_row_id', $sku_row_id);
		Session::put('sku_rpm', $sku_rpm);
		Session::put('remarks', $remarks);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		
		// $skudatarecord = pg_query($db, "INSERT INTO rm_opskudata(operator_id, machine_id, sku_id, rpm, remark, sku_start_at) VALUES('$operator_id','$machine_id','$sku_row_id','$sku_rpm','$remarks','$now')");
		$skudatarecord = pg_query($db, "INSERT INTO rm_opskudata(operator_id, machine_id, sku_id, rpm, remark) VALUES('$operator_id','$machine_id','$sku_row_id','$sku_rpm','$remarks')returning id");
		
		$newskuid = pg_result($skudatarecord,'0','id');
		Session::put('latestskuid',$newskuid);
			
		if($skudatarecord){
			$sku = pg_query($db, "SELECT sku_name FROM rm_skus WHERE id=$sku_row_id");
			while($sku_row = pg_fetch_array($sku)){
				// echo "<pre>";
				// print_r($sku_row);
				// echo "</pre>";
				$sku_name = $sku_row['sku_name'];
				Session::put('sku_name', $sku_name);
				$production_graphics = 'production_graphics';
				Session::put('production_graphics', $production_graphics);
			}
			//dd($sku_row['sku_name'])
			return redirect('/operator');
		}else{
			echo "SORRY!!! Technical Error!";
		}
	}
	
	public function pcu_unit($id){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		// dd();
		$pcu_plant_query = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$id");
		$pcu_plant_array = array();
		while($pcu_plant_row = pg_fetch_array($pcu_plant_query)){
			$unit_id = $pcu_plant_row['id'];
			$unit_name = $pcu_plant_row['plant_name'];
			$pcu_plant_array[] = array(
				"plant_id" => $unit_id,
				"plant_name" => $unit_name
			);
		}
		$json = json_encode($pcu_plant_array);
		return $json;
	}
	
	public function pcu_plant($id){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		// dd($id);
		
	}
	
	
	public function operatorcodverfication(Request $req){
		// dd($req);
		$op_name = $req->operator_name;
		$shift_duration = $req->shift_duration;
		// dd($shift_duration);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");	
		
		$operator_query = pg_query($db, "SELECT * FROM rm_users WHERE login_name = '$op_name'");
		while($operator_row = pg_fetch_array($operator_query)){
			// echo "<pre>";
			// print_r($operator_row);
			// echo "</pre>";
			//dd();
			
			$operator_id = $operator_row['id'];
			$operator_firstname = $operator_row['first_name'];
			$user_type_id = $operator_row['user_type_id'];
			$status = $operator_row['status'];
			if($user_type_id == 5){
				//echo "operator";				
			}else{
				$usertype = "You are not Operator";
				return Redirect::back()->withErrors($usertype);	
			}
			
			if($status == 't'){
				//echo "TRUE";
			}else{
				$status = "You STATUS is not ACTIVE";
				return Redirect::back()->withErrors($status);	
			}
			Session::put('operator_id',$operator_id);
			Session::put('op_name',$op_name);
			Session::put('first_name',$first_name);
			Session::put('shift_duration',$shift_duration);
			return view('operatorsubmit');
		}
		
	}
	
	// ################################# UNIT CRUD ############################
	public function newunit(Request $req){
		//dd($req);
		$unit_code = $req->new_unit_code;
		$unit_name = $req->new_unit_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$selectunit = pg_query($db, "SELECT unit_name FROM rm_units WHERE unit_name = '$unit_name'");
		if(pg_num_rows($selectunit)){
			return redirect()->back()->withErrors('Unit with this name or code already exist');
		}
		
		$unitadd = pg_query($db, "INSERT INTO rm_units(unit_code, unit_name,status) VALUES('$unit_code','$unit_name','$status')");
		if($unitadd){	
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_unit = 'admin_unit';		
			Session::put('admin_unit',$admin_unit);	
			$message = "UNIT HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			return redirect('/');
		}else{
			echo "SORRY!!! UNIT HAS NOT BEEN CREATED";
		}
	}
	
	public function unitedit($id){
		//dd($id);
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$unitedit = pg_query($db, "SELECT * FROM rm_units WHERE id=$id");
		$edit_by_unit = array();
		while($unit_row = pg_fetch_array($unitedit)){
			// echo "<pre>";
			// print_r($unit_row);
			// echo "</pre>";
			// $unit_code = $unit_row['unit_code']; 
			// $unit_name = $unit_row['unit_name']; 
			$edit_by_unit[] = $unit_row;
		}	
		$unit_id = $edit_by_unit[0]['id'];
		$unit_code = $edit_by_unit[0]['unit_code'];
		$unit_name = $edit_by_unit[0]['unit_name'];
		$status = $edit_by_unit[0]['status'];
		
		Session::put('unit_id', $unit_id);
		Session::put('unit_code', $unit_code);
		Session::put('unit_name', $unit_name);
		Session::put('status', $status);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editunit');
		
	}	
	
	public function unitupdate(Request $req){
		//dd($req);
		$id = $req->unit_id;
		$unit_code = $req->unit_code;
		$unit_name = $req->unit_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$unitupdate = pg_query($db, "UPDATE rm_units SET unit_code = '$unit_code', unit_name = '$unit_name', status = '$status' WHERE id=$id");
		if($unitupdate){
			$message = "UNIT HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_unit = 'admin_unit';
			Session::put('admin_unit',$admin_unit);			
			return redirect('/');
		}else{
			echo "SORRY!!! UNIT HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function unitdelete(Request $req){
		//dd($req);
		$id = $req->unit_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$unitdelete = pg_query($db, "DELETE FROM rm_units WHERE id = $id");
		if($unitdelete){
			$message = "UNIT HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_unit = 'admin_unit';
			Session::put('admin_unit',$admin_unit);	
			return redirect('/');
		}else{
			echo "SORRY!!! UNIT HAS NOT BEEN DELETED";
		}
	}
	
	// ################################ PLANT CRUD ###########################
	public function newplant(Request $req){
		//dd($req);
		$unit_id = $req->plant_unit_id;
		$plant_code = $req->new_plant_code;
		$plant_name = $req->new_plant_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantadd = pg_query($db, "INSERT INTO rm_plants(unit_id, plant_code, plant_name,status) VALUES('$unit_id','$plant_code','$plant_name','$status')");
		if($plantadd){			
			$message = "PLANT HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_cascade');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_plant = 'admin_plant';
			Session::put('admin_plant',$admin_plant);
			Session()->forget('unit_id');
			return redirect('/');
		}else{
			echo "SORRY!!! PLANT HAS NOT BEEN CREATED";
		}
	}
	
	public function plantedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantedit = pg_query($db, "SELECT * FROM rm_plants WHERE id=$id");
		$edit_by_plant = array();
		while($plant_row = pg_fetch_array($plantedit)){
			$edit_by_plant[] = $plant_row;
		}	
		$plant_id = $edit_by_plant[0]['id'];
		$unit_id = $edit_by_plant[0]['unit_id'];
		$plant_code = $edit_by_plant[0]['plant_code'];
		$plant_name = $edit_by_plant[0]['plant_name'];
		$status = $edit_by_plant[0]['status'];
		
		Session::put('plant_id', $plant_id);
		Session::put('unit_id', $unit_id);
		Session::put('plant_code', $plant_code);
		Session::put('plant_name', $plant_name);
		Session::put('status', $status);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editplant');		
	}	
	
	public function plantupdate(Request $req){
		$id = Session::get('plant_id');
		//$id = $req->id;
		$plant_unit_id = $req->plant_unit_id;
		$plant_code = $req->plant_code;
		$plant_name = $req->plant_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantupdate = pg_query($db, "UPDATE rm_plants SET unit_id = '$plant_unit_id', plant_code = '$plant_code', plant_name = '$plant_name', status = '$status' WHERE id=$id");
		if($plantupdate){
			$message = "PLANT HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			Session::forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_plant = 'admin_plant';
			Session::put('admin_plant',$admin_plant);			
			return redirect('/');
		}else{
			echo "SORRY!!! PLANT HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function plantdelete(Request $req){
		//dd($req);
		$id = $req->plant_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantdelete = pg_query($db, "DELETE FROM rm_plants WHERE id = $id");
		if($plantdelete){
			$message = "PLANT HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			Session::forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_plant = 'admin_plant';
			Session::put('admin_plant',$admin_plant);	
			return redirect('/');
		}else{
			echo "SORRY!!! PLANT HAS NOT BEEN DELETED";
		}
	}
	// ################################ PLANT PINCODE CRUD ###########################
	public function newplantpincode(Request $req){
		//dd($req);
		$unit_id = $req->unit_name;
		$plant_name = $req->plant_name;
		$pin_code = $req->pin_code;
		
		// print_r($productmixexist);
		// if(pg_num_rows($productmixexist)>0){
			// return view('add-plant-pincode')->withErrors('Machine Code and Product Code Already Exist');
		// }
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmixexist = pg_query($db, "SELECT * FROM rm_pincodes WHERE unit_id=$unit_id AND plant_id = $plant_name");
		if(pg_num_rows($productmixexist) > 0){
			$msg = "Duplicate entry of Machine id or product id";
			return view('addplant-pincode')->withErrors($msg);
		}
		$plantadd = pg_query($db, "INSERT INTO rm_pincodes(unit_id, plant_id, pin_code) VALUES('$unit_id','$plant_name','$pin_code')");
		if($plantadd){			
			$message = "PLANT HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_plant_pincode = 'admin_plant_pincode';
			Session::put('admin_plant_pincode',$admin_plant_pincode);			
			return redirect('/');
		}else{
			echo "SORRY!!! PLANT HAS NOT BEEN CREATED";
		}
	}
	
	public function plantpincodeedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantedit = pg_query($db, "SELECT * FROM rm_pincodes WHERE id=$id");
		$edit_by_plant = array();
		while($plant_row = pg_fetch_array($plantedit)){
			$edit_by_plant[] = $plant_row;
		}	
		$plantpin_id = $edit_by_plant[0]['id'];
		$unit_id = $edit_by_plant[0]['unit_id'];
		$plant_id = $edit_by_plant[0]['plant_id'];
		$pincode = $edit_by_plant[0]['pin_code'];
		//dd($plant_id);
		Session::put('plantpin_id', $id);
		Session::put('unit_id', $unit_id);
		Session::put('plant_id', $plant_id);
		Session::put('pincode', $pincode);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editplant-pincode');		
	}	
	
	public function plantpincodeupdate(Request $req){
		//$id = Session::get('plant_id');
		//$id = $req->id;
		//dd($req);
		$id = $req->plantpinid;
		$unit_name = $req->unit_name;
		$plant_name = $req->plant_name;
		$pin_code = $req->pin_code;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantpinupdate = pg_query($db, "UPDATE rm_pincodes SET unit_id = '$unit_name', plant_id = '$plant_name', pin_code = '$pin_code' WHERE id=$id");
		if($plantpinupdate){
			$message = "PLANT PIN HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			Session::forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_plant_pincode = 'admin_plant_pincode';
			Session::put('admin_plant_pincode',$admin_plant_pincode);			
			return redirect('/');
		}else{
			echo "SORRY!!! PLANT HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function plantpincodedelete(Request $req){
		//dd($req);
		$id = $req->plantpin_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantpindelete = pg_query($db, "DELETE FROM rm_pincodes WHERE id = $id");
		if($plantpindelete){
			$message = "PLANT HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			Session::forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_plant_pincode = 'admin_plant_pincode';
			Session::put('admin_plant_pincode',$admin_plant_pincode);	
			return redirect('/');
		}else{
			echo "SORRY!!! PLANT HAS NOT BEEN DELETED";
		}
	}
	
	// ################################ CASCADE CRUD ###########################
	public function newcascade(Request $req){
		// dd($req);
		$unit_id = $req->unit_id;
		$plant_id = $req->plant_id;
		$cascade_code = $req->new_cascade_code;
		$cascade_name = $req->new_cascade_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$cascadeadd = pg_query($db, "INSERT INTO rm_cascades(unit_id, plant_id, cascade_code, cascade_name, status) VALUES('$unit_id','$plant_id','$cascade_code','$cascade_name','$status')");
		if($cascadeadd){			
			$message = "CASCADE HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_plant');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_cascade = 'admin_cascade';
			Session::put('admin_cascade',$admin_cascade);
			Session()->forget('unit_id');
			return redirect('/');
		}else{
			echo "SORRY!!! CASCADE HAS NOT BEEN CREATED";
		}
	}
	
	public function cascadeedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$cascadeedit = pg_query($db, "SELECT * FROM rm_cascades WHERE id=$id");
		$edit_by_cascade = array();
		while($plant_row = pg_fetch_array($cascadeedit)){
			// echo "<pre>";
			// print_r($plant_row);
			// echo "</pre>";
			$edit_by_cascade[] = $plant_row;
		}	
		$unit_id = $edit_by_cascade[0]['unit_id'];
		$plant_id = $edit_by_cascade[0]['plant_id'];
		$cascade_code = $edit_by_cascade[0]['cascade_code'];
		$cascade_name = $edit_by_cascade[0]['cascade_name'];
		$status = $edit_by_cascade[0]['status'];
		
		//Session::put('id', $id);
		Session::put('cas_id', $id);
		Session::put('unit_id', $unit_id);
		Session::put('plant_id', $plant_id);
		Session::put('cascade_code', $cascade_code);
		Session::put('cascade_name', $cascade_name);
		Session::put('status', $status);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editcascade');		
	}	
	
	public function cascadeupdate(Request $req){
		//dd($req);
		$id = $req->cascade_id;
		$unit_id = $req->unit_name;
		$plant_id = $req->plant_name;
		$cascade_code = $req->cascade_code;
		$cascade_name = $req->cascade_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantupdate = pg_query($db, "UPDATE rm_cascades SET unit_id = '$unit_id', plant_id = '$plant_id', cascade_code = '$cascade_code', cascade_name = '$cascade_name', status = '$status' WHERE id=$id");
		if($plantupdate){
			$message = "CASCADE HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_cascade = 'admin_cascade';
			Session::put('admin_cascade',$admin_cascade);			
			return redirect('/');
		}else{
			echo "SORRY!!! CASCADE HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function cascadedelete(Request $req){
		//dd($req);
		$id = $req->cascade_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantdelete = pg_query($db, "DELETE FROM rm_cascades WHERE id = $id");
		if($plantdelete){
			$message = "CASCADE HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_cascade = 'admin_cascade';
			Session::put('admin_cascade',$admin_cascade);	
			return redirect('/');
		}else{
			echo "SORRY!!! CASCADE HAS NOT BEEN DELETED";
		}
	}
	
	//################################ MACHINE CRUD ###########################
	public function newmachine(Request $req){
		// dd($req);
		$unit_id = $req->unit_code;
		$plant_id = $req->plant_code;
		$cascade_id = $req->cascade_code;
		$machine_type_id = $req->machine_type;
		$machine_regno = $req->machine_reg;
		$machine_name = $req->machine_name;
		$breakdown_limit = $req->breakdown_limit;
		// $bct_cld_limit = $req->bct_cld_limit;
		$imei = $req->imei_number;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$machineregno = pg_query($db, "SELECT * FROM rm_machines WHERE machine_regno = '$machine_regno'");
		$machineimei = pg_query($db, "SELECT * FROM rm_machines WHERE imei = '$imei'");
		$machinemachinename = pg_query($db, "SELECT * FROM rm_machines WHERE machine_name = '$machine_name'");
		if(pg_num_rows($machineregno)>0){
			$success = "MACHINE REGNO ALREADY EXIST";
			return Redirect::back()->withErrors($success);
		}
		if(pg_num_rows($machineimei)>0){
			$success = "MACHINE IMEI NO ALREADY EXIST";
			return Redirect::back()->withErrors($success);
		}
		if(pg_num_rows($machinemachinename)>0){
			$success = "MACHINE NAME ALREADY EXIST";
			return Redirect::back()->withErrors($success);
		}
		
		$machineadd = pg_query($db, "INSERT INTO rm_machines(unit_id, plant_id, cascade_id, machine_type_id, machine_regno, machine_name, imei, status, breakdown_limit) VALUES('$unit_id','$plant_id','$cascade_id','$machine_type_id','$machine_regno','$machine_name','$imei','$status','$breakdown_limit')");
		if($machineadd){			
			$message = "MACHINE HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_machine = 'admin_machine';
			Session::put('admin_machine',$admin_machine);
			return redirect('/');
		}else{
			echo "SORRY!!! MACHINE HAS NOT BEEN CREATED";
		}
	}
	
	public function machineedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$machineedit = pg_query($db, "SELECT * FROM rm_machines WHERE id=$id");
		$edit_by_machine = array();
		while($machine_row = pg_fetch_array($machineedit)){
			// echo "<pre>";
			// print_r($machine_row);
			// echo "</pre>";
			$edit_by_machine[] = $machine_row;
		}	
		$unit_id = $edit_by_machine[0]['unit_id'];
		$plant_id = $edit_by_machine[0]['plant_id'];
		$cascade_id = $edit_by_machine[0]['cascade_id'];
		$machine_type = $edit_by_machine[0]['machine_type_id'];
		$machine_regno = $edit_by_machine[0]['machine_regno'];
		$imei = $edit_by_machine[0]['imei'];
		$machine_name = $edit_by_machine[0]['machine_name'];
		$breakdown_limit = $edit_by_machine[0]['breakdown_limit'];
		// $bct_cld_limit = $edit_by_machine[0]['bct_cld_limit'];
		$status = $edit_by_machine[0]['status'];
		
		Session::put('machine_id', $id);
		Session::put('unit_id', $unit_id);
		Session::put('plant_id', $plant_id);
		Session::put('cascade_id', $cascade_id);
		Session::put('machine_type', $machine_type);
		Session::put('machine_regno', $machine_regno);
		Session::put('breakdown_limit', $breakdown_limit);
		Session::put('machine_name', $machine_name);
		Session::put('imei', $imei);
		Session::put('status', $status);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editmachine');		
	}	
	
	public function machineupdate(Request $req){
		//dd($req);
		$id = $req->machine_id;
		$unit_id = $req->unit_code;
		$plant_id = $req->plant_code;
		$cascade_id = $req->cascade_code;
		$machine_type_id = $req->machine_type;
		$machine_reg = $req->machine_regno;
		$imei = $req->imei_number;
		$machine_name = $req->machine_name;
		$breakdown_limit = $req->breakdown_limit;
		// $bct_cld_limit = $req->bct_cld_limit;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$machineupdate = pg_query($db, "UPDATE rm_machines SET unit_id = '$unit_id', plant_id = '$plant_id', cascade_id = '$cascade_id', machine_type_id = '$machine_type_id', machine_regno = '$machine_reg', machine_name = '$machine_name', status = '$status', imei = '$imei', breakdown_limit = '$breakdown_limit' WHERE id=$id");
		if($machineupdate){
			$message = "MACHINE HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_machine = 'admin_machine';
			Session::put('admin_machine',$admin_machine);		
			return redirect('/');
		}else{
			echo "SORRY!!! MACHINE HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function machinedelete(Request $req){
		//dd($req);
		$id = $req->machine_value;
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$plantdelete = pg_query($db, "DELETE FROM rm_machines WHERE id = $id");
		if($plantdelete){
			$message = "MACHINE HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_products');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_machine = 'admin_machine';
			Session::put('admin_machine',$admin_machine);		
			return redirect('/');
		}else{
			echo "SORRY!!! MACHINE HAS NOT BEEN DELETED";
		}
	}	
	
	
	//################################ PRODUCT CRUD ###########################
	public function newproduct(Request $req){
		// dd($req);
		$product_code = $req->product_code;
		$product_name = $req->product_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productadd = pg_query($db, "INSERT INTO rm_products(product_code, product_name, status) VALUES('$product_code','$product_name','$status')");
		if($productadd){			
			$message = "PRODUCT HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_products = 'admin_products';
			Session::put('admin_products',$admin_products);
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN CREATED";
		}
	}
	
	public function productedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productedit = pg_query($db, "SELECT * FROM rm_products WHERE id=$id");
		$edit_by_product = array();
		while($product_row = pg_fetch_array($productedit)){
			// echo "<pre>";
			// print_r($product_row);
			// echo "</pre>";
			// $unit_code = $unit_row['unit_code']; 
			// $unit_name = $unit_row['unit_name']; 
			$edit_by_product[] = $product_row;
		}	
		$product_code = $edit_by_product[0]['product_code'];
		$product_name = $edit_by_product[0]['product_name'];
		$status = $edit_by_product[0]['status'];
		
		Session::put('product_id', $id);
		Session::put('product_code', $product_code);
		Session::put('product_name', $product_name);
		Session::put('status', $status);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editproduct');		
	}	
	
	public function productupdate(Request $req){
		//dd($req);
		$id = $req->product_id;
		$product_code = $req->product_code;
		$product_name = $req->product_name;
		// $bct = $req->bct;
		$status = $req->status;
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productupdate = pg_query($db, "UPDATE rm_products SET product_code = '$product_code', product_name = '$product_name', status = '$status' WHERE id=$id");
		if($productupdate){
			$message = "PRODUCT HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_products = 'admin_products';
			Session::put('admin_products',$admin_products);		
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function productdelete(Request $req){
		//dd($req);
		$id = $req->product_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productdelete = pg_query($db, "DELETE FROM rm_products WHERE id = $id");
		if($productdelete){
			$message = "PRODUCT HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_products = 'admin_products';
			Session::put('admin_products',$admin_products);	
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN DELETED";
		}
	}
	
	
	//################################ PRODUCT MIXER BCT CRUD ###########################
	public function newproductmixerbct(Request $req){
		//dd($req);
		$product_machine = $req->product_machine;
		$product_name = $req->product_name;
		$bct = $req->bct;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmixexist = pg_query($db, "SELECT * FROM rm_mixerbct WHERE machine_id= $product_machine AND product_id = $product_name");
		//print_r($productmixexist);
		// if(pg_num_rows($productmixexist)>0){
			// return view('add-plant-pincode')->withErrors('Machine Code and Product Code Already Exist');
		// }
		if(pg_num_rows($productmixexist) > 0){
			$msg = "Duplicate entry of Machine id or product id";
			return Redirect::back()->withErrors($msg);
		}
		$productadd = pg_query($db, "INSERT INTO rm_mixerbct(machine_id, product_id, bct) VALUES('$product_machine','$product_name','$bct')");
		if($productadd){			
			$message = "PRODUCT  MIXER HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_products');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_bct = 'admin_bct';
			Session::put('admin_bct',$admin_bct);
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN CREATED";
		}
	}
	
	public function productmixerbctedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmixedit = pg_query($db, "SELECT * FROM rm_mixerbct WHERE id=$id");
		$edit_by_product_mixer = array();
		while($product_mixer_row = pg_fetch_array($productmixedit)){
			// echo "<pre>";
			// print_r($product_mixer_row);
			// echo "</pre>";
			// $unit_code = $unit_row['unit_code']; 
			// $unit_name = $unit_row['unit_name']; 
			$edit_by_product_mixer[] = $product_mixer_row;
		}	
		$machine_code = $edit_by_product_mixer[0]['machine_id'];
		$product_name = $edit_by_product_mixer[0]['product_id'];
		$bct = $edit_by_product_mixer[0]['bct'];
		
		//Session::put('id', $id);
		Session::put('product_mix_id', $id);
		Session::put('machine_code', $machine_code);
		Session::put('product_name', $product_name);
		Session::put('bct', $bct);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editproductmixerbct');		
	}	
	
	public function productmixerbctupdate(Request $req){
		//dd($req);
		$id = $req->product_mixer_id;
		$product_machine = $req->product_machine;
		$product_name = $req->product_name;
		$bct = $req->bct;
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmixupdate = pg_query($db, "UPDATE rm_mixerbct SET machine_id = '$product_machine', product_id = '$product_name', bct = '$bct' WHERE id=$id");
		if($productmixupdate){
			$message = "PRODUCT MIXER BCT HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_products');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_bct = 'admin_bct';
			Session::put('admin_bct',$admin_bct);
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT MIXER HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function productmixerbctdelete(Request $req){
		//dd($req);
		$id = $req->product_bct_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmixdelete = pg_query($db, "DELETE FROM rm_mixerbct WHERE id = $id");
		if($productmixdelete){
			$message = "PRODUCT MIXER HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_products');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_bct = 'admin_bct';
			Session::put('admin_bct',$admin_bct);
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN DELETED";
		}
	}
	
	
	//################################ PRODUCT MAPPING CRUD ###########################
	public function newproductplantmapping(Request $req){
		//dd($req);
		$plant = $req->plant;
		$product = $req->product;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmappingexist = pg_query($db, "SELECT * FROM rm_plantproducts WHERE plant_id= $plant AND product_id = $product");		
		// if(pg_num_rows($productmixexist)>0){
			// return view('add-plant-pincode')->withErrors('Machine Code and Product Code Already Exist');
		// }
		if(pg_num_rows($productmappingexist) > 0){
			//dd('');
			$msg = "Duplicate entry of Plant ID or Product ID";
			return Redirect::back()->withErrors($msg);
		}
		
		$productmapping = pg_query($db, "INSERT INTO rm_plantproducts(plant_id, product_id) VALUES('$plant','$product')");
		if($productmapping){			
			$message = "PRODUCT MAPPING HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_products');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_product_mapping = 'admin_product_mapping';
			Session::put('admin_product_mapping',$admin_product_mapping);
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN CREATED";
		}
	}
	
	public function productplantmapping($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmappingedit = pg_query($db, "SELECT * FROM rm_plantproducts WHERE id=$id");
		$edit_by_product_map = array();
		while($product_plant_row = pg_fetch_array($productmappingedit)){
			// echo "<pre>";
			// print_r($product_plant_row);
			// echo "</pre>";
			$edit_by_product_plant[] = $product_plant_row;
		}	
		$plant_name = $edit_by_product_plant[0]['plant_id'];
		$product_name = $edit_by_product_plant[0]['product_id'];
		
		//Session::put('id', $id);
		Session::put('plant_map_id', $id);
		Session::put('plant_name', $plant_name);
		Session::put('product_name', $product_name);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editproduct-plant-mapping');		
	}	
	
	public function productplantmappingupdate(Request $req){
		//dd($req);
		$id = $req->plant_map_id;
		$plant = $req->plant;
		$product = $req->product;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productmappingupdate = pg_query($db, "UPDATE rm_plantproducts SET plant_id = '$plant', product_id = '$product' WHERE id=$id");
		if($productmappingupdate){
			$message = "PRODUCT MAPPING HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_products');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_product_mapping = 'admin_product_mapping';
			Session::put('admin_product_mapping',$admin_product_mapping);		
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function productplantmappingdelete(Request $req){
		//dd($req);
		$id = $req->plant_map_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productdelete = pg_query($db, "DELETE FROM rm_plantproducts WHERE id = $id");
		if($productdelete){
			$message = "PRODUCT MAPPING HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_products');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_product_mapping = 'admin_product_mapping';
			Session::put('admin_product_mapping',$admin_product_mapping);	
			return redirect('/');
		}else{
			echo "SORRY!!! PRODUCT HAS NOT BEEN DELETED";
		}
	}
	
	
	//################################ SHIFT CRUD ###########################
	public function newshift(Request $req){
		//dd($req);
		$shift_code = $req->new_shift_code;
		$shift_name = $req->new_shift_name;
		$shift_start_at = $req->new_shift_start_at.":00";
		$shift_end_at = $req->new_shift_end_at.":00";
		$status = $req->status;
		//dd($new_shift_start_at);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$productadd = pg_query($db, "INSERT INTO rm_shifts(shift_code, shift_name, shift_start_at, shift_end_at, status) VALUES('$shift_code','$shift_name','$shift_start_at','$shift_end_at','$status')");
		if($shiftadd){			
			$message = "SHIFT HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_products');
			Session::forget('admin_machine_product_skus');
			$admin_shifts = 'admin_shifts';
			Session::put('admin_shifts',$admin_shifts);
			return redirect('/');
		}else{
			echo "SORRY!!! MACHINE HAS NOT BEEN CREATED";
		}
	}
	
	public function shiftedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$shiftedit = pg_query($db, "SELECT * FROM rm_shifts WHERE id=$id");
		$edit_by_shift = array();
		while($shift_row = pg_fetch_array($shiftedit)){
			// echo "<pre>";
			// print_r($shift_row);
			// echo "</pre>";
			// $unit_code = $unit_row['unit_code']; 
			// $unit_name = $unit_row['unit_name']; 
			$edit_by_shift[] = $shift_row;
		}	
		//dd();
		$shift_code = $edit_by_shift[0]['shift_code'];
		$shift_name = $edit_by_shift[0]['shift_name'];
		$shift_start_at = $edit_by_shift[0]['shift_start_at'];
		$shift_end_at = $edit_by_shift[0]['shift_end_at'];
		$status = $edit_by_shift[0]['status'];
		
		//Session::put('id', $id);
		Session::put('shift_id', $id);
		Session::put('shift_code', $shift_code);
		Session::put('shift_name', $shift_name);
		Session::put('shift_start_at', $shift_start_at);
		Session::put('shift_end_at', $shift_end_at);
		Session::put('status', $status);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editshift');		
	}	
	
	public function shiftupdate(Request $req){
		//dd($req);
		$id = $req->shift_id;
		$shift_code = $req->edit_shift_code;
		$shift_name = $req->edit_shift_name;
		$shift_start_at = $req->edit_shift_start_at.":00";
		$shift_end_at = $req->edit_shift_end_at.":00";
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$shiftupdate = pg_query($db, "UPDATE rm_shifts SET shift_code = '$shift_code', shift_name = '$shift_name', shift_start_at = '$shift_start_at', shift_end_at = '$shift_end_at', status = '$status' WHERE id=$id");
		if($shiftupdate){
			$message = "SHIFT HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_products');
			Session::forget('admin_machine_product_skus');
			$admin_shifts = 'admin_shifts';
			Session::put('admin_shifts',$admin_shifts);		
			return redirect('/');
		}else{
			echo "SORRY!!! SHIFT HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function shiftdelete(Request $req){
		//dd($req);
		$id = $req->shift_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$shiftdelete = pg_query($db, "DELETE FROM rm_shifts WHERE id = $id");
		if($shiftdelete){
			$message = "SHIFT HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_products');
			Session::forget('admin_machine_product_skus');
			$admin_shifts = 'admin_shifts';
			Session::put('admin_shifts',$admin_shifts);	
			return redirect('/');
		}else{
			echo "SORRY!!! MACHINE HAS NOT BEEN DELETED";
		}
	}
	//################################ SKU CRUD ###########################
	public function newsku(Request $req){
		// dd($req);
		$product_id = $req->product_id;
		$sku_code = $req->sku_code;
		$sku_name = $req->sku_name;
		$pack_size = $req->pack_size;
		$pack_size_unit = $req->packing_unit;
		$unit_per_cld = $req->unit_per_cld;
		$sku_type = $req->sku_type;
		// $rpm_min = $req->rpm_min;
		// $rpm_max = $req->rpm_max;
		// $rpm_final = $req->final_rpm;
		$rpm_min = 0;
		$rpm_max = 0;
		$rpm_default = $req->rpm_default;
		$remark = $req->remark;
		$status = $req->status;
		$packing_type = $req->packing_type;
		$rpm_interval = $req->rpm_interval;
		//dd($status);
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$skuexist = pg_query($db, "SELECT * FROM rm_skus WHERE sku_code = '$sku_code'");
		if(pg_num_rows($skuexist)>0){
			$msg = "SKU CODE ALREADY EXIST";
			return Redirect::back()->withErrors($msg);
		}
		
		$skuadd = pg_query($db, "INSERT INTO rm_skus(product_id, sku_code, sku_name, pack_size, pack_size_unit, unit_per_cld, sku_type, rpm_default, remark, status, rpm_min, rpm_max, packing_type_id, rpm_interval) VALUES('$product_id','$sku_code','$sku_name','$pack_size','$pack_size_unit','$unit_per_cld','$sku_type','$rpm_default','$remark','$status','$rpm_min','$rpm_max','$packing_type','$rpm_interval')");
		if($skuadd){			
			$message = "SKU HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_machine_product_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			$admin_skus = 'admin_skus';
			Session::put('admin_skus',$admin_skus);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN CREATED";
		}
	}
	
	public function skuedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skuedit = pg_query($db, "SELECT * FROM rm_skus WHERE id=$id");
		$edit_by_sku = array();
		while($sku_row = pg_fetch_array($skuedit)){
			// echo "<pre>";
			// print_r($sku_row);
			// echo "</pre>";
			$edit_by_sku[] = $sku_row;
		}	
		$product_id = $edit_by_sku[0]['product_id'];
		$sku_code = $edit_by_sku[0]['sku_code'];
		$sku_name = $edit_by_sku[0]['sku_name'];
		$pack_size = $edit_by_sku[0]['pack_size'];
		$pack_size_unit = $edit_by_sku[0]['pack_size_unit'];
		$unit_per_cld = $edit_by_sku[0]['unit_per_cld'];
		$sku_type = $edit_by_sku[0]['sku_type'];
		$rpm_default = $edit_by_sku[0]['rpm_default'];
		$remark = $edit_by_sku[0]['remark'];
		$status = $edit_by_sku[0]['status'];
		$rpm_min = $edit_by_sku[0]['rpm_min'];
		$rpm_max = $edit_by_sku[0]['rpm_max'];		
		$packing_type_id = $edit_by_sku[0]['packing_type_id'];	
		$rpm_interval = $edit_by_sku[0]['rpm_interval'];

		// dd($final_rpm);
		
		Session::put('sku_id', $id);
		Session::put('product_id', $product_id);
		Session::put('sku_code', $sku_code);
		Session::put('sku_name', $sku_name);
		Session::put('pack_size', $pack_size);
		Session::put('pack_size_unit', $pack_size_unit);
		Session::put('unit_per_cld', $unit_per_cld);
		Session::put('sku_type', $sku_type);
		Session::put('rpm_default', $rpm_default);
		Session::put('remark', $remark);
		Session::put('status', $status);
		Session::put('rpm_min', $rpm_min);
		Session::put('rpm_max', $rpm_max);
		Session::put('packing_type_id', $packing_type_id);
		Session::put('rpm_interval', $rpm_interval);
				
		return redirect('/editsku');		
	}	
	
	public function skuupdate(Request $req){
		//dd($req);
		$id = $req->sku_id;
		$product_id = $req->product_id;
		$sku_code = $req->sku_code;
		$sku_name = $req->sku_name;
		$pack_size = $req->pack_size;
		$pack_size_unit = $req->packing_unit;
		$unit_per_cld = $req->unit_per_cld;
		$sku_type = $req->sku_type;
		$rpm_default = $req->rpm_default;
		$remark = $req->remark;
		$status = $req->status;
		// $rpm_min = $req->rpm_min;
		// $rpm_max = $req->rpm_max;
		$rpm_min = 0;
		$rpm_max = 0;
		$packing_type = $req->packing_type;
		$rpm_interval = $req->rpm_interval;
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skuupdate = pg_query($db, "UPDATE rm_skus SET product_id='$product_id', sku_code='$sku_code', sku_name='$sku_name', pack_size='$pack_size', pack_size_unit='$pack_size_unit', unit_per_cld='$unit_per_cld', sku_type='$sku_type', rpm_default='$rpm_default', remark='$remark', status='$status', rpm_min='$rpm_min', rpm_max='$rpm_max', packing_type_id='$packing_type', rpm_interval='$rpm_interval' WHERE id=$id");
		
		if($skuupdate){
			$message = "SKU HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_machine_product_skus');
			$admin_skus = 'admin_skus';
			Session::put('admin_skus',$admin_skus);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function skudelete(Request $req){
		//dd($req);
		$id = $req->sku_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skudelete = pg_query($db, "DELETE FROM rm_skus WHERE id = $id");
		if($skudelete){
			$message = "SKU HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_products');
			Session::forget('admin_shifts');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_machine_product_skus');
			$admin_skus = 'admin_skus';
			Session::put('admin_skus',$admin_skus);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN DELETED";
		}
	}	
	
	//################################ MACHINE PRODUCT SKU CRUD ###########################
	public function newmachineproductsku(Request $req){
		// dd($req);
		$plant_id = $req->plant;
		$machine_id = $req->machine;
		$product_id = $req->product;
		$sku_id = $req->skuname;
		$rpm = $req->rpm;
				
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
				
		$skuadd = pg_query($db, "INSERT INTO rm_machine_sku_rpm(plant_id, machine_id, product_id, sku_id, rpm) VALUES('$plant_id','$machine_id','$product_id','$sku_id','$rpm')");
		if($skuadd){			
			$message = "MACHINE PRODUCT SKU HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			$admin_machine_product_skus = 'admin_machine_product_skus';
			Session::put('admin_machine_product_skus',$admin_machine_product_skus);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN CREATED";
		}
	}
	
	public function machineproductskuedit($id){
		// dd($id);		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$machineproductedit = pg_query($db, "SELECT * FROM rm_machine_sku_rpm WHERE id=$id");
		$edit_by_product_sku = array();
		while($product_sku_row = pg_fetch_array($machineproductedit)){
			// echo "<pre>";
			// print_r($product_sku_row);
			// echo "</pre>";
			$edit_by_product_sku[] = $product_sku_row;
		}	
		// dd('here');
		$plant_id = $edit_by_product_sku[0]['plant_id'];
		$machine_id = $edit_by_product_sku[0]['machine_id'];		
		$product_id = $edit_by_product_sku[0]['product_id'];		
		$sku_id = $edit_by_product_sku[0]['sku_id'];		
		$rpm = $edit_by_product_sku[0]['rpm'];		

		// dd($final_rpm);
		
		Session::put('machineproductsku_idd', $id);
		Session::put('plant_idd', $plant_id);
		Session::put('machine_idd', $machine_id);
		Session::put('product_idd', $product_id);
		Session::put('sku_idd', $sku_id);		
		Session::put('rpmm', $rpm);		
				
		return redirect('/editmachine-product-sku');		
	}	
	
	public function machineproductskuupdate(Request $req){
		//dd($req);
		$id = $req->machineproductid;
		// dd($id);
		$plant_id = $req->plant;
		$machine_id = $req->machine;
		$product_id = $req->product;
		$sku_id = $req->skuname;
		$rpm = $req->rpm;		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skuupdate = pg_query($db, "UPDATE rm_machine_sku_rpm SET plant_id='$plant_id', machine_id='$machine_id', product_id='$product_id', sku_id='$sku_id', rpm='$rpm' WHERE id=$id");
		
		if($skuupdate){
			$message = "MACHINE PRODUCT SKU HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			$admin_machine_product_skus = 'admin_machine_product_skus';
			Session::put('admin_machine_product_skus',$admin_machine_product_skus);
			return redirect('/');
		}else{
			echo "SORRY!!! MACHINE PRODUCT SKU HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function machineproductskudelete(Request $req){
		//dd($req);
		$id = $req->sku_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skudelete = pg_query($db, "DELETE FROM rm_skus WHERE id = $id");
		if($skudelete){
			$message = "SKU HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_products');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			$admin_machine_product_skus = 'admin_machine_product_skus';
			Session::put('admin_machine_product_skus',$admin_machine_product_skus);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN DELETED";
		}
	}
	
	//################################ SKU UNIT CRUD ###########################
	public function newskuunit(Request $req){
		//dd($req);
		$unit_code = $req->new_unit_code;
		$unit_name = $req->new_unit_name;
		$status = $req->status;
		//dd($status);
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skuunitadd = pg_query($db, "INSERT INTO rm_skupackingunits(unit_code, unit_name, status) VALUES('$unit_code','$unit_name','$status')");
		if($skuunitadd){			
			$message = "SKU UNIT HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_machine_product_skus');
			$admin_sku_unit = 'admin_sku_unit';
			Session::put('admin_sku_unit',$admin_sku_unit);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN CREATED";
		}
	}
	
	public function skuunitedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skuunitedit = pg_query($db, "SELECT * FROM rm_skupackingunits WHERE id=$id");
		$edit_by_sku_unit = array();
		while($skuunit_row = pg_fetch_array($skuunitedit)){
			// echo "<pre>";
			// print_r($skuunit_row);
			// echo "</pre>";
			$edit_by_sku_unit[] = $skuunit_row;
		}
		//dd('here');
		$unit_code = $edit_by_sku_unit[0]['unit_code'];
		$unit_name = $edit_by_sku_unit[0]['unit_name'];
		$status = $edit_by_sku_unit[0]['status'];
		
		Session::put('skuunit_id', $id);
		Session::put('unit_code', $unit_code);
		Session::put('unit_name', $unit_name);
		Session::put('status', $status);
		return redirect('/editskuunit');		
	}	
	
	public function skuunitupdate(Request $req){
		//dd($req);
		$id = $req->skuunit_id;
		$unit_code = $req->edit_unit_code;
		$unit_name = $req->edit_unit_name;
		$status = $req->status;
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skuunitupdate = pg_query($db, "UPDATE rm_skupackingunits SET unit_code='$unit_code', unit_name='$unit_name', status='$status' WHERE id=$id");
		
		if($skuunitupdate){
			$message = "SKU UNIT HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_machine_product_skus');
			$admin_sku_unit = 'admin_sku_unit';
			Session::put('admin_sku_unit',$admin_sku_unit);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function skuunitdelete(Request $req){
		//dd($req);
		$id = $req->skuunit_value;
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skudelete = pg_query($db, "DELETE FROM rm_skupackingunits WHERE id = $id");
		if($skudelete){
			$message = "SKU UNIT HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_machine_product_skus');
			$admin_sku_unit = 'admin_sku_unit';
			Session::put('admin_sku_unit',$admin_sku_unit);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU HAS NOT BEEN DELETED";
		}
	}
	
	//################################ SKU TYPE CRUD ###########################
	public function newskutypes(Request $req){
		//dd($req);
		$type_code = $req->new_sku_type_code;
		$type_name = $req->new_sku_type_name;
		$status = $req->status;
		//dd($status);
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skuadd = pg_query($db, "INSERT INTO rm_skupackingtypes(type_code, type_name, status) VALUES('$type_code','$type_name','$status')");
		if($skuadd){			
			$message = "SKU TYPE HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_unit');
			Session::forget('admin_machine_product_skus');
			$admin_sku_type = 'admin_sku_type';
			Session::put('admin_sku_type',$admin_sku_type);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU TYPE HAS NOT BEEN ADDED";
		}
	}
	
	public function skutypesedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skutypeedit = pg_query($db, "SELECT * FROM rm_skupackingtypes WHERE id=$id");
		$edit_by_skutype = array();
		while($skutype_row = pg_fetch_array($skutypeedit)){
			// echo "<pre>";
			// print_r($skutype_row);
			// echo "</pre>";
			$edit_by_skutype[] = $skutype_row;
		}
		$skutype_code = $edit_by_skutype[0]['type_code'];
		$skutype_name = $edit_by_skutype[0]['type_name'];
		$status = $edit_by_skutype[0]['status'];
		
		Session::put('skutype_id', $id);
		Session::put('skutype_code', $skutype_code);
		Session::put('skutype_name', $skutype_name);
		Session::put('status', $status);
				
		return redirect('/editskutypes');		
	}	
	
	public function skutypesupdate(Request $req){
		//dd($req);
		$id = $req->sku_type_id;
		$type_code = $req->edit_sku_type_code;
		$type_name = $req->edit_sku_type_name;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skutypeupdate = pg_query($db, "UPDATE rm_skupackingtypes SET type_code='$type_code', type_name='$type_name', status='$status' WHERE id=$id");
		
		if($skutypeupdate){
			$message = "SKU TYPE HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_unit');
			Session::forget('admin_machine_product_skus');
			$admin_sku_type = 'admin_sku_type';
			Session::put('admin_sku_type',$admin_sku_type);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU TYPE HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function skutypesdelete(Request $req){
		//dd($req);
		$id = $req->skutype_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$skudelete = pg_query($db, "DELETE FROM rm_skupackingtypes WHERE id = $id");
		if($skudelete){
			$message = "SKU HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_unit');
			Session::forget('admin_machine_product_skus');
			$admin_sku_type = 'admin_sku_type';
			Session::put('admin_sku_type',$admin_sku_type);
			return redirect('/');
		}else{
			echo "SORRY!!! SKU TYPE HAS NOT BEEN DELETED";
		}
	}
	
	//################################ BREAK REASON CRUD ###########################
	public function newbreakreason(Request $req){		
		// $breakdown_icon = $req->file('breakdown_icon')->getClientOriginalName();
		$breakdown_code = $req->breakdown_code;
		$breakdown_name = $req->new_cascade_code;
		$status = $req->status;
		$imageName = time().'.'.$req->breakdown_icon->getClientOriginalExtension();
        $req->breakdown_icon->move(public_path('images'), $imageName);
		$image = public_path('images/'.$imageName);		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$breakdownadd = pg_query($db, "INSERT INTO rm_breakdownreasons(breakdown_code, breakdown_reason, status, breakdown_icon) VALUES('$breakdown_code','$breakdown_name','$status','$imageName')");
		if($breakdownadd){			
			$message = "BREAKDOWN HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_subreasons');
			Session::forget('admin_shifts');
			Session::forget('admin_machine_product_skus');
			$admin_reasons = 'admin_reasons';
			Session::put('admin_reasons',$admin_reasons);
			return redirect('/');
		}else{
			echo "SORRY!!! BREAKDOWN HAS NOT BEEN CREATED";
		}
	}
	
	public function breakreasonedit($id){
		// dd($id);		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$breakdownedit = pg_query($db, "SELECT * FROM rm_breakdownreasons WHERE id=$id");
		$edit_by_breakdown = array();
		
		while($breakdown_row = pg_fetch_array($breakdownedit)){
			$edit_by_breakdown[] = $breakdown_row;
		}	
		$breakdown_code = $edit_by_breakdown[0]['breakdown_code'];
		$breakdown_reason = $edit_by_breakdown[0]['breakdown_reason'];
		$status = $edit_by_breakdown[0]['status'];
		
		Session::put('breakdown_id', $id);
		Session::put('breakdown_code', $breakdown_code);
		Session::put('breakdown_reason', $breakdown_reason);		
		Session::put('status', $status);		
		//dd(Session::get('unit_code'));
		
		return redirect('/editbreakreason');		
	}	
	
	public function breakreasonupdate(Request $req){
		// dd($req);
		$id = $req->breakdown_id;
		$breakdown_code = $req->breakdown_code;
		$breakdown_reason = $req->breakdown_reason;
		$status = $req->status;
		
		if($req->breakdown_icon == ''){
			echo "NO";
			$imageName = $req->bkimg;
		}else{
			$imag = $req->bkimg;
			unlink(public_path('images/'.$imag));
			$imageName = time().'.'.$req->breakdown_icon->getClientOriginalExtension();
			$req->breakdown_icon->move(public_path('images'), $imageName);
			$image = public_path('images/'.$imageName);
		}
		// dd('here');
		// dd($req->breakdown_icon);
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");		
		
		$breakreasonupdate = pg_query($db, "UPDATE rm_breakdownreasons SET breakdown_code = '$breakdown_code', breakdown_icon='$imageName', breakdown_reason = '$breakdown_reason', status = '$status' WHERE id=$id");
		if($breakreasonupdate){
			$message = "BREAKDOWN REASON HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_reasons = 'admin_reasons';
			Session::put('admin_reasons',$admin_reasons);
			return redirect('/');
		}else{
			echo "SORRY!!! BREAKDOWN REASON HAS NOT BEEN UPDATED";
		}		
	}		
	
	public function breakreasondelete(Request $req){
		//dd($req);
		$id = $req->breakdown_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$breakreasondelete = pg_query($db, "DELETE FROM rm_breakdownreasons WHERE id = $id");
		if($breakreasondelete){
			$message = "BREAKDOWN REASON HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_reasons = 'admin_reasons';
			Session::put('admin_reasons',$admin_reasons);
			return redirect('/');
		}else{
			echo "SORRY!!! BREAKDOWN HAS NOT BEEN DELETED";
		}
	}
	
	//################################ BREAK SUB-REASON CRUD ###########################
	public function newbreaksubreason(Request $req){
		// dd($req);
		$breakdown_id = $req->breadkdown_id;
		$subbreakdown_code = $req->new_subreason_code;
		$subbreakdown_reason = $req->new_breakdown_reason;
		$status = $req->status;
		$machines = $req->machines;
		
		$imageName = time().'.'.$req->subbreakdown_icon->getClientOriginalExtension();
        $req->subbreakdown_icon->move(public_path('images'), $imageName);
		$image = public_path('images/'.$imageName);
		// dd($imageName);
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$subbreakreasonexist = pg_query($db, "SELECT subbreakdown_code FROM rm_breakdownsubreasons WHERE subbreakdown_code = '$subbreakdown_code'");
		if(pg_num_rows($subbreakreasonexist) > 0){			
			$msg = "Duplicate entry of breakdown reason";
			return Redirect::back()->withErrors($msg);			
		}
		$subbreakdownadd = pg_query($db, "INSERT INTO rm_breakdownsubreasons(breakdown_id, subbreakdown_code, subbreakdown_reason, status,subbreakicon) VALUES('$breakdown_id','$subbreakdown_code','$subbreakdown_reason','$status','$imageName') returning id");
		
		// $id = pg_exec($db, $subbreakdownadd);
		$main_id = pg_result($subbreakdownadd,'0','id');
		// dd($main_id);
		
		foreach($machines as $m){
			echo $m;		
			$breakreason = pg_query($db, "INSERT INTO rm_machine_subreasons(sub_reason_id, machine_id) VALUES('".$main_id."','".$m."')");
		}
		
		if($breakreason){			
			$message = "SUB-BREAKDOWN HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_machine_product_skus');
			$admin_subreasons = 'admin_subreasons';
			Session::put('admin_subreasons',$admin_subreasons);
			return redirect('/');
		}else{
			echo "SORRY!!! SUB-BREAKDOWN HAS NOT BEEN CREATED";
		}
	}
	
	public function breaksubreasonedit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$breaksubreasonedit = pg_query($db, "SELECT * FROM rm_breakdownsubreasons WHERE id=$id");
		$edit_by_subreason = array();
		while($subreason_row = pg_fetch_array($breaksubreasonedit)){
			// echo "<pre>";
			// print_r($subreason_row);
			// echo "</pre>";
			$edit_by_subreason[] = $subreason_row;
		}	
		$subreason_breakdown = $edit_by_subreason[0]['breakdown_id'];
		$subreason_code = $edit_by_subreason[0]['subbreakdown_code'];
		$subreason_name = $edit_by_subreason[0]['subbreakdown_reason'];
		$status = $edit_by_subreason[0]['status'];
		
		//Session::put('id', $id);
		Session::put('subreason_id', $id);
		Session::put('subreason_code', $subreason_code);
		Session::put('subreason_breakdown', $subreason_breakdown);
		Session::put('subreason_name', $subreason_name);
		Session::put('status', $status);
		
		//dd(Session::get('unit_code'));
		
		return redirect('/editbreaksubreason');		
	}	
	
	public function breaksubreasonupdate(Request $req){
		// dd($req);
		$id = $req->subreason_id;
		$breakdown_id = $req->breadkdown_id;
		$subreason_code = $req->edit_subreason_code;
		$breakdown_reason = $req->edit_breakdown_reason;
		$status = $req->status;
		$machines = $req->machines;
		// dd($req->subbreakdown_icon);
		if($req->subbreakdown_icon == ''){
			// echo "NO";
			$imageName = $req->sbkimg;
			// dd('NO');
		}else{
			// echo "YES";
			// dd('YES');
			$imag = $req->sbkimg;
			unlink(public_path('images/'.$imag));
			$imageName = time().'.'.$req->subbreakdown_icon->getClientOriginalExtension();
			$req->subbreakdown_icon->move(public_path('images'), $imageName);
			$image = public_path('images/'.$imageName);
		}
		// dd($req->sbkimg);
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		// dd($machines);		
		
		$breaksubreasonupdate = pg_query($db, "UPDATE rm_breakdownsubreasons SET breakdown_id = '$breakdown_id', subbreakdown_code = '$subreason_code', subbreakdown_reason = '$breakdown_reason', status = '$status', subbreakicon='$imageName' WHERE id=$id");
		
		$subreasonmachinedelete = pg_query($db, "DELETE FROM rm_machine_subreasons WHERE sub_reason_id = $id");
		foreach($req->machines as $m){
			// echo $m;		
			$breakreason = pg_query($db, "INSERT INTO rm_machine_subreasons(sub_reason_id, machine_id) VALUES('".$id."','".$m."')");
		}
		
		if($breaksubreasonupdate){
			$message = "SUB-BREAKDOWN HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_machine_product_skus');
			$admin_subreasons = 'admin_subreasons';
			Session::put('admin_subreasons',$admin_subreasons);
			return redirect('/');
		}else{
			echo "SORRY!!! SUB-BREAKDOWN HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function breaksubreasondelete(Request $req){
		// dd($req);
		$id = $req->subbreakdown_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$breaksubreasondelete = pg_query($db, "DELETE FROM rm_breakdownsubreasons WHERE id = $id");
		if($breaksubreasondelete){
			$message = "SUB-BREAKDOWN HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_users');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('product_mixer_bct_info');
			Session::forget('product_plant_mapping_info');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_machine_product_skus');
			$admin_subreasons = 'admin_subreasons';
			Session::put('admin_subreasons',$admin_subreasons);
			return redirect('/');
		}else{
			echo "SORRY!!! SUB-BREAKDOWN HAS NOT BEEN DELETED";
		}
	}
	
	//################################ USER CRUD ###########################
	public function newuser(Request $req){
		// dd($req->all());
		// date_default_timezone_set('Asia/Kolkata');
		$create_date_time = date('d-m-Y H:i');
		// $create_date_time;
		// dd($req);
		
		$mobile = 0;
		$password = '';
		$password_confirm = '';
		$login_name = $req->loginname;
		$first_name = $req->firstname;
		$last_name = $req->lastname;
		$email_id = $req->email;
		$pwd = $req->password;
		$mobile_no = $req->mobile;
		$user_type_id = $req->designation_id;
		$create_date = $create_date_time;
		$create_by = 1;
		$status = $req->status;
		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		
		if($email_id == NULL && $pwd == NULL  && $mobile_no == NULL ){
			$email_id = '';
			$pwd = '';
			$mobile_no = 0;
			// dd('here3333');
		}else{
			$email_exist = pg_query($db, "SELECT * FROM rm_users WHERE email_id = '$email_id'");
			if(pg_num_rows($email_exist)>0){
				$success = "USER EMAIL ALREADY EXIST";
				return Redirect::back()->withErrors($success);
			}
		}
		$user_exist = pg_query($db, "SELECT * FROM rm_users WHERE login_name = '$login_name'");
		if(pg_num_rows($user_exist)>0){
			$success = "USER ALREADY EXIST";
			return Redirect::back()->withErrors($success);
		}
		
		$useradd = pg_query($db, "INSERT INTO rm_users(status, full_rights, login_name, first_name, last_name, email_id, pwd, mobile_no, user_type_id, create_date, create_by) VALUES('$status','true','$login_name','$first_name','$last_name','$email_id','$pwd','$mobile_no','$user_type_id','$create_date','$create_by')");
		
		if($useradd){			
			$message = "USER HAS BEEN CREATED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_users = 'admin_users';
			Session::put('admin_users',$admin_users);			
			return redirect('/');
		}else{
			echo "OOP! USER NOT HAS BEEN CREATED";
		}
	}
	
	public function useredit($id){
		//dd($id);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$useredit = pg_query($db, "SELECT * FROM rm_users WHERE id=$id");
		$edit_by_user = array();
		while($user_row = pg_fetch_array($useredit)){
			// echo "<pre>";
			// print_r($user_row);
			// echo "</pre>";
			$edit_by_user[] = $user_row;
		}		
		
		$status = $edit_by_user[0]['status'];
		$full_rights = $edit_by_user[0]['full_rights'];
		$login_name = $edit_by_user[0]['login_name'];
		$first_name = $edit_by_user[0]['first_name'];
		$last_name = $edit_by_user[0]['last_name'];
		$email_id = $edit_by_user[0]['email_id'];
		$pwd = $edit_by_user[0]['pwd'];
		$mobile_no = $edit_by_user[0]['mobile_no'];
		$user_type_id = $edit_by_user[0]['user_type_id'];
		//dd($full_rights);
		Session::put('user_id', $id);
		Session::put('status', $status);
		Session::put('full_rights', $full_rights);
		Session::put('login_name', $login_name);
		Session::put('first_name', $first_name);
		Session::put('last_name', $last_name);	
		Session::put('email_id', $email_id);
		Session::put('pwd', $pwd);
		Session::put('mobile_no', $mobile_no);
		Session::put('user_type_id', $user_type_id);	

		//echo Session::get('user_id');
		// dd();
		return redirect('/edituser');		
	}	
	
	public function userupdate(Request $req){
		//dd($req);
		$id	= Session::get('user_id');
		$login_name = $req->loginname;
		$first_name = $req->firstname;
		$last_name = $req->lastname;
		$email_id = $req->email;
		$pwd = $req->password;
		$mobile_no = $req->mobile;
		$user_type_id = $req->designation_id;
		// date_default_timezone_set('Asia/Kolkata');
		$create_date_time = date('d-m-Y H:i');
		$create_date = $create_date_time;
		$create_by = 1;
		$status = $req->status;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$userupdate = pg_query($db, "UPDATE rm_users SET login_name = '$login_name', first_name = '$first_name', last_name = '$last_name', email_id = '$email_id', pwd = '$pwd', mobile_no = '$mobile_no', user_type_id = '$user_type_id', create_date = '$create_date', create_by = '$create_by', status = '$status' WHERE id=$id");
		if($userupdate){
			$message = "USER HAS BEEN UPDATED SUCCESSFULLY";
			Session::put('message', $message);
			//session()->forget('unit_id');
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_user = 'admin_users';
			Session::put('admin_users',$admin_user);		
			return redirect('/');
		}else{
			echo "SORRY!!! MACHINE HAS NOT BEEN UPDATED";
			//return redirect('/');
		}		
	}		
	
	public function userdelete(Request $req){
		//dd($req);
		$id = $req->user_value;
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		$userdelete = pg_query($db, "DELETE FROM rm_users WHERE id = $id");
		
		if($userdelete){
			$message = "USER HAS BEEN DELETED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			Session::forget('admin_machine_product_skus');
			$admin_users = 'admin_users';
			Session::put('admin_users',$admin_users);		
			return redirect('/');
		}else{
			echo "SORRY!!! USER HAS NOT BEEN DELETED";
		}
	}
	//###################################################################################	
	public function opreason(Request $req){	
		// dd($req);
		$operator_id = Session::get('operator_id');
		//echo "<br/>";
		$machine_id = Session::get('machine_id');
		//echo "<br/>";
		// $downreason_id = $req->breakdown_reason;		
		$downreason_id = $req->breakreasonvalue;
		$timeflag = $req->timeflag;
		//echo "<br/>";				
		
		if($req->subreasonvalue == null){
			//dd('here');
			$downsubreason_id = 0;
		}else{
			$downsubreason_id = $req->subreasonvalue;
		}
		
		if($req->breakdown_remark == null){
			//dd('Nothing');
			$remark = 'No Remarks';
		}else{
			$remark = $req->breakdown_remark;
		}
		//dd($remark);
		//echo "<br/>";		
		// $downtime_start_at = $req->start_time.":00";
		$downtime_start_at = $req->start_time;
		// $downtime_start_at = $req->start_time;
		if($timeflag == 1){
			$downtime_end_at = $req->end_time.":00";
		}else{
			$downtime_end_at = $req->end_timevalue;
		}
		$opshift_code = Session::get('shift_duration');
		//echo "<br/>";
		$entry_by = 1;
		//echo "<br/>";
		$currentdate = date("d-m-Y");
		$entry_date = $currentdate;
		// $entry_date = $req->currentdate;
		// dd($entry_date);
		// echo $entry_date;
		// echo "<br/>";
		// dd($req);
		$newdate = date("Y-m-d", strtotime($entry_date));
		// $todaydate = date_start_at
		// dd($newdate);
		//echo $newdate;
		$date_start_at = $newdate." ".$downtime_start_at;
		$date_end_at = $newdate." ".$downtime_end_at;
		// dd($newdate);
		// dd($final);	
		//dd($downtime_end_at);
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
				
		if($timeflag == 1){
			$operatortimecheck = pg_query($db, "SELECT date_start_at,date_end_at FROM rm_opdownreasons WHERE operator_id = $operator_id ORDER BY date_end_at DESC");
			$opertortimerow = pg_fetch_row($operatortimecheck);
			// echo "<pre>";
			// print_r($opertortimerow);
			// echo "</pre>";
			$opertortimerow[0];
			$opertortimerow[1];
			// echo "<br/>";
			// echo "========";
			// echo $date_start_at;
			// echo "<br/>";
			// echo $date_end_at;
			// echo "<br/>";
			if($date_start_at < $opertortimerow[0]){
				$success = "START TIME IS LESSER THAN PREVIOUS START BREAKDOWN";
				return Redirect::back()->withErrors($success);	
			}
			if($date_start_at < $opertortimerow[1]){
				$success = "START TIME IS LESSER THAN PREVIOUS END BREAKDOWN";
				return Redirect::back()->withErrors($success);	
			}
			if($date_end_at < $opertortimerow[0]){
				$success = "END TIME IS LESSER THAN PREVIOUS START BREAKDOWN";
				return Redirect::back()->withErrors($success);	
			}
			if($date_end_at < $opertortimerow[1]){
				$success = "END TIME IS LESSER THAN PREVIOUS END BREAKDOWN";
				return Redirect::back()->withErrors($success);	
			}
			if($date_end_at < $date_start_at){
				$success = "END TIME IS LESSER THAN START BREAKDOWN";
				return Redirect::back()->withErrors($success);	
			}			
		}
		
		// echo $operator_id;
		// echo "<br/>";
		// echo $machine_id;
		// echo "<br/>";
		if($downreason_id == ''){
			$downreason_id = 1;
			//
		}else{
			$downreason_id;
		}
		
		/// Auto submit breakdown reasons
		if($req->operatorstarttime){
			$date_start_at = $date_start_at." ".$req->operatorstarttime;
		}
		if($req->operatorendtime){
			$date_end_at = $date_end_at." ".$req->operatorendtime;
		}
		// echo "<br/>";
		// echo $date_end_at;
		// echo "<br/>";
		// dd('here');
		// echo "======";
		// echo "<br/>";
		// echo $operator_id;
		// echo "<br/>";
		// echo $machine_id;
		// echo "<br/>";
		// echo $downreason_id;
		// echo "<br/>";
		// echo $downsubreason_id;
		// echo "<br/>";
		// echo $remark;
		// echo "<br/>";
		// echo $opshift_code;
		// echo "<br/>";
		// echo $entry_by;
		// echo "<br/>";
		// echo $newdate;
		// echo "<br/>";
		// echo $date_start_at;
		// echo "<br/>";
		// echo $date_end_at;
		// dd('here');
		// dd($req->operatorstarttime);
		// echo "<br/>";
		
		$operatorreason = pg_query($db, "INSERT INTO rm_opdownreasons(operator_id, machine_id, downreason_id, downsubreason_id, remark, opshift_code, entry_by, entry_date, date_start_at, date_end_at)
		VALUES ('$operator_id','$machine_id','$downreason_id','$downsubreason_id','$remark','$opshift_code','$entry_by','$newdate','$date_start_at','$date_end_at')");	
	
		// echo $operatorreason;
		// dd('1');
		if($operatorreason){
			$success = "DOWNTIME REASON HAS BEEN SUCCESSFULLY ADDED";
			$downtime_reason = 'downtime_reason';
			Session::put('downtime_reason',$downtime_reason);
			return Redirect::back()->withErrors($success);	
		}else{
			echo "OPERATOR REASON HAS NOT BEEN ADDED";
		}
	}
	
	public function pcuhealthlive(){
		//dd('hereee');
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$health_query = pg_query($db, "SELECT * FROM rm_live_data");
		// dd($health_query);
		$health_info = array();		
		while($health_row = pg_fetch_array($health_query)){
			// echo "<pre>";
			// print_r($health_row);
			// echo "</pre>";
			// dd($health_row);
			$regno = $health_row['reg_no'];
			$imei_query = pg_query($db, "SELECT imei FROM rm_machines WHERE machine_regno = '$regno'");
			while($imei_row = pg_fetch_array($imei_query)){
				// echo "<pre>";
				// print_r($imei_row['imei']);
				// echo "</pre>";			
				//dd();
				$health_info[] = array(
					'bus_id' => $health_row['bus_id'],
					'reg_no' => $health_row['reg_no'],
					'pkt_cnt' => $health_row['pkt_cnt'],
					'sats_view' => $health_row['sats_view'],
					'gsm_rssi' => $health_row['gsm_rssi'],
					'batt_volt' => $health_row['batt_volt'],
					'imei' => $imei_row['imei'],
					'batt_chg_cnt' => $health_row['batt_chg_cnt'],
					'main_volt' => $health_row['veh_batt_volt']
				);
			}
		}
		$json = json_encode($health_info);
		return $json;
	}
	
	public function logout(Request $req){
		Session::flush();
		session_unset();
		return redirect('/login'); 
	}
	
	public function operatorlogout(Request $req){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$logggedin = 0;		
		$endtime = date('Y-m-d H:i:s');;
		// echo Session::get('latestskuid');
		if(Session::get('operatorskuid')){
			$latestskuid = Session::get('operatorskuid');
		}else{
			$latestskuid = Session::get('latestskuid');
		}
		
		// dd($endtime);	
		if(Session::get('loggedopid')){
			$operator_id = Session::get('loggedopid');	
		}else{
			$operator_id = Session::get('operator_id');	
		}
		if(Session::get('loggedmachineid')){
			$machine_id = Session::get('loggedmachineid');	
		}else{
			$machine_id = Session::get('machine_id');	
		}
		// dd(Session::get('loggedopid'));
		// dd($req);
		// dd($machine_id);
		// dd(Session::all());
		$operatorlogout = pg_query($db, "UPDATE public.rm_oplogins SET loggedin='$logggedin' WHERE operator_id=$operator_id");		
		$machinelogout = pg_query($db, "UPDATE public.rm_machines SET loggedin='$logggedin' WHERE id=$machine_id");	
		if($latestskuid != ''){
			$operatorskulogout = pg_query($db, "UPDATE public.rm_opskudata SET sku_end_at='$endtime' WHERE id=$latestskuid");
		}		
		Session::flush();
		session_unset();		
		return redirect('/operatorlogin'); 
	}
	
	
	////////////################################# Filter Section ############################
	public function addplantpinajax($id){
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$plant_query = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$id");
		$plantarray = array();
		while($plant_row = pg_fetch_array($plant_query)){
			// echo "<pre>";
			// print_r($plant_row);
			// echo "<pre>";
			$plant_id = $plant_row['id'];
			$plant_name = $plant_row['plant_name'];
			// dd($plant_name);
			$plantarray[] = array(
				"plant_id" => $plant_id,
				"plant_name" => $plant_name
			);			
		}
		//dd($plantarray);
		$json = json_encode($plantarray);	
		return $json;		
	}
	
	public function addcascadeajax($id){		
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$plant_query = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$id");
		$plantarray = array();
		while($plant_row = pg_fetch_array($plant_query)){
			// echo "<pre>";
			// print_r($plant_row);
			// echo "<pre>";
			$plant_id = $plant_row['id'];
			$plant_name = $plant_row['plant_name'];
			//dd($plant_name);
			$plantarray[] = array(
				"plant_id" => $plant_id,
				"plant_name" => $plant_name
			);			
		}
		//dd($plantarray);
		$json = json_encode($plantarray);	
		return $json;	
	}
	public function addmachineajax($id){		
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$plant_query = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$id");
		$plantmachineinfo = array();
		while($plant_row = pg_fetch_array($plant_query)){
			// echo "<pre>";
			// print_r($plant_row);
			// echo "<pre>";
			$plant_id = $plant_row['id'];
			$plant_name = $plant_row['plant_name'];
			//dd($plant_name);
			$plantmachineinfo[] = array(
				"plant_id" => $plant_id,
				"plant_name" => $plant_name
			);			
		}
		
		$json = json_encode($plantmachineinfo);	
		return $json;	
	}
	public function addmachineplantajax($id){	
		//dd($id);
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$machine_query = pg_query($db, "SELECT * FROM rm_cascades WHERE plant_id=$id");
		$cascadeinfo = array();
		while($machine_row = pg_fetch_array($machine_query)){
			// echo "<pre>";
			// print_r($machine_row);
			// echo "<pre>";
			$cascadeid = $machine_row['id'];
			$cascadename = $machine_row['cascade_name'];
			//dd($plant_name);
			$cascadeinfo[] = array(
				"cascadeid" => $cascadeid,
				"cascadename" => $cascadename
			);			
		}
		
		$casjson = json_encode($cascadeinfo);	
		// dd($json);
		return $casjson;	
	}
	public function plantcasmachine($id){	
		//dd($id);
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$machine_query = pg_query($db, "SELECT * FROM rm_machines WHERE cascade_id=$id");
		$cascademachine = array();
		while($machine_row = pg_fetch_array($machine_query)){
			// echo "<pre>";
			// print_r($machine_row);
			// echo "<pre>";
			$machineid = $machine_row['id'];
			$machinename = $machine_row['machine_name'];
			//dd($plant_name);
			$cascademachine[] = array(
				"machineid" => $machineid,
				"machinename" => $machinename
			);			
		}
		
		$casmachinejson = json_encode($cascademachine);	
		// dd($json);
		return $casmachinejson;	
	}
	public function editplantpinajax($id){	
		//dd($id);
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$pinplant_query = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$id");
		$pinplantarray = array();
		while($pinplant_row = pg_fetch_array($pinplant_query)){
			// echo "<pre>";
			// print_r($machine_row);
			// echo "<pre>";
			$plantid = $pinplant_row['id'];
			$plantname = $pinplant_row['plant_name'];
			//dd($plant_name);
			$pinplantarray[] = array(
				"plantid" => $plantid,
				"plantname" => $plantname
			);			
		}
		
		$pinplantjson = json_encode($pinplantarray);	
		// dd($json);
		return $pinplantjson;	
	}
	public function editcascadeajax($id){	
		//dd($id);
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$pinplant_query = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$id");
		$casarray = array();
		while($pinplant_row = pg_fetch_array($pinplant_query)){
			// echo "<pre>";
			// print_r($machine_row);
			// echo "<pre>";
			$plantid = $pinplant_row['id'];
			$plantname = $pinplant_row['plant_name'];
			//dd($plant_name);
			$casarray[] = array(
				"plantid" => $plantid,
				"plantname" => $plantname
			);			
		}
		
		$cascadejson = json_encode($casarray);	
		// dd($json);
		return $cascadejson;	
	}
	public function editproductajax($id){	
		//dd($id);
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$productajaxquery = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$id");
		$productajaxarray = array();
		while($pinplant_row = pg_fetch_array($productajaxquery)){
			// echo "<pre>";
			// print_r($machine_row);
			// echo "<pre>";
			$productplantid = $pinplant_row['id'];
			$productplantname = $pinplant_row['plant_name'];
			//dd($plant_name);
			$productajaxarray[] = array(
				"productplantid" => $productplantid,
				"productplantname" => $productplantname
			);			
		}
		
		$productplantjson = json_encode($productajaxarray);	
		// dd($json);
		return $productplantjson;	
	}
	public function editproductplantajax($id){	
		//dd($id);
		$id = $id;
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$productplantajaxquery = pg_query($db, "SELECT * FROM rm_cascades WHERE plant_id=$id");
		$productplantajaxarray = array();
		while($productplant_row = pg_fetch_array($productplantajaxquery)){
			// echo "<pre>";
			// print_r($machine_row);
			// echo "<pre>";
			$productcasid = $productplant_row['id'];
			$productcasname = $productplant_row['cascade_name'];
			//dd($plant_name);
			$productplantajaxarray[] = array(
				"productcasid" => $productcasid,
				"productcasname" => $productcasname
			);			
		}
		// dd($productplantajaxarray);
		$productcasjson = json_encode($productplantajaxarray);	
		// dd($json);
		return $productcasjson;	
	}
	public function onlyconnectedpcu(){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$onlyconnectedquery = pg_query($db, "SELECT * FROM rm_live_data WHERE batt_chg_cnt=1");
		$onlyconnectedarray = array();
		while($productplantrow = pg_fetch_array($onlyconnectedquery)){
			// $regno = $productplantrow['reg_no'];
			// $imei_query = pg_query($db, "SELECT imei FROM rm_machines WHERE machine_regno = '$regno'");
			// while($imei_row = pg_fetch_array($imei_query)){
				// $onlyconnectedarray[] = array(
					// 'bus_id' => $productplantrow['bus_id'],
					// 'reg_no' => $productplantrow['reg_no'],
					// 'pkt_cnt' => $productplantrow['pkt_cnt'],
					// 'sats_view' => $productplantrow['sats_view'],
					// 'gsm_rssi' => $productplantrow['gsm_rssi'],
					// 'batt_volt' => $productplantrow['batt_volt'],
					// 'imei' => $imei_row['imei'],
					// 'batt_chg_cnt' => $productplantrow['batt_chg_cnt'],
					// 'main_volt' => $productplantrow['veh_batt_volt']
				// );			
			// }
			$regno = $productplantrow['reg_no'];
			$imei_query = pg_query($db, "SELECT imei FROM rm_machines WHERE machine_regno = '$regno'");
			while($imei_row = pg_fetch_array($imei_query)){
				// echo "<pre>";
				// print_r($imei_row['imei']);
				// echo "</pre>";			
				//dd();
				$health_info[] = array(
					'bus_id' => $productplantrow['bus_id'],
					'reg_no' => $productplantrow['reg_no'],
					'pkt_cnt' => $productplantrow['pkt_cnt'],
					'sats_view' => $productplantrow['sats_view'],
					'gsm_rssi' => $productplantrow['gsm_rssi'],
					'batt_volt' => $productplantrow['batt_volt'],
					'imei' => $imei_row['imei'],
					'batt_chg_cnt' => $productplantrow['batt_chg_cnt'],
					'main_volt' => $productplantrow['veh_batt_volt']
				);
			}
		}
		$connectjson = json_encode($onlyconnectedarray);
		return $connectjson;
	}
	public function onlydisconnectedpcu(){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$onlyconnectedquery = pg_query($db, "SELECT * FROM rm_live_data WHERE batt_chg_cnt=0");
		$onlyconnectedarray = array();
		while($productplantrow = pg_fetch_array($onlyconnectedquery)){
			// $regno = $productplantrow['reg_no'];
			// $imei_query = pg_query($db, "SELECT imei FROM rm_machines WHERE machine_regno = '$regno'");
			// while($imei_row = pg_fetch_array($imei_query)){
				// $onlyconnectedarray[] = array(
					// 'bus_id' => $productplantrow['bus_id'],
					// 'reg_no' => $productplantrow['reg_no'],
					// 'pkt_cnt' => $productplantrow['pkt_cnt'],
					// 'sats_view' => $productplantrow['sats_view'],
					// 'gsm_rssi' => $productplantrow['gsm_rssi'],
					// 'batt_volt' => $productplantrow['batt_volt'],
					// 'imei' => $imei_row['imei'],
					// 'batt_chg_cnt' => $productplantrow['batt_chg_cnt'],
					// 'main_volt' => $productplantrow['veh_batt_volt']
				// );			
			// }
			$regno = $productplantrow['reg_no'];
			$imei_query = pg_query($db, "SELECT imei FROM rm_machines WHERE machine_regno = '$regno'");
			while($imei_row = pg_fetch_array($imei_query)){
				// echo "<pre>";
				// print_r($imei_row['imei']);
				// echo "</pre>";			
				//dd();
				$health_info[] = array(
					'bus_id' => $productplantrow['bus_id'],
					'reg_no' => $productplantrow['reg_no'],
					'pkt_cnt' => $productplantrow['pkt_cnt'],
					'sats_view' => $productplantrow['sats_view'],
					'gsm_rssi' => $productplantrow['gsm_rssi'],
					'batt_volt' => $productplantrow['batt_volt'],
					'imei' => $imei_row['imei'],
					'batt_chg_cnt' => $productplantrow['batt_chg_cnt'],
					'main_volt' => $productplantrow['veh_batt_volt']
				);
			}
		}
		$connectjson = json_encode($onlyconnectedarray);
		return $connectjson;
	}
	
	public function pcuplantmachine($id){
		// dd($id);
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$pcu_plantdata = pg_query($db, "SELECT machine_regno FROM rm_machines WHERE plant_id=$id");
		while($pcu_datarow = pg_fetch_array($pcu_plantdata)){
			$machineregno = $pcu_datarow['machine_regno'];
			// dd($machineregno);
			$onlyconnectedquery = pg_query($db, "SELECT * FROM rm_live_data WHERE reg_no= '$machineregno'");
			$pcuplantdatainfo = array();
			while($productplantrow = pg_fetch_array($onlyconnectedquery)){
				// $regno = $productplantrow['reg_no'];
				// $imei_query = pg_query($db, "SELECT imei FROM rm_machines WHERE machine_regno = '$regno'");
				// while($imei_row = pg_fetch_array($imei_query)){
					$pcuplantdatainfo[] = array(
						'bus_id' => $productplantrow['bus_id'],
						'reg_no' => $productplantrow['reg_no'],
						'pkt_cnt' => $productplantrow['pkt_cnt'],
						'sats_view' => $productplantrow['sats_view'],
						'gsm_rssi' => $productplantrow['gsm_rssi'],
						'batt_volt' => $productplantrow['batt_volt'],
						// 'imei' => $imei_row['imei'],
						'batt_chg_cnt' => $productplantrow['batt_chg_cnt'],
						'main_volt' => $productplantrow['veh_batt_volt']
					);			
				// }
			}
			$pcuplantjson = json_encode($pcuplantdatainfo);
			return $pcuplantjson;
		}
	}
	
	// public function pkt_cnt($regno,$starttime,$endtime){
	public function pkt_cnt(Request $req){
		// echo Session::get('machine_regno');
		// dd(Session::all());
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$machine_id = Session::get('machine_id');
 		$operatorid = Session::get('operator_id');
 		$skuid = Session::get('sku_row_id');
		// dd($skuid);
		$today = date('Y-m-d')." 00:00:00";
		// $today = date('2020-09-23')." 00:00:00";
		// dd($today);
		
		// dd($today);
		$operatorlogquery = pg_query($db, "SELECT sku_start_at, sku_end_at FROM rm_opskudata WHERE sku_id = $skuid and operator_id = $operatorid and machine_id=$machine_id and sku_start_at > '$today' order by sku_start_at limit 1");
		$operatorlog = array();
		while($operalogrow = pg_fetch_array($operatorlogquery)){
			// echo "<pre>";
			// print_r($operalogrow['sku_start_at']);
			// echo "</pre>";			
			$operatorlog[] = array(
				'start' => $operalogrow['sku_start_at'],
				'end' => $operalogrow['sku_end_at']
			);			
		}
		
		
		$datetime = $operatorlog[0]['start'];
		$starttime = date('Y-m-d H:i:s', strtotime($datetime));
		// dd($entrytime);
		
		// echo $starttime;
		// date_default_timezone_set('Asia/Kolkata');
		$endtime = date('Y-m-d H:i:s');
		// Display value of variable
		// echo $endtime;
		// dd($endtime);
		$regno = Session::get('machine_regno');
		// dd($regno);
		// echo $starttime;
		// dd($endtime);
		$adist="0";$tdist="0";$old_dist="0";
		
		
 	 	$dic="select pkt_cnt  from rm_production_data where reg_no='$regno' and pkt_cnt>=0 and	date_time_entry > '$starttime' and date_time_entry <= '$endtime'";		
		
		$dis=pg_exec($db,$dic);
		//dd($dis);
        $initial=0;	
		for($r=0;$r<pg_numrows($dis);$r++){
			$dist=pg_result($dis,$r,'pkt_cnt');			
			if($r==0){
				$initial=$dist;
			}
			if($old_dist>$dist && $dist<=1 && $adist==0){
				$adist=$adist+$old_dist;
				$tdist="0";
			}
			if($old_dist<$dist){
				$tdist=$dist;
			}
			$old_dist=$dist;
		}
		$cnttotal=($tdist+$adist) - $initial;
		return $cnttotal;
	}	
	
	public function pcudatainfo(Request $req){		
		//dd($req);		
		$unitid = $req->unitid;	
		$plantid = $req->plantid;
		// dd($plantid);
		$status = $req->status;
		// dd($status);
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		// dd($req);
		if($status == '1')
		{
			 #$onlyconnectedquery = "and rm_live_data.date_time_entry > timestamp 'now()' - interval '15minutes'";
			 $onlyconnectedquery = "and rm_live_data.batt_chg_cnt='1'";
		}
		if($status == '2')
		{
			 #$onlyconnectedquery = "and rm_live_data.date_time_entry < timestamp 'now()' - interval '15minutes'";
			 $onlyconnectedquery = "and rm_live_data.batt_chg_cnt='0'";
		}
		if($status == '0')
		{
			 $onlyconnectedquery = "";
		}
		$pcuarray = array(); 
		if($plantid > 0)
		{
	//		$pcudatainfo_query = pg_query($db, "SELECT machine_regno,imei FROM rm_machines WHERE plant_id=$plantid");
			$livemachine = pg_query($db, "SELECT rm_machines.imei,rm_live_data.* FROM rm_live_data,rm_machines WHERE rm_machines.plant_id=$plantid and rm_live_data.reg_no=rm_machines.machine_regno $onlyconnectedquery order by rm_live_data.reg_no");
		}
		else if($unitid > 0)
		{
//			$pcudatainfo_query = pg_query($db, "SELECT machine_regno,imei FROM rm_machines WHERE unit_id=$unitid");
			$livemachine = pg_query($db, "SELECT rm_machines.imei,rm_live_data.* FROM rm_live_data,rm_machines WHERE rm_machines.unit_id=$unitid and rm_live_data.reg_no=rm_machines.machine_regno $onlyconnectedquery order by rm_live_data.reg_no");
		}
		else
		{
//			$pcudatainfo_query = pg_query($db, "SELECT machine_regno,imei FROM rm_machines");
			$livemachine = pg_query($db, "SELECT rm_machines.imei,rm_live_data.* FROM rm_live_data,rm_machines WHERE rm_live_data.reg_no=rm_machines.machine_regno $onlyconnectedquery order by rm_live_data.reg_no");
		}

		//while($pcudatarow = pg_fetch_array($pcudatainfo_query)){
			// echo "<pre>";
			// print_r($pcudatarow);
			// echo "</pre>";
		//	$machine_regno = $pcudatarow['machine_regno'];
//		$livemachine = pg_query($db, "SELECT * FROM rm_live_data WHERE reg_no = '$machine_regno'");
			// dd($livemachine);
			
		//	if(pg_num_rows($livemachine)>0){
				while($machine_row = pg_fetch_array($livemachine)){
					$pcuarray[] = array(
						'busid' => $machine_row['bus_id'],
						'imei' => $machine_row['imei'],
						'regno' => $machine_row['reg_no'],
						'pktcnt' => $machine_row['pkt_cnt'],
						'satsview' => $machine_row['sats_view'],
						'gsmrssi' => $machine_row['gsm_rssi'],
						'battchgcnt' => $machine_row['batt_chg_cnt'],
						'battvolt' => $machine_row['batt_volt'],
						'datetime' => $machine_row['date_time_entry'],
						'mainvolt' => $machine_row['veh_batt_volt']
					);
				}
				$connectjson = json_encode($pcuarray);
				return $connectjson;
				// echo $unitid;		
				// echo $plantid;
				// echo $status;
				// dd($connectjson);
			//}
		//}
		// $connectjson = json_encode($pcuarray);		
		// return $connectjson;

	}
	
	public function svunit($id){
		// $id = $id;
		// dd('hiiiii');
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$svuplant = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id = $id");
		$svplantarray = array();
		while($svuplantrow = pg_fetch_array($svuplant)){
			// echo "<pre>";
			// print_r($svuplantrow);
			// echo "</pre>";
			$id = $svuplantrow['id'];
			$plantname = $svuplantrow['plant_name'];
			$svplantarray[] = array(
				'plantid' => $id,
				'plantname' => $plantname
			);
		}
		$json = json_encode($svplantarray);
		return $json;
	}
	
	public function svplant($id){
		// $id = $id;
		// dd('hiiiii');
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$svucascade = pg_query($db, "SELECT * FROM rm_cascades WHERE plant_id = $id");
		$svcascadearray = array();
		while($svucascaderow = pg_fetch_array($svucascade)){
			// echo "<pre>";
			// print_r($svuplantrow);
			// echo "</pre>";
			$id = $svucascaderow['id'];
			$cascadename = $svucascaderow['cascade_name'];
			$svcascadearray[] = array(
				'cascadeid' => $id,
				'cascadename' => $cascadename
			);
		}
		$json = json_encode($svcascadearray);
		return $json;
	}
	
	public function svcascade($id){
		// $id = $id;
		// dd('hiiiii');
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$svumachine = pg_query($db, "SELECT * FROM rm_machines WHERE cascade_id = $id");
		$svmachinearray = array();
		while($svumachinerow = pg_fetch_array($svumachine)){
			// echo "<pre>";
			// print_r($svuplantrow);
			// echo "</pre>";
			$id = $svumachinerow['id'];
			$machinename = $svumachinerow['machine_name'];
			$svmachinearray[] = array(
				'machineid' => $id,
				'machinename' => $machinename
			);
		}
		$json = json_encode($svmachinearray);
		return $json;
	}
	
	// public function svdata(Request $req){
		// dd($req);
	// }
	
	
	/* ===================================================================== */
	public function getActualProduction($machine_regno,$start_at,$end_at) {
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
        $adist="0";$tdist="0";$old_dist="0";
        $dic="select pkt_cnt  from rm_production_data where reg_no='$machine_regno' and pkt_cnt>=0 and date_time_entry > '$start_at' and date_time_entry <= '$end_at'";

       // echo $dic;
        $dis=pg_exec($db,$dic);
        $initial = "0";
        for($r=0;$r<pg_numrows($dis);$r++)
        {
            $dist=pg_result($dis,$r,'pkt_cnt');
                if($r==0)
                {
                    $initial=$dist;
                }
                if($old_dist>$dist && $dist<=1 && $adist==0)
                {
                    $adist=$adist+$old_dist;
                    $tdist="0";
                }
                if($old_dist<$dist)
                {
                    $tdist=$dist;
                }
            $old_dist=$dist;
        }
        $cnttotal=($tdist+$adist) - $initial;
        return $cnttotal;
    } 
	
	public function showData($machine = '',$unit = '',$plant = '',$cascade = '',$shift = '',$fromDate = '' ,$toDate = '', $test='helloooo'){		
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");

		$new_data = $_GET;
		$new_data_post = $_POST;
		if(count($new_data) > 0) {
			$plant = !empty($new_data['p']) ? $new_data['p'] : $plant;
			$machine = !empty($new_data['m']) ? $new_data['m'] : $machine;
			$unit = !empty($new_data['u']) ? $new_data['u'] : $unit;
			$cascade = !empty($new_data['c']) ? $new_data['c'] : $cascade;
			$send_p = $plant; 
			$send_m = $machine;
			$send_u = $unit;
			$send_c = $cascade;
			// get names 
		    $get_m_name = "SELECT machine_name FROM rm_machines WHERE id = $machine";
				$result_m = pg_exec($db,$get_m_name); 
				while ($m_data = pg_fetch_array($result_m)) {
					$send_m_n = $m_data['machine_name'];
				}
			$get_p_name = "SELECT plant_name FROM rm_plants WHERE id = $plant"; 
				$result_p = pg_exec($db,$get_p_name);
				while ($p_data = pg_fetch_array($result_p)) {
					$send_p_n = $p_data['plant_name'];
				}
			$get_c_name = "SELECT cascade_name FROM rm_cascades WHERE id = $cascade"; 
				$result_c = pg_exec($db,$get_c_name); 
				while ($C_data = pg_fetch_array($result_c)) {
					$send_c_n = $C_data['cascade_name'];
				}
			$get_u_name = "SELECT unit_name FROM rm_units WHERE id = $unit"; 
				$result_u = pg_exec($db,$get_u_name); 
				while ($u_data = pg_fetch_array($result_u)) {
					$send_u_n = $u_data['unit_name'];
				}
		//echo 'send_m_n :  '.$send_m_n."\n";
		} else {
			$send_p = ''; 
			$send_m = ''; 
			$send_u = '';
			$send_c = ''; 
			$send_p_n = ''; 
			$send_m_n = ''; 
			$send_u_n = ''; 
			$send_c_n = '';
		}


		$current_date_time = Carbon::now('Asia/Kolkata')->toDateTimeString();
		$carbon = Carbon::today();
       		$get_current_date = $carbon->format('Y-m-d');
		$get_date = $carbon->format('d/m/Y');
		
		$get_M  = (!empty($machine) && $machine != 0) ? ' AND rm_machines.id = '.$machine : '';
		$get_C  = (!empty($cascade) && $cascade != 0) ? ' AND rm_cascades.id = '.$cascade : '';
		$get_S  = (!empty($shift) && $shift != 0) ? ' AND rm_shifts.id = '.$shift : '';
		$get_U = (!empty($unit) && $unit != 0 && empty($plant)) ? ' AND rm_machines.unit_id = '.$unit : '';
		$get_U_P = (!empty($unit) && $unit != 0 && !empty($plant) && $plant != 0) ? ' AND rm_machines.unit_id = '.$unit .' AND rm_machines.plant_id = '.$plant : '';
		$flag = 0;
		
		if(empty($get_M) && empty($get_C) && empty($get_S) && empty($get_U)  && empty($get_U_P) ) {
            // $where = "WHERE ";
            if(!empty($fromDate) && !empty($toDate)) {
                $where = " WHERE (logintime::date BETWEEN '$fromDate' AND '$toDate') ";
            } else {
                $where = " WHERE logintime::date = '$get_current_date' ";
            }
			$condition = '';
			$flag = 1;
		} else if(!empty($get_M) || !empty($get_C) || !empty($get_S) || !empty($get_U) || !empty($get_U_P)) {
            if(!empty($fromDate) && !empty($toDate)) {
                $checkdate = " AND (logintime::date BETWEEN '$fromDate' AND '$toDate') ";
            } else {
                $checkdate = " AND logintime::date = '$get_current_date' ";
            }
		   $flag = 2;
		   $where = 'WHERE ';
		   $final = $get_M.$get_C.$get_S.$get_U.$get_U_P.$checkdate;
		   $condition =  substr($final, 4);
		} 
			
	     $qry = "SELECT rm_oplogins.id as test_id,rm_shifts.shift_name,rm_shifts.shift_code,rm_users.first_name,rm_users.last_name,rm_oplogins.operator_id,
						rm_machines.machine_name,rm_machines.machine_regno,rm_oplogins.machine_id,rm_oplogins.logintime,rm_mixerbct.bct,
						rm_cascades.cascade_name,rm_cascades.cascade_code,rm_shifts.shift_end_at,rm_shifts.shift_start_at,rm_machinetypes.output_counter_type,
						EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
						EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new 
				FROM rm_oplogins
				LEFT JOIN rm_shifts ON rm_shifts.shift_code = rm_oplogins.shift_code
				LEFT JOIN rm_users ON rm_users.id = rm_oplogins.operator_id
				LEFT JOIN rm_machines ON rm_machines.id = rm_oplogins.machine_id
				LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id
				LEFT JOIN rm_mixerbct ON rm_mixerbct.machine_id =  rm_machines.id
				LEFT JOIN rm_cascades ON rm_machines.cascade_id = rm_cascades.id $where $condition 
				ORDER BY rm_oplogins.id DESC "; 

		//echo $qry; //die;
  
		$result = pg_exec($db,$qry);
		$array = [];
		if (pg_num_rows($result) > 0) {
			while ($recData = pg_fetch_array($result)) {

				//echo "<pre>"; print_r($recData);
				$opId = $recData['operator_id'];
				$mId = $recData['machine_id'];
				if($recData['shift_part'] > 0) {
					$init = $recData['shift_part'];
				} else if($recData['shift_part'] < 0) {
					$init = $recData['shift_part_new']/2;
				}
				$minutes = floor($init / 60);
				$recData['shift_part']  = $minutes;
				$qry_1 = "SELECT rm_opskudata.sku_start_at,rm_opskudata.sku_end_at,
								rm_skus.sku_code,rm_skus.sku_name,rm_skus.rpm_default,
								rm_skus.pack_size,rm_skus.unit_per_cld,rm_skus.rpm_default as standard_rpm	
						 FROM rm_oplogins
						 LEFT JOIN rm_opskudata ON rm_opskudata.machine_id = rm_oplogins.machine_id
						 LEFT JOIN rm_skus ON rm_opskudata.sku_id = rm_skus.id 
						 WHERE rm_opskudata.operator_id = $opId AND rm_opskudata.machine_id = $mId  ";
				$result_1 = pg_exec($db,$qry_1);

				
				if (pg_num_rows($result_1) > 0) {
					while ($recData_1 = pg_fetch_array($result_1)) {

						$start_sku = $recData_1['sku_start_at'];
                      $qry_2 = "SELECT EXTRACT(EPOCH FROM (date_end_at::time - date_start_at::time)) as breakdown_part
                            FROM rm_oplogins
                            LEFT JOIN rm_opdownreasons ON rm_opdownreasons.machine_id = rm_oplogins.machine_id 
                            WHERE rm_opdownreasons.machine_id = $mId AND rm_opdownreasons.operator_id = $opId 
                                AND date_start_at > '$start_sku'"; 

						//echo $qry_2."\n"."</br>";
                        $result_2 = pg_exec($db,$qry_2);
                        if (pg_num_rows($result_2) > 0) {
                            while ($recData_2 = pg_fetch_array($result_2)) {
                                $part_shift = $recData_2['breakdown_part'];
                                $minutes_2 = floor($part_shift / 60);
                                $recData['breakdown_part'] = $minutes_2;
                            }
                        } else {
                            $recData['breakdown_part'] = '-';
                        }
                        
						$recData['sku_start_at'] = $recData_1['sku_start_at'];
						$recData['sku_end_at'] = $recData_1['sku_end_at'];
						$recData['sku_code'] = $recData_1['sku_code'];
						$recData['sku_name'] = $recData_1['sku_name'];
						$recData['rpm_default'] = !empty($recData_1['standard_rpm']) ? $recData_1['standard_rpm'] : '';
                       
                        // calculate OEE %
                            $machine_regno = $recData['machine_regno'];
                            if($recData['shift_code'] == 'E' || $recData['shift_code'] == 'M') {  
                                $start_at = date('Y-m-d', strtotime($recData['logintime']))." ".$recData['shift_start_at'];
                                $end_at = date('Y-m-d', strtotime($recData['logintime']))." ".$recData['shift_end_at'];
                            } else if($recData['shift_code'] == 'N') {
                                $start_at = date('Y-m-d', strtotime($recData['logintime']))." ".$recData['shift_start_at'];
                                $end_at = date('Y-m-d', strtotime("+1 day", strtotime($recData['logintime'])))." ".$recData['shift_end_at'];
                            }
                            $actual_production = $this->getActualProduction($machine_regno,$start_at,$end_at);
                            $total_production = $actual_production * $recData_1['unit_per_cld'];
							$recData['total_production'] = $total_production;
                        if($recData['output_counter_type'] == 'CLD' ) { 
							 // calculate target production 
							$target_production = (!empty($recData_1['standard_rpm'])) ? $recData_1['standard_rpm'] * $recData['shift_part'] : '';
                            $get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
							$recData['oee'] = ($get_oee != '') ? round($get_oee,2)."%" : '-';
							$recData['target_production'] = $target_production;
                        }else { 
                            if($recData_1['sku_end_at'] != null) { 	
                                $startTime = Carbon::parse($recData_1['sku_start_at']);
                                $endTime = Carbon::parse($recData_1['sku_end_at']);
								 $totalMixDuration =  $startTime->diff($endTime)->format('%H');
								 $get_batches_made = $totalMixDuration*60/$recData['bct'];
								 $get_target_batches = $recData['shift_part']/$recData['bct'];
                            	 $recData['oee'] = (( $get_batches_made/$get_target_batches )*100).'%';
                            } else {
                            	$startTime = Carbon::parse($recData_1['sku_start_at']);
                                $endTime = Carbon::parse($end_at);
								$totalMixDuration =  $startTime->diff($endTime)->format('%H');
								$get_batches_made = $totalMixDuration*60/$recData['bct'];
								$get_target_batches = $recData['shift_part']/$recData['bct'];
							    $recData['oee'] = (( $get_batches_made/$get_target_batches )*100).'%';
                            }
                        }				  
                    }    
				} else {
							 $recData['breakdown_part'] = '-';
							 $recData['oee'] = ''; 
						}
 			$recData['cld'] = (!empty($actual_production)) ? $actual_production : '0';
                   $recData['logintime'] = date('d M,Y', strtotime($recData['logintime']));
				$array[] = $recData;
			}
		} else {
			$array = [];
		}
		
		if($test == 'helloooo') {
			return view('report',['data' => $array,'date' => $get_date,'send_p' => $send_p,'send_u' => $send_u,'send_m' => $send_m,'send_c' => $send_c,'send_p_n' => $send_p_n,'send_m_n' => $send_m_n,'send_u_n' => $send_u_n,'send_c_n' => $send_c_n,'time' => date('d M,Y g:i A', strtotime($current_date_time))]);
		} else if((!empty($get_M) || !empty($get_C) || !empty($get_S) || !empty($get_U) || !empty($get_U_P))  ) {
			return $array;
        } else {
			return $array;
		}
			
	}
	
public function showGraph($machine = '',$unit = '',$plant = '',$cascade = '',$shift = ''){

		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$new_data = $_GET;
		$carbon = Carbon::today(); 
		$current_date_time =  Carbon::now('Asia/Kolkata')->toDateTimeString();
		$current_date =  $carbon->format('Y-m-d');
		$current_date_new =  $carbon->format('Y-m-d');
		if(isset($_POST['txtStartDate']) && !empty($_POST['txtStartDate']) && $_POST['txtStartDate'] < $current_date) {
			$filter = $this->showGraphFilter($new_data,$_POST['txtStartDate']);
			
			// echo '--------------------------------'."<pre>";
			// print_r($filter);die;
			if(gettype($filter) == 'array') { 
				return view('show_graph',[
					'data' => $filter['data'],
					'working_time' => $filter['working_time'],
					'working_time2' => $filter['working_time2'],
					'm_reg' => $filter['m_reg'],
					'current_array' => $filter['current_array'],
					'prev_array' => $filter['prev_array'],
					'production' => $filter['production'],
					'brk' => $filter['brk'],
					'left' => $filter['left'],
					'no_prod' => $filter['no_prod'],
					'again_array' => $filter['again_array'],
					'plant_id' => $filter['plant_id'],
					'unit_id' => $filter['unit_id'],
					'cascade_id' => $filter['cascade_id'],
					'machine_id' => $filter['machine_id'],
					'flag' => 'array',
					'check' => 'prev',
					'f_oee' => $filter['f_oee'],
					'graph_show' => $filter['graph_show'],
					]);
				} else{
					$get_detail = explode("**",$filter);
					return view('show_graph',[
						'flag' => 'string',
						'message' => $get_detail[1],
						'machine' => $get_detail[0],
						'working_time2' => $_POST['txtStartDate']
						]);
					// echo $filter; die;
				}
		}
		
		

		if(count($new_data) > 0) {
			$machine = !empty($new_data['m']) ? $new_data['m'] : $machine;
			$plant = !empty($new_data['p']) ? $new_data['p'] : $plant;
			$unit = !empty($new_data['u']) ? $new_data['u'] : $unit;
			$cascade = !empty($new_data['c']) ? $new_data['c'] : $cascade;
			// get names 
		    $get_m_name = "SELECT machine_name,machine_regno FROM rm_machines WHERE id = $machine";
				$result_m = pg_exec($db,$get_m_name); 
				while ($m_data = pg_fetch_array($result_m)) {
					$send_m_n = $m_data['machine_name'];
					$machine_regno = $m_data['machine_regno'];
				}
		}
		
		
		$working_time = date('d M,Y', strtotime($current_date_time));
		$working_time2 = date('Y-m-d');
		$next_day = date('Y-m-d', strtotime("+1 day", strtotime($current_date)));
		$next_day_new = date('Y-m-d', strtotime("+1 day", strtotime($current_date_new)));
		$current_time =  Carbon::now('Asia/Kolkata')->toTimeString(); 
		$mid = $machine;
		$m_reg = $machine_regno; 

		$check_oplogin = "SELECT logintime from rm_oplogins WHERE machine_id = $mid AND logintime::date = '$current_date' ORDER BY id ASC LIMIT 1";
		$result_login = pg_exec($db,$check_oplogin);
		$op_logintime = pg_result($result_login,'0','logintime');
		// $mid = '88';
		// $m_reg = 'VIM_M2';
			$qry_1 = "SELECT rm_opskudata.id as main_id,rm_opskudata.sku_id,rm_opskudata.sku_start_at,rm_opskudata.sku_end_at,
								rm_skus.sku_code,rm_skus.sku_name,rm_skus.rpm_default,
								rm_skus.pack_size,rm_skus.unit_per_cld,rm_skus.rpm_default as standard_rpm
								
					FROM rm_opskudata
					LEFT JOIN rm_skus ON rm_opskudata.sku_id = rm_skus.id 
					WHERE rm_opskudata.machine_id = $mid AND sku_start_at::date = '$current_date' ";
			$result_1 = pg_exec($db,$qry_1);

			$qry_2 = "SELECT rm_machinetypes.output_counter_type,rm_mixerbct.bct
						FROM rm_machines
						LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id 
						LEFT JOIN rm_mixerbct ON rm_mixerbct.machine_id =  rm_machines.id
						WHERE rm_machines.id =  $mid ";
			$result_2 = pg_exec($db,$qry_2);
			$output_counter_type = pg_result($result_2,'0','output_counter_type');
			$bct = pg_result($result_2,'0','bct');

				while ($recData = pg_fetch_array($result_1)) {

					$get_sku_rpm = "SELECT rpm as sku_rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $recData[sku_id]";
					$result_sku_r = pg_exec($db,$get_sku_rpm);
					if(pg_num_rows($result_sku_r) > 0) {
						$sku_rpm = pg_result($result_sku_r,'0','sku_rpm');
					} else {
						$sku_rpm = 0;
					}
					$array[] = array(
							'sku_code' => $recData['sku_code'],
							'standard_rpm' => $recData['standard_rpm'],
							'pack_size' =>  $recData['pack_size'],
							'unit_per_cld' =>  $recData['unit_per_cld'],
							'sku_start_at' =>  $recData['sku_start_at'],
							'sku_end_at' =>  $recData['sku_end_at'],
							'sku_rpm' => $sku_rpm,
						);
				}
		// get shift str and end time 
		$get_shift_start = "SELECT shift_start_at FROM rm_shifts WHERE shift_code = 'M'";
			$result_str = pg_exec($db,$get_shift_start); 
			$shift_start_at = $current_date.' '.pg_result($result_str,'0','shift_start_at');
		$get_shift_end = "SELECT shift_end_at FROM rm_shifts WHERE shift_code = 'N'";
			$result_end = pg_exec($db,$get_shift_end); 
			$shift_end_at = $next_day.' '.pg_result($result_end,'0','shift_end_at');
		// cal 1 hour intervals
		$ReturnArray = array ();// Define output
		$ReturnArray_new = array();
		$ReturnArray_1 = array(); 
		$StartTime    = strtotime ($shift_start_at); //Get Timestamp
		$EndTime      = strtotime ($shift_end_at); //Get Timestamp
		$StartTime_new    = strtotime ($shift_start_at); //Get Timestamp
		$EndTime_new      = strtotime ($shift_end_at); //Get Timestamp
		$Duration     = 60; 
		$Duration_new     = 120;    
		$AddMins  = $Duration * 60;
		$AddMins_new  = $Duration_new * 60;

		// get actual shift production
			$check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
							EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_start_at < '$current_time' AND shift_end_at >= '$current_time' ";
			$exe_shift_qry = pg_query($db,$check_shift);
			if(pg_num_rows($exe_shift_qry) > 0) {
				while ($data_2 = pg_fetch_array($exe_shift_qry)) {

					if($data_2['shift_part'] > 0) {
						$init = $data_2['shift_part'];
					} else if($data_2['shift_part'] < 0) {
						$init = $data_2['shift_part_new']/2;
					}
					$minutes = floor($init / 60);
					$shift_min[]  = $minutes;
					$total_sft_min = floor($init / 60);
				$current_shift = $data_2['shift_code'];
				}
			} else {
			$check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
				EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_code = 'N' ";
				$exe_shift_qry = pg_query($db,$check_shift);
				if(pg_num_rows($exe_shift_qry) > 0) {
					while ($data_2 = pg_fetch_array($exe_shift_qry)) {
						if($data_2['shift_part'] > 0) {
							$init = $data_2['shift_part'];
						} else if($data_2['shift_part'] < 0) {
							$init = $data_2['shift_part_new']/2;
						}
						$minutes = floor($init / 60);
						$shift_min[]  = $minutes;
						$total_sft_min = floor($init / 60);
						$current_shift = $data_2['shift_code'];
					}
				}
			}
	
		while ($StartTime <= $EndTime) //Run loop
		{
			$ReturnArray[] = date ("G:i", $StartTime);
			$StartTime += $AddMins; //Endtime check
		}

		while ($StartTime_new <= $EndTime_new) //Run loop
		{
			$ReturnArray_new[] = date ("G:i", $StartTime_new);
			$StartTime_new += $AddMins_new; //Endtime check
		}

		$total_array = [];
		$total_array_new = [];
		$final_array = [];
		$current_array = [];
		// echo "<pre>";
		// print_r($ReturnArray);

		// echo "<pre>";
		// print_r($ReturnArray_new);
		// die;

		foreach($ReturnArray as $val) {
			if(date('H:i',strtotime($val)) == '00:00') {
				$from_date = $current_date.' '.date('H:i', strtotime("-1 hour", strtotime($val)));
				$to_date = $next_day.' '.date('H:i',strtotime($val));
				$get_interval = date('H',strtotime($val));
				$get_interval_time = date('H:i',strtotime($val));
				$current_date = $next_day;
			} else {
				$from_date = $current_date.' '.date('H:i', strtotime("-1 hour", strtotime($val)));
				$to_date = $current_date.' '.date('H:i',strtotime($val));
				$get_interval_time = date('H:i',strtotime($val));
				$get_interval = date('H',strtotime($val));
			}
			
			$actual_production = $this->getActualProduction($m_reg,$from_date,$to_date);
			// echo $from_date.'---'.$to_date.'---'.$actual_production."</br>";
			$total_array[] = array('interval' => $get_interval,'int_time' => $get_interval_time,'date' => $current_date ,'cld' => $actual_production ,'from_date' => $from_date , 'to_date' => $to_date);
		}

		foreach($ReturnArray_new as $val_new) {
			// echo $val_new;
			if(date('H:i',strtotime($val_new)) == '01:00') {
				$from_date_new = $current_date_new.' '.date('H:i', strtotime("-2 hour", strtotime($val_new)));
				$to_date_new = $next_day_new.' '.date('H:i',strtotime($val_new));
				$get_interval_new = date('H',strtotime($val_new));
				$get_interval_time_new = date('H:i',strtotime($val_new));
				$current_date_new = $next_day_new;
			} else {
				$from_date_new = $current_date_new.' '.date('H:i', strtotime("-2 hour", strtotime($val_new)));
				$to_date_new = $current_date_new.' '.date('H:i',strtotime($val_new));
				$get_interval_time_new = date('H:i',strtotime($val_new));
				$get_interval_new = date('H',strtotime($val_new));
			}
			
			$actual_production_new = $this->getActualProduction($m_reg,$from_date_new,$to_date_new);
			// echo $from_date.'---'.$to_date.'---'.$actual_production."</br>";
			$total_array_new[] = array('interval' => $get_interval_new,'int_time' => $get_interval_time_new,'date' => $current_date_new ,'cld' => $actual_production_new ,'from_date' => $from_date_new, 'to_date' => $to_date_new);
		}
		// echo "<pre>";
		// print_r($total_array);
		// echo "<pre>";
		// print_r($total_array_new);
		// die;
		$array_prod = [];
		$array_brk = [];
		$array_prod_left = [];
		$array_nothing = [];
		$check_status = 0;

		

		foreach($array as $getData) {
			foreach($total_array as $getInt) {
				//-----------------------------FOR OEE GRAPH-----------------------------------
				//check current shift 
				 $get_shift_time = "SELECT shift_end_at,shift_code FROM rm_shifts 
								WHERE  shift_start_at <= '$getInt[int_time]' AND shift_end_at >= '$getInt[int_time]' ";  
								//echo "<pre>";    
				$result_sft_time = pg_exec($db,$get_shift_time); 
				while ($sft_recData = pg_fetch_array($result_sft_time)) {
					if($sft_recData['shift_code'] == 'E' || $sft_recData['shift_code'] == 'M') {  
						$sku_sft_end_at = $current_date.' '.$sft_recData['shift_end_at'];
					} else if($sft_recData['shift_code'] == 'N') {
						$sku_sft_end_at = $next_day.' '.$sft_recData['shift_end_at'];
					}
				}
				$sku_end_at = !empty($getData['sku_end_at']) ? $getData['sku_end_at'] : $sku_sft_end_at; 
				// End ----------------
				if($getData['sku_start_at'] <= $getInt['to_date'] && $sku_end_at >= $getInt['to_date']) {
					
					$total_production = $getInt['cld'] * $getData['unit_per_cld'];
					//---------------------OEE--------
						$target_production = (!empty($getData['sku_rpm'])) ? $getData['sku_rpm'] * $Duration : '';
						// echo '----'.$getInt['from_date'].' ===== '.$getInt['to_date'].'------';
							if($output_counter_type == 'CLD' ) {
								$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
								$oee = ($get_oee != '') ? ceil($get_oee) : 1;
							}else { 
								
								if($bct > 0) { 
									$totalMixDuration =   $current_shift == 'M' ? $shift_min[0] : ( $current_shift != 'M' && $current_shift != 'N') ? $shift_min[0]*2 : ($current_shift == 'N' ? $shift_min[0]*3 : '');
									$get_target_batches = $totalMixDuration/$bct;
									$get_batches_made = (abs(strtotime($getInt['from_date']) - strtotime($current_date_time)) / 60)/$bct;
									$oee = ceil((( $get_batches_made/$get_target_batches )*100));
								} else {
									$oee = 1;
								}
								
							}
					// FINAL ARRAY-------------        
					// echo $getInt['cld'].' * '.$getData['unit_per_cld'].'/ '.$getData['sku_rpm'] .'* '.$Duration.'--------'.$oee."</br>";
					if($current_date_time > $getInt['from_date']) {
						if($oee >= 85) {
							$color = 'green';
						} else if($oee < 85 && $oee >= 75) {
							$color = '#b5b503';
						} else {
							$color = 'red';
						}
						$final_array[] = array('interval' => $getInt['interval'],'date' => $getInt['date'] ,'cld' => $getInt['cld'] ,'from_date' => $getInt['from_date'] , 'to_date' => $getInt['to_date'] , 'total_prod' => $total_production  ? $total_production : '0' ,'oee' => $oee? $oee : '0' ,'color' => $color);
					}
				}
				//cal current overall running production
					// if($current_date_time >= $getInt['from_date'] && $current_date_time <= $getInt['to_date']) {
					// 	$total_production = $getInt['cld'] * $getData['unit_per_cld'];
					// 	//---------------------OEE--------
					// 		$target_production = (!empty($getData['standard_rpm'])) ? $getData['standard_rpm'] * $Duration : '';
					// 			if($output_counter_type == 'CLD' ) {
					// 				//echo 'TP:  '.$total_production.'   TGP:  '.$target_production.'   CLD:  '.$getInt['cld']."</br>";
					// 				$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
					// 				$oee = ($get_oee != '') ? ceil($get_oee) : 1;
					// 			}else { 
					// 				if($bct > 0) { 
					// 					$totalMixDuration =  1; //HOUR 
					// 					$get_batches_made = $totalMixDuration*60/$bct;
					// 					$get_target_batches = $Duration/$bct;
					// 					$oee = ceil((( $get_batches_made/$get_target_batches )*100));
					// 				} else {
					// 					$oee = 1;
					// 				}
					// 			}
					// 	// current ARRAY-------------        
					// 	$current_array[] = array('interval' => $getInt['interval'],
					// 					'output_counter_type' => $output_counter_type,
					// 					'sku_code' => $getData['sku_code'],
					// 					'pack_size' => $getData['pack_size'],
					// 					'unit_per_cld' => $getData['unit_per_cld'],
					// 					'date' => $getInt['date'] ,
					// 					'cld' => $getInt['cld'] ,
					// 					'from_date' => $getInt['from_date'] , 
					// 					'to_date' => $getInt['to_date'] ,
					// 					'total_prod' => $total_production  ? $total_production : '0' ,
					//					'oee' => $oee? $oee : '0'
					// 				);
					// }
			}
		} 


		$current_array_prev = [];
		$oee_total = 0;
		$prod_total = 0;
//  NEW CODE 
		// GET current and previous SKU details 
		$sku_str = $total_array[0]['to_date'];
		$sku_end = $current_date_time;

		// check end time for shift running 
		$check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
		EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_start_at < '$current_time' AND shift_end_at >= '$current_time' ";
		$exe_shift_qry = pg_query($db,$check_shift);
		if(pg_num_rows($exe_shift_qry) > 0) {
			while ($data_2 = pg_fetch_array($exe_shift_qry)) {

				if($data_2['shift_part'] > 0) {
					$init = $data_2['shift_part'];
				} else if($data_2['shift_part'] < 0) {
					$init = $data_2['shift_part_new']/2;
				}
				$minutes = floor($init / 60);
				$shift_min[]  = $minutes;
				$end_time[] =  $data_2['shift_end_at'];
				$start_time[] =  $data_2['shift_start_at'];
				
				$flag = 1;
				$current_shift = $data_2['shift_code'];
			}
		} else {
			$check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
			EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_code = 'N' ";
			$exe_shift_qry = pg_query($db,$check_shift);
			if(pg_num_rows($exe_shift_qry) > 0) {
				while ($data_2 = pg_fetch_array($exe_shift_qry)) {
					if($data_2['shift_part'] > 0) {
						$init = $data_2['shift_part'];
					} else if($data_2['shift_part'] < 0) {
						$init = $data_2['shift_part_new']/2;
					}
					$minutes = floor($init / 60);
					$shift_min[]  = $minutes;
					$end_time[] = $data_2['shift_end_at'];
					$start_time[] =  $data_2['shift_start_at'];
					$flag = 2;
					$current_shift = $data_2['shift_code'];
				}
			}
		}
		
		$str_shift_time = date('Y-m-d',strtotime($current_date_time)).' '.$start_time[0];
		$end_shift_time = date('Y-m-d',strtotime($current_date_time)).' '.$end_time[0];
		// qry 
		  $get_all_sku_detail_cnt = "SELECT count(*)
								FROM rm_opskudata 
								JOIN rm_skus On rm_skus.id = rm_opskudata.sku_id
								JOIN rm_machines ON rm_machines.id = rm_opskudata.machine_id
								JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id
								WHERE rm_opskudata.sku_start_at BETWEEN '$sku_str' AND '$sku_end' AND rm_opskudata.machine_id = $mid
								AND rm_machines.id = $mid ";
		 $exe_get_all_sku_detail_cnt = pg_query($db,$get_all_sku_detail_cnt);
		 $count = pg_result($exe_get_all_sku_detail_cnt,'0','count');

		  $get_all_sku_detail = "SELECT sku_id,sku_start_at,rm_machinetypes.output_counter_type,sku_end_at,rm_skus.sku_code,rm_skus.pack_size,rm_skus.unit_per_cld,rm_skus.pack_size_unit
								FROM rm_opskudata 
								JOIN rm_skus On rm_skus.id = rm_opskudata.sku_id
								JOIN rm_machines ON rm_machines.id = rm_opskudata.machine_id
								JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id
								WHERE rm_opskudata.sku_start_at BETWEEN '$sku_str' AND '$sku_end' AND rm_opskudata.machine_id = $mid AND rm_machines.id = $mid ";
		$exe_get_all_sku_detail = pg_query($db,$get_all_sku_detail);

			if(pg_num_rows($exe_get_all_sku_detail) > 0) {

                $j = 0;
				while ($data_sku = pg_fetch_array($exe_get_all_sku_detail)) {
					$j++;
					$get_bct = "SELECT bct FROM rm_mixerbct WHERE machine_id = $mid";
					$result_bct = pg_exec($db,$get_bct);
					if(pg_num_rows($result_bct) > 0) {
						$bct = pg_result($result_bct,'0','bct');
					} else {
						$bct = 0;
					}
					// $bct = $data_sku['bct'];
					if($data_sku['output_counter_type'] == 'CLD') {
						if (!empty($data_sku['sku_end_at']) && $data_sku['sku_end_at'] <= $current_date_time){
							$actual_prod_sku = $this->getActualProduction($m_reg,$data_sku['sku_start_at'],$data_sku['sku_end_at']);
							// echo $m_reg.'----'.$data_sku['sku_start_at'].'----'.$current_date_time.'---'.$data_sku['sku_end_at'].'---'.$actual_prod_sku."</br>";
							$total_shift_min =  abs(strtotime($data_sku['sku_start_at']) - strtotime($current_date_time)) / 60;
						}else {
							$actual_prod_sku = $this->getActualProduction($m_reg,$data_sku['sku_start_at'],$end_shift_time);
							// echo $m_reg.'----'.$data_sku['sku_start_at'].'----'.$end_shift_time.'---'.$actual_prod_sku."</br>";
							$total_shift_min =  abs(strtotime($data_sku['sku_start_at']) - strtotime($end_shift_time)) / 60;
						}
						$get_sku_rpm = "SELECT rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $data_sku[sku_id]";
						$exe_get_sku_rpm = pg_query($db,$get_sku_rpm);
						if(pg_num_rows($exe_get_sku_rpm) > 0) {
							while ($final_rpm_data = pg_fetch_array($exe_get_sku_rpm)) {
								$final_rpm = $final_rpm_data['rpm'];
								// OEE cal.
								$total_production = $actual_prod_sku*$data_sku['unit_per_cld']; 
								$target_production = $total_shift_min*$final_rpm;
								$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : ''; 
								$oee_total += ceil($get_oee);
								$prod_total += $total_production;

							}
						} else {
							$get_oee = 0;
							$oee_total += $get_oee;
							$prod_total += 0;
						}
					} else {
						if(!empty($data_sku['sku_end_at'])) {
								$currMixDuration = abs(strtotime($data_sku['sku_start_at']) - strtotime($data_sku['sku_end_at'])) / 60;
								if($current_shift == 'M') { 
									$setTime = $shift_min[0];
								} else if($current_shift == 'N') {
									$setTime = $shift_min[0]*2;
								} else {
									$setTime = $shift_min[0]*3;
								}
								$totalMixDuration = $setTime;
								if($bct > 0) { 
									$get_batches_made = $currMixDuration/$bct;
									$get_target_batches = $totalMixDuration/$bct;
									$oee_total = ceil(($get_batches_made/$get_target_batches)*100);
									$prod_total = $get_batches_made;
									$actual_prod_sku = ceil($get_batches_made);
								} else {
									$actual_prod_sku = 0;
								}
						} else {
								$currMixDuration = abs(strtotime($data_sku['sku_start_at']) - strtotime($current_date_time)) / 60;
								if($current_shift == 'M') { 
									$setTime = $shift_min[0];
								} else if($current_shift == 'N') {
									$setTime = $shift_min[0]*2;
								} else {
									$setTime = $shift_min[0]*3;
								}
								$totalMixDuration = $setTime;
								// echo $current_shift.'--'.$shift_min[0].'---'.$totalMixDuration."</br>";
						    	if($bct > 0) { 
									$get_batches_made = $currMixDuration/$bct;
									$get_target_batches = $totalMixDuration/$bct;
									$oee_total = ceil(($get_batches_made/$get_target_batches)*100);
									$prod_total = $get_batches_made;
									$actual_prod_sku = ceil($get_batches_made);
								} else {
									$actual_prod_sku = 0;
								}
						}
					}
					if($data_sku['output_counter_type'] == 'CLD') {
						$filter = 3;
					} else {
						$filter = 4;
					}
					// change production to tons 
						if($data_sku['pack_size_unit'] == 'KG' || $data_sku['pack_size_unit'] == 'kg' || $data_sku['pack_size_unit'] == 'Kg' || $data_sku['pack_size_unit'] == 'kG') { 
							$cal_prod = number_format($prod_total/1000 , $filter, '.', ',');
						} else if($data_sku['pack_size_unit'] == 'GM' || $data_sku['pack_size_unit'] == 'gm' || $data_sku['pack_size_unit'] == 'Gm' || $data_sku['pack_size_unit'] == 'gM') { 
							$cal_prod = number_format($prod_total/1000000 , $filter, '.', ',');
						} else if($data_sku['pack_size_unit'] == 'ML' || $data_sku['pack_size_unit'] == 'ml' || $data_sku['pack_size_unit'] == 'Ml' || $data_sku['pack_size_unit'] == 'mL') { 
							$cal_prod = number_format($prod_total*0.000001 , $filter, '.', ',');
						} else if($data_sku['pack_size_unit'] == 'LTR' || $data_sku['pack_size_unit'] == 'ltr' || $data_sku['pack_size_unit'] == 'Ltr' || $data_sku['pack_size_unit'] == 'lTr') { 
							$cal_prod = number_format($prod_total*0.00098200557276378 , $filter, '.', ',');
						}
						// echo "</br>".$data_sku['sku_start_at'] .'<='.$current_date_time.' && '.$data_sku['sku_end_at'].' || '.$data_sku['sku_start_at'].' < '.$current_date_time.' && '.$data_sku['sku_end_at'].' > '.$current_date_time;

						// echo "++++".$oee_total.'++++'.$j.' == '.$count."</br>";
						if($j == $count) {
							$f_oee = $oee_total;
						} else {
							$f_oee = 0;
						}

					if( ($data_sku['sku_start_at'] <= $current_date_time && empty($data_sku['sku_end_at']) ) 
						|| ($data_sku['sku_start_at'] < $current_date_time && $data_sku['sku_end_at'] > $current_date_time) ) {
						
						$current_array[] = array(
							'output_counter_type' => $data_sku['output_counter_type'],
							'sku_code' => $data_sku['sku_code'],
							'sku_id' => $data_sku['sku_id'],
							'pack_size' => $data_sku['pack_size'],
							'pack_size_unit' => $data_sku['pack_size_unit'],
							'unit_per_cld' => $data_sku['unit_per_cld'],
							'date' => '',
							'cld' => $actual_prod_sku ,
							'bct' => $bct > 0 ? $bct : 1,
							'from_date' => '' , 
							'to_date' => '' ,
							'total_prod' => $cal_prod == '0.000' ? 0 : $cal_prod > 0.9 ? ceil($cal_prod) : $cal_prod,
							'oee' => $oee_total,
							'sku_type' => 'Current',
							'current_shift' => $current_shift,
						);
					} else {
						$current_array_prev[] = array(
							'output_counter_type' => $data_sku['output_counter_type'],
							'sku_code' => $data_sku['sku_code'],
							'sku_id' => $data_sku['sku_id'],
							'pack_size' => $data_sku['pack_size'],
							'pack_size_unit' => $data_sku['pack_size_unit'],
							'unit_per_cld' => $data_sku['unit_per_cld'],
							'date' => '',
							'cld' => '',
							'bct' => $bct,
							'from_date' => $data_sku['sku_start_at'] , 
							'to_date' => $data_sku['sku_end_at'] ,
							'total_prod' => $cal_prod == '0.000' ? 0 : $cal_prod > 0.9 ? ceil($cal_prod) : $cal_prod,
							'oee' => $oee_total,
							'sku_type' => 'Previous',
							'current_shift' => '',
						);
					}
				}
			}
// echo $f_oee;
//*
//* New Code ****************************************Startssssssssssss*****************************************************
//*
//*
// // ----------------FOR PRODUCTION GRAPH---------------------------------
	$get_machine_type = "SELECT rm_machinetypes.output_counter_type FROM rm_machines LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id WHERE rm_machines.id = $mid ";
	$get_machine_type_exe = pg_query($db,$get_machine_type);
	$output_counter_type = pg_result($get_machine_type_exe,'0','output_counter_type');
	$check_mreg = substr($m_reg, 0, 3);
	$again_array = [];
	$get_all_array = [];
	$brk_var_test = 0;
	if($output_counter_type == 'CLD' && $check_mreg != 'VIM') { 
		$graph_show = 'cld';
		// if($output_counter_type == 'CLD' && $check_mreg != 'VIM') { 
			$key_val = '';
			foreach($total_array as $key=>$testarray) {
				if(date('Y-m-d H:m:i',strtotime($testarray['from_date'])) < $op_logintime && $op_logintime < date('Y-m-d H:m:i',strtotime($testarray['to_date'])) ) {
					$key_val = $key;
				}
			}
			if($key_val > 0) {
				for($i=0;$i < $key_val;$i++){
					unset($total_array[$i]);
				}
			} else if($key_val == 0){
				for($i=0;$i <= $key_val;$i++){
					unset($total_array[$i]);
				}
			}
			
			// $get_all_array[] = array ('Y','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X');
			$get_all_array[] = array ('Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration');
			$again_array[] = array('Time Intervals','Production','BreakDown','Re-Production');
			foreach($total_array as $getInt) {
				$get_check_int = '';
				$get_check_int_new = '';
				if($getInt['interval'] > 0) {
					$graph_interval = ltrim($getInt['interval'], '0');
				} else {
					$graph_interval = '0';
				}

					$new_from = $getInt['from_date'];
					$new_to = $getInt['to_date'];
					// get 10 mint interval 
					$start = new DateTime($new_from);
					$end = new DateTime($new_to);
					$start_time = $start->format('H:i'); // Get time Format in Hour and minutes
					$end_time = $end->format('H:i');
					$i=0;
					$interval = 10;
					while(strtotime($start_time) <= strtotime($end_time)){
						$start = $start_time;
						$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
						$start_time = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
						$i++;
						if(strtotime($start_time) <= strtotime($end_time)){
							$time[$i]['start'] = $start;
							$time[$i]['end'] = $end;
						}
					}
					$i = 0;
					$brk_var = 0;
					$act_prod = 0;
					foreach($time as $f_int) {
						
						$i++;
						$cur_date = date('Y-m-d');
						$f_str = date('Y-m-d').' '.$f_int['start'];
						$f_end = date('Y-m-d').' '.$f_int['end'];
						// echo '----'.$f_str.'------'.$f_end.'----'."</br>";
						$check_var_2 = 0;	
					
						 $check_int_brk_1 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND (date_start_at >= '$f_str' AND date_start_at <= '$f_end') AND date_start_at >= '$f_str' AND date_end_at >= '$f_end' "; 
						$check_int_brk_qry_1 = pg_query($db,$check_int_brk_1); 
						if(pg_num_rows($check_int_brk_qry_1) > 0) {
							$check_var_2 = 1;	
							$int_brk_str_1 = pg_result($check_int_brk_qry_1,'0','date_start_at');
							$brk_prev_1 = abs(strtotime($f_end) - strtotime($int_brk_str_1)) / 60;
							$brk_var = floor($brk_prev_1); 
							$brk_var_get = floor($brk_prev_1);
							$act_prod = 10 - $brk_var;
						} 

						 $check_int_brk_2 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND  date_start_at <= '$f_str' AND date_start_at <= '$f_end' AND date_end_at >= '$f_end'"; 
						$check_int_brk_qry_2 = pg_query($db,$check_int_brk_2);
						if(pg_num_rows($check_int_brk_qry_2) > 0) {
							
							$int_brk_str_2 = pg_result($check_int_brk_qry_2,'0','date_start_at');
							$brk_prev_2 = abs(strtotime($f_end) - strtotime($int_brk_str_2)) / 60;
							$brk_var = 10;
							$act_prod = 0;
						} 

						$check_int_brk_3 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND  (date_end_at > '$f_str' AND date_end_at <= '$f_end') AND date_start_at <= '$f_str' AND date_end_at <= '$f_end' "; 
						$check_int_brk_qry_3 = pg_query($db,$check_int_brk_3);
						if(pg_num_rows($check_int_brk_qry_3) > 0) {
							$int_brk_end_3 = pg_result($check_int_brk_qry_3,'0','date_end_at');
							$brk_prev_3 = abs(strtotime($int_brk_end_3) - strtotime($f_str)) / 60;
							if($check_var_2 = 0) { 
								$brk_var = floor($brk_prev_3);
								$act_prod = 10 - $brk_var;
							} else {
								$brk_var = floor($brk_prev_3) + $brk_var_get;
								$act_prod = 10 - $brk_var;
							}
						} 
					
					$get_sku_int = "SELECT rm_opskudata.*,rm_skus.unit_per_cld FROM rm_opskudata join rm_skus ON rm_skus.id = rm_opskudata.sku_id WHERE machine_id = $mid AND sku_start_at::date = '$cur_date'"; 
						$get_sku_int_qry = pg_query($db,$get_sku_int);
						if(pg_num_rows($get_sku_int_qry) > 0) {
							while ($get_sku_int_data = pg_fetch_array($get_sku_int_qry)) {
								$chck_sku_str = $get_sku_int_data['sku_start_at'];
								$chck_sku_end = $get_sku_int_data['sku_end_at'];
								// $brk_var = 0;
								// echo '******'.$chck_sku_str.' <= '.$f_str.'----'.$chck_sku_end.'  >=  '.$f_end.'******';
								// if(($chck_sku_str <= $f_str.':00' && empty($chck_sku_end)) || ($chck_sku_str <= $f_str.':00' && $chck_sku_end >= $f_end.':00')) {
									$chck_sku = $get_sku_int_data['sku_id'];
									$chck_unit_per_cld = $get_sku_int_data['unit_per_cld'];
								$get_sku_rpm = "SELECT rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $chck_sku";
									$exe_get_sku_rpm = pg_query($db,$get_sku_rpm);
									$get_rpm_int = pg_result($exe_get_sku_rpm,'0','rpm');
									// OEE cal.
									$act_int_prod = $this->getActualProduction($m_reg,$f_str,$f_end);
									$total_production = $act_int_prod*$chck_unit_per_cld; 
									$target_production = 10*$get_rpm_int;
									$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
									$oee_total = ceil($get_oee);
									if($brk_var > 0) { 
										$f_act_p = $act_prod;
									} else {
										$f_act_p = 10;
									}
									if($oee_total >= 85) {
										$get_check_int = '0,0,'.$f_act_p.','.(int)$brk_var.',';
									}else if($oee_total < 85 && $oee_total >= 75) {
										$get_check_int = '0,'.$f_act_p.',0,'.(int)$brk_var.',';
									}else {
										// if($oee_total > 0) {
											$get_check_int = $f_act_p.',0,0,'.(int)$brk_var.',';
										// } else {
											// $get_check_int = $f_act_p.',0,0,0,';
										// }
									} 
								// } else {
								// 	$get_check_int = '10,0,0,0,';
								// }
							}
						} else {
							$get_check_int = '10,0,0,0,';
						}

						// echo $brk_var.'-----'.$act_prod."</br>";
						$get_check_int_new .= $get_check_int;
						if($getInt['to_date'] < $current_date_time) {
							if($i == 6) {
								$add_graph = ($graph_interval - 1).'-'.$graph_interval;
								// $add_date = date('d',strtotime($new_from)).'/'.$add_graph; 
								$add_date = date('d',strtotime($new_from)).'/'.$add_graph."\r\n".' Production:'.$total_production.'/'.$target_production; 
								$new_add_string = $get_check_int_new;
								$get_string = rtrim($new_add_string, ',');
								$sorted_array = array_map('intval', explode(',', $get_string));
								array_unshift($sorted_array,$add_date);
								$get_all_array[]  = $sorted_array;
							}
						}
						$brk_var = 0;
						$act_prod = 0;
					}
					
		// 			// ----------------FOR PRODUCTION GRAPH ENDS HERE -------------------
			}
	} else if($check_mreg == 'VIM') { 
		$graph_show = 'vim';
			$key_val = '';
			foreach($total_array_new as $key=>$testarray) {
				if(date('Y-m-d H:m:i',strtotime($testarray['from_date'])) < $op_logintime && $op_logintime < date('Y-m-d H:m:i',strtotime($testarray['to_date'])) ) {
					$key_val = $key;
				}
			}
			if($key_val > 0) {
				for($i=0;$i < $key_val;$i++){
					unset($total_array_new[$i]);
				}
			} else if($key_val == 0){
				for($i=0;$i <= $key_val;$i++){
					unset($total_array_new[$i]);
				}
			}
			
			$get_all_array[] = array ('Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration');
			// $get_all_array[] = array ('Y','A','B','C','D','E','F','G');
			$again_array[] = array('Time Intervals','Production','BreakDown','Re-Production');
			foreach($total_array_new as $getInt) {
				// echo "<pre>";
				// print_r($getInt);
				// echo "</pre>";
				
				$get_check_int = '';
				$get_check_int_new = '';
				if($getInt['interval'] > 0) {
					$graph_interval = ltrim($getInt['interval'], '0');
				} else {
					$graph_interval = '0';
				}

				//echo "<pre>";	print_r($getInt); 
					$new_from = $getInt['from_date'];
					$new_to = $getInt['to_date'];
					$cur_date = date('Y-m-d');

					//echo '----'.$new_from.'------'.$new_to.'+++++++++++'."</br>";
					
					// get 10 mint interval 
					$start = new DateTime($new_from);
					$end = new DateTime($new_to);
					$start_time = $start->format('H:i'); // Get time Format in Hour and minutes
					$end_time = $end->format('H:i');
					$i=0;
					$interval = 10;
					while(strtotime($start_time) <= strtotime($end_time)){
						$start = $start_time;
						$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
						$start_time = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
						$i++;
						if(strtotime($start_time) <= strtotime($end_time)){
							$time[$i]['start'] = $start;
							$time[$i]['end'] = $end;
						}
					}
					$i = 0;
					
					 $check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at >= '$new_from' AND date_end_at <= '$new_to' "; // between case 
					$check_int_brk_qry = pg_query($db,$check_int_brk);
						if(pg_num_rows($check_int_brk_qry) > 0) {
							while ($check_data = pg_fetch_array($check_int_brk_qry)) {

								 $int_brk_str = $check_data['date_start_at'];
								 $int_brk_end = $check_data['date_end_at']; 
								 
								 $get_brk_var = abs(strtotime($int_brk_end) - strtotime($int_brk_str)) / 60;
								 
								 $brk_var_test += ceil($get_brk_var);
								 $check_date = $int_brk_str;
								 // echo $int_brk_end.'----'.$int_brk_str.'----'.$brk_var_test."</br>";
							}
							 $check_int_brk_again = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at <= '$new_from' AND date_end_at >= '$new_from' AND date_end_at <= '$new_to' ";
							    $check_int_brk_ag_qry = pg_query($db,$check_int_brk_again);
								if(pg_num_rows($check_int_brk_ag_qry) > 0) {
									$int_brk_end = pg_result($check_int_brk_ag_qry,'0','date_end_at');
									
									$brk_var_again = abs(strtotime($int_brk_end) - strtotime($new_from)) / 60; 

									// echo $int_brk_end.'----'.$new_from.'-----'.$brk_var_test.' + '.$brk_var_again."</br>";
									$brk_var = ceil($brk_var_test+$brk_var_again);
								} else {
									$brk_var = 0;
								}
							$brk_var_test = 0;


							
						} else {
							$check = 0;
							 $check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at >= '$new_from' AND date_start_at <= '$new_to' AND date_end_at >= '$new_to' "; // right side big case  
							 $check_int_brk_qry = pg_query($db,$check_int_brk);
							if(pg_num_rows($check_int_brk_qry) > 0) {
								$check = 1;
								$int_brk_str = pg_result($check_int_brk_qry,'0','date_start_at');
								$check_date = $int_brk_str;
								$brk_var = ceil(abs(strtotime($new_to) - strtotime($int_brk_str)) / 60);
								// echo $new_to.'----'.$int_brk_str.'-----'.$brk_var."</br>";
							} else {
								$brk_var = 0;
							}
							if($check == 0) {
								 $check_int_brk_again = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_end_at >= '$new_from' AND date_end_at <= '$new_to' ";
								 $check_int_brk_ag_qry = pg_query($db,$check_int_brk_again);
								if(pg_num_rows($check_int_brk_ag_qry) > 0) {
									$int_brk_str = pg_result($check_int_brk_ag_qry,'0','date_start_at');
									$check_date = $int_brk_str;
									$brk_var = ceil(abs(strtotime($int_brk_str) - strtotime($new_from)) / 60);
									// echo $int_brk_str.'----'.$new_from.'-----'.$brk_var."</br>";
								} else {
									$brk_var = 0;
								}
							}

							
						}

						// sku data 
								$get_sku_int = "SELECT rm_opskudata.*,rm_skus.unit_per_cld FROM rm_opskudata join rm_skus ON rm_skus.id = rm_opskudata.sku_id WHERE machine_id = $mid AND sku_start_at::date = '$cur_date'"; 
									$get_sku_int_qry = pg_query($db,$get_sku_int);
									if(pg_num_rows($get_sku_int_qry) > 0) {
										while ($get_sku_int_data = pg_fetch_array($get_sku_int_qry)) {
											 $chck_sku_str = $get_sku_int_data['sku_start_at'];
											 $chck_sku_end = $get_sku_int_data['sku_end_at']; 
			// echo "brk var : ".$brk_var."</br>";
											// echo '******'.$chck_sku_str.'--'.$new_from.'----'.$chck_sku_end.'----'.$new_to.'******'."</br>";
											if(($chck_sku_str <= $new_from.':00' && empty($chck_sku_end)) ||  ($chck_sku_str <= $new_from.':00' && $chck_sku_end <= $new_to.':00') || ($chck_sku_str >= $new_from.':00' && $chck_sku_end >= $new_to.':00')) {
												$chck_sku = $get_sku_int_data['sku_id'];
												$chck_unit_per_cld = $get_sku_int_data['unit_per_cld'];
											    $get_sku_rpm = "SELECT rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $chck_sku";
												$exe_get_sku_rpm = pg_query($db,$get_sku_rpm);
												$get_rpm_int = pg_result($exe_get_sku_rpm,'0','rpm');
												// OEE cal.
												
												$act_int_prod = $this->getActualProduction($m_reg,$new_from,$new_to);
												// echo '******'.$m_reg.'--'.$new_from.'----'.$new_to."++++++".$act_int_prod."</br>";
												$total_production = $act_int_prod*$chck_unit_per_cld; 
												$target_production = 120*$get_rpm_int;
												$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
												$oee_total = ceil($get_oee);

												if($brk_var == 0) {
													$prod_val = 120;
												} else {
													$prod_val = 120 - $brk_var;
												}

												if($oee_total >= 85) {
													if($check_date > $new_from) {
														$get_check_int = '0,0,'.$prod_val.','.(int)$brk_var.',0,0,0,';
													} else {
														$get_check_int = '0,0,0,'.(int)$brk_var.',0,0,'.$prod_val.',';
													}
													
												}else if($oee_total < 85 && $oee_total >= 75) {
													if($check_date > $new_from) {
														$get_check_int = '0,'.$prod_val.',0,'.(int)$brk_var.',0,0,0,';
													} else {
														$get_check_int = '0,0,0,'.(int)$brk_var.',0,'.$prod_val.',0,';
													}
												}else {
													// if($check_date > $new_from) {
														if($oee_total > 0) {
															$get_check_int = $prod_val.',0,0,'.(int)$brk_var.',0,0,0,';
														} else {
															$get_check_int = '120,0,0,0,0,0,0,';
														}
													// } else {
													// 	if($oee_total > 0) {
													// 		$get_check_int = '0,0,0,'.(int)$brk_var.','.$prod_val.',0,0,';
													// 	} else {
													// 		$get_check_int = '120,0,0,0,0,0,0,';
													// 	}
													// }
												} 
											} else {
												$get_check_int = '120,0,0,0,0,0,0,';
											}
										}
									} else {
										$get_check_int = '120,0,0,0,0,0,0,';
									}
									// $get_check_int_new .= $get_check_int;
									if($getInt['to_date'] < $current_date_time) {
										
									// 	if($i == 6) {
											$add_graph = ($graph_interval - 2).'-'.$graph_interval;
											$add_date = 'Date & Time: '.date('d',strtotime($new_from)).'/'.$add_graph."\r\n".' Production:'.$total_production.'/'.$target_production; 
											$new_add_string = $get_check_int;
											$get_string = rtrim($new_add_string, ',');
											$sorted_array = array_map('intval', explode(',', $get_string));
											array_unshift($sorted_array,$add_date);
											$get_all_array[]  = $sorted_array;
									// 	}
									}
							// sku data ends 
					// echo "<br/>";
					// echo $total_production; 
					// echo "<br/>";
					// echo $target_production;
					// echo "<br/>";
		// 			// ----------------FOR PRODUCTION GRAPH ENDS HERE -------------------
			}
	} else if($output_counter_type != 'CLD' && $check_mreg != 'VIM') {
		$graph_show = 'batch';
		// if($output_counter_type == 'CLD' && $check_mreg != 'VIM') { 
			$key_val = '';
			foreach($total_array as $key=>$testarray) {
				if(date('Y-m-d H:m:i',strtotime($testarray['from_date'])) < $op_logintime && $op_logintime < date('Y-m-d H:m:i',strtotime($testarray['to_date'])) ) {
					$key_val = $key;
				}
			}
			if($key_val > 0) {
				for($i=0;$i < $key_val;$i++){
					unset($total_array[$i]);
				}
			} else if($key_val == 0){
				for($i=0;$i <= $key_val;$i++){
					unset($total_array[$i]);
				}
			}
			
			$get_all_array[] = array ('Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration');
			// $get_all_array[] = array ('Y','A','B','C','D','E','F','G','H');
			$again_array[] = array('Time Intervals','Production','BreakDown','Re-Production');

			// echo "<pre>";
			// print_r($total_array);
			// die;
			foreach($total_array as $getInt) {
				$get_check_int = '';
				$get_check_int_new = '';
				if($getInt['interval'] > 0) {
					$graph_interval = ltrim($getInt['interval'], '0');
				} else {
					$graph_interval = '0';
				}

			//echo "<pre>";	print_r($getInt); 
					$new_from = $getInt['from_date'];
					$new_to = $getInt['to_date'];
					// echo '----'.$new_from.'------'.$new_to.'+++++++++++'."</br>";
					
					// get 10 mint interval 
					$start = new DateTime($new_from);
					$end = new DateTime($new_to);
					$start_time = $start->format('H:i'); // Get time Format in Hour and minutes
					$end_time = $end->format('H:i');
					$i=0;
					$interval = 30;
					while(strtotime($start_time) <= strtotime($end_time)){
						$start = $start_time;
						$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
						$start_time = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
						$i++;
						if(strtotime($start_time) <= strtotime($end_time)){
							$time[$i]['start'] = $start;
							$time[$i]['end'] = $end;
						}
					}
					$i = 0;
					$act_prod = 0;
					$brk_var = 0;
					foreach($time as $f_int) {
						$chck_2 = 0;
						$chck_ag = 0;
						$i++;
						$cur_date = date('Y-m-d');
						$f_str = date('Y-m-d').' '.$f_int['start'];
						$f_end = date('Y-m-d').' '.$f_int['end'];
						// echo '----'.$f_str.'------'.$f_end.'+++++++++++'."</br>";

						 $check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at >= '$f_str' AND date_end_at <= '$f_end' "; // between case 
						$check_int_brk_qry = pg_query($db,$check_int_brk);
						if(pg_num_rows($check_int_brk_qry) > 0) {
							// echo '1111';
							$int_brk_str = pg_result($check_int_brk_qry,'0','date_start_at');
							$int_brk_end = pg_result($check_int_brk_qry,'0','date_end_at');
							$brk_prev = abs(strtotime($int_brk_end) - strtotime($int_brk_str)) / 60;
							$brk_var = floor($brk_prev);
							$act_prod = 30 - $brk_var ;
						} 
						$check_int_brk_2 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND (date_start_at >= '$f_str' AND date_start_at <= '$f_end')  AND date_end_at >= '$f_end' "; // between case 
						$check_int_brk_qry_2 = pg_query($db,$check_int_brk_2);
						if(pg_num_rows($check_int_brk_qry_2) > 0) {
							$chck_2 = 1;
							$int_brk_str = pg_result($check_int_brk_qry_2,'0','date_start_at');
							$int_brk_end = pg_result($check_int_brk_qry_2,'0','date_end_at');
							$brk_prev = abs(strtotime($f_end) - strtotime($int_brk_str)) / 60;
							$brk_var = floor($brk_prev);
							$act_prod = 30 - $brk_var ;
						} 

						 $check_int_brk_1 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at <= '$f_str' AND date_end_at >= '$f_str' AND  date_end_at <= '$f_end'"; // right side big case  
						$check_int_brk_qry_1 = pg_query($db,$check_int_brk_1);
						if(pg_num_rows($check_int_brk_qry_1) > 0) {
							
							$int_brk_end = pg_result($check_int_brk_qry_1,'0','date_end_at');
							$brk_prev = abs(strtotime($f_str) - strtotime($int_brk_end)) / 60;
							if($chck_2 = 0) {
								$brk_var = floor($brk_prev);
								$act_prod = 30 - $brk_var ;
							} else {
								$chck_ag = 1;
								$brk_var = floor($brk_prev);
								$act_prod = 30 - $brk_var ;
							}
						}
						// echo "</br>";
					// $check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at <= '$f_str' AND date_end_at >= '$f_end' "; // between case 
					// 	$check_int_brk_qry = pg_query($db,$check_int_brk);
					// 	if(pg_num_rows($check_int_brk_qry) > 0) {
					// 		$brk_var = 30;
					// 	} else {
					// $check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at <= '$f_str' AND date_end_at <= '$f_end' "; // right side big case  
					// 		$check_int_brk_qry = pg_query($db,$check_int_brk);
					// 		if(pg_num_rows($check_int_brk_qry) > 0) {
					// 			$int_brk_end = pg_result($check_int_brk_qry,'0','date_end_at');
					// 			$brk_prev = abs(strtotime($f_end) - strtotime($int_brk_end)) / 60;
					// 			// again situation 
					// 		$check_int_brk_again = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at <= '$f_str' AND date_end_at >= '$f_end' "; // left side case 
					// 			$check_int_brk_qry_again = pg_query($db,$check_int_brk_again);
					// 			if(pg_num_rows($check_int_brk_qry_again) > 0) {
					// 				$int_brk_str = pg_result($check_int_brk_qry,'0','date_start_at');
					// 				$brk_prev_again = abs(strtotime($f_str) - strtotime($int_brk_str)) / 60;
					// 				$brk_var = 30 - $brk_prev + $brk_prev_again;
					// 			} else {
					// 				$brk_var = 30 - $brk_prev;
					// 			}
					// 		} else {
					// 			$brk_var = 0;
					// 		}
					// 	}
						// echo $brk_var.'----'.$act_prod.'</br>';
						
					$get_sku_int = "SELECT rm_opskudata.*,rm_skus.unit_per_cld FROM rm_opskudata join rm_skus ON rm_skus.id = rm_opskudata.sku_id WHERE machine_id = $mid AND sku_start_at::date = '$cur_date'"; 
						$get_sku_int_qry = pg_query($db,$get_sku_int);
						if(pg_num_rows($get_sku_int_qry) > 0) {
							while ($get_sku_int_data = pg_fetch_array($get_sku_int_qry)) {
								$chck_sku_str = $get_sku_int_data['sku_start_at'];
								$chck_sku_end = $get_sku_int_data['sku_end_at'];

								// echo '******'.$chck_sku_str.'--'.$f_str.'----'.$chck_sku_end.'----'.$f_end.'******';
								// if(($chck_sku_str <= $f_str.':00' && empty($chck_sku_end)) || ($chck_sku_str <= $f_str.':00' && $chck_sku_end >= $f_end.':00')) {
									$chck_sku = $get_sku_int_data['sku_id'];
									$chck_unit_per_cld = $get_sku_int_data['unit_per_cld'];
									// OEE cal.
									// $act_int_prod = $this->getActualProduction($m_reg,$f_str,$f_end);
									// echo '******'.$m_reg.'--'.$f_str.'----'.$f_end.'----'.$act_int_prod.'******'."</br>";
									// $total_production = $act_int_prod*$chck_unit_per_cld; 
									// $target_production = 10*$get_rpm_int;
									// $get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
									// $oee_total = ceil($get_oee);

									$currMixDuration = abs(strtotime($f_str) - strtotime($current_date_time)) / 60;
									// echo $f_str.'-------'.$current_date_time.'---'."</br>";
								    $totalMixDuration = 30*60;
								    if($bct > 0) {
										$get_batches_made = $currMixDuration/$bct;
										$get_target_batches = $totalMixDuration/$bct;
										$oee_total = ceil(($get_batches_made/$get_target_batches)*100);
									} else {
										$oee_total = 0;
									}
									// $prod_total += $get_batches_made;
									// $actual_prod_sku = ceil($get_batches_made);
									if($brk_var > 0) { 
										$f_act_p = $act_prod;
									} else {
										$f_act_p = 30;
									}
									if($oee_total >= 85) {
										$get_check_int = '0,0,'.$f_act_p.','.(int)$brk_var.',';
									}else if($oee_total < 85 && $oee_total >= 75) {
										$get_check_int = '0,'.$f_act_p.',0,'.(int)$brk_var.',';
									}else {
										if($oee_total > 0) {
											$get_check_int = $f_act_p.',0,0,'.(int)$brk_var.',';
										} else {
											$get_check_int = $f_act_p.',0,0,0,';
										}
									} 
								// } else {
								// 	$get_check_int = '0,0,0,0,';
								// }
							}
						} else {
							$get_check_int = '30,0,0,0,';
						}
						$act_prod = 0;
						$brk_var = 0;

						$get_check_int_new .= $get_check_int;
						if($getInt['to_date'] < $current_date_time) {
							if($i == 2) {
								$add_graph = ($graph_interval - 1).'-'.$graph_interval;
								// $add_date = date('d',strtotime($new_from)).'/'.$add_graph; 
								$add_date = 'Date & Time: '.date('d',strtotime($new_from)).'/'.$add_graph."\r\n".' Production:'.$total_production.'/'.$target_production; 
								$new_add_string = $get_check_int_new;
								$get_string = rtrim($new_add_string, ',');
								$sorted_array = array_map('intval', explode(',', $get_string));
								array_unshift($sorted_array,$add_date);
								$get_all_array[]  = $sorted_array;
							}
						}
					}
		// 			// ----------------FOR PRODUCTION GRAPH ENDS HERE -------------------
			}
	}
// echo "<pre>";
// print_r($get_all_array);
// echo "</pre>";
	// die;
//*
//* New Code *****************************************ENdssssssssssss****************************************************
//*
//*

	// echo 'final_array------'."<pre>";
	// print_r($final_array);
	
// die;
		$columns = array_column($final_array, 'from_date');
		array_multisort($columns, SORT_ASC, $final_array);
		// echo "<pre>";
		// print_r($final_array);
		// die;
		return view('show_graph',[
			'data' => $final_array,
			'working_time' => $working_time,
			'working_time2' => $working_time2,
			'm_reg' => $m_reg,
			'current_array' => count($current_array) > 0 ? $current_array[0] : $current_array,
			'prev_array' => $current_array_prev,
			'production' => $array_prod,
			'brk' => $array_brk,
			'left' => $array_prod_left,
			'no_prod' => $array_nothing,
			'plant_id' => $plant,
			'unit_id' => $unit,
			'cascade_id' => $cascade,
			'machine_id' => $machine,
			'flag' => 'array',
			'again_array' => $get_all_array,
			'check' => 'curr',
			'graph_show' => $graph_show,
			'f_oee' => $f_oee
			]);
	}


	public function showGraphFilter($new_data,$date){
				
				$host = Session::get('host');
				$dbname = Session::get('dbname');
				$user = Session::get('user');
				$pass = Session::get('pass');
				$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
				$new_data = $_GET;
				// $new_data_post = $_POST['txtStartDate'];
				
				
				if(count($new_data) > 0) {
					$machine = !empty($new_data['m']) ? $new_data['m'] :'';
					$plant = !empty($new_data['p']) ? $new_data['p'] : '';
					$unit = !empty($new_data['u']) ? $new_data['u'] : '';
					$cascade = !empty($new_data['c']) ? $new_data['c'] : '';
					// get names 
					$get_m_name = "SELECT machine_name,machine_regno FROM rm_machines WHERE id = $machine";
						$result_m = pg_exec($db,$get_m_name); 
						while ($m_data = pg_fetch_array($result_m)) {
							$send_m_n = $m_data['machine_name'];
							$machine_regno = $m_data['machine_regno'];
						}
				}
				$carbon = Carbon::today(); 
				// $current_date_time =  Carbon::now('Asia/Kolkata')->toDateTimeString();
				$current_date = !empty($date) ? $date : $carbon->format('Y-m-d');
				$working_time = date('d M,Y', strtotime($current_date));
				$working_time2 = $date;
				$next_day = date('Y-m-d', strtotime("+1 day", strtotime($current_date)));
				$current_time =  Carbon::now('Asia/Kolkata')->toTimeString(); 
				$mid = $machine;
				$m_reg = $machine_regno; 
		
				$check_oplogin = "SELECT logintime from rm_oplogins WHERE machine_id = $mid AND logintime::date = '$current_date' ORDER BY id ASC LIMIT 1";
				$result_login = pg_exec($db,$check_oplogin);
				if(pg_num_rows($result_login) > 0) {
					$op_logintime = pg_result($result_login,'0','logintime');
				} else {
					$message = $m_reg.'**'.'Machine was not logged-In by any of the operator on '.date('d M,Y', strtotime($current_date)) ;
					return $message;
				}
				
				// $mid = '88';
				// $m_reg = 'VIM_M2';
					 $qry_1 = "SELECT rm_opskudata.id as main_id,rm_opskudata.sku_start_at,rm_opskudata.sku_id,rm_opskudata.sku_end_at,
										rm_skus.sku_code,rm_skus.sku_name,rm_skus.rpm_default,
										rm_skus.pack_size,rm_skus.pack_size_unit,rm_skus.unit_per_cld,rm_skus.rpm_default as standard_rpm	
							FROM rm_opskudata
							LEFT JOIN rm_skus ON rm_opskudata.sku_id = rm_skus.id 
							WHERE rm_opskudata.machine_id = $mid AND sku_start_at::date = '$current_date' ";
					$result_1 = pg_exec($db,$qry_1);
		
					
					$qry_2 = "SELECT rm_machinetypes.output_counter_type,rm_mixerbct.bct
								FROM rm_machines
								LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id 
								LEFT JOIN rm_mixerbct ON rm_mixerbct.machine_id =  rm_machines.id
								WHERE rm_machines.id =  $mid ";
					$result_2 = pg_exec($db,$qry_2);
					$output_counter_type = pg_result($result_2,'0','output_counter_type');
					$bct = pg_result($result_2,'0','bct');
					
					if(pg_num_rows($result_1) > 0) {
						while ($recData = pg_fetch_array($result_1)) {
							$get_sku_rpm = "SELECT rpm as sku_rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $recData[sku_id]";
							$result_sku_r = pg_exec($db,$get_sku_rpm);
							if(pg_num_rows($result_sku_r) > 0) {
								$sku_rpm = pg_result($result_sku_r,'0','sku_rpm');
							} else {
								$sku_rpm = 0;
							}
							$array[] = array(
									'sku_code' => $recData['sku_code'],
									'standard_rpm' => $recData['standard_rpm'],
									'pack_size' =>  $recData['pack_size'],
									'unit_per_cld' =>  $recData['unit_per_cld'],
									'sku_start_at' =>  $recData['sku_start_at'],
									'sku_end_at' =>  $recData['sku_end_at'],
									'sku_rpm' =>  $sku_rpm,
								);
						}
					}
		
				// get shift str and end time 
				$get_shift_start = "SELECT shift_start_at FROM rm_shifts WHERE shift_code = 'M'";
					$result_str = pg_exec($db,$get_shift_start); 
					$shift_start_at = $current_date.' '.pg_result($result_str,'0','shift_start_at');
				$get_shift_end = "SELECT shift_end_at FROM rm_shifts WHERE shift_code = 'N'";
					$result_end = pg_exec($db,$get_shift_end); 
					$shift_end_at = $next_day.' '.pg_result($result_end,'0','shift_end_at');
				// cal 1 hour intervals
				$ReturnArray = array ();// Define output
				$ReturnArray_1 = array(); 
				$StartTime    = strtotime ($shift_start_at); //Get Timestamp
				$EndTime      = strtotime ($shift_end_at); //Get Timestamp
				$Duration     = 60;    
				$AddMins  = $Duration * 60;
				$late_time = [];
				$start_time_f = [];		
				// get actual shift production
					$check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
									EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_start_at < '$current_time' AND shift_end_at >= '$current_time' ";
					$exe_shift_qry = pg_query($db,$check_shift);
					if(pg_num_rows($exe_shift_qry) > 0) {
						while ($data_2 = pg_fetch_array($exe_shift_qry)) {
		
							if($data_2['shift_part'] > 0) {
								$init = $data_2['shift_part'];
							} else if($data_2['shift_part'] < 0) {
								$init = $data_2['shift_part_new']/2;
							}
							$total_sft_min = floor($init / 60);
							$end_time[] =  $data_2['shift_end_at'];
						$start_time[] =  $data_2['shift_start_at'];
						$current_shift = $data_2['shift_code'];
						}
					} else {
					$check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
						EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_code = 'N' ";
						$exe_shift_qry = pg_query($db,$check_shift);
						if(pg_num_rows($exe_shift_qry) > 0) {
							while ($data_2 = pg_fetch_array($exe_shift_qry)) {
								if($data_2['shift_part'] > 0) {
									$init = $data_2['shift_part'];
								} else if($data_2['shift_part'] < 0) {
									$init = $data_2['shift_part_new']/2;
								}
								$total_sft_min = floor($init / 60);
								$end_time[] = $data_2['shift_end_at'];
								$late_time[]  = $data_2['shift_end_at'];
								$start_time[] =  $data_2['shift_start_at'];
								$current_shift = $data_2['shift_code'];
							}
						}
					}
			
					// get last shift time 
					$check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
									EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_code = 'M' ";
					$exe_shift_qry = pg_query($db,$check_shift);
					if(pg_num_rows($exe_shift_qry) > 0) {
						while ($data_2 = pg_fetch_array($exe_shift_qry)) {
		
							if($data_2['shift_part'] > 0) {
								$init = $data_2['shift_part'];
							} else if($data_2['shift_part'] < 0) {
								$init = $data_2['shift_part_new']/2;
							}
							// $total_sft_min = floor($init / 60);
							// $end_time[] =  $data_2['shift_end_at'];
							$start_time_f[] =  $data_2['shift_start_at'];
							// $current_shift = $data_2['shift_code'];
						}
					} 
					$check_shift_last = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
						EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_code = 'N' ";
						$exe_shift_qry_last = pg_query($db,$check_shift_last);
						if(pg_num_rows($exe_shift_qry_last) > 0) {
							while ($data_2_l = pg_fetch_array($exe_shift_qry_last)) {
								if($data_2_l['shift_part'] > 0) {
									$init = $data_2_l['shift_part'];
								} else if($data_2_l['shift_part'] < 0) {
									$init = $data_2_l['shift_part_new']/2;
								}
								$minutes = floor($init / 60);
								$shift_min[]  = $minutes;
								$late_time[]  = $data_2_l['shift_end_at'];
							}
						}
		
				while ($StartTime <= $EndTime) //Run loop
				{
					$ReturnArray[] = date ("G:i", $StartTime);
					$StartTime += $AddMins; //Endtime check
				}
		
				$total_array = [];
				$final_array = [];
				$current_array = [];
		
		
			
				foreach($ReturnArray as $val) {
					if(date('H:i',strtotime($val)) == '00:00') {
						$from_date = $current_date.' '.date('H:i', strtotime("-1 hour", strtotime($val)));
						$to_date = $next_day.' '.date('H:i',strtotime($val));
						$get_interval = date('H',strtotime($val));
						$get_interval_time = date('H:i',strtotime($val));
						$current_date = $next_day;
					} else {
						$from_date = $current_date.' '.date('H:i', strtotime("-1 hour", strtotime($val)));
						$to_date = $current_date.' '.date('H:i',strtotime($val));
						$get_interval_time = date('H:i',strtotime($val));
						$get_interval = date('H',strtotime($val));
					}
					$actual_production = $this->getActualProduction($m_reg,$from_date,$to_date);
					$total_array[] = array('interval' => $get_interval,'int_time' => $get_interval_time,'date' => $current_date ,'cld' => $actual_production ,'from_date' => $from_date , 'to_date' => $to_date);
				}
		
				// echo "<pre>";
				// print_r($total_array);
				// die;
				$array_prod = [];
				$array_brk = [];
				$array_prod_left = [];
				$array_nothing = [];
				$check_status = 0;
		
			
				foreach($array as $getData) {
					foreach($total_array as $getInt) {

						
						//-----------------------------FOR OEE GRAPH-----------------------------------
								//check current shift 
									$get_shift_time = "SELECT shift_end_at,shift_code FROM rm_shifts 
													WHERE  shift_start_at <= '$getInt[int_time]' AND shift_end_at >= '$getInt[int_time]' ";      
									$result_sft_time = pg_exec($db,$get_shift_time); 
									while ($sft_recData = pg_fetch_array($result_sft_time)) {
										if($sft_recData['shift_code'] == 'E' || $sft_recData['shift_code'] == 'M') {  
											$sku_sft_end_at = $current_date.' '.$sft_recData['shift_end_at'];
										} else if($sft_recData['shift_code'] == 'N') {
											$sku_sft_end_at = $next_day.' '.$sft_recData['shift_end_at'];
										}
									}
									$sku_end_at = !empty($getData['sku_end_at']) ? $getData['sku_end_at'] : $sku_sft_end_at; 
								// End ----------------
								if($getData['sku_start_at'] <= $getInt['to_date'] && $sku_end_at >= $getInt['to_date']) {
									$total_production = $getInt['cld'] * $getData['unit_per_cld'];
									//---------------------OEE--------
										$target_production = (!empty($getData['sku_rpm'])) ? $getData['sku_rpm'] * $Duration : '';
											if($output_counter_type == 'CLD' ) {
												$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
												$oee = ($get_oee != '') ? ceil($get_oee) : 1;
											}else { 
												if($bct > 0) { 
													$totalMixDuration =  1; //HOUR 
													$get_batches_made = $totalMixDuration*60/$bct;
													$get_target_batches = (abs(strtotime($getInt['from_date']) - strtotime($getInt['to_date'])) / 60)/$bct;
													$oee = ceil((( $get_batches_made/$get_target_batches )*100));
												} else {
													$oee = 1;
												}
											}
									// FINAL ARRAY-------------        
		
										if($oee >= 85) {
											$color = 'green';
										} else if($oee < 85 && $oee >= 75) {
											$color = '#b5b503';
										} else {
											$color = 'red';
										}
										$final_array[] = array('interval' => $getInt['interval'],'output_counter_type' => $output_counter_type,'sku_code' => $getData['sku_code'],'pack_size' => $getData['pack_size'],'unit_per_cld' => $getData['unit_per_cld'],'date' => $getInt['date'] ,'cld' => $getInt['cld'] ,'from_date' => $getInt['from_date'] , 'to_date' => $getInt['to_date'] , 'total_prod' => $total_production  ? $total_production : '0' ,'oee' => $oee? $oee : '0' ,'color' => $color);
								
								}		//cal current overall running production 
								
		
					}
					
				} 
			
		
		// // ----------------FOR PRODUCTION GRAPH---------------------------------
			$get_machine_type = "SELECT rm_machinetypes.output_counter_type FROM rm_machines LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id WHERE rm_machines.id = $mid ";
			$get_machine_type_exe = pg_query($db,$get_machine_type);
			$output_counter_type = pg_result($get_machine_type_exe,'0','output_counter_type');
			$check_mreg = substr($m_reg, 0, 3);
			$again_array = [];
			$get_all_array = [];
			$brk_var_test = 0;
			if($output_counter_type == 'CLD' && $check_mreg != 'VIM') { 
				$graph_show = 'cld';
				// if($output_counter_type == 'CLD' && $check_mreg != 'VIM') { 
					$key_val = '';
					foreach($total_array as $key=>$testarray) {
						if(date('Y-m-d H:m:i',strtotime($testarray['from_date'])) < $op_logintime && $op_logintime < date('Y-m-d H:m:i',strtotime($testarray['to_date'])) ) {
							$key_val = $key;
						}
					}
					if($key_val > 0) {
						for($i=0;$i < $key_val;$i++){
							unset($total_array[$i]);
						}
					} else if($key_val == 0){
						for($i=0;$i <= $key_val;$i++){
							unset($total_array[$i]);
						}
					}
					
					$get_all_array[] = array ('Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration');
					// $get_all_array[] = array ('Y','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X');
					$again_array[] = array('Time Intervals','Production','BreakDown','Re-Production');
					foreach($total_array as $getInt) {
						$get_check_int = '';
						$get_check_int_new = '';
						if($getInt['interval'] > 0) {
							$graph_interval = ltrim($getInt['interval'], '0');
						} else {
							$graph_interval = '0';
						}
		
							$new_from = $getInt['from_date'];
							$new_to = $getInt['to_date'];
							// get 10 mint interval 
							$start = new DateTime($new_from);
							$end = new DateTime($new_to);
							$start_time = $start->format('H:i'); // Get time Format in Hour and minutes
							$end_time = $end->format('H:i');
							$i=0;
							$interval = 10;
							while(strtotime($start_time) <= strtotime($end_time)){
								$start = $start_time;
								$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
								$start_time = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
								$i++;
								if(strtotime($start_time) <= strtotime($end_time)){
									$time[$i]['start'] = $start;
									$time[$i]['end'] = $end;
								}
							}
							$i = 0;
							$brk_var = 0;
							$act_prod = 0;
							foreach($time as $f_int) {
								
								$i++;
								$cur_date = date('Y-m-d');
								$f_str = date('Y-m-d').' '.$f_int['start'];
								$f_end = date('Y-m-d').' '.$f_int['end'];
								// echo '----'.$f_str.'------'.$f_end.'----'."</br>";
								$check_var_2 = 0;	
							
								$check_int_brk_1 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND (date_start_at >= '$f_str' AND date_start_at <= '$f_end') AND date_start_at >= '$f_str' AND date_end_at >= '$f_end' "; 
								$check_int_brk_qry_1 = pg_query($db,$check_int_brk_1); 
								if(pg_num_rows($check_int_brk_qry_1) > 0) {
									$check_var_2 = 1;	
									$int_brk_str_1 = pg_result($check_int_brk_qry_1,'0','date_start_at');
									$brk_prev_1 = abs(strtotime($f_end) - strtotime($int_brk_str_1)) / 60;
									$brk_var = floor($brk_prev_1); 
									$brk_var_get = floor($brk_prev_1);
									$act_prod = 10 - $brk_var;
								} 
		
								$check_int_brk_2 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND  date_start_at <= '$f_str' AND date_start_at <= '$f_end' AND date_end_at >= '$f_end'"; 
								$check_int_brk_qry_2 = pg_query($db,$check_int_brk_2);
								if(pg_num_rows($check_int_brk_qry_2) > 0) {
									
									$int_brk_str_2 = pg_result($check_int_brk_qry_2,'0','date_start_at');
									$brk_prev_2 = abs(strtotime($f_end) - strtotime($int_brk_str_2)) / 60;
									$brk_var = 10;
									$act_prod = 0;
								} 
		
								$check_int_brk_3 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND  (date_end_at > '$f_str' AND date_end_at <= '$f_end') AND date_start_at <= '$f_str' AND date_end_at <= '$f_end' "; 
								$check_int_brk_qry_3 = pg_query($db,$check_int_brk_3);
								if(pg_num_rows($check_int_brk_qry_3) > 0) {
									$int_brk_end_3 = pg_result($check_int_brk_qry_3,'0','date_end_at');
									$brk_prev_3 = abs(strtotime($int_brk_end_3) - strtotime($f_str)) / 60;
									if($check_var_2 = 0) { 
										$brk_var = floor($brk_prev_3);
										$act_prod = 10 - $brk_var;
									} else {
										$brk_var = floor($brk_prev_3) + $brk_var_get;
										$act_prod = 10 - $brk_var;
									}
								} 
							
							$get_sku_int = "SELECT rm_opskudata.*,rm_skus.unit_per_cld FROM rm_opskudata join rm_skus ON rm_skus.id = rm_opskudata.sku_id WHERE machine_id = $mid AND sku_start_at::date = '$cur_date'"; 
								$get_sku_int_qry = pg_query($db,$get_sku_int);
								if(pg_num_rows($get_sku_int_qry) > 0) {
									while ($get_sku_int_data = pg_fetch_array($get_sku_int_qry)) {
										$chck_sku_str = $get_sku_int_data['sku_start_at'];
										$chck_sku_end = $get_sku_int_data['sku_end_at'];
										// $brk_var = 0;
										// echo '******'.$chck_sku_str.' <= '.$f_str.'----'.$chck_sku_end.'  >=  '.$f_end.'******';
										// if(($chck_sku_str <= $f_str.':00' && empty($chck_sku_end)) || ($chck_sku_str <= $f_str.':00' && $chck_sku_end >= $f_end.':00')) {
											$chck_sku = $get_sku_int_data['sku_id'];
											$chck_unit_per_cld = $get_sku_int_data['unit_per_cld'];
										$get_sku_rpm = "SELECT rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $chck_sku";
											$exe_get_sku_rpm = pg_query($db,$get_sku_rpm);
											$get_rpm_int = pg_result($exe_get_sku_rpm,'0','rpm');
											// OEE cal.
											$act_int_prod = $this->getActualProduction($m_reg,$f_str,$f_end);
											$total_production = $act_int_prod*$chck_unit_per_cld; 
											$target_production = 10*$get_rpm_int;
											$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
											$oee_total = ceil($get_oee);
											if($brk_var > 0) { 
												$f_act_p = $act_prod;
											} else {
												$f_act_p = 10;
											}
											if($oee_total >= 85) {
												$get_check_int = '0,0,'.$f_act_p.','.(int)$brk_var.',';
											}else if($oee_total < 85 && $oee_total >= 75) {
												$get_check_int = '0,'.$f_act_p.',0,'.(int)$brk_var.',';
											}else {
												// if($oee_total > 0) {
													$get_check_int = $f_act_p.',0,0,'.(int)$brk_var.',';
												// } else {
													// $get_check_int = $f_act_p.',0,0,0,';
												// }
											} 
										// } else {
										// 	$get_check_int = '10,0,0,0,';
										// }
									}
								} else {
									$get_check_int = '10,0,0,0,';
								}
		
								// echo $brk_var.'-----'.$act_prod."</br>";
								$get_check_int_new .= $get_check_int;
								// if($getInt['to_date'] < $current_date_time) {
									if($i == 6) {
										$add_graph = ($graph_interval - 1).'-'.$graph_interval;
										$add_date = date('d',strtotime($new_from)).'/'.$add_graph; 
										$new_add_string = $get_check_int_new;
										$get_string = rtrim($new_add_string, ',');
										$sorted_array = array_map('intval', explode(',', $get_string));
										array_unshift($sorted_array,$add_date);
										$get_all_array[]  = $sorted_array;
									}
								// }
								$brk_var = 0;
								$act_prod = 0;
							}
							
				// 			// ----------------FOR PRODUCTION GRAPH ENDS HERE -------------------
					}
			} else if($check_mreg == 'VIM') { 
				$graph_show = 'vim';
					$key_val = '';
					foreach($total_array_new as $key=>$testarray) {
						if(date('Y-m-d H:m:i',strtotime($testarray['from_date'])) < $op_logintime && $op_logintime < date('Y-m-d H:m:i',strtotime($testarray['to_date'])) ) {
							$key_val = $key;
						}
					}
					if($key_val > 0) {
						for($i=0;$i < $key_val;$i++){
							unset($total_array_new[$i]);
						}
					} else if($key_val == 0){
						for($i=0;$i <= $key_val;$i++){
							unset($total_array_new[$i]);
						}
					}
					
					// $get_all_array[] = array ('Y','A','B','C','D','E','F','G');
					$get_all_array[] = array ('Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration');
					$again_array[] = array('Time Intervals','Production','BreakDown','Re-Production');
					foreach($total_array_new as $getInt) {
						// echo "<pre>";
						// print_r($getInt);
		
						$get_check_int = '';
						$get_check_int_new = '';
						if($getInt['interval'] > 0) {
							$graph_interval = ltrim($getInt['interval'], '0');
						} else {
							$graph_interval = '0';
						}
		
					//echo "<pre>";	print_r($getInt); 
							$new_from = $getInt['from_date'];
							$new_to = $getInt['to_date'];
							$cur_date = date('Y-m-d');
							// echo '----'.$new_from.'------'.$new_to.'+++++++++++'."</br>";
							
							// get 10 mint interval 
							$start = new DateTime($new_from);
							$end = new DateTime($new_to);
							$start_time = $start->format('H:i'); // Get time Format in Hour and minutes
							$end_time = $end->format('H:i');
							$i=0;
							$interval = 10;
							while(strtotime($start_time) <= strtotime($end_time)){
								$start = $start_time;
								$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
								$start_time = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
								$i++;
								if(strtotime($start_time) <= strtotime($end_time)){
									$time[$i]['start'] = $start;
									$time[$i]['end'] = $end;
								}
							}
							$i = 0;
							
							$check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at >= '$new_from' AND date_end_at <= '$new_to' "; // between case 
							$check_int_brk_qry = pg_query($db,$check_int_brk);
								if(pg_num_rows($check_int_brk_qry) > 0) {
									while ($check_data = pg_fetch_array($check_int_brk_qry)) {
		
										$int_brk_str = $check_data['date_start_at'];
										$int_brk_end = $check_data['date_end_at']; 
										
										$get_brk_var = abs(strtotime($int_brk_end) - strtotime($int_brk_str)) / 60;
										
										$brk_var_test += ceil($get_brk_var);
										$check_date = $int_brk_str;
										// echo $int_brk_end.'----'.$int_brk_str.'----'.$brk_var_test."</br>";
									}
									$check_int_brk_again = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at <= '$new_from' AND date_end_at >= '$new_from' AND date_end_at <= '$new_to' ";
										$check_int_brk_ag_qry = pg_query($db,$check_int_brk_again);
										if(pg_num_rows($check_int_brk_ag_qry) > 0) {
											$int_brk_end = pg_result($check_int_brk_ag_qry,'0','date_end_at');
											
											$brk_var_again = abs(strtotime($int_brk_end) - strtotime($new_from)) / 60; 
		
											// echo $int_brk_end.'----'.$new_from.'-----'.$brk_var_test.' + '.$brk_var_again."</br>";
											$brk_var = ceil($brk_var_test+$brk_var_again);
										} else {
											$brk_var = 0;
										}
									$brk_var_test = 0;
		
		
									
								} else {
									$check = 0;
									$check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at >= '$new_from' AND date_start_at <= '$new_to' AND date_end_at >= '$new_to' "; // right side big case  
									$check_int_brk_qry = pg_query($db,$check_int_brk);
									if(pg_num_rows($check_int_brk_qry) > 0) {
										$check = 1;
										$int_brk_str = pg_result($check_int_brk_qry,'0','date_start_at');
										$check_date = $int_brk_str;
										$brk_var = ceil(abs(strtotime($new_to) - strtotime($int_brk_str)) / 60);
										// echo $new_to.'----'.$int_brk_str.'-----'.$brk_var."</br>";
									} else {
										$brk_var = 0;
									}
									if($check == 0) {
										$check_int_brk_again = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_end_at >= '$new_from' AND date_end_at <= '$new_to' ";
										$check_int_brk_ag_qry = pg_query($db,$check_int_brk_again);
										if(pg_num_rows($check_int_brk_ag_qry) > 0) {
											$int_brk_str = pg_result($check_int_brk_ag_qry,'0','date_start_at');
											$check_date = $int_brk_str;
											$brk_var = ceil(abs(strtotime($int_brk_str) - strtotime($new_from)) / 60);
											// echo $int_brk_str.'----'.$new_from.'-----'.$brk_var."</br>";
										} else {
											$brk_var = 0;
										}
									}
		
									
								}
		
								// sku data 
										$get_sku_int = "SELECT rm_opskudata.*,rm_skus.unit_per_cld FROM rm_opskudata join rm_skus ON rm_skus.id = rm_opskudata.sku_id WHERE machine_id = $mid AND sku_start_at::date = '$cur_date'"; 
											$get_sku_int_qry = pg_query($db,$get_sku_int);
											if(pg_num_rows($get_sku_int_qry) > 0) {
												while ($get_sku_int_data = pg_fetch_array($get_sku_int_qry)) {
													$chck_sku_str = $get_sku_int_data['sku_start_at'];
													$chck_sku_end = $get_sku_int_data['sku_end_at']; 
					// echo "brk var : ".$brk_var."</br>";
													// echo '******'.$chck_sku_str.'--'.$new_from.'----'.$chck_sku_end.'----'.$new_to.'******'."</br>";
													if(($chck_sku_str <= $new_from.':00' && empty($chck_sku_end)) ||  ($chck_sku_str <= $new_from.':00' && $chck_sku_end <= $new_to.':00') || ($chck_sku_str >= $new_from.':00' && $chck_sku_end >= $new_to.':00')) {
														$chck_sku = $get_sku_int_data['sku_id'];
														$chck_unit_per_cld = $get_sku_int_data['unit_per_cld'];
														$get_sku_rpm = "SELECT rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $chck_sku";
														$exe_get_sku_rpm = pg_query($db,$get_sku_rpm);
														$get_rpm_int = pg_result($exe_get_sku_rpm,'0','rpm');
														// OEE cal.
														
														$act_int_prod = $this->getActualProduction($m_reg,$new_from,$new_to);
														// echo '******'.$m_reg.'--'.$new_from.'----'.$new_to.'******'.$act_int_prod."</br>";
														$total_production = $act_int_prod*$chck_unit_per_cld; 
														$target_production = 120*$get_rpm_int;
														$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
														$oee_total = ceil($get_oee);
		
														if($brk_var == 0) {
															$prod_val = 120;
														} else {
															$prod_val = 120 - $brk_var;
														}
		
														if($oee_total >= 85) {
															if($check_date > $new_from) {
																$get_check_int = '0,0,'.$prod_val.','.(int)$brk_var.',0,0,0,';
															} else {
																$get_check_int = '0,0,0,'.(int)$brk_var.',0,0,'.$prod_val.',';
															}
															
														}else if($oee_total < 85 && $oee_total >= 75) {
															if($check_date > $new_from) {
																$get_check_int = '0,'.$prod_val.',0,'.(int)$brk_var.',0,0,0,';
															} else {
																$get_check_int = '0,0,0,'.(int)$brk_var.',0,'.$prod_val.',0,';
															}
														}else {
															// if($check_date > $new_from) {
																if($oee_total > 0) {
																	$get_check_int = $prod_val.',0,0,'.(int)$brk_var.',0,0,0,';
																} else {
																	$get_check_int = '120,0,0,0,0,0,0,';
																}
															// } else {
															// 	if($oee_total > 0) {
															// 		$get_check_int = '0,0,0,'.(int)$brk_var.','.$prod_val.',0,0,';
															// 	} else {
															// 		$get_check_int = '120,0,0,0,0,0,0,';
															// 	}
															// }
														} 
													} else {
														$get_check_int = '120,0,0,0,0,0,0,';
													}
												}
											} else {
												$get_check_int = '120,0,0,0,0,0,0,';
											}
											// $get_check_int_new .= $get_check_int;
											// if($getInt['to_date'] < $current_date_time) {
											// 	if($i == 6) {
													$add_graph = ($graph_interval - 2).'-'.$graph_interval;
													$add_date = date('d',strtotime($new_from)).'/'.$add_graph; 
													$new_add_string = $get_check_int;
													$get_string = rtrim($new_add_string, ',');
													$sorted_array = array_map('intval', explode(',', $get_string));
													array_unshift($sorted_array,$add_date);
													$get_all_array[]  = $sorted_array;
											// 	}
											// }
									// sku data ends 
							
				// 			// ----------------FOR PRODUCTION GRAPH ENDS HERE -------------------
					}
			} else if($output_counter_type != 'CLD' && $check_mreg != 'VIM') {
				$graph_show = 'batch';
				// if($output_counter_type == 'CLD' && $check_mreg != 'VIM') { 
					$key_val = '';
					foreach($total_array as $key=>$testarray) {
						if(date('Y-m-d H:m:i',strtotime($testarray['from_date'])) < $op_logintime && $op_logintime < date('Y-m-d H:m:i',strtotime($testarray['to_date'])) ) {
							$key_val = $key;
						}
					}
					if($key_val > 0) {
						for($i=0;$i < $key_val;$i++){
							unset($total_array[$i]);
						}
					} else if($key_val == 0){
						for($i=0;$i <= $key_val;$i++){
							unset($total_array[$i]);
						}
					}
					
					$get_all_array[] = array ('Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration','Duration');
					// $get_all_array[] = array ('Y','A','B','C','D','E','F','G','H');
					$again_array[] = array('Time Intervals','Production','BreakDown','Re-Production');
		
					// echo "<pre>";
					// print_r($total_array);
					// die;
					foreach($total_array as $getInt) {
						$get_check_int = '';
						$get_check_int_new = '';
						if($getInt['interval'] > 0) {
							$graph_interval = ltrim($getInt['interval'], '0');
						} else {
							$graph_interval = '0';
						}
		
					//echo "<pre>";	print_r($getInt); 
							$new_from = $getInt['from_date'];
							$new_to = $getInt['to_date'];
							// echo '----'.$new_from.'------'.$new_to.'+++++++++++'."</br>";
							
							// get 10 mint interval 
							$start = new DateTime($new_from);
							$end = new DateTime($new_to);
							$start_time = $start->format('H:i'); // Get time Format in Hour and minutes
							$end_time = $end->format('H:i');
							$i=0;
							$interval = 30;
							while(strtotime($start_time) <= strtotime($end_time)){
								$start = $start_time;
								$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
								$start_time = date('H:i',strtotime('+'.$interval.' minutes',strtotime($start_time)));
								$i++;
								if(strtotime($start_time) <= strtotime($end_time)){
									$time[$i]['start'] = $start;
									$time[$i]['end'] = $end;
								}
							}
							$i = 0;
							$act_prod = 0;
							$brk_var = 0;
							foreach($time as $f_int) {
								$chck_2 = 0;
								$chck_ag = 0;
								$i++;
								$cur_date = date('Y-m-d');
								$f_str = date('Y-m-d').' '.$f_int['start'];
								$f_end = date('Y-m-d').' '.$f_int['end'];
								// echo '----'.$f_str.'------'.$f_end.'+++++++++++'."</br>";
		
								$check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at >= '$f_str' AND date_end_at <= '$f_end' "; // between case 
								$check_int_brk_qry = pg_query($db,$check_int_brk);
								if(pg_num_rows($check_int_brk_qry) > 0) {
									// echo '1111';
									$int_brk_str = pg_result($check_int_brk_qry,'0','date_start_at');
									$int_brk_end = pg_result($check_int_brk_qry,'0','date_end_at');
									$brk_prev = abs(strtotime($int_brk_end) - strtotime($int_brk_str)) / 60;
									$brk_var = floor($brk_prev);
									$act_prod = 30 - $brk_var ;
								} 
								$check_int_brk_2 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND (date_start_at >= '$f_str' AND date_start_at <= '$f_end')  AND date_end_at >= '$f_end' "; // between case 
								$check_int_brk_qry_2 = pg_query($db,$check_int_brk_2);
								if(pg_num_rows($check_int_brk_qry_2) > 0) {
									$chck_2 = 1;
									$int_brk_str = pg_result($check_int_brk_qry_2,'0','date_start_at');
									$int_brk_end = pg_result($check_int_brk_qry_2,'0','date_end_at');
									$brk_prev = abs(strtotime($f_end) - strtotime($int_brk_str)) / 60;
									$brk_var = floor($brk_prev);
									$act_prod = 30 - $brk_var ;
								} 
		
								$check_int_brk_1 = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at <= '$f_str' AND date_end_at >= '$f_str' AND  date_end_at <= '$f_end'"; // right side big case  
								$check_int_brk_qry_1 = pg_query($db,$check_int_brk_1);
								if(pg_num_rows($check_int_brk_qry_1) > 0) {
									
									$int_brk_end = pg_result($check_int_brk_qry_1,'0','date_end_at');
									$brk_prev = abs(strtotime($f_str) - strtotime($int_brk_end)) / 60;
									if($chck_2 = 0) {
										$brk_var = floor($brk_prev);
										$act_prod = 30 - $brk_var ;
									} else {
										$chck_ag = 1;
										$brk_var = floor($brk_prev);
										$act_prod = 30 - $brk_var ;
									}
								}
								// echo "</br>";
							// $check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at <= '$f_str' AND date_end_at >= '$f_end' "; // between case 
							// 	$check_int_brk_qry = pg_query($db,$check_int_brk);
							// 	if(pg_num_rows($check_int_brk_qry) > 0) {
							// 		$brk_var = 30;
							// 	} else {
							// $check_int_brk = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at::date = '$cur_date' AND date_start_at <= '$f_str' AND date_end_at <= '$f_end' "; // right side big case  
							// 		$check_int_brk_qry = pg_query($db,$check_int_brk);
							// 		if(pg_num_rows($check_int_brk_qry) > 0) {
							// 			$int_brk_end = pg_result($check_int_brk_qry,'0','date_end_at');
							// 			$brk_prev = abs(strtotime($f_end) - strtotime($int_brk_end)) / 60;
							// 			// again situation 
							// 		$check_int_brk_again = "SELECT * FROM rm_opdownreasons WHERE machine_id = $mid AND date_start_at <= '$f_str' AND date_end_at >= '$f_end' "; // left side case 
							// 			$check_int_brk_qry_again = pg_query($db,$check_int_brk_again);
							// 			if(pg_num_rows($check_int_brk_qry_again) > 0) {
							// 				$int_brk_str = pg_result($check_int_brk_qry,'0','date_start_at');
							// 				$brk_prev_again = abs(strtotime($f_str) - strtotime($int_brk_str)) / 60;
							// 				$brk_var = 30 - $brk_prev + $brk_prev_again;
							// 			} else {
							// 				$brk_var = 30 - $brk_prev;
							// 			}
							// 		} else {
							// 			$brk_var = 0;
							// 		}
							// 	}
								// echo $brk_var.'----'.$act_prod.'</br>';
								
							$get_sku_int = "SELECT rm_opskudata.*,rm_skus.unit_per_cld FROM rm_opskudata join rm_skus ON rm_skus.id = rm_opskudata.sku_id WHERE machine_id = $mid AND sku_start_at::date = '$cur_date'"; 
								$get_sku_int_qry = pg_query($db,$get_sku_int);
								if(pg_num_rows($get_sku_int_qry) > 0) {
									while ($get_sku_int_data = pg_fetch_array($get_sku_int_qry)) {
										$chck_sku_str = $get_sku_int_data['sku_start_at'];
										$chck_sku_end = $get_sku_int_data['sku_end_at'];
		
										// echo '******'.$chck_sku_str.'--'.$f_str.'----'.$chck_sku_end.'----'.$f_end.'******';
										// if(($chck_sku_str <= $f_str.':00' && empty($chck_sku_end)) || ($chck_sku_str <= $f_str.':00' && $chck_sku_end >= $f_end.':00')) {
											$chck_sku = $get_sku_int_data['sku_id'];
											$chck_unit_per_cld = $get_sku_int_data['unit_per_cld'];
											// OEE cal.
											// $act_int_prod = $this->getActualProduction($m_reg,$f_str,$f_end);
											// echo '******'.$m_reg.'--'.$f_str.'----'.$f_end.'----'.$act_int_prod.'******'."</br>";
											// $total_production = $act_int_prod*$chck_unit_per_cld; 
											// $target_production = 10*$get_rpm_int;
											// $get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
											// $oee_total = ceil($get_oee);
		
											$currMixDuration = abs(strtotime($f_str) - strtotime($f_end)) / 60;
											$totalMixDuration = 30*60;
											if($bct > 0) {
												$get_batches_made = $currMixDuration/$bct;
												$get_target_batches = $totalMixDuration/$bct;
												$oee_total = ceil(($get_batches_made/$get_target_batches)*100);
											} else {
												$oee_total = 0;
											}
											// $prod_total += $get_batches_made;
											// $actual_prod_sku = ceil($get_batches_made);
											if($brk_var > 0) { 
												$f_act_p = $act_prod;
											} else {
												$f_act_p = 30;
											}
											if($oee_total >= 85) {
												$get_check_int = '0,0,'.$f_act_p.','.(int)$brk_var.',';
											}else if($oee_total < 85 && $oee_total >= 75) {
												$get_check_int = '0,'.$f_act_p.',0,'.(int)$brk_var.',';
											}else {
												if($oee_total > 0) {
													$get_check_int = $f_act_p.',0,0,'.(int)$brk_var.',';
												} else {
													$get_check_int = $f_act_p.',0,0,0,';
												}
											} 
										// } else {
										// 	$get_check_int = '0,0,0,0,';
										// }
									}
								} else {
									$get_check_int = '30,0,0,0,';
								}
								$act_prod = 0;
								$brk_var = 0;
		
								$get_check_int_new .= $get_check_int;
								// if($getInt['to_date'] < $current_date_time) {
									if($i == 2) {
										$add_graph = ($graph_interval - 1).'-'.$graph_interval;
										$add_date = date('d',strtotime($new_from)).'/'.$add_graph; 
										$new_add_string = $get_check_int_new;
										$get_string = rtrim($new_add_string, ',');
										$sorted_array = array_map('intval', explode(',', $get_string));
										array_unshift($sorted_array,$add_date);
										$get_all_array[]  = $sorted_array;
									}
								// }
							}
				// 			// ----------------FOR PRODUCTION GRAPH ENDS HERE -------------------
					}
			}
			// echo 'get_all_array---'."<pre>";
			// print_r($get_all_array);
		
		//*
		//* New Code *****************************************ENdssssssssssss****************************************************
		//*
		//*
		
		// die;
		
				$columns = array_column($final_array, 'from_date');
				array_multisort($columns, SORT_ASC, $final_array);
		
				
		
		// nEW CODE -----------------------------------------------------------------------------------------------
		
		
		
				$current_array_prev = [];
				$oee_total = 0;
				$prod_total = 0;
				$current_date_time = date('Y-m-d H:i:s');
				//  NEW CODE 
				// GET current and previous SKU details 
				$sku_str = $final_array[0]['from_date'];
				$sku_end = $current_date_time;
				$str_shift_time = date('Y-m-d',strtotime($date)).' '.$start_time_f[0];
				$end_shift_time = date('Y-m-d',strtotime($next_day)).' '.$late_time[0];
				// qry 	
				$get_all_sku_detail_cnt = "SELECT count(*)
								FROM rm_opskudata 
								JOIN rm_skus On rm_skus.id = rm_opskudata.sku_id
								JOIN rm_machines ON rm_machines.id = rm_opskudata.machine_id
								JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id
								WHERE rm_opskudata.sku_start_at BETWEEN '$sku_str' AND '$sku_end' AND rm_opskudata.machine_id = $mid
								AND rm_machines.id = $mid ";
				$exe_get_all_sku_detail_cnt = pg_query($db,$get_all_sku_detail_cnt);
				$count = pg_result($exe_get_all_sku_detail_cnt,'0','count');
				
				$get_all_sku_detail = "SELECT sku_id,sku_start_at,rm_machinetypes.output_counter_type,sku_end_at,rm_skus.sku_code,rm_skus.pack_size,rm_skus.unit_per_cld,rm_skus.pack_size_unit 
										FROM rm_opskudata 
										JOIN rm_skus On rm_skus.id = rm_opskudata.sku_id
										
										JOIN rm_machines ON rm_machines.id = rm_opskudata.machine_id
										JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id
										WHERE rm_opskudata.sku_start_at BETWEEN '$str_shift_time' AND '$end_shift_time' AND sku_end_at IS NOT NULL AND rm_opskudata.machine_id = $mid";
				$exe_get_all_sku_detail = pg_query($db,$get_all_sku_detail);
		
					if(pg_num_rows($exe_get_all_sku_detail) > 0) {
						$j = 0;
						while ($data_sku = pg_fetch_array($exe_get_all_sku_detail)) {
							$get_bct = "SELECT bct FROM rm_mixerbct WHERE machine_id = $mid";
							$result_bct = pg_exec($db,$get_bct);
							if(pg_num_rows($result_bct) > 0) {
								$bct = pg_result($result_bct,'0','bct');
							} else {
								$bct = 0;
							}
							if($data_sku['output_counter_type'] == 'CLD') {
								if(!empty($data_sku['sku_end_at'])) {
									$actual_prod_sku = $this->getActualProduction($m_reg,$data_sku['sku_start_at'],$data_sku['sku_end_at']);
									$total_shift_min =  abs(strtotime($data_sku['sku_start_at']) - strtotime($data_sku['sku_end_at'])) / 60;
								} else {
									$actual_prod_sku = $this->getActualProduction($m_reg,$data_sku['sku_start_at'],$end_shift_time);
									$total_shift_min =  abs(strtotime($data_sku['sku_start_at']) - strtotime($end_shift_time)) / 60;
								}
								$get_sku_rpm = "SELECT rpm FROM rm_machine_sku_rpm WHERE machine_id = $mid AND sku_id = $data_sku[sku_id]";
								$exe_get_sku_rpm = pg_query($db,$get_sku_rpm);
								if(pg_num_rows($exe_get_sku_rpm) > 0) {
									while ($final_rpm_data = pg_fetch_array($exe_get_sku_rpm)) {
										$final_rpm = $final_rpm_data['rpm'];
										// OEE cal.
										$total_production = $actual_prod_sku*$data_sku['unit_per_cld']; 
										$target_production = $total_shift_min*$final_rpm;
										$get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
										$oee_total += ceil($get_oee);
										$prod_total += $total_production;
									}
								} else {
									$get_oee = 0;
									$oee_total += $get_oee;
									$prod_total += 0;
								}
							} else {
								if(!empty($data_sku['sku_end_at'])) {
								   $currMixDuration = abs(strtotime($str_shift_time) - strtotime($data_sku['sku_end_at'])) / 60;
									$totalMixDuration =   $shift_min[0]*3;
									$get_batches_made = $currMixDuration/$bct;
									$get_target_batches = $totalMixDuration/$bct;
									$oee_total = (!empty($target_production)) ? ceil(($get_batches_made/$get_target_batches)*100) : '';
									$prod_total += $get_batches_made;
									$actual_prod_sku = ceil($get_batches_made);
								} else {
									$currMixDuration = abs(strtotime($str_shift_time) - strtotime($end_shift_time)) / 60;
									$totalMixDuration =   $shift_min[0]*3;
									$get_batches_made = $currMixDuration/$bct;
									$get_target_batches = $totalMixDuration/$bct;
									$oee_total = (!empty($target_production)) ? ceil(($get_batches_made/$get_target_batches)*100) : '';
									$prod_total += $get_batches_made;
									$actual_prod_sku = ceil($get_batches_made);
								}
							}
							if($data_sku['output_counter_type'] == 'CLD') {
								$filter = 3;
							} else {
								$filter = 4;
							}
							// change production to tons 
								if($data_sku['pack_size_unit'] == 'KG' || $data_sku['pack_size_unit'] == 'kg' || $data_sku['pack_size_unit'] == 'Kg' || $data_sku['pack_size_unit'] == 'kG') { 
									$cal_prod = number_format($prod_total/1000 ,$filter, '.', ',');
								} else if($data_sku['pack_size_unit'] == 'GM' || $data_sku['pack_size_unit'] == 'gm' || $data_sku['pack_size_unit'] == 'Gm' || $data_sku['pack_size_unit'] == 'gM') { 
									$cal_prod = number_format($prod_total/1000000 , $filter, '.', ',');
								} else if($data_sku['pack_size_unit'] == 'ML' || $data_sku['pack_size_unit'] == 'ml' || $data_sku['pack_size_unit'] == 'Ml' || $data_sku['pack_size_unit'] == 'mL') { 
									$cal_prod = number_format($prod_total*0.000001 , $filter, '.', ',');
								} else if($data_sku['pack_size_unit'] == 'LTR' || $data_sku['pack_size_unit'] == 'ltr' || $data_sku['pack_size_unit'] == 'Ltr' || $data_sku['pack_size_unit'] == 'lTr') { 
									$cal_prod = number_format($prod_total*0.00098200557276378 , $filter, '.', ',');
								}

								if($j == $count) {
									$f_oee = $oee_total;
								} else {
									$f_oee = 0;
								}
		
								//echo '+CP++'.$cal_prod.'====';
							if( ($data_sku['sku_start_at'] <= $date && empty($data_sku['sku_end_at']) ) || ($data_sku['sku_start_at'] < $date && $data_sku['sku_end_at'] > $date) || ($data_sku['sku_start_at'] < $date && $data_sku['sku_end_at'] < $date)) {
								$current_array[] = array(
									'output_counter_type' => $data_sku['output_counter_type'],
									'sku_code' => $data_sku['sku_code'],
									'sku_id' => $data_sku['sku_id'],
									'pack_size' => $data_sku['pack_size'],
									'pack_size_unit' => $data_sku['pack_size_unit'],
									'unit_per_cld' => $data_sku['unit_per_cld'],
									'date' => '',
									'cld' => $actual_prod_sku,
									'bct' => $bct,
									'from_date' => '' , 
									'to_date' => '' ,
									'total_prod' => $cal_prod == '0.000' ? 0 : $cal_prod,
									'oee' => $oee_total,
									'sku_type' => 'Current',
									'current_shift' => $current_shift,
								);
							} else {
								$current_array_prev[] = array(
									'output_counter_type' => $data_sku['output_counter_type'],
									'sku_code' => $data_sku['sku_code'],
									'sku_id' => $data_sku['sku_id'],
									'pack_size' => $data_sku['pack_size'],
									'pack_size_unit' => $data_sku['pack_size_unit'],
									'unit_per_cld' => $data_sku['unit_per_cld'],
									'date' => '',
									'cld' => '',
									'bct' => $bct,
									'from_date' => $data_sku['sku_start_at'] , 
									'to_date' => $data_sku['sku_end_at'] ,
									'total_prod' => $cal_prod == '0.000' ? 0 : ($cal_prod > '0.9' ? ceil($cal_prod) : $cal_prod),
									'oee' => $oee_total,
									'sku_type' => 'Previous',
									'current_shift' => '',
								);
							}
						}
					}
	
				$send_array = array (
					'data' => $final_array,
					'working_time' => $working_time,
					'working_time2' => $working_time2,
					'm_reg' => $m_reg,
					'current_array' => $current_array,
					'prev_array' => $current_array_prev,
					'production' => $array_prod,
					'brk' => $array_brk,
					'left' => $array_prod_left,
					'no_prod' => $array_nothing,
					'plant_id' => $plant,
					'unit_id' => $unit,
					'cascade_id' => $cascade,
					'machine_id' => $machine,
					'again_array' => $get_all_array,
					'f_oee' => $f_oee,
					'graph_show' => $graph_show
				);
				return $send_array;
			}

public function showActual(Request $request,$check_plant = ''){

	//dd($request);die;
		$return_plant = (isset($request['plantid']) && !empty($request['plantid'])) ? $request['plantid'] : '';
		

            $host = Session::get('host');
            $dbname = Session::get('dbname');
            $user = Session::get('user');
            $pass = Session::get('pass');
            $db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
            $user = Session::get('email');

            //echo '================================================================'.$user;
            $check_user = pg_query($db,"SELECT id FROM rm_users WHERE email_id = '$user'");
            $ID = pg_result($check_user,'0','id');
            $get_rights = pg_query($db,"SELECT unit_id,plant_id FROM rm_userrights WHERE user_id = $ID ");
            if(pg_num_rows($get_rights) > 0) {
				$output = [];
				$end_time = [];
				$start_time = [];
				$flag = 0;
				$shift_min = [];
				$final = [];
				$get_plant = [];
				$get_all_plants = [];
				$current_shift = [];
				$check_first_shift_str = '';
				$check_first_shift_end = '';
				while ($get_rights_data = pg_fetch_array($get_rights)) {
					$unit = $get_rights_data['unit_id'];
					$plant= $get_rights_data['plant_id'];
					// $array_unit = explode(",",$unit_id);
					// $array_plant = explode(",",$plant_id);
				
				// if(count($array_unit) > 0 && count($array_plant) > 0 ) {
						//if both unit and plant are assigned 
						// foreach($array_unit as $unit) {
							// foreach($array_plant as $plant) {
						$get_data = "SELECT rm_machines.id as mach_id,rm_plants.id as p_id,rm_plants.plant_name,rm_machines.cascade_id,machine_name,machine_regno,rm_machines.unit_id,rm_machines.plant_id,rm_machinetypes.output_counter_type,rm_mixerbct.bct,rm_cascades.cascade_name FROM rm_machines LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id LEFT JOIN rm_mixerbct ON rm_mixerbct.machine_id = rm_machines.id LEFT JOIN rm_plants ON rm_plants.id = rm_machines.plant_id LEFT JOIN rm_cascades ON rm_cascades.id = rm_machines.cascade_id  WHERE rm_machines.unit_id = $unit AND rm_machines.plant_id = $plant";
							$exe_qry = pg_query($db,$get_data);
								if(pg_num_rows($exe_qry) > 0) {
									while ($data_1 = pg_fetch_array($exe_qry)) {
									$output[] = array('unit_id' => $data_1['unit_id'],'mach_id' => $data_1['mach_id'],'cascade_id' => $data_1['cascade_id'],'cascade_name' => $data_1['cascade_name'],'plant_id' => $data_1['p_id'],'plant_name' => $data_1['plant_name'],'machine_name' => $data_1['machine_name'],'machine_regno' => $data_1['machine_regno'],'output_counter_type' => $data_1['output_counter_type'],'bct' => $data_1['bct']);
										
									}
								}
							$get_plant[] = $plant;
							// }
						// }
					//} else  {
						// if either unit or plant is assigned
					//     if(count($array_unit) > 0) {
					//         foreach($array_unit as $unit) {
					//             $get_data = "SELECT rm_machines.id as mach_id,rm_plants.id as p_id,rm_plants.plant_name,rm_machines.cascade_id,machine_name,machine_regno,rm_machines.unit_id,rm_machines.plant_id,rm_machinetypes.output_counter_type,rm_mixerbct.bct,rm_cascades.cascade_name FROM rm_machines LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id LEFT JOIN rm_mixerbct ON rm_mixerbct.machine_id = rm_machines.id LEFT JOIN rm_plants ON rm_plants.id = rm_machines.plant_id LEFT JOIN rm_cascades ON rm_cascades.id = rm_machines.cascade_id WHERE rm_machines.unit_id = $unit";
					//             $exe_qry = pg_query($db,$get_data);
					//             if(pg_num_rows($exe_qry) > 0) {
					//                 while ($data_1 = pg_fetch_array($exe_qry)) {
					//                    $output[] = array('unit_id' => $data_1['unit_id'],'mach_id' => $data_1['mach_id'],'cascade_id' => $data_1['cascade_id'],'cascade_name' => $data_1['cascade_name'],'plant_id' => $data_1['p_id'],'plant_name' => $data_1['plant_name'],'machine_name' => $data_1['machine_name'],'machine_regno' => $data_1['machine_regno'],'output_counter_type' => $data_1['output_counter_type'],'bct' => $data_1['bct']);
					// 				   $get_plant = array('plant_id' => $plant_id);
					// 				}
					//             }
					//         }
					//     } else if(count($array_plant) > 0) {
					//         foreach($array_plant as $plant) {
					//             $get_data = "SELECT rm_machines.id as mach_id,rm_plants.id as p_id,rm_plants.plant_name,rm_machines.cascade_id,machine_name,machine_regno,rm_machines.unit_id,rm_machines.plant_id,rm_machinetypes.output_counter_type,rm_mixerbct.bct,rm_cascades.cascade_name FROM rm_machines LEFT JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id LEFT JOIN rm_mixerbct ON rm_mixerbct.machine_id = rm_machines.id LEFT JOIN rm_plants ON rm_plants.id = rm_machines.plant_id LEFT JOIN rm_cascades ON rm_cascades.id = rm_machines.cascade_id WHERE rm_machines.plant_id = $plant";
					//             $exe_qry = pg_query($db,$get_data);
					//             if(pg_num_rows($exe_qry) > 0) {
					//                 while ($data_1 = pg_fetch_array($exe_qry)) {
 					//                    $output[] = array('unit_id' => $data_1['unit_id'],'mach_id' => $data_1['mach_id'],'cascade_id' => $data_1['cascade_id'],'cascade_name' => $data_1['cascade_name'],'plant_id' => $data_1['p_id'],'plant_name' => $data_1['plant_name'],'machine_name' => $data_1['machine_name'],'machine_regno' => $data_1['machine_regno'],'output_counter_type' => $data_1['output_counter_type'],'bct' => $data_1['bct']);
					// 				   $get_plant = array('plant_id' => $plant_id);
					// 				}
					//             }
					//         }
					//     }
					// }
				}
            } else {
                // if no rights are given 
                $get_data = "SELECT rm_machines.id as mach_id,machine_name,rm_plants.id as p_id,rm_plants.plant_name,rm_machines.cascade_id,machine_regno,rm_machines.unit_id,rm_machines.plant_id,rm_machinetypes.output_counter_type,rm_mixerbct.bct,rm_cascades.cascade_name FROM rm_machines LEFt JOIN rm_machinetypes ON rm_machinetypes.id = rm_machines.machine_type_id LEFT JOIN rm_mixerbct ON rm_mixerbct.machine_id = rm_machines.id LEFT JOIN rm_plants ON rm_plants.id = rm_machines.plant_id LEFT JOIN rm_cascades ON rm_cascades.id = rm_machines.cascade_id";
                $exe_qry = pg_query($db,$get_data);
                if(pg_num_rows($exe_qry) > 0) {
                    while ($data_1 = pg_fetch_array($exe_qry)) {
                       $output[] = array('unit_id' => $data_1['unit_id'],'mach_id' => $data_1['mach_id'],'cascade_id' => $data_1['cascade_id'],'cascade_name' => $data_1['cascade_name'],'plant_id' => $data_1['p_id'],'plant_name' => $data_1['plant_name'],'machine_name' => $data_1['machine_name'],'machine_regno' => $data_1['machine_regno'],'output_counter_type' => $data_1['output_counter_type'],'bct' => $data_1['bct']);
					}
					// get all plants 
					$all_plants = "SELECT id FROM rm_plants";
					$exe_qry_plants = pg_query($db,$all_plants);
					while ($data_all = pg_fetch_array($exe_qry_plants)) { 
						
						$get_plant[] = $data_all['id'];
					}
					//$get_plant = array('plant_id' => implode("," , $get_all_plants));
                }
			}

			//echo "<pre>";
			//print_r($get_plant);die;
             // get current ongoing shift time 
           	$carbon = Carbon::today();
          	$current_date_time = Carbon::now('Asia/Kolkata')->toDateTimeString();
            $dt = Carbon::now();
            $current_time =  $dt->toTimeString();
            $get_current_date = $carbon->format('Y-m-d');
           
            // check end time for shift running 
            $check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
            EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_start_at < '$current_time' AND shift_end_at >= '$current_time' ";
            $exe_shift_qry = pg_query($db,$check_shift);
            //echo $check_shift."\n";
            if(pg_num_rows($exe_shift_qry) > 0) {
                while ($data_2 = pg_fetch_array($exe_shift_qry)) {

                    if($data_2['shift_part'] > 0) {
                        $init = $data_2['shift_part'];
                    } else if($data_2['shift_part'] < 0) {
                        $init = $data_2['shift_part_new']/2;
                    }
                    $minutes = floor($init / 60);
                    $shift_min[]  = $minutes;
                   $end_time[] =  $data_2['shift_end_at'];
				   $start_time[] =  $data_2['shift_start_at'];
				   
				   $flag = 1;
				   $current_shift = $data_2['shift_code'];
                }
            } else {
                $check_shift = "SELECT shift_code,shift_end_at,shift_start_at,EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
                EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new   FROM rm_shifts WHERE shift_code = 'N' ";
                $exe_shift_qry = pg_query($db,$check_shift);
                if(pg_num_rows($exe_shift_qry) > 0) {
                    while ($data_2 = pg_fetch_array($exe_shift_qry)) {
                        if($data_2['shift_part'] > 0) {
                            $init = $data_2['shift_part'];
                        } else if($data_2['shift_part'] < 0) {
                            $init = $data_2['shift_part_new']/2;
                        }
                        $minutes = floor($init / 60);
                        $shift_min[]  = $minutes;
                        $end_time[] = $data_2['shift_end_at'];
                        $start_time[] =  $data_2['shift_start_at'];
						$flag = 2;
						$current_shift = $data_2['shift_code'];
                    }
                }
			}

			$str_shift_time = date('Y-m-d',strtotime($current_date_time)).' '.$start_time[0];
			$end_shift_time = date('Y-m-d',strtotime($current_date_time)).' '.$end_time[0];
	

			$prod_total = 0;
			$oee_total = 0;
            // get actual production 
            foreach($output as $val) {
				
                $machine_regno = $val['mach_id'];
                $p_id = $val['plant_id'];
                $plant_name = $val['plant_name'];
                $mach_regno = $val['machine_regno'];
                $output_counter_type = $val['output_counter_type'];
                $bct = $val['bct'];
                $start_at = date('Y-m-d', strtotime($get_current_date))." ".$start_time[0];
                if($flag == 1) {
                    $end_at = $current_date_time;
                } else {
                    $end_at = date('Y-m-d', strtotime("+1 day", strtotime($get_current_date)))." ".$current_time;
				}
				$check_first_shift_str = $start_at;
				$check_first_shift_end = $end_at;
                $total_shift_min = $shift_min[0];
                $actual_production = $this->getActualProduction($mach_regno,$start_at,$end_at);
                // echo $mach_regno.'---'.$start_at.'----'.$end_at.'----'.$actual_production."</br>";
			   // check first production with cld or not 
			   $check_oplogin = "SELECT logintime from rm_oplogins WHERE machine_id = $machine_regno AND logintime::date = '$get_current_date' ORDER BY id ASC LIMIT 1";
				$result_login = pg_exec($db,$check_oplogin);
				if(pg_num_rows($result_login) > 0) {
					$first_flag = 'Y';
				} else {
					$first_flag = 'N';
				}

				$get_first_production = $this->getActualProduction($mach_regno,$check_first_shift_str,$current_date_time);

                // get pack size and unit per cld 
                 $get_size = "SELECT rm_skus.id as check_sku_id,rm_skus.pack_size,rm_skus.pack_size_unit,rm_skus.unit_per_cld,rpm_default,rm_opskudata.sku_start_at,rm_opskudata.sku_end_at FROM rm_opskudata 
                            JOIN rm_skus ON rm_skus.id =  rm_opskudata.sku_id 
                            WHERE rm_opskudata.machine_id = $machine_regno AND ( (rm_opskudata.sku_start_at BETWEEN '$start_at' AND '$end_at') OR (rm_opskudata.sku_end_at IS NULL AND rm_opskudata.sku_start_at::date = '$get_current_date'))  ORDER BY rm_opskudata.id DESC  LIMIT 1";
                $exe_size_qry = pg_query($db,$get_size);

                if(pg_num_rows($exe_size_qry) > 0) {
					while ($data_3 = pg_fetch_array($exe_size_qry)) {
						// $oee_total = 0;
						if($output_counter_type == 'CLD' && !empty($data_3['check_sku_id'])) { 
							// echo 'pppppppppppppppppppppppppppppppppppppp';
							// $select_rpm = "SELECT rpm from rm_machine_sku_rpm WHERE machine_id = $machine_regno AND sku_id = $data_3[check_sku_id]"; 
							// $select_rpm_result = pg_exec($db,$select_rpm);
							// if(pg_num_rows($select_rpm_result) > 0) {
							// 	$get_sku_rpm = pg_result($select_rpm_result,'0','rpm');
							// } else {
							// 	$get_sku_rpm = 0;
							// }
							// $pack_size = $data_3['pack_size'];
							// $unit_per_cld = $data_3['unit_per_cld'];
							// $rpm_default = $get_sku_rpm;
							// $total_production = $actual_production*$unit_per_cld;
							// $target_production = $total_shift_min*$rpm_default;
							// $get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : '';
							// //cal OEE % Packing MAchine
							// $oee = ($get_oee != '') ? ceil($get_oee)."%" : "0%";
							// echo '++++'.$mach_regno.'----------------'.$data_3['sku_start_at'].'--------------'.$end_shift_time.'++++'; echo "</br>";
							// if(!empty($data_3['sku_end_at'])) {
								$actual_prod_sku = $this->getActualProduction($mach_regno,$str_shift_time,$end_shift_time);
								
								// $total_shift_min =  abs(strtotime($data_3['sku_start_at']) - strtotime($data_3['sku_end_at'])) / 60;
							// } else {
								// $actual_prod_sku = $this->getActualProduction($mach_regno,$data_3['sku_start_at'],$end_shift_time);
								// echo '++++'.$mach_regno.'----------------'.$str_shift_time.'--------------'.$current_date_time.'------'.$actual_prod_sku; echo "</br>";
								$total_shift_min =  abs(strtotime($str_shift_time) - strtotime($end_shift_time)) / 60;
								// echo '++++'.$mach_regno.'----------------'.$str_shift_time.'--------------'.$current_date_time.'------'.$actual_prod_sku; echo "</br>";
							// }
							 $get_sku_rpm = "SELECT rpm FROM rm_machine_sku_rpm WHERE machine_id = $machine_regno AND sku_id = $data_3[check_sku_id]"; 
							$exe_get_sku_rpm = pg_query($db,$get_sku_rpm);
							if(pg_num_rows($exe_get_sku_rpm) > 0) {
								while ($final_rpm_data = pg_fetch_array($exe_get_sku_rpm)) {
									  $final_rpm = $final_rpm_data['rpm']; 
									// OEE cal.
							          $total_production = $actual_prod_sku*$data_3['unit_per_cld'];  
									  $target_production = $total_shift_min*$final_rpm; 
									   $get_oee = (!empty($target_production)) ? ($total_production/$target_production)*100 : ''; 
									   $oee_total = $get_oee; 
									  $prod_total = $total_production; 
								}
							} else {
								$get_oee = 0;
								$oee_total = $get_oee;
								$prod_total = 0;
							}
						}else{
							$currMixDuration = abs(strtotime($data_3['sku_start_at']) - strtotime($current_date_time)) / 60;
							$totalMixDuration = $shift_min[0];
								if($bct > 0) { 
										$get_batches_made = $currMixDuration/$bct; 
										$get_target_batches = $totalMixDuration/$bct; 
										$oee_total = ceil(($get_batches_made/$get_target_batches)*100);
										$prod_total = $get_batches_made;
								}
					}	
					// change production to tons 
					if($data_3['pack_size_unit'] == 'KG' || $data_3['pack_size_unit'] == 'kg' || $data_3['pack_size_unit'] == 'Kg' || $data_3['pack_size_unit'] == 'kG') { 
						$cal_prod = number_format($prod_total/1000 , 3, '.', ',');
					} else if($data_3['pack_size_unit'] == 'GM' || $data_3['pack_size_unit'] == 'gm' || $data_3['pack_size_unit'] == 'Gm' || $data_3['pack_size_unit'] == 'gM') { 
						$cal_prod = number_format($prod_total/1000000 , 3, '.', ',');
					} else if($data_3['pack_size_unit'] == 'ML' || $data_3['pack_size_unit'] == 'ml' || $data_3['pack_size_unit'] == 'Ml' || $data_3['pack_size_unit'] == 'mL') { 
						$cal_prod = number_format($prod_total*0.000001 , 3, '.', ',');
					} else if($data_3['pack_size_unit'] == 'LTR' || $data_3['pack_size_unit'] == 'ltr' || $data_3['pack_size_unit'] == 'Ltr' || $data_3['pack_size_unit'] == 'lTr') { 
						$cal_prod = number_format($prod_total*0.00098200557276378 , 3, '.', ',');
					}
					$prod_total = 0;
				}
					//echo ' ++CP+'.$cal_prod.'====';
					$final[] = array('mach_id' => $val['mach_id'],'first_flag' => $first_flag,'first_prod' => $get_first_production,'unit_id' => $val['unit_id'],'cascade_id' => $val['cascade_id'], 'cascade_name' => $val['cascade_name'], 'machine_name' => $val['machine_name'],'total_production' => ($cal_prod > 0.9 ? number_format($cal_prod,1, '.', ','): ($cal_prod == '0.000' ? 0 : $cal_prod)),'oee' => ceil($oee_total),'plant_id' => $p_id,'plant_name' => $plant_name);		
					
				} else {

					$final[] = array('mach_id' => $val['mach_id'],'first_flag' => $first_flag,'first_prod' => $get_first_production,'unit_id' => $val['unit_id'],'cascade_id' => $val['cascade_id'], 'cascade_name' => $val['cascade_name'],'machine_name' => $val['machine_name'],'total_production' => '','oee' => '','plant_id' => $p_id,'plant_name' => $plant_name);	
				}
		}
			
			
			$temp_array = array();
			$i = 0;
			$key_array = array();
			$key = 'mach_id';
			foreach($final as $val) {
				if (!in_array($val[$key], $key_array)) {
					$key_array[$i] = $val[$key];
                    $temp_array[$i] = $val;
                }
                $i++;
            } 

			// echo "<pre>";
			// print_r($temp_array);
			
			// die;

	    return view('supervisor_current',['shift' => $current_shift[0],'time' => date('d M,Y g:i A', strtotime($current_date_time)),'data' => $temp_array,'get_plant_list' => $get_plant,'return_plant' => $return_plant]);
    }
	public function showDetail(Request $request){
        // echo 'In-------------------';
        $data = array(
            'U' => $request['U'],
            'P' => $request['P'],
            'C' => $request['C'],
            'M' => $request['M'],
            'S' => $request['S'],
        );
        $get_condition = $this->showData($request['M'],$request['U'],$request['P'],$request['C'],$request['S'],$request['FD'],$request['TD'],$request['T']);
        // $get_condition = json_encode($data);
		//echo "<pre>";
		//dd($get_condition);
        return json_encode($get_condition);
    }



public function liveprodata(){
		// echo Session::get('machine_regno');
		// dd(Session::all());
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$machine_id = Session::get('machine_id');
 		$operatorid = Session::get('operator_id');
 		$skuid = Session::get('sku_row_id');
		// dd($skuid);
		$today = date('Y-m-d')." 00:00:00";
		
		// dd($today);
		$operatorlogquery = pg_query($db, "SELECT sku_start_at, sku_end_at FROM rm_opskudata WHERE sku_id = $skuid and operator_id = $operatorid and machine_id=$machine_id and sku_start_at > '$today' order by sku_start_at limit 1");
		$operatorlog = array();
		while($operalogrow = pg_fetch_array($operatorlogquery)){
			// echo "<pre>";
			// print_r($operalogrow['sku_start_at']);
			// echo "</pre>";			
			$operatorlog[] = array(
				'start' => $operalogrow['sku_start_at'],
				'end' => $operalogrow['sku_end_at']
			);			
		}
		
		
		$datetime = $operatorlog[0]['start'];
		$starttime = date('Y-m-d H:i:s', strtotime($datetime));
		// dd($entrytime);
		
		// echo $starttime;
		// date_default_timezone_set('Asia/Kolkata');
		$endtime = date('Y-m-d H:i:s');
		// Display value of variable
		// echo $endtime;
		// dd($endtime);
		$regno = Session::get('machine_regno');
		// dd($regno);
		// echo $starttime;
		// dd($endtime);
		$adist="0";$tdist="0";$old_dist="0";
		
		
 	 	$dic="select pkt_cnt  from rm_production_data where reg_no='$regno' and pkt_cnt>=0 and	date_time_entry > '$starttime' and date_time_entry <= '$endtime'";		
		
		$dis=pg_exec($db,$dic);
		//dd($dis);
        $initial=0;	
		for($r=0;$r<pg_numrows($dis);$r++){
			$dist=pg_result($dis,$r,'pkt_cnt');			
			if($r==0){
				$initial=$dist;
			}
			if($old_dist>$dist && $dist<=1 && $adist==0){
				$adist=$adist+$old_dist;
				$tdist="0";
			}
			if($old_dist<$dist){
				$tdist=$dist;
			}
			$old_dist=$dist;
		}
		$cnttotal=($tdist+$adist) - $initial;
		return $cnttotal;
	}
	
	public function liveprodction2(){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		

		$adist="0";$tdist="0";$old_dist="0";
		// dd(session()->all());
		$regno = Session::get('machine_regno');
		// $dic="SELECT pkt_cnt from rm_production_data WHERE reg_no='$regno'  and pkt_cnt>=0 and	date_time_entry > '$starttime' and date_time_entry <= '$endtime'";	
		$dic="SELECT pkt_cnt from rm_production_data WHERE reg_no='$regno'";	
		
		$dis=pg_exec($db,$dic);
		//dd($dis);
		$initial=0;	
		for($r=0;$r<pg_numrows($dis);$r++){
			$dist=pg_result($dis,$r,'pkt_cnt');			
			if($r==0){
				$initial=$dist;
			}
			if($old_dist>$dist && $dist<=1 && $adist==0){
				$adist=$adist+$old_dist;
				$tdist="0";
			}
			if($old_dist<$dist){
				$tdist=$dist;
			}
			$old_dist=$dist;
		}
		$cnttotal=($tdist+$adist) - $initial;
		return $cnttotal;
	}
	
	public function liveprodction(){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$machine_id = Session::get('machine_id');
 		$operatorid = Session::get('operator_id');
 		$skuid = Session::get('sku_row_id');
		// dd($skuid);
		$today = date('Y-m-d')." 00:00:00";
		
		// dd($today);
		$operatorlogquery = pg_query($db, "SELECT sku_start_at, sku_end_at FROM rm_opskudata WHERE sku_id = $skuid and operator_id = $operatorid and machine_id=$machine_id and sku_start_at > '$today' order by sku_start_at limit 1");
		$operatorlog = array();
		while($operalogrow = pg_fetch_array($operatorlogquery)){
			// echo "<pre>";
			// print_r($operalogrow['sku_start_at']);
			// echo "</pre>";			
			$operatorlog[] = array(
				'start' => $operalogrow['sku_start_at'],
				'end' => $operalogrow['sku_end_at']
			);			
		}		
		
		$datetime = $operatorlog[0]['start'];
		$starttime = date('Y-m-d H:i:s', strtotime($datetime));
		// dd($entrytime);
		
		// echo $starttime;
		// date_default_timezone_set('Asia/Kolkata');
		$endtime = date('Y-m-d H:i:s');
		// Display value of variable
		// echo $endtime;
		// dd($endtime);
		$regno = Session::get('machine_regno');
		// dd($regno);
		// echo $starttime;
		// dd($endtime);
		$adist="0";$tdist="0";$old_dist="0";
		
		
 	 	$dic="SELECT pkt_cnt FROM rm_production_data WHERE reg_no='$regno' and pkt_cnt>=0 and	date_time_entry > '$starttime' and date_time_entry <= '$endtime'";		
		
		$dis=pg_exec($db,$dic);
		//dd($dis);
        $initial=0;	
		for($r=0;$r<pg_numrows($dis);$r++){
			$dist=pg_result($dis,$r,'pkt_cnt');			
			if($r==0){
				$initial=$dist;
			}
			if($old_dist>$dist && $dist<=1 && $adist==0){
				$adist=$adist+$old_dist;
				$tdist="0";
			}
			if($old_dist<$dist){
				$tdist=$dist;
			}
			$old_dist=$dist;
		}
		$cnttotal=($tdist+$adist) - $initial;
		return $cnttotal;
	}
	
	// public function getunitboxplant($id){
		// dd($id);
		// $host = Session::get('host');
		// $dbname = Session::get('dbname');
		// $user = Session::get('user');
		// $pass = Session::get('pass');
		// $db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		// $userrole = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id = $id");
		// $plantarray = array();
		// while($plantrow = pg_fetch_array($userrole)){
			// echo "<pre>";
			// print_r($plantrow);
			// echo "</pre>";
			// $plantarray[] = array(
				// 'plantid' => $plantrow['id'],
				// 'plantname' => $plantrow['plant_name']
			// );
		// }
		// $json = json_encode($plantarray);
		// return $json;
	// }
	public function userrolesubmit(Request $req){
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		$userid = $req->userid;
		
		$userroleexist = pg_query($db, "SELECT user_id FROM rm_userrights WHERE user_id = $userid");
		if(pg_num_rows($userroleexist)>0){
			// echo "User Already Exist";			// 
			$deluserrole = pg_query($db, "DELETE FROM public.rm_userrights WHERE user_id = $userid");
		}
		$main_data = $req->plantrow;
		foreach($main_data as $val) {
			$data = explode("_",$val);
			// $plant = $data[0];
			// $unit = $data[1];
			$plant = $data[1];
			$unit = $data[0];
			$userrole = pg_query($db, "INSERT INTO public.rm_userrights(user_id, unit_id, plant_id)
					VALUES ('$userid', '".$unit."', '".$plant."')");
		}
		if($userrole){
			// echo " Successfully";
			$message = "ROLE HAS BEEN ASSIGNED SUCCESSFULLY";
			Session::put('message', $message);
			session()->forget('unit_id');
			Session::forget('admin_unit');
			Session::forget('admin_plant');
			Session::forget('admin_cascade');
			Session::forget('admin_machine');
			Session::forget('admin_products');
			Session::forget('admin_skus');
			Session::forget('admin_plant_pincode');
			Session::forget('admin_bct');
			Session::forget('admin_product_mapping');
			Session::forget('admin_sku_type');
			Session::forget('admin_sku_unit');
			Session::forget('admin_reasons');
			Session::forget('admin_subreasons');
			$admin_users = 'admin_users';
			Session::put('admin_users',$admin_users);			
			return redirect('/');
		}else{
			echo "Role Has Not Been Assigned Successfully";
		}
	}

   

}

