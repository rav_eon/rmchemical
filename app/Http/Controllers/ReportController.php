<?php
namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
Use App\User;
Use Session;
use Redirect;
use Illuminate\Support\Facades\DB; 
use Carbon\Carbon;

class ReportController extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $db;
    public function __construct()
    {
        $host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');	
        $this->db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
    }

    public function getActualProduction($machine_regno,$start_at,$end_at) {
		// dd('report controller ');
        $adist="0";$tdist="0";$old_dist="0";
        $dic="select pkt_cnt  from rm_production_data where reg_no='$machine_regno' and pkt_cnt>=0 and date_time_entry > '$start_at' and date_time_entry <= '$end_at'";
        $dis=pg_exec($this->db,$dic);
        $initial = "0";
        for($r=0;$r<pg_numrows($dis);$r++)
        {
            $dist=pg_result($dis,$r,'pkt_cnt');
                if($r==0)
                {
                    $initial=$dist;
                }
                if($old_dist>$dist && $dist<=1 && $adist==0)
                {
                    $adist=$adist+$old_dist;
                    $tdist="0";
                }
                if($old_dist<$dist)
                {
                    $tdist=$dist;
                }
            $old_dist=$dist;
        }
        $cnttotal=($tdist+$adist) - $initial;
        return $cnttotal;
    } 

    public function showData(){

        $qry = "SELECT rm_shifts.shift_name,rm_shifts.shift_code,rm_users.first_name,rm_users.last_name,rm_oplogins.operator_id,
                        rm_machines.machine_name,rm_machines.machine_regno,rm_oplogins.machine_id,rm_oplogins.logintime,
                        rm_cascades.cascade_name,rm_cascades.cascade_code,rm_shifts.shift_end_at,rm_shifts.shift_start_at,
                        EXTRACT(EPOCH FROM (shift_end_at::time - shift_start_at::time)) as shift_part ,
                        EXTRACT(EPOCH FROM (shift_start_at::time - shift_end_at::time)) as shift_part_new 
                FROM rm_oplogins
                LEFT JOIN rm_shifts ON rm_shifts.shift_code = rm_oplogins.shift_code
                LEFT JOIN rm_users ON rm_users.id = rm_oplogins.operator_id
                LEFT JOIN rm_machines ON rm_machines.id = rm_oplogins.machine_id
                LEFT JOIN rm_cascades ON rm_machines.cascade_id = rm_cascades.id";
        $result = pg_exec($this->db,$qry);
        $array = [];
        if (pg_num_rows($result) > 0) {
            while ($recData = pg_fetch_array($result)) {
                $opId = $recData['operator_id'];
                $mId = $recData['machine_id'];
                if($recData['shift_part'] > 0) {
                    $init = $recData['shift_part'];
                } else if($recData['shift_part'] < 0) {
                    $init = $recData['shift_part_new']/2;
                }
                $minutes = floor($init / 60);
                $recData['shift_part']  = $minutes;
                $qry_1 = "SELECT rm_opskudata.sku_start_at,rm_opskudata.sku_end_at,
                                rm_skus.sku_code,rm_skus.sku_name,rm_skus.rpm_default
                         FROM rm_oplogins
                         LEFT JOIN rm_opskudata ON rm_opskudata.machine_id = rm_oplogins.machine_id
                         LEFT JOIN rm_skus ON rm_opskudata.sku_id = rm_skus.id 
                         WHERE rm_opskudata.operator_id = $opId AND rm_opskudata.machine_id = $mId";
                $result_1 = pg_exec($this->db,$qry_1);

                $qry_2 = "SELECT EXTRACT(EPOCH FROM (date_end_at::time - date_start_at::time)) as breakdown_part
                         FROM rm_oplogins
                         LEFT JOIN rm_opdownreasons ON rm_opdownreasons.machine_id = rm_oplogins.machine_id 
                         WHERE rm_opdownreasons.machine_id = $mId AND rm_opdownreasons.operator_id = $opId";
                $result_2 = pg_exec($this->db,$qry_2);
                
                if (pg_num_rows($result_1) > 0) {
                    while ($recData_1 = pg_fetch_array($result_1)) {
                        $recData['sku_start_at'] = $recData_1['sku_start_at'];
                        $recData['sku_end_at'] = $recData_1['sku_end_at'];
                        $recData['sku_code'] = $recData_1['sku_code'];
                        $recData['sku_name'] = $recData_1['sku_name'];
                        $recData['rpm_default'] = $recData_1['rpm_default'];
                    }   
                }
                if (pg_num_rows($result_2) > 0) {
                    while ($recData_2 = pg_fetch_array($result_2)) {
                        $part_shift = $recData_2['breakdown_part'];
                        $minutes_2 = floor($part_shift / 60);
                        $recData['breakdown_part'] = $minutes_2; 
                    }   
                } else {
                    $recData['breakdown_part'] = '';
                }
                 // calculate total production 
                 $total_production = $recData['rpm_default']* $recData['shift_part'];
                 $recData['total_production'] = $total_production;
                 // calculate OEE %
                     $machine_regno = $recData['machine_regno'];
                    if($recData['shift_code'] == 'E' || $recData['shift_code'] == 'M') {  
                        $start_at = date('Y-m-d', strtotime($recData['logintime']))." ".$recData['shift_start_at'];
                        $end_at = date('Y-m-d', strtotime($recData['logintime']))." ".$recData['shift_end_at'];
                    } else if($recData['shift_code'] == 'N') {
                        $start_at = date('Y-m-d', strtotime($recData['logintime']))." ".$recData['shift_start_at'];
                        $end_at = date('Y-m-d', strtotime("+1 day", strtotime($recData['logintime'])))." ".$recData['shift_end_at'];
                    }
                    $actual_production = $this->getActualProduction($machine_regno,$start_at,$end_at);
                    $current_date_time = Carbon::now('Asia/Kolkata')->toDateTimeString();

                    // echo $actual_production;
                $array[] = $recData;
            }
        }
        return view('report',['data' => $array,'time' => $current_date_time]);
    }

    public function showDetail(Request $request){
        echo 'In-------------------';
        $data = array(
            'U' => $request['U'],
            'P' => $request['P'],
            'C' => $request['C'],
            'M' => $request['M'],
            'S' => $request['S'],
        );
        return response()->json(array('arr'=>$data));
    }


}