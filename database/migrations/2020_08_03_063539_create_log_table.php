<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log', function (Blueprint $table) {
			$table->increments('id'); 
			$table->string('oerator_name'); 
			$table->string('shift_title'); 
			$table->integer('designation_id'); 
			$table->integer('plant'); 
			$table->integer('pin_session_id'); 
			$table->integer('pin_session_value'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
    }
}
