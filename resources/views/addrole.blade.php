@extends('header')
<style>
    .vtabs {
		width: 100% !important;
	}
	.vtabs .tabs-vertical {
		width: 204px !important;
	}
	.plant_detail_1 > a {
		color: #272c33 !important;
	}.table td, .table th {
		border-color: #009efb !important;
	}
	.dev_style {
		color: #272c33 !important;
		font-weight: normal !important;
	}
	.card-body.heading-block {
		padding: 10px;
		background: #999595;
	}
	.heading-text span {
		color: white;
	}

	.vtabs {
	width: 100% !important;
	}
	.vtabs .tabs-vertical {
	width: 204px !important;
	}
	.plant_detail_1 > a {
	color: #272c33 !important;
	}.table td, .table th {
	border-color: #5b826f !important;
	}
	.dev_style {
	color: #272c33 !important;
	font-weight: normal !important;
	}
	/* .card-body.heading-block {
	padding: 10px;
	background: #999595;
	} */
	.heading-text span {
	color: #174d33;
	font-style: italic;
	}
	.heading-text {
	height : 60px;
	}

	.admin_display_bar{
		background: #3c6382;
		color: white;
	}

	.container-contact100 {
		min-height: 65vh !important;
	} 
	.unitboxx1 ~ .unitboxx1 {
		display: none;
	}
	.unitboxx2 ~ .unitboxx2 {
		display: none;
	}	
	
	
</style>
	<?php 
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		// Session::forget('admin_plant');
		// Session::forget('admin_plant_pincode');
		// Session::forget('admin_cascade');
		// $admin_unit = 'admin_unit';
		// Session::put('admin_unit', $admin_unit);
	?>
	
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo fa fa-user" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                       
                    </ul>
					<div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
						Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>										
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">											
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
									<!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
       
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <!--div class="page-wrapper"-->
        <div class="">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid"> <br/> 

        <link rel="stylesheet" type="text/css" href="../resources/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="../resources/css/main.css">
	<body> 
		
		<?php	
			// =============== User rights
			$userrrole = pg_query($db, "SELECT * FROM rm_userrights WHERE user_id = $userrole");
			$unituserarray =array();
			$plantuserarray =array();
			// $unittttt =array();
			// $plantttt =array();
			$i = 0;
			$j = 0;
			while($userrow = pg_fetch_array($userrrole)){
				$unittttt = $userrow['unit_id'];
				$plantttt = $userrow['plant_id'];						
				$unituserarray[] = $unittttt."_".$plantttt;							
				// echo "<pre>";
				// print_r($unituserarray);
				// echo "</pre>";						
			}
			
			
			// =============== Unit
			$unitarray = array();
			$unitid = array();
			$unitname = array();
			$unitrole = pg_query($db, "SELECT * FROM rm_units");
			while($unitrow = pg_fetch_array($unitrole)){
				// echo "<pre>";
				// print_r($unitrow);
				// echo "</pre>";
				$unitid[] = $unitrow['id'];
				$unitname[] = $unitrow['unit_name'];
				// $unitarray[] = $unitid."_".$unitname;
			}						
			// echo "<pre>";
			// print_r($unitarray[0]);
			// print_r($unitid[0]);
			// echo "<br/>";
			// print_r($unitid[1]);
			// echo "<br/>";
			// print_r($unitname[0]);
			// echo "<br/>";
			// print_r($unitname[1]);
			// echo "<br/>";
			// echo "</pre>";
				
				
				
			// =============== Plant
			$plantarray = array();						
			$plantrole = pg_query($db, "SELECT * FROM rm_plants");						
			while($planttrow = pg_fetch_array($plantrole)){
				// echo "<pre>";
				// print_r($planttrow);
				// echo "</pre>";
				$plantid = $planttrow['id'];
				$uplantid = $planttrow['unit_id'];
				$plantname = $planttrow['plant_name'];
				
				$plantarray[] = $uplantid."_".$plantid;	
				
			}
			$FinalArray = [];					
		
			$check_u = array_diff($plantarray, $unituserarray);
			
			foreach($unituserarray as $checkd) {
				$FinalArray[] = array('id'=>$checkd ,'type' => 'C');
			}
			foreach($check_u as $uncheckd) {
				$FinalArray[] = array('id'=>$uncheckd ,'type' => 'U');
			}			
		?>				
		
		<!-- Main Content Display Start Here Static -->
		
		<div class="container-contact100">
			<div class="wrap-contact100">
				<form method="post" action="userrolesubmit">					
					<span class="contact100-form-title">User Role</span>		
					<input type="hidden" class="userroleinfo" name="userid" value="<?php echo $userrole; ?>">
					@csrf
					@if($errors->any())
						<div style="background-color:red; margin-bottom:10px; padding: 14px; color:white; border-radius: 10px;">	
							{{ implode('', $errors->all(':message')) }}
						</div>
					@endif
					<div class="row">					
						<div class="col-sm-4 unitrole">
							<h4>Select Unit</h4>									
							<?php 
								$original_unit = $unitid[0];
								// print_r($original_unit);
								// echo "++++++++++";
								// echo "<br/>";
								foreach($FinalArray as $values) { 							
									$check_unit = explode('_',$values['id']);
									// print_r($check_unit[0]);
									// echo "=======";
									$unit_1 = $check_unit[0];
									if($unit_1 == $original_unit) { 
							?>
								
									<?php if($values['type'] == 'C'){ ?>
										<!-- Plant -->	
										<div class="unitboxx2">	
											<input type="checkbox" id="unitbox" class="unitbox unitts unit1" value="<?php echo $original_unit; ?>" name="unitrow[]" checked disabled>&nbsp; <?php echo $unitname[0]; ?><br/>
										</div>											
							<?php	
											break;
									} else {  ?>					
							
								<!-- Plant -->	
								<div class="unitboxx2">	
									<input type="checkbox" id="unitbox" class="unitbox unitts unit1" value="<?php echo $original_unit; ?>" name="unitrow[]" disabled>&nbsp; <?php echo $unitname[0]; ?><br/><br/>
								</div>
							<?php		
										break;
										}   
									} 
								} 
							?>
						</div>						
						<div class="col-sm-8 plantrole">
							<h4>Select Plant</h4>
							<?php 
								$actual_unit = '1';
								foreach($FinalArray as $values) { 							
								$check_unit = explode('_',$values['id']);
								$unit_1 = $check_unit[0]; 
								if($unit_1 == $actual_unit) { 
									$planttt = explode("_",$values['id']);
							?>
									<?php if($values['type'] == 'C'){ ?>
										<!-- Plant -->	
										<div class="unitboxx2">	
											<input type="checkbox" id="plantbox" class="plantbox<?php echo $j; ?> plantts plant1" value="<?php echo $values['id']; ?>"  name="plantrow[]" checked > &nbsp; 
											<?php
												$plant_nameee = $planttt[1];
												$plantrolename = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id='$plant_nameee'");$plantrolerow = pg_fetch_array($plantrolename);
												print_r($plantrolerow['plant_name']);
											?><br/>
										</div>
							<?php
									} else {  ?>					
							
								<!-- Plant -->	
								<div class="plantboxx">	
									<input type="checkbox" id="plantbox" class="plantbox<?php echo $j; ?> plantts plant1" value="<?php echo $values['id']; ?>" name="plantrow[]"> &nbsp; <?php // print_r($planttt[1]);  ?>
									<?php
										$plant_nameee = $planttt[1];
										$plantrolename = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id='$plant_nameee'");$plantrolerow = pg_fetch_array($plantrolename);
										print_r($plantrolerow['plant_name']);
									?><br/>
								</div>
							<?php	} ?>							
							<?php  } } ?>
							
						</div>	
					</div><hr/>
					<div class="row">
						<div class="col-sm-4 unitrole">
							<h4>Select Unit</h4>									
							<div class="unitboxx">	
								<?php 
								$original_unit = $unitid[1];
								// print_r($original_unit);
								// echo "++++++++++";
								// echo "<br/>";
								foreach($FinalArray as $values) { 							
									$check_unit = explode('_',$values['id']);
									// print_r($check_unit);
									// echo "=======";
									$unit_1 = $check_unit[0]; 									
									if($unit_1 == $original_unit) { 
							?>
									<?php if($values['type'] == 'C'){ ?>
										<!-- Plant -->	
										<div class="unitboxx2">	
											<input type="checkbox" id="unitbox2" class="unitbox unitts2 unit1" value="<?php echo $original_unit; ?>" name="unitrow[]" checked disabled>&nbsp; <?php echo $unitname[1]; ?><br/>										
										</div>
							<?php	
											break;     
									} else {  ?>					
							
								<!-- Plant -->	
								<div class="unitboxx2">	
									<input type="checkbox" id="unitbox3" class="unitbox unitts2 unit1" value="<?php echo $original_unit; ?>" name="unitrow[]" disabled>&nbsp; <?php echo $unitname[1]; ?><br/><br/>
								</div>
							<?php		break;
										}   
									} 
								} 
							?>
							</div>
						</div>
						<div class="col-sm-8 plantrole">
								<h4>Select Plant</h4>
						<?php 
							$actual_unit = '2';
							// echo "<pre>";
							// print_r($FinalArray);
							// echo "</pre>";
							foreach($FinalArray as $values) { 							
								$check_unit = explode('_',$values['id']);
								$unit_1 = $check_unit[0]; 								
								if($unit_1 == $actual_unit) { 
									// echo $unit_1;
									// echo $actual_unit;
									$planttt = explode("_",$values['id']);
									// echo "<pre>";
									// print_r($planttt);
									// echo "</pre>";
						?>
							
								<?php if($values['type'] == 'C'){ ?>
									<!-- Plant -->	
									<div class="plantboxx">	
										<input type="checkbox" id="plantbox" class="plantbox<?php echo $j; ?> plant2" value="<?php echo $values['id']; ?>"  name="plantrow[]" checked > &nbsp; 
										<?php
											$plant_nameee = $planttt[1];
											$plantrolename = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id='$plant_nameee'");$plantrolerow = pg_fetch_array($plantrolename);
											print_r($plantrolerow['plant_name']);
										?>
										<br/>
									</div>											
						<?php	} else {  ?>					
						
							<!-- Plant -->	
							<div class="plantboxx">	
								<input type="checkbox" id="plantbox" class="plantbox<?php echo $j; ?> plant2" value="<?php echo $values['id']; ?>"   name="plantrow[]" > &nbsp; 
								<?php
									$plant_nameee = $planttt[1];
									$plantrolename = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id='$plant_nameee'");$plantrolerow = pg_fetch_array($plantrolename);
									print_r($plantrolerow['plant_name']);
								?><br/>
							</div>
						<?php	} ?>
						
						<?php  } } ?>
						
						</div>	
					</div>
					<br/>
					<input class="btn btn-success submitrole" type="submit" style="width:100%; border-radius:25px; padding:15px 0;" name="submit" value="Submit">
					<button type="button" class="btn back_button" onclick="window.location.href='userreturn'">Back</button>
				</form>	
			</div>
		</div>
		
		<!-- Main Content End Here Static -->
		
		
		
		
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            
@extends('footer')