@extends('header')
<style>
   
	.admin_display_bar{
		background: #3c6382;
		color: white;
	}
	.container-contact100 {
		min-height: 65vh !important;
	}
	div.ex3 {
		/* background-color: lightblue; */
		width: 100%;
		height: 75%;
		overflow: auto;
	}
</style>
	<?php 
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	?>
		
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo fa fa-user" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>					
					
					<div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!--li class="nav-item hidden-sm-down">
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                        </li-->Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">											
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
       
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <!--div class="page-wrapper"-->
        <div class="">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid"><br/>
		<link rel="stylesheet" type="text/css" href="../resources/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="../resources/css/main.css">

	<body> 
		<div class="container-contact100 ex3">
			<div class="wrap-contact100">
				<?php
					if(Session::get('email_existed')){
						$email_existed = Session::get('email_existed');						
						echo "<h3 style='background-color:#0A7B83; padding:10px; border-radius:15px; text-align:center; color:white;'>".$email_existed."</h3>";					
					}
					
				?>
				<form method="post" action="newuser" class="contact100-form validate-form">
					
					<div class="contact100-form-title" style="padding-bottom:0px !important; margin:10px 0;">
						Add New User
					</div>
					
					@if($errors->any())
					<div style="background-color:red; padding: 14px; color:white; border-radius: 10px;">		
						 {{ implode('', $errors->all(':message')) }}
					</div><br/>
					@endif
					
					{{ csrf_field() }}	
					
					<div class="wrap-input100 input100-select user_type">
						<span class="label-input100">User Type</span>
						<div> 
						 <select class="selection-2 form-control @error('designation_id') is-invalid @enderror form-style select_designation" name="designation_id" required>
							<option value="">Select User Type</option>
							<?php
																	
								$rm_designation_table = pg_query($db, "SELECT * FROM rm_usertypes");
							?>
							<?php
									while($row_designation = pg_fetch_array($rm_designation_table)){
								?>
								<option value="<?php echo $row_designation['id']; ?>"><?php echo ucwords($row_designation['type_name']); ?></option>
								<?php
									}
								?>
							</select>
						</div>
						<span class="focus-input100"></span>
					</div>
					
					<div class="wrap-input100 validate-input" data-validate="Login Name is required">
						<span class="label-input100">Login Name</span>
						<input class="input100 " type="text" name="loginname" placeholder="Enter Login Name" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="First Name is required">
						<span class="label-input100">First Name</span>
						<input class="input100" type="text" name="firstname" placeholder="Enter First Name" required>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Last Name is required">
						<span class="label-input100">Last Name</span>
						<input class="input100" type="text" name="lastname" placeholder="Enter Last Name" required>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" id="onlyemail" data-validate="E-Mail Address is required">
						<span class="label-input100">E-Mail Address</span>
						<input class="input100" id="opemail" type="email" name="email" placeholder="Enter E-Mail Address" required>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input"  id="onlymob" data-validate="Mobile Number is required">
						<span class="label-input100">Mobile Number</span>
						<input type="number" class= "input100" id="opmob" name="mobile" placeholder="+91-888-888-8888" pattern="[0-9]{10}" maxlength="12"  title="Ten digits code" required />    
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input"  id="onlypass" data-validate="Password is required">
						<span class="label-input100">Password</span>
						<input class="input100 pass_value" id="password" type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
  title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="Enter Password" required>
						
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input"  id="onlycpass" data-validate="Confirm Password is required">
						<span class="label-input100">Confirm Password</span>
						<input class="input100" id="cpassword" type="password" name="password_confirm" placeholder="Enter Confirm Password"required> 
						<span class="focus-input100"></span>
					</div>
					
					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Status</span>&nbsp;&nbsp;
						<input type="radio" name="status" value="true" required> True &nbsp;
						<input type="radio" name="status" value="false" required> False
						<span class="focus-input100"></span>
					</div>

					<div class="container-contact100-form-btn">
						<div class="wrap-contact100-form-btn">
							<div class="contact100-form-bgbtn"></div>
							<button class="contact100-form-btn final_submit">
								<span>
									Add
									<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
								</span>
							</button>
						</div>
					</div>
					<button type="button" class="btn back_button" onclick="window.location.href='userreturn'">							
						Back
					</button>
				</form>
			</div>
		</div>
    <div id="dropDownSelect1"></div>

    <!-- =====================  Raveena Bora Form End ============================== -->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            

@extends('footer')