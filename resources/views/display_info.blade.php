@extends('header')
	<?php
		//if(empty($_SESSION)){
	?>
			<!--script> window.location = 'login'; </script-->
	<?php
		//}
	?>
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
	<?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    <span>
        
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">                          
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ URL::asset($logo) }}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{ URL::asset($logo) }}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Manager</h4>
                                                <p class="text-muted">varun@rmc.com</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                   <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
                                    <li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
                                </ul>
                            </div>
                        </li>                       
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
      
        @extends('sidebar')
        
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">                       
                        <div class="dropdown float-right mr-2 hidden-sm-down">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> View Production </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> 
                                <a class="dropdown-item" href="display_info?regno=SRF_BOON">Boon Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_PSM">PSM Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L1">Packing Machine01</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L2">Packing Machine02</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L3">Packing Machine03</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_FINAL">Final Machine</a>
                            </div>
                        </div>
                    </div>
				</div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->              
               
                <!-- Row -->
				<div>
					<!-- ========================== MATCH PUNBUS REG NO WITH RMCHEMICALS PCU DEVICE ID==============================-->
					<h2>View Production</h2>
					<?php
						$regno = $_GET['regno'];						
						$query = pg_query($db, "SELECT * FROM alerts order by devicetime desc  LIMIT 2");
						//$rm_query = pg_query($db, "SELECT * FROM packet_counter_unit LIMIT 2");
				   
					?>
					<?php 
						$host = Session::get('host');
						$dbname = Session::get('dbname');
						$user = Session::get('user');
						$pass = Session::get('pass');
						// echo $host;
						// echo $dbname;
						// echo $user;
						$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
					
					?>

					<form method="post" action="display_info_fetch?regno={{$regno}}">
						@csrf
						<input type="date" name="txtStartDate" required>
						<select name="shift_hours" id="shift_hours">
								<option value="morning" name="morning_shift">Morning Shift</option>
								<option value="evening" name="evening_shift">Evening Shift</option>
								<option value="night" name="night_shift">Night Shift</option>
						</select>
						<select name="machine_select" id="machine_select">
							<option name="boon_machine" value="SRF_BOON">BOON Machine</option>
							<option name="psm_machine" value="SRF_PSM">PSM Machine</option>
							<option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
							<option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
							<option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
							<option name="final_machine" value="SRF_FINAL">Final Machine</option>
						</select>
						<input type="submit" name="search" value="Search">
					</form>

					<!--############################# DATE FILTER SEARCH ENDS --#############################---->



					 <!--############################# LIMITS --#############################---->               
					<?php 

						//############################# LIMITS STARTS --#############################---->          
						$yesterday_date = date('Y-m-d',strtotime("-1 days"));
						//echo $yesterday_date;
						$yesterday_start_time = "00:00:00";
						$yesterday_end_time = "24:00:00";

						$start_date_time_mix = $yesterday_date." ".$yesterday_start_time;
						$end_date_time_mix = $yesterday_date." ".$yesterday_end_time;
						
						
						$packet_counter_unit_display = pg_query($db, "SELECT count(*) FROM alerts WHERE regno='$regno' AND devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
						
						//############################# LIMITS ENDS --#############################---->    
						
						//$packet_counter_unit_display = pg_query($db, "SELECT count(*) FROM alerts WHERE deviceid=$id ");
						if($regno == 'SRF_BOON'){
							$count_of_today = "BOON MACHINE OVERALL PRODUCTION<br/><strong>".pg_result($packet_counter_unit_display, 0)."</strong>";
						}else if($regno == 'SRF_PSM'){
							$count_of_today = "PSM MACHINE OVERALL PRODUCTION<br/><strong>".pg_result($packet_counter_unit_display, 0)."</strong>";
						}else if($regno == 'SRF_L1'){
							$count_of_today = "PACKING MACHINE 01 OVERALL PRODUCTION<br/><strong>".pg_result($packet_counter_unit_display, 0)."</strong>";
						}else if($regno == 'SRF_L2'){
							$count_of_today = "PACKING MACHINE 02 OVERALL PRODUCTION<br/><strong>".pg_result($packet_counter_unit_display, 0)."</strong>";
						}else if($regno == 'SRF_L3'){
							$count_of_today = "PACKING MACHINE 03 OVERALL PRODUCTION<br/><strong>".pg_result($packet_counter_unit_display, 0)."</strong>";
						}else if($regno == 'SRF_FINAL'){
							$count_of_today = "FINAL MACHINE OVERALL PRODUCTION<br/><strong>".pg_result($packet_counter_unit_display, 0)."</strong>";
						}

					   
						$packet_counter_unit = pg_query($db, "SELECT * FROM alerts WHERE regno='$regno' ORDER BY devicetime desc LIMIT 30");
						// $packet_counter_unit = pg_query($db, "SELECT * FROM alerts WHERE deviceid=2020-07-11 15:44:10");
						//die($packet_counter_unit);
						if (!$packet_counter_unit) {
							echo "An error occurred.\n";
							exit;
						}        
					?>
					
					
					<div class="row">
						<!-- Column -->
						<div class="col-lg-4 col-md-6">
							<div class="card bg-info">
								<div class="card-body">
									<h4 class="card-title">Production Information</h4>
									<div id="myCarousel" class="carousel slide" data-ride="carousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="carousel-item flex-column active">
												<p class="text-white"><span class="font-bold"><?php //echo $srf_boon; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php echo $count_of_today; ?></span></h3>
											</div>
											<div class="carousel-item flex-column">
												<p class="text-white"><span class="font-bold"><?php //echo $srf_boon; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php echo $count_of_today; ?></span></h3>
											</div>
											<div class="carousel-item flex-column">
												<p class="text-white"><span class="font-bold"><?php //echo $srf_boon; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php echo $count_of_today; ?></span></h3>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>        
						<!-- Column -->
					</div>
					<!-- Row --> 
					<?php //echo $id; ?>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="card">
								<div class="card-body">
									<table class="table">
										<thead>
											<tr>
												<th>ID</th>
												<th>Machine Name</th>
												<th>Device Time</th>
												<th>Registration Number</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$i=1;
												while ($packet = pg_fetch_array($packet_counter_unit)) {
													// echo  "<pre>";
													// print_r($packet);
													// echo "</pre>";
													// die('1111');
											?> 
												<tr>                 
													<td><?php echo $i; ?></td>
													<?php 
														// die($packet[8]);
													   //$machine_info = pg_query($db, "SELECT * FROM rm_packet_counter_unit");
													   
													   $machine_info = pg_query($db, "SELECT * FROM rm_packet_counter_unit WHERE device_name = '".$packet['regno']."'");
													   
														while($machine_detail = pg_fetch_array($machine_info)){     
															// echo  "<pre>";
															// print_r($machine_detail);
															// echo "</pre>";
															// die('1111');
															$machine_name = pg_query($db, "SELECT * FROM rm_machines WHERE id = '".$machine_detail[0]."'"); 
															while($machine_name_info = pg_fetch_array($machine_name)){
																// echo  "<pre>";
																// print_r($machine_name_info);
																// echo "</pre>";
																// die('1111');
															   
													?>
													<td><?php echo $machine_name_info['machine_name']; ?></td>
													<?php
															}
														}
													   
													?>
													<td><?php echo $packet[2]; ?></td> 
													
													<!--td><?php //echo str_replace('_', ' ', $packet[1]);?></td-->				  
													<td><?php echo str_replace('_', ' ', $packet[8]);?></td>				  
												</tr>      
											<?php
													$i++;
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>					
				</div>               
			</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@extends('footer')