@extends('header')
	
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <span>
      <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>  
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="../resources/assets/images/rmchemicals_logo.png" alt="homepage" class="dark-logo" style="width: 90px;">                            
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ URL::asset($logo) }}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{ URL::asset($logo) }}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Manager</h4>
                                                <p class="text-muted">varun@rmc.com</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
                                    <li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
                                </ul>
                            </div>
                        </li>                       
                    </ul>
                </div>
            </nav>
        </header>
       
        @extends('sidebar')
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <div class="dropdown float-right mr-2 hidden-sm-down">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> View Production </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> 
                                <a class="dropdown-item" href="display_info?regno=SRF_BOON">Boon Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_PSM">PSM Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L1">Packing Machine01</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L2">Packing Machine02</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L3">Packing Machine03</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_FINAL">Combiner</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->              
                
                
                <!-- =============================================================== -->
                
                <!-- Row -->
            <div>

            <!-- =============== MATCH PUNBUS REG NO WITH RMCHEMICALS PCU DEVICE ID==============================-->
            <h2>View Production</h2>
            <?php
                //$id = $id;
                $txtStartDate = $txtStartDate;
                $final_start_time = '';
                $final_end_time = '';
                $machine_name = $machine_select;
				
			/// ############################################################################################	
			$host = Session::get('host');
			$dbname = Session::get('dbname');
			$user = Session::get('user');
			$pass = Session::get('pass');
			// echo $host;
			// echo $dbname;
			// echo $user;
			$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	
	
				//$shift_timing_query = pg_query($rm_db, "SELECT * FROM shift_hours");
				$shift_timing_query = pg_query($db, "SELECT * FROM rm_shifts");
				
				
				
				while($shift_row = pg_fetch_array($shift_timing_query)){
					// echo "<pre>";
					// print_r($shift_row);					
					// echo "</pre>";
					//dd('shift_table');
					//print_r($shift_row);
					$shift = $shift_row['shift_name'];
					//$shift_start_time = $shift_row['shift_from'];
					//$shift_end_time = $shift_row['shift_to'];
					//echo $shift_end_time;
					//echo "</pre>";
					
					//if($shift_hours == 'morning_shift' && $shift == 'morning_shift'){
					if($shift_hours == 'morning' && $shift == 'morning'){
						//echo "MORNING";
						$shift_start_time = $shift_row['shift_start_at'];
						$shift_end_time = $shift_row['shift_end_at'];
						//dd($shift_start_time);
						
						$final_start_time = $txtStartDate." ".$shift_start_time;
						$final_end_time = $txtStartDate." ".$shift_end_time;
						
					}else if($shift_hours == 'evening' && $shift == 'evening'){
						//echo "EVENING";
						$shift_start_time = $shift_row['shift_start_at'];
						$shift_end_time = $shift_row['shift_end_at'];
						
						//dd($shift_start_time);
						
						$final_start_time = $txtStartDate." ".$shift_start_time;
						$final_end_time = $txtStartDate." ".$shift_end_time;
						
					}else if($shift_hours == 'night'  && $shift == 'night'){
						//echo "NIGHT";
						$shift_start_time = $shift_row['shift_start_at'];
						$shift_end_time = $shift_row['shift_end_at'];
						
						//dd($shift_start_time);
						
						$final_start_time = $txtStartDate." ".$shift_start_time;
						$final_end_time = $txtStartDate." ".$shift_end_time;
					}
				}
			
			/// ############################################################################################	
				
                // if($shift_hours == 'morning_shift'){
                    //echo 'morning';
                    // $morning_start_time = '07:00:00';
                    // $morning_end_time = '15:00:00';
                    // $final_start_time = $txtStartDate.' '.$morning_start_time;
                    // $final_end_time = $txtStartDate.' '.$morning_end_time;
                // }else if($shift_hours == 'evening_shift'){
                    //echo 'evening';  
                    // $evening_start_time = '15:00:00';
                    // $evening_end_time = '23:00:00';
                    // $final_start_time = $txtStartDate.' '.$evening_start_time;
                    // $final_end_time = $txtStartDate.' '.$evening_end_time;
                // }else if($shift_hours == 'night_shift'){
                    //echo 'night';
                    // $night_start_time = '23:00:00';
                    // $night_end_time = '07:00:00';
                    // $final_start_time = $txtStartDate.' '.$night_start_time;
                    // $final_end_time = $txtStartDate.' '.$night_end_time;
                // }
               
                
                $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = '$machine_name'");
                
                
            ?>           
            

        <form method="post" action="display_info_fetch">
            @csrf
            <input type="date" name="txtStartDate" value="<?php echo $txtStartDate; ?>" required>
            <select name="shift_hours" id="shift_hours">
                        <?php                               
                            if($shift_hours == 'morning'){
                        ?>
                            <option value="morning" name="morning" selected>Morning Shift</option>
                            <option value="evening" name="evening">Evening Shift</option>
                            <option value="night" name="night">Night Shift</option>
                        <?php
                            }
                        ?>   
                        <?php                               
                            if($shift_hours == 'evening'){
                        ?>
                            <option value="morning" name="morning">Morning Shift</option>
                            <option value="evening" name="evening" selected>Evening Shift</option>
                            <option value="night" name="night">Night Shift</option>
                        <?php
                            }
                        ?>   
                        <?php                               
                            if($shift_hours == 'night'){
                        ?>
                            <option value="morning" name="morning">Morning Shift</option>
                            <option value="evening" name="evening">Evening Shift</option>
                            <option value="night" name="night" selected>Night Shift</option>
                        <?php
                            }
                        ?> 
                <?php
                    //}
                ?>                            
            </select>
            <select name="machine_select" id="machine_select">
            <?php 
                if($machine_name == 'SRF_BOON'){
            ?>
                    <option name="boon_machine" value="SRF_BOON" selected>BOON Machine</option>
                    <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                    <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                    <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                    <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                    <option name="final_machine" value="SRF_FINAL">Final Machine</option>
            <?php
                        // echo "SRF_BOON";
                }
            ?>
                <?php 
                    if($machine_name == 'SRF_PSM'){
                ?>
                    <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                    <option name="psm_machine" value="SRF_PSM" selected>PSM Machine</option>
                    <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                    <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                    <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                    <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                <?php
                        //echo "SRF_PSM";
                    }
                ?>
                <?php 
                    if($machine_name == 'SRF_L1'){
                ?>
                    <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                    <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                    <option name="packing_machine1" value="SRF_L1" selected>Packing Machine 1</option>
                    <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                    <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                    <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                <?php
                        //echo "SRF_L1";
                    }
                ?>
                <?php 
                    if($machine_name == 'SRF_L2'){
                ?>
                    <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                    <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                    <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                    <option name="packing_machine2" value="SRF_L2" selected>Packing Machine 2</option>
                    <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                    <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                <?php
                        //echo "SRF_L2";
                    }
                ?>
                <?php 
                    if($machine_name == 'SRF_L3'){
                ?>
                    <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                    <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                    <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                    <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                    <option name="packing_machine3" value="SRF_L3" selected>Packing Machine 3</option>
                    <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                <?php
                        //echo "SRF_L3";
                    }
                ?>
                <?php 
                    if($machine_name == 'SRF_FINAL'){
                ?>
                    <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                    <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                    <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                    <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                    <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                    <option name="final_machine" value="SRF_FINAL" selected>Final Machine</option>
                <?php
                        // echo "SRF_FINAL";
                    }
                ?>  
            </select>
            <!--input type="hidden" name="device_id" value="{{$machine_name}}"-->
            <input type="submit" name="search" value="Search">
        </form>

		<!--############################# DATE FILTER SEARCH ENDS --#############################---->
		<!--############################# LIMITS --#############################---->               
    <?php    
        // echo $final_start_time;
		// echo $final_end_time;
				
		//$packet_counter_unit_counts = pg_query($db, "SELECT count(*) FROM alerts  WHERE regno='$machine_name' AND devicetime BETWEEN '2020-07-24 15:00:00' AND '2020-07-24 23:00:00' ");
		
		$packet_counter_unit_counts = pg_query($db, "SELECT count(*) FROM alerts  WHERE regno='$machine_name' AND devicetime BETWEEN '".$final_start_time."' and '".$final_end_time."'");
		
		// echo "<pre>";
		
		// print_r($packet_counter_unit_counts);
		// echo "</pre>";
		
		//$packet_counter_unit_counts = pg_query($db, "SELECT count(*) FROM alerts  WHERE regno='$machine_name' AND devicetime BETWEEN '$final_start_time' and '$final_end_time'");
		//print_r($packet_counter_unit_counts);

        $packet_counter_unit = pg_query($db, "SELECT * FROM alerts  WHERE regno='$machine_name' AND devicetime BETWEEN '".$final_start_time."' and '".$final_end_time."' LIMIT 30");
		
		//$packet_counter_unit = pg_query($db, "SELECT * FROM alerts  WHERE regno='$machine_name' AND devicetime BETWEEN '$final_start_time' AND '$final_end_time' LIMIT 30");

        //$packet_counter_unit = pg_query($db, "SELECT * FROM alerts  WHERE deviceid=$id AND devicetime BETWEEN '2020-07-14 07:00:00' AND '2020-07-14 15:00:00'");

        //die($packet_counter_unit);
        if (!$packet_counter_unit) {
            echo "An error occurred.\n";
            exit;
        }   
        
    ?>
    
    <?php //echo $id; ?>
    <div>
        <?php
            $morning = '';
            $evening = '';
            $night = '';
			
            if($shift_hours == 'morning'){
                //echo 'morning';
                $morning = pg_result($packet_counter_unit_counts, 0);
            }else if($shift_hours == 'evening'){
                //echo 'evening';  
                $evening = pg_result($packet_counter_unit_counts, 0);
            }else if($shift_hours == 'night'){
                //echo 'night';
                $night = pg_result($packet_counter_unit_counts, 0);
            }

        ?>
        <!--##########################################################################################################-->
        

            <div class="row">
                <!-- Column -->
                <?php                               
                    if($shift_hours == 'morning'){
						//dd('morning');
                ?>
                <!--div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Morning Shift Production</h4>
                            <div class="text-right">
                                <!--h2 class="font-light mb-0"><i class="ti-arrow-up text-success"></i><?php //echo pg_result($query, 0); ?></h2-->
                                <!--h2 class="font-light mb-0" style="text-align:center;"><i class="text-success"></i><?php //echo $morning; ?></h2>
                                <h2 style="text-align:center;"><span class="text-muted">Total Production</span></h2>
                            </div><br/><br/>
                            <!--span class="text-success">80%</span-->
                            <!--div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div-->
				
				
				
				<!-- Column -->
				<div class="col-lg-4 col-md-6">
					<div class="card bg-info-6">
						<div class="card-body">
							<h4 class="card-title">Morning Shift Production</h4>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<p class="text-white"><span class="font-bold"><?php echo $morning; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"><?php echo $morning; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"><?php echo $morning; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>        
				<!-- Column -->
				
				
				
                <?php
                    }else if($shift_hours == 'evening'){
						//dd('evening');
                ?>
                <!-- Column -->
                <!-- Column -->
				<div class="col-lg-4 col-md-6">
					<div class="card bg-info-2">
						<div class="card-body">
							<h4 class="card-title">Evening Shift Production</h4>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<p class="text-white"><span class="font-bold"><?php echo $evening; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"><?php echo $evening; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"><?php echo $evening; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>        
				<!-- Column -->
                <?php
                    }else if($shift_hours == 'night'){
                ?>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-4 col-md-6">
					<div class="card bg-info-3">
						<div class="card-body">
							<h4 class="card-title">Night Shift Production</h4>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<p class="text-white"><span class="font-bold"><?php echo $night; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"><?php echo $night; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"><?php echo $night; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold"><?php //echo $count_of_today; ?></span></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>        
				<!-- Column -->
                <?php
                    }
                ?>
                <!-- Column -->               
            </div>
            <!-- Row --> 
        
        <!--##########################################################################################################-->
    </div>
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="card">
				<div class="card-body">
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Machine Name</th>
								<th>Device Time</th>
								<th>Registration Number</th>
							</tr>
						</thead>
						<tbody>
							<?php			
								//if()
								$i=1;
								while ($packet = pg_fetch_array($packet_counter_unit)) {	
									// echo "<pre>";
									// print_r($packet);
									// echo "</pre>";    
									// die('111');	
									
							?>
									<tr>                 
										<td><?php echo $i; ?></td>
										<?php 
											$machine_info = pg_query($db, "SELECT * FROM rm_packet_counter_unit WHERE device_name = '".$packet[8]."'");
											while($machine_detail = pg_fetch_array($machine_info)){
												// echo "<pre>";
												// print_r($machine_detail[2]);
												// echo "</pre>";    
												// die('111');											
											//$machine_name = pg_query($db, "SELECT * FROM rm_machines WHERE machine_name = '".$machine_detail[2]."'"); 
											$machine_device_name = $machine_detail[2];
											$machine_name = pg_query($db, "SELECT * FROM rm_machines WHERE machine_regno = '$machine_device_name'"); 
											//dd($machine_name);
											while($machine_name_info = pg_fetch_array($machine_name)){ 
												// echo "<pre>";
                                                // print_r($machine_name_info);
                                                // echo "</pre>";    
											    // die('111');	
										?>
										<td><?php echo $machine_name_info['machine_name']; ?></td>
										<?php
												}
											}
										   
										?>
										<td><?php echo $packet[2]; ?></td>  
										<td><?php echo str_replace('_', ' ', $packet[8]);?></td>
										
									</tr>      
							<?php
									$i++;
									
								}
							?>
						</tbody>
					</table> 
				</div>
			</div>
		</div>
	</div>
</div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@extends('footer')