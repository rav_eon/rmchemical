@extends('header')
<style>
	.container-contact100 {
		min-height: 65vh !important;
	}

	.admin_display_bar{
		background: #3c6382;
		color: white;
	}
</style>
	<?php 
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");	
	?>
		
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo fa fa-user" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>
					<div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
						Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">											
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <!--div class="page-wrapper"-->
        <div class="">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid"><br/> 
			<link rel="stylesheet" type="text/css" href="../resources/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../resources/css/main.css">

	<body> 
	<div class="container-contact100">
		<div class="wrap-contact100">
			<form method="post" action="machineproductskuupdate" class="contact100-form validate-form">
				@csrf
				<span class="contact100-form-title">
					Update Machine Product SKU Mapping
				</span>	
				@if($errors->any())
					<div style="background-color:red; padding: 14px; color:white; border-radius: 10px;">	
						{{ implode('', $errors->all(':message')) }}
					</div><br/>
				@endif
				<?php
					$machineproductid = Session::get('machineproductsku_idd');
					//echo "<br/>";
					$machineproductplantid = Session::get('plant_idd');
					//echo "<br/>";
					$machineproductmachineid = Session::get('machine_idd');
					//echo "<br/>";
					$machineproductproid = Session::get('product_idd');
					//echo "<br/>";
					$machineproductskuid = Session::get('sku_idd');		
					//echo "<br/>";
					$machineproductrpmm = Session::get('rpmm');	
					// echo $new_plant_row['id'];
					// echo $machineproductid;
				?>
				<input type="hidden" name="machineproductid" value="<?php echo $machineproductid; ?>">
				<div class="wrap-input100 input100-select">
					<span class="label-input100">Plant Name</span>
					<div>
					 <select class="selection-2 productskuplant" name="plant" required>
						<option value="">Choose Plant</option>
						<?php
							$new_plant = pg_query($db, "SELECT * FROM rm_plants");
							while($new_plant_row = pg_fetch_array($new_plant)){													
						?>
						<option value="<?php echo $new_plant_row['id']; ?>" <?php echo($new_plant_row['id'] == $machineproductplantid)?'selected':''; ?>>
							<?php echo $new_plant_row['plant_name']; ?>
						</option>
							<?php
								}
							?>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 input100-select">
					<span class="label-input100">Machine Name</span>
					<div>
						<?php // echo $machineproductmachineid; ?>
						<select class="selection-2 productskumachine" id="productskumachinee" name="machine" required>
							<!--option class="plantmachineee" value="">Please select Plant to choose Machine</option-->
							<?php
								$new_machine = pg_query($db, "SELECT * FROM rm_machines WHERE plant_id = $machineproductplantid");
								while($new_machine_row = pg_fetch_array($new_machine)){													
							?>
							<option value="<?php echo $new_machine_row['id']; ?>" <?php echo($new_machine_row['id'] == $machineproductmachineid)?'selected':''; ?>>
								<?php echo $new_machine_row['machine_name']; ?>
							</option>
							<?php
								}
							?>
						</select>
					</div>
					<input type="hidden" id="prodvalue" value="1">
					<input type="hidden" class="promac" value="<?php echo $machineproductmachineid; ?>">
					<span class="focus-input100"></span>
				</div>					
				<div class="wrap-input100 input100-select proo">
					<span class="label-input100">Product Name</span>
					<div>
						<select class="selection-2 productskupro" name="product" required>
							<!--option value="">Choose Product</option-->
							<?php
							   $new_product = pg_query($db, "SELECT * FROM rm_products");
							   while($new_product_row = pg_fetch_array($new_product)){
							?>
							<option value="<?php echo $new_product_row['id']; ?>" <?php echo($new_product_row['id'] == $machineproductproid)?'selected':''; ?>>
							   <?php echo $new_product_row['product_name']; ?>
							</option>
							<?php
								}
							?>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>					
				<div class="wrap-input100 input100-select proosku">
					<span class="label-input100">SKU Name</span>
					<div>
						<?php 
							// $machineproductskuid; 
							// echo "=====<br/>";
							// $new_sku = pg_query($db, "SELECT * FROM rm_skus WHERE product_id=$machineproductskuid");
							// while($new_sku_row = pg_fetch_array($new_sku)){
								// echo $new_sku_row['id'];
								// echo "<br/>";
							// }
						?>
						
						<select class="selection-2 productskuu" name="skuname" required>
							<!--option value="" class="skuidd">Select Product to choose SKU</option-->
							<?php
								$new_sku = pg_query($db, "SELECT * FROM rm_skus WHERE product_id=$machineproductproid");
								while($new_sku_row = pg_fetch_array($new_sku)){
							?>
							<option value="<?php echo $new_sku_row['id']; ?>" <?php echo ($new_sku_row['id'] == $machineproductskuid)?'selected':''; ?>>
							   <?php echo $new_sku_row['sku_name']; ?>
							</option>
							<?php
								}
							?>
						</select>
						<input type="hidden" class="prodskuvalue" value="1">
						<input type="hidden" class="prosku" value="<?php echo $machineproductskuid; ?>">
					</div>
					<span class="focus-input100"></span>
				</div>					
				<div class="wrap-input100 input100-select">
					<span class="label-input100">RPM</span>
					<div>
						<input type="number" class="rpmval" name="rpm" pattern="[0-9]" min="1" max="9999" placeholder="Please input RPM value" title="number only" style="width:100%;" value="<?php echo $machineproductrpmm; ?>" required>
					</div>
					<span class="focus-input100"></span>
				</div>					
				
				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn formsubmission">
							<span>
								Update
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
						</button>
					</div>
				</div>
				<button type="button" class="btn back_button" onclick="window.location.href='machineproductskureturn'">
					Back
				</button>
			</form>
		</div>
	</div>
	<div id="dropDownSelect1"></div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
            
@extends('footer')