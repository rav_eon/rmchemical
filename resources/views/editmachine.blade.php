@extends('header')
<style>
   .container-contact100 {
		min-height: 65vh !important;
	}
	div.ex3 {
		/* background-color: lightblue; */
		width: 100%;
		height: 75%;
		overflow: auto;
	}

	.admin_display_bar{
		background: #3c6382;
		color: white;
	}
</style>
	<?php 
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	
	?>
		
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo fa fa-user" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>
					<div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
						Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">											
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
      
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <!--div class="page-wrapper"-->
        <div class="">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <br/> 
			<link rel="stylesheet" type="text/css" href="../resources/css/select2.min.css">
			<link rel="stylesheet" type="text/css" href="../resources/css/main.css">


	<body> 
	<div class="container-contact100 ex3">
		<div class="wrap-contact100">
            <form method="post" action="machineupdate" class="contact100-form validate-form">
                <span class="contact100-form-title">
                    Edit Machine Details
                </span>
				@csrf
				@if($errors->any())
					<div style="background-color:red; margin-bottom:10px; padding: 14px; color:white; border-radius: 10px;">	
						{{ implode('', $errors->all(':message')) }}
					</div>
				@endif
                <div class="wrap-input100 input100-select">
                    <span class="label-input100">Unit Name</span>
					<div>
						<select class="selection-2" name="unit_code" id="machineunitid" required>
							<?php
								$unit_id = Session::get('unit_id');
								//echo $unit_id;
								$new_plant_unit = pg_query($db, "SELECT * FROM rm_units");
								while($new_unit_row = pg_fetch_array($new_plant_unit)){
							?>							
							<option value="<?php echo $new_unit_row['id']; ?>" <?php echo($new_unit_row['id'] == $unit_id ? 'selected' : ''); ?>>
								<?php echo $new_unit_row['unit_name']; ?>
							</option>
							<?php
								}
							?>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>
                

                <div class="wrap-input100 input100-select">
                    <span class="label-input100">Plant Name</span>
					<div>
                     <select class="selection-2" name="plant_code" id="machineplantid" required>
                        <?php
							$unit_id = Session::get('unit_id');
							$plant_id = Session::get('plant_id');
							// echo $unit_id;
							$new_plant_unit = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id=$unit_id");
							while($edi_plant_row = pg_fetch_array($new_plant_unit)){
						?>							
						<option value="<?php echo $edi_plant_row['id']; ?>" <?php echo($edi_plant_row['id'] == $plant_id ? 'selected' : ''); ?>>
							<?php echo $edi_plant_row['plant_name']; ?>
						</option>
						<?php
							}
						?>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

              

                <div class="wrap-input100 input100-select">
                    <span class="label-input100">Cascade Name</span>
					<div>
                     <select class="selection-2" name="cascade_code" id="machinecascadeid" required>
                        <?php
							$cascade_id = Session::get('cascade_id');
							// echo $unit_id;
							$cascade_unit = pg_query($db, "SELECT * FROM rm_cascades");
							while($edit_cascade_row = pg_fetch_array($cascade_unit)){
						?>							
							<option value="<?php echo $edit_cascade_row['id']; ?>" <?php echo($edit_cascade_row['id'] == $cascade_id ? 'selected' : ''); ?>>
								<?php echo $edit_cascade_row['cascade_name']; ?>
							</option>
						<?php
							}
						?>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>
                
                <div class="wrap-input100 input100-select">
                    <span class="label-input100">Machine Type</span>
					<div>
						
						<select class="selection-2" name="machine_type" required>
							<?php
								$machine_type_id = Session::get('machine_type');
								echo $machine_type_id;
								$edit_machine_type = pg_query($db, "SELECT * FROM rm_machinetypes");
								while($edit_machinetype_row = pg_fetch_array($edit_machine_type)){															
							?>							
							<option value="<?php echo $edit_machinetype_row['id']; ?>" <?php echo($edit_machinetype_row['id'] == $machine_type_id ? 'selected' : ''); ?>>
								<?php echo $edit_machinetype_row['machine_type_name']; ?>
							</option>
							<?php								
								}
							?>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>
				
                <div class="wrap-input100 validate-input" data-validate="Machine Reg Number is required">
                    <span class="label-input100">Machine Reg Number</span>
                    <input class="input100" type="hidden" name="machine_id" value="{{ Session::get('machine_id') }}" id="machine_id">
                    <input class="input100" type="text" name="machine_regno" value="{{ Session::get('machine_regno') }}" id="machine_reg" required>
                    <span class="focus-input100"></span>
                </div>
                <div class="wrap-input100 validate-input" data-validate="Machine Name is required">
                    <span class="label-input100">Machine Name</span>
                    <input class="input100" type="text" name="machine_name" value="{{ Session::get('machine_name') }}" id="machine_name" required>
                    <span class="focus-input100"></span>
                </div>
				<div class="wrap-input100 validate-input" data-validate="Breakdown Time Limit is required">
                    <span class="label-input100">Breakdown Limit</span>
                    <input class="input100" type="number" name="breakdown_limit" title="Enter Number Only" value="{{ Session::get('breakdown_limit') }}"required>
                    <span class="focus-input100"></span>
                </div>
				<!-- <div class="wrap-input100 validate-input" data-validate="BCT/CLD Time Limit is required">
                    <span class="label-input100">BCT/CLD Limit</span>
                    <input class="input100" type="number" name="bct_cld_limit" title="Enter Number Only" placeholder="Enter Breakdown Limit" required>
                    <span class="focus-input100"></span>
                </div> -->
				<div class="wrap-input100 validate-input" data-validate="Machine Reg Number is required">
                    <span class="label-input100">IMEI Number</span>
                    <input class="input100" type="number" name="imei_number" value="{{ Session::get('imei') }}" required>
                    <span class="focus-input100"></span>
                </div>
				<div class="wrap-input100 validate-input">
                    <span class="label-input100">Status</span><br/>
					<?php
						//echo Session::get('status');
						if(Session::get('status') == 't'){
					?>
						<input type="radio" name="status" value="true" checked>True &nbsp;
						<input type="radio" name="status" value="false">False
					<?php							
						}else{
					?>
						<input type="radio" name="status" value="true" >True &nbsp;
						<input type="radio" name="status" value="false" checked>False
					<?php
						}
					?>
                    <span class="focus-input100"></span>
                </div>
                <div class="container-contact100-form-btn">
                    <div class="wrap-contact100-form-btn">
                        <div class="contact100-form-bgbtn"></div>
                        <button class="contact100-form-btn">
                            <span>
                                Update
                                <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                </div>
				<button type="button" class="btn back_button" onclick="window.location.href='machinereturn'">							
						Back
					</button>
            </form>
		</div>
	</div>
    <div id="dropDownSelect1"></div>

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            

@extends('footer')