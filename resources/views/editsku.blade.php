@extends('header')
<style>
	.container-contact100 {
		min-height: 65vh !important;
	}

	.admin_display_bar{
		background: #3c6382;
		color: white;
	}
	
	#rpm1, #rpm2, #rpm3{
		-moz-appearance: none;
		-webkit-appearance: none;
		-o-appearance: none;
		outline: none;
		content: none;
		margin-left: 5px;
	}

	#rpm1:before, #rpm2:before, #rpm3:before {
		font-family: "FontAwesome";
		content: "\f00c";
		font-size: 25px;
		color: transparent !important;
		background: #fff;
		width: 25px;
		height: 25px;
		border: 2px solid black;
		margin-right: 5px;
	}

	#rpm1:checked:before, #rpm2:checked:before, #rpm3:checked:before {
		color: black !important;
	}
</style>
	
	<?php 
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	
	?>
		
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo fa fa-user" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>
					<div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!--li class="nav-item hidden-sm-down">
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                        </li-->Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">											
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
       
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <!--div class="page-wrapper"-->
        <div class="">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid"><br/> 
		<link rel="stylesheet" type="text/css" href="../resources/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="../resources/css/main.css">

	<body> 
		<div class="container-contact100">
			<div class="wrap-contact100">
				<form method="post" action="skuupdate" class="contact100-form validate-form">
					<span class="contact100-form-title">
						Edit SKU
					</span>
					@csrf
					@if($errors->any())
						<div style="background-color:red; margin-bottom:10px; padding: 14px; color:white; border-radius: 10px;">	
							{{ implode('', $errors->all(':message')) }}
						</div>
					@endif
					
					<div class="wrap-input100 input100-select">
						<span class="label-input100">Product Name</span>
						<div>
							<input type="hidden" value="{{ Session::get('sku_id') }}" name="sku_id">
							<select class="selection-2" name="product_id" required>
								<?php
									$product_id = Session::get('product_id');
									$new_product = pg_query($db, "SELECT * FROM rm_products");
									while($new_product_row = pg_fetch_array($new_product)){										
								?>
								<option value="<?php echo $new_product_row['id']; ?>" <?php echo $new_product_row['id'] == $product_id?'selected':''; ?>>
									<?php echo $new_product_row['product_name']; ?>
								</option>
								<?php
									}
								?>
							</select>
						</div>
						<span class="focus-input100"></span>
					</div>
					
					
					<div class="wrap-input100 input100-select">
						<span class="label-input100">Packing Type</span>
						<div>
							<select class="selection-2" name="packing_type" required>
								<?php
									$packing_type = Session::get('packing_type_id');
									$new_packing_type = pg_query($db, "SELECT * FROM rm_skupackingtypes");
									while($new_packing_row = pg_fetch_array($new_packing_type)){															
								?>
								<option value="<?php echo $new_packing_row['id']; ?>" <?php echo $new_packing_row['id'] == $packing_type ? 'selected' : ''; ?>>
									<?php echo $new_packing_row['type_name']; ?>
								</option>
								<?php
									}
								?>
							</select>
						</div>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Pack Size Unit</span>
						<div>				
							<select class="selection-2" name="packing_unit" required>
								<?php
									$pack_size_unit = Session::get('pack_size_unit');
									$new_packing_unit = pg_query($db, "SELECT * FROM rm_skupackingunits");
									while($new_packingunit_row = pg_fetch_array($new_packing_unit)){
								?>
								<option value="<?php echo $new_packingunit_row['unit_code']; ?>" <?php echo $new_packingunit_row['unit_code'] == $pack_size_unit?'selected':''; ?>>
									<?php echo $new_packingunit_row['unit_name']; ?>
								</option>
								<?php
									}
								?>
							</select>
						</div>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Cascade Name is ">
						<span class="label-input100">SKU Code</span>
						<input class="input100" type="text" name="sku_code" value="{{ Session::get('sku_code') }} " required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input" >
						<span class="label-input100">SKU Name</span>
						<input class="input100" type="text" name="sku_name" value="{{ Session::get('sku_name') }} " required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Pack Size</span>
						<input class="input100" type="text" name="pack_size" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ Session::get('pack_size') }} " required />
						<span class="focus-input100"></span>
					</div>				
					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Unit Per CLD</span>
						<input class="input100" type="text" name="unit_per_cld" value="{{ Session::get('unit_per_cld') }} " oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input" >
						<span class="label-input100">SKU Type</span>
						<input class="input100" type="text" name="sku_type" value="{{ Session::get('sku_type') }} " required>
						<span class="focus-input100"></span>
					</div>
					<?php						
						$rpm_min = Session::get('rpm_min');
						//echo "<br/>";
						$rpm_max = Session::get('rpm_max');
						//echo "<br/>";
						$rpm_default = Session::get('rpm_default');
					?>
					<div class="wrap-input100 validate-input" required>	
						<span class="label-input100">Default RPM</span>
						<input class="input100 rpm_default" type="text" name="rpm_default" placeholder="Default RPM" title="Please enter Only numeric value"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $rpm_default; ?>" required>
						<span class="focus-input100"></span>					
					</div>	
					
					<div class="wrap-input100 validate-input" required>
						<span class="label-input100">RPM Interval</span>
						<input class="input100" type="text" name="rpm_interval" placeholder="Enter RPM Interval" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" title="Please enter Only numeric value" value="{{ Session::get('rpm_interval') }} " required>
						<span class="focus-input100"></span>
					</div>	
					<div class="wrap-input100 validate-input">
						<span class="label-input100">Status</span><br/>
						<?php
							//echo Session::get('status');
							if(Session::get('status') == 't'){
						?>
							<input type="radio" name="status" value="true" checked>True &nbsp;
							<input type="radio" name="status" value="false">False
						<?php							
							}else{
						?>
							<input type="radio" name="status" value="true" >True &nbsp;
							<input type="radio" name="status" value="false" checked>False
						<?php
							}
						?>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Remarks</span>
						<input class="input100" type="text" name="remark" value="{{ Session::get('remark') }} ">
						<span class="focus-input100"></span>
					</div>
					
					<div class="container-contact100-form-btn">
						<div class="wrap-contact100-form-btn">
							<div class="contact100-form-bgbtn"></div>
							<button class="contact100-form-btn">
								<span>
									Update
									<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
								</span>
							</button>
						</div>
					</div>
					<button type="button" class="btn back_button" onclick="window.location.href='skureturn'">							
							Back
						</button>
				</form>
			</div>
		</div>
	<div id="dropDownSelect1"></div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            
@extends('footer')