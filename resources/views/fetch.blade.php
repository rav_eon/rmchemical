@extends('header')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

    <style>
        .topbar{
            margin-top:-25px;
        }        
    </style>
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
	<?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    <span>
        
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ URL::asset($logo) }}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{ URL::asset($logo) }}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Manager</h4>
                                                <p class="text-muted">varun@rmc.com</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
                                    <li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
                                </ul>
                            </div>
                        </li>                       
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @extends('sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                       
                        <div class="dropdown float-right mr-2 hidden-sm-down">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> View Production </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> 
                                <a class="dropdown-item" href="display_info?regno=SRF_BOON">Boon Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_PSM">PSM Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L1">Packing Machine01</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L2">Packing Machine02</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L3">Packing Machine03</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_FINAL">Final Machine</a>
                            </div>
                        </div>                        
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->   
                
                <!-- Row -->
            <div>
            <!-- =============================================================== -->
           
            <?php				
				$host = Session::get('host');
				$dbname = Session::get('dbname');
				$user = Session::get('user');
				$pass = Session::get('pass');
				// echo $host;
				// echo $dbname;
				// echo $user;
				$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");

                $morning = '';
                $evening = '';
                $night = '';
                $select_option='';
               // $dev_time = '';
                if($machine_name == 'ALL'){
                    //echo "ok";
                    $select_option = '<option name="all_machine" value="ALL">ALL Machine</option>
                    <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                    <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                    <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                    <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                    <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                    <option name="final_machine" value="SRF_FINAL">Final Machine</option>';
                }

                $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = '$machine_name' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
				
				 // $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = '$machine_name' LIMIT 3");
				 // while($sss = pg_fetch_array($query)){
					 // echo "<pre>";
					 // print_r($sss);
					 // echo "</pre>";
				 // }
				
				$shift_result = pg_result($query, 0);
				//echo $shift_result;
                
                // if($shift_start_time == '07:00:00' && $shift_end_time == '15:00:00'){
                    // $morning = pg_result($query, 0);
                    // //echo $morning;
                // }else if($shift_start_time == '15:00:00' && $shift_end_time == '23:00:00'){
                    // echo "Evening";
                    // $evening = pg_result($query, 0);
                    // //echo $evening;
                // }else if($shift_start_time == '23:00:00' && $shift_end_time == '07:00:00'){
                    // $night = pg_result($query, 0);
                    // //echo $night
                // }     
                             
            ?>
            <!-- =============================================================== -->
            <?php //echo $id; ?>

    
            <h2>View Production</h2>
            <!-- ################################ FORM STARTS ############################################## -->
            
            
            <!-- Row -->
            <div>
                <form method="post" action="fetch">
                    @csrf
                    <input type="date" name="txtStartDate" value="<?php echo $start_date; ?>" required>
                    <select name="shift_hours" id="shift_hours">
                        <?php                               
                            if($shift_time == 'morning'){
                        ?>
                            <option value="morning" name="morning" selected>Morning Shift</option>
                            <option value="evening" name="evening">Evening Shift</option>
                            <option value="night" name="night">Night Shift</option>
                        <?php
                            }
                        ?>   
                        <?php                               
                            if($shift_time == 'evening'){
                        ?>
                            <option value="morning" name="morning">Morning Shift</option>
                            <option value="evening" name="evening" selected>Evening Shift</option>
                            <option value="night" name="night">Night Shift</option>
                        <?php
                            }
                        ?>   
                        <?php                               
                            if($shift_time == 'night'){
                        ?>
                            <option value="morning" name="morning">Morning Shift</option>
                            <option value="evening" name="evening">Evening Shift</option>
                            <option value="night" name="night" selected>Night Shift</option>
                        <?php
                            }
                        ?>                            
                    </select>

                    <select name="machine_select" id="machine_select">
                        <?php 
                            if($machine_name == 'SRF_BOON'){

                        ?>
                            <option name="all_machine" value="ALL">ALL Machine</option>
                            <option name="boon_machine" value="SRF_BOON" selected>BOON Machine</option>
                            <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                            <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                            <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                            <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                            <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                        <?php
                                //echo "SRF_BOON";
                            }
                        ?>
                        <?php 
                            if($machine_name == 'SRF_PSM'){
                        ?>
                            <option name="all_machine" value="ALL">ALL Machine</option>
                            <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                            <option name="psm_machine" value="SRF_PSM" selected>PSM Machine</option>
                            <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                            <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                            <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                            <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                        <?php
                                //echo "SRF_PSM";
                            }
                        ?>
                        <?php 
                            if($machine_name == 'SRF_L1'){
                        ?>
                            <option name="all_machine" value="ALL">ALL Machine</option>
                            <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                            <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                            <option name="packing_machine1" value="SRF_L1" selected>Packing Machine 1</option>
                            <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                            <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                            <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                        <?php
                                //echo "SRF_L1";
                            }
                        ?>
                        <?php 
                            if($machine_name == 'SRF_L2'){
                        ?>
                            <option name="all_machine" value="ALL">ALL Machine</option>
                            <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                            <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                            <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                            <option name="packing_machine2" value="SRF_L2" selected>Packing Machine 2</option>
                            <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                            <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                        <?php
                                //echo "SRF_L2";
                            }
                        ?>
                        <?php 
                            if($machine_name == 'SRF_L3'){
                        ?>
                            <option name="all_machine" value="ALL">ALL Machine</option>
                            <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                            <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                            <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                            <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                            <option name="packing_machine3" value="SRF_L3" selected>Packing Machine 3</option>
                            <option name="final_machine" value="SRF_FINAL">Final Machine</option>
                        <?php
                                //echo "SRF_L3";
                            }
                        ?>
                        <?php 
                            if($machine_name == 'SRF_FINAL'){
                        ?>
                            <option name="all_machine" value="ALL">ALL Machine</option>
                            <option name="boon_machine" value="SRF_BOON">BOON Machine</option>
                            <option name="psm_machine" value="SRF_PSM">PSM Machine</option>
                            <option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
                            <option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
                            <option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
                            <option name="final_machine" value="SRF_FINAL" selected>Final Machine</option>
                        <?php
                                //echo "SRF_FINAlllllllllllllL";
                            } else{
                        ?> 
                        
                        <?php    
                                echo $select_option;
                            // if($machine_name = "ALL"){ 
                            //   echo "OK";
                           }
                        
                        ?> 
                                              
                    </select>
                    <input type="submit" name="search" value="Search">
                </form>
            </div>
            <!-- ############################################# FORM ENDS ####################################################### -->
            <div class="row">                              
				<div class="col-lg-4 col-md-6">
					<div class="card bg-info-3">
						<div class="card-body">
							<h4 class="card-title">Unit Title</h4>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<p class="text-white"><span class="font-bold"></span></p>
										<h3 class="text-white font-light"><?php echo "Unit 1"; ?></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"></span></p>
										<h3 class="text-white font-light"><?php echo "Unit 1"; ?></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"></span></p>
										<h3 class="text-white font-light"><?php echo "Unit 1"; ?></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="card bg-info-2">
						<div class="card-body">
							<h4 class="card-title">Plant Title</h4>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<p class="text-white"><span class="font-bold"></span></p>
										<h3 class="text-white font-light"><?php echo "Surf Excel"; ?></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"></span></p>
										<h3 class="text-white font-light"><?php echo "Surf Excel"; ?></h3>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"></span></p>
										<h3 class="text-white font-light"><?php echo "Surf Excel"; ?></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="card bg-info-6">
						<div class="card-body">
							<h4 class="card-title">Machine Title</h4>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<p class="text-white"><span class="font-bold"></span></p>
										<h2 class="text-white font-light"><?php //echo $machine_name; ?>
											<?php                                 
												echo str_replace('_', ' ', $machine_name)." Machine";
											?>
											
										</h2>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"></span></p>
										<h2 class="text-white font-light">
											<?php                                 
												echo str_replace('_', ' ', $machine_name)." Machine";
											?>
										</h2>
									</div>
									<div class="carousel-item flex-column">
										<p class="text-white"><span class="font-bold"></span></p>
										<h2 class="text-white font-light">
											<?php                                 
												echo str_replace('_', ' ', $machine_name)." Machine";
											?>
										</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>                
                
            </div>
            <!-- #################################### Machine Performance ############################################### -->
			<?php 
                // ################################ CHART REPRESENTATION ############################################## -->
                $chart_representation = pg_query($db, "SELECT regno, count(*) as chart FROM alerts WHERE devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix' GROUP BY regno");		
				
                
            ?>
			<?php
				$chart_representation_count = pg_query($db, "SELECT regno, count(*) as chart FROM alerts WHERE devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix' GROUP BY regno");	
				$count_chart = 0;
				while($chart_value_result = pg_fetch_array($chart_representation_count)){
					"['".$chart_value_result["regno"]."',".$chart_value_result["chart"]."],";
					$count_chart++;
				}
				$count_chart;
				if($count_chart > 0){
			?>
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<h3>Graphical Representation</h3>
						<div id="piechart" style="width:900px height:500px"></div>
					</div>
				</div><br/>
			<?php	
				}else{
			?>				
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="card no_graphical_data">
							<div class="card-body">
								<h4 class="card-title" style="color:black; text-align:center;">Graphical Representation</h4>					
								<p class="text-white"><span class="font-bold"></span></p>
								<h3 style="color:red; text-align:center;">NO PRODUCTION FOR</h3>
								<h5 style="text-align:center;">This Machine/Shift</h5>
							</div>
						</div>					
					</div>					
				</div><br/>
				
			<?php	
				}
			?>
			
            
            <!-- #################################### Machine Performance ############################################### -->
            <div class="row">
                <!-- Column -->
                <?php 
                    if($machine_name == 'ALL'){ 
                        $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_BOON' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
                        $srf_boon = pg_result($query, 0);
                        //echo $srf_boon;
                        //echo "<br/>";
    
                        $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_PSM' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
                        $srf_psm = pg_result($query, 0);
                        //echo $srf_psm;
                        //echo "<br/>";
    
                        $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_L1' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
                        $srf_l1 = pg_result($query, 0);
                        //echo $srf_l1;
                        //echo "<br/>";
    
                        $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_L2' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
                        $srf_l2 = pg_result($query, 0);
                       // echo $srf_l2;
                        //echo "<br/>";
    
                        $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_L3' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
                        $srf_l3 = pg_result($query, 0);
                        //echo $srf_l3;
                        //echo "<br/>";
    
                        $query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_FINAL' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
                        $srf_final = pg_result($query, 0);
                        //echo $srf_final;  
                
                ?>
                        <div class="col-lg-4 col-md-6">
                            <div class="card bg-info">
                                <div class="card-body">
									<h4 class="card-title">Production Infomation</h4>
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="carousel-item flex-column active">
												<p class="text-white"><span class="font-bold"><?php echo $srf_boon; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF BOON PRODUCTION</span></h3>
											</div>
											<div class="carousel-item flex-column">
												<p class="text-white"><span class="font-bold"><?php echo $srf_boon; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF BOON PRODUCTION</span></h3>
											</div>
											<div class="carousel-item flex-column">
												<p class="text-white"><span class="font-bold"><?php echo $srf_boon; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF BOON PRODUCTION</span></h3>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6">
                            <div class="card bg-info-2">
                                <div class="card-body">
                                    <h4 class="card-title">PSM Machine Production</h4>
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="carousel-item flex-column active">
												<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
												<p class="text-white"><span class="font-bold"><?php echo $srf_psm; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF PSM PRODUCTION</span></h3>
												<!-- <div class="text-white mt-3">
													<i>- john doe</i>
												</div> -->
											</div>
											<div class="carousel-item flex-column">
												<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
												<p class="text-white"><span class="font-bold"><?php echo $srf_psm; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF PSM PRODUCTION</span></h3>
												<!-- <div class="text-white mt-3">
													<i>- john doe</i>
												</div> -->
											</div>
											<div class="carousel-item flex-column">
												<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
												<p class="text-white"><span class="font-bold"><?php echo $srf_psm; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF PSM PRODUCTION</span></h3>
												<!-- <div class="text-white mt-3">
													<i>- john doe</i>
												</div> -->
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="card bg-info-3">
                                <div class="card-body">
                                    <h4 class="card-title">Packing Machine 1 Production</h4>
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l1; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L1 PRODUCTION</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l1; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L1 PRODUCTION</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l1; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L1 PRODUCTION</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
								</div>
							</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="card bg-info-4">
                                <div class="card-body">
                                    <h4 class="card-title">Packing Machine 2 Production</h4>
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l2; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L2 Machine Production</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l2; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L2 Machine Production</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l2; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L2 Machine Production</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
								</div>
							</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="card bg-info-5">
                                <div class="card-body">
                                    <h4 class="card-title">Packing Machine 3 Production</h4>
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l3; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L3 PRODUCTION</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l3; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L3 PRODUCTION</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php echo $srf_l3; ?></span></p>
										<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L3 PRODUCTION</span></h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
								</div>
							</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="card bg-info-6">
                                <div class="card-body">
                                    <h4 class="card-title">Final Machine Production</h4>
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="carousel-item flex-column active">
												<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
												<p class="text-white"><span class="font-bold"><?php echo $srf_final; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF FINAL PRODUCTION</span></h3>
												<!-- <div class="text-white mt-3">
													<i>- john doe</i>
												</div> -->
											</div>
											<div class="carousel-item flex-column">
												<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
												<p class="text-white"><span class="font-bold"><?php echo $srf_final; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF FINAL PRODUCTION</span></h3>
												<!-- <div class="text-white mt-3">
													<i>- john doe</i>
												</div> -->
											</div>
											<div class="carousel-item flex-column">
												<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
												<p class="text-white"><span class="font-bold"><?php echo $srf_final; ?></span></p>
												<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF FINAL PRODUCTION</span></h3>
												<!-- <div class="text-white mt-3">
													<i>- john doe</i>
												</div> -->
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                <?php
                    }else{
                       // die("other category");
                       //echo "other category";

                    //if($shift_hours == 'morning_shift'){
					if($shift_time == 'morning_shift'){
                ?>
                <div class="col-lg-3 col-md-6">
                    <div class="card bg-info-2">
                        <div class="card-body">
                            <h4 class="card-title">Morning Shift Production</h4>
                           							
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $morning; ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $morning; ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $morning; ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>										
										</h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
								</div>
							</div>							
                        </div>
                    </div>
                </div>
                <?php
                    //}else if($shift_hours == 'evening_shift'){
					}else if($shift_time == 'evening_shift'){	
                ?>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card bg-info-3">
                        <div class="card-body">
                            <h4 class="card-title">Evening Shift Production</h4>
                           							
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $evening; ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $evening; ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $evening; ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
								</div>
							</div>							
                        </div>
                    </div>
                </div>
                <?php
                    //}else if($shift_hours == 'night_shift'){
					}else if($shift_time == 'night_shift'){
                ?>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card bg-info-4">
                        <div class="card-body">
                            <h4 class="card-title">Night Shift Production</h4>
                           							
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="carousel-item flex-column active">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $night ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $night ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
									<div class="carousel-item flex-column">
										<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
										<p class="text-white"><span class="font-bold"><?php //echo $night ?><?php echo $shift_result; ?></span></p>
										<h3 class="text-white font-light">Total Production<br>
											<span class="font-bold">
												 <?php                                 
													echo str_replace('_', ' ', $machine_name)." Machine";
												?>
											</span>
										</h3>
										<!-- <div class="text-white mt-3">
											<i>- john doe</i>
										</div> -->
									</div>
								</div>
							</div>							
                        </div>
                    </div>
                </div>
                <?php
                       }
                    }
                    //}
                ?>
                <!-- Column -->
            </div>
            <!-- Row -->                  
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
       
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <!-- ===================PIE CHART ------------>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				
    <script>
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart(){
            var data = google.visualization.arrayToDataTable([
                ['Reg', 'chart'],
                <?php
					$count_chart = 1;
                    while($chart_value = pg_fetch_array($chart_representation)){
                        echo "['".$chart_value["regno"]."',".$chart_value["chart"]."],";
						$count_chart++;
                    }
					//echo $count_chart;
                ?>                    
            ]);

            var options = {
                title: 'Machine Performance',
                hAxis: {title: 'Machine Name',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 20}
            };
            //var chart = new google.visualization.BubbleChart(document.getElementById('piechart'));
            var chart = new google.visualization.AreaChart(document.getElementById('piechart'));
            //var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
   
@extends('footer')