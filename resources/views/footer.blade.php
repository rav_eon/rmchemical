	<?php
		if( Session::get('designation_id') == 1){
			echo "<style>.admin_display_footer {
				left: 0px !important; 
				text-align:center;
			}</style>";
		}	
	?>		
	<!-- ============================================================== -->
	<!-- footer -->
	<!-- ============================================================== -->

	<div id="footer">
		<div id="cyear"></div>
	</div>
		<!-- ============================================================== -->
		<!-- End footer -->
		<!-- ============================================================== -->
	</div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->   

    <!-- ============================================================== -->
    
	
	<?php 
		// dd($user_type_id = Session::get('user_type_id'));
		// echo "================operatorid=".$user_type_id; 
	?>
	<script src="../resources/assets/plugins/jquery/jquery.min.js"></script>
	<!-- ==================== on screen keyboard start ========================================== -->	
	<!--script src="https://cdn.jsdelivr.net/npm/simple-keyboard@latest/build/index.js"></script-->
	<!--script src="https://code.jquery.com/jquery.min.js"></script-->
	<!--script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script-->
	<?php
		if( Session::get('designation_id') == 5){	
	?>
	<script type="text/javascript" src="../resources/keyboard/main.js"></script>
	<link rel="stylesheet" type="text/css" href="../resources/keyboard/styles.css">
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).keyboard({
				// language: 'us:English, arabic:العَرَبِيَّة, vietnamese:tiếng Việt, hindi:हिन्दी',
				language: 'us:English',
				enterKey: function () {
					alert('Hey there! This is a callback function example.');
				},
				keyboardPosition: 'bottom',
				directEnter: false,
				showSelectedLanguage: true
			});
		});
	</script>
	<?php
		}
	?>
	<!-- keyboard form start -->	
	<!-- ==================== on screen keyboard end ========================== -->
	
	
	
	
	
	
	
	
    <!-- Bootstrap tether Core JavaScript -->
    <!--script src="../resources/assets/plugins/bootstrap/js/popper.min.js"></script-->
    <script src="../resources/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../resources/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../resources/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../resources/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../resources/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="../resources/js/custom.min.js"></script>
    <script src="../resources/assets/plugins/toast-master/js/jquery.toast.js"></script>
    <!-- Chart JS -->
    <!--script src="../resources/js/dashboard1.js"></script-->
    <script src="../resources/js/toastr.js"></script>
	<script src="../resources/js/select2.min.js"></script>
	<!-- ============================================================== -->
	<!-- Style switcher -->
	<!-- ============================================================== -->		
	
	<script src="../resources/js/tpicker.js"></script>
	<script src="../resources/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
	<!-- Calendar JavaScript -->
	<script src="../resources/assets/plugins/moment/moment.js"></script>
	<script src='../resources/assets/plugins/calendar/dist/fullcalendar.min.js'></script>
	<script src="../resources/assets/plugins/calendar/dist/jquery.fullcalendar.js"></script>
	<script src="../resources/assets/plugins/calendar/dist/cal-init.js"></script>	
	<script>
		var dteNow = new Date();
		var intYear = '&copy; '+dteNow.getFullYear()+' RM Chemicals. All Rights Reserved.';
		$('#cyear').html(intYear);
	</script>
    <script>
        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
		<!-- =====================  Raveena Bora Form End ============================== -->
    </script>

	<?php $img_location = "../resources/assets/images/"; ?>  

    <script>
        $(document).ready(function(){
            $('.unit1').on('click', function(){
                //$('.plant_info').css('display','block');
                $('.plant_info').toggle();
                $('.pcu_info').css('display','none');
                //alert('unit1');
				$('.pcu_info2').css('display','none');
				$('.pcu_info').css('display','none');
            });

            $('.unit2').on('click', function(){
                //alert('NO PLANT AVAILABLE ON UNIT 2');
				$('.pcu_info2').css('display','none');
				$('.pcu_info').css('display','none');
            });

            $('.plant_detail_1').on('click', function(){
                $('.pcu_info').toggle();
                //$('.pcu_info').css('display','block');
            });

            $('.unit1_live_data').on('click', function(){
                $('.unit_plant_live_data').css('display','block');
                // $('.unit_plant_live_data').css('display','block');
                // $('.machines_live_data').css('display','none');
            });

            $('.unit2_live_data').on('click', function(){
                alert('No Plant Available on this UNIT');
            });

            $('.plant_data1').on('click', function(){
                $('.machines_live_data').css('display','block');
            });			

			$('.excel_plant').on('click', function(){
				$('.pcu_info').css('display','block');
				$('.pcu_info2').css('display','none');
			});

			$('.rin_plant').on('click', function(){
				$('.pcu_info2').css('display','block');
				$('.pcu_info').css('display','none');
			});

			

			//$('.select_designation').on('click', function(){

			$("select.select_designation").change(function(){
				var selectedDesignation = $(this).children("option:selected").val();
				//alert("You have selected the Designation - " + selectedDesignation);
				if(selectedDesignation == 2 || selectedDesignation == 3 || selectedDesignation == 4 || selectedDesignation == 5 || selectedDesignation == 6){
					$('.shift_column').css('display','block');
					$('.select_shift').prop('required', true);
				}else if(selectedDesignation == 1){
					$('.shift_column').css('display','none');
					$('.select_shift').prop('required', false);
				}
			});
			//############## Admin Section #################
			$('.admin_unit').on('click', function(){	
				$('.unit_info').css('display','block');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');				

				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_unit').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.admin_plant').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','block');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.admin_cascade').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','block');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');				
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.admin_machine').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','block');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			

			$('.admin_products').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','block');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.add_button').css('display','block');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			

			$('.admin_shifts').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				//$('.shift_info').css('display','none');
				$('.shift_info').css('display','block');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.add_button').css('display','block');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_shifts').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			

			$('.admin_pcu_health').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.pcu_status_info').css('display','block');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.add_button').css('display','none');
				$('.jumbo_panel').css('display','none');				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_pcu_health').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	

				// $.ajax({
					// type:'get',
					// url:'pcuhealthlive',
					// dataType:'json',
					// success:function(data){
						// var live_health = '';
						// var info = '';
						// var connection = '<img src="<?php echo $img_location."green.png"; ?>" width="5%">';
						// var k=1;

						// $.each(data, function(i, health){
							// console.log(data[i].pkt_cnt);
							// var battery = '';
							// var bat_status = data[i].batt_chg_cnt;
							// var i = '';	
							// if(bat_status == 1){
								// var battery = '<img src="<?php echo $img_location."batt_2.ico"; ?>">';	
							// }else{
								// var battery = '<img src="<?php echo $img_location."lowbattery.png"; ?>" style="width:20%;">';	
							// }

							// if(data[i].pkt_cnt == -10){
								// data[i].pkt_cnt = 0;
							// }

							// var count = i+1;
							// live_health += '<tr><td>'+count+'</td><td>'+data[i].bus_id+'</td><td>'+data[i].imei+'</td><td>'+data[i].reg_no+'</td><td>'+data[i].pkt_cnt+'</td><td>'+data[i].sats_view+'</td><td>'+data[i].gsm_rssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+data[i].batt_volt+'</td><td>'+data[i].main_volt+'</td><td>'+connection+'</td></tr>';
							// i++;
						// });
							// $('.live_health_info').empty().append(live_health);	
					// }
				// });				

				// $.ajax({
					// type:'get',
					// url:'pcudatainfo',
					// data:{unitid: 0, plantid: 0, status: 0},
					// dataType:'json',
					// success:function(data){
						// alert(data);	
						// var connected = '';
						// var info = '';
						// var battery = '';

						// var connectt = '<img src="<?php echo $img_location."green.png"; ?>" width="5%">';
						// $.each(data, function(i, connection){
							// var batstatus = data[i].battchgcnt;
							// var pktcnt = data[i].pktcnt;
							// var busid = data[i].busid;
							// var imei = data[i].imei;
							// var regno = data[i].regno;
							// var satsview = data[i].satsview;
							// var gsmrssi = data[i].gsmrssi;
							// var battvolt = data[i].battvolt;
							// var mainvolt = data[i].mainvolt;
							// alert(batstatus);
							// alert(pktcnt);
							// alert(busid);
							// var i = '';								

							// if(busid == '' && imei == ''){
								// var battery = '';
								// var count = '';
								// connectt = '';
								// var countter = i+1;
								// alert('not id');
								// connected += '<tr><td>'+countter+'</td><td>'+busid+'</td><td>'+imei+'</td><td>'+regno+'</td><td>'+pktcnt+'</td><td>'+satsview+'</td><td>'+gsmrssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+battvolt+'</td><td>'+mainvolt+'</td><td>'+connectt+'</td></tr>';
								// i++;
							// }else{
								// if(batstatus == 1){
									// var battery = '<img src="<?php echo $img_location."batt_2.ico"; ?>">';
								// }else{
									// var battery = '<img src="<?php echo $img_location."lowbattery.png"; ?>" style="width:20%;">';
								// }

								// if(pktcnt == -10){
									// pktcnt = 0;
								// }
								// if(i == 0){
									// i = 1;
								// }
								// alert('yes id');
								// var countter = i+1;
								// countter = count+1;
								// connected += '<tr><td>'+countter+'</td><td>'+busid+'</td><td>'+imei+'</td><td>'+regno+'</td><td>'+pktcnt+'</td><td>'+satsview+'</td><td>'+gsmrssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+battvolt+'</td><td>'+mainvolt+'</td><td>'+connectt+'</td></tr>';
								// i++;
							// }
							// alert(connected);
						// });
						// $('.live_health_info').empty().append(connected);
					// }
				// });
			});
			

			$('.admin_users').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.pcu_status_info').css('display','block');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','block');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			

			$('.admin_skus').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','block');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.admin_machine_product_skus').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','block');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});			

			$('.admin_sku_unit').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','block');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.admin_sku_type').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','block');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			

			$('.admin_reasons').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','block');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			

			$('.admin_subreasons').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','block');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
			});

			$('.admin_plant_pincode').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');	
				$('.plant_pincode_info').css('display','block');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.admin_bct').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','block');
				$('.product_plant_mapping_info').css('display','none');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_product_mapping').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});	
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.admin_product_mapping').on('click', function(){
				$('.unit_info').css('display','none');
				$('.plant_info').css('display','none');
				$('.plant_pincode_info').css('display','none');
				$('.cascade_info').css('display','none');
				$('.machine_info').css('display','none');
				$('.product_info').css('display','none');
				$('.product_mixer_bct_info').css('display','none');
				$('.product_plant_mapping_info').css('display','block');
				$('.shift_info').css('display','none');
				$('.sku_info').css('display','none');
				$('.machineproductskusection_info').css('display','none');
				$('.sku_units').css('display','none');
				$('.sku_types').css('display','none');
				$('.breakdown_reason').css('display','none');
				$('.breakdown_subreason').css('display','none');
				$('.users_info').css('display','none');
				$('.admin_panel').css('display','none');
				$('.jumbo_panel').css('display','none');
				$('.pcu_status_info').css('display','none');
				$('.admin_pcu_health').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});				

				$('.admin_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_cascade').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_products').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_shifts').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_users').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_machine_product_skus').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_plant_pincode').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_bct').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_product_mapping').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.admin_sku_unit').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_sku_type').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_reasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.admin_subreasons').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.opt_select2').on('click', function(){
				$(this).css('display','none');
			});
			

			//############## Operator Section #################
			//$('.input_panel').css({'box-shadow':'0 4px 4px rgba(255,0,0,0.15)'});
			// $('.input_output').css('display','none');
			// $('.production_graphics').css('display','none');
			// $('.downtime_reason_box').css('display','none');

			$('.input_panel').on('click', function(){
				$('.input_output').css('display','block');
				$('.production_graphics').css('display','none');
				$('.downtime_reason_box').css('display','none');
				$('.input_panel').css({'box-shadow':'0 4px 4px rgba(255,0,0,0.15)'});
				$('.production_data').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.downtime_reason').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.production_data').on('click', function(){
				$('.input_output').css('display','none');
				$('.production_graphics').css('display','block');
				$('.downtime_reason_box').css('display','none');
				$('.input_panel').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.production_data').css({'box-shadow':'0 4px 4px rgba(255,0,0,0.15)'});
				$('.downtime_reason').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
			});

			$('.downtime_reason').on('click', function(){
				$('.input_output').css('display','none');
				$('.production_graphics').css('display','none');
				$('.downtime_reason_box').css('display','block');
				$('.input_panel').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.production_data').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.downtime_reason').css({'box-shadow':'0 4px 4px rgba(255,0,0,0.15)'});
				$("#empModal").modal("show");				
				var dt = new Date();
				var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
				$('.downrealtime').val(time);
				// $('.timeflag').val(0);	
				// alert('uuu');	
				// $('#empModal').modal({
					// backdrop: 'static',
					// keyboard: false
				// });
				
				
				$('.breakdownmodalclose').on('click', function(){
					$("#areyousuretoclose").modal("show");
				});
				
				// $('.yesamsuretoclose').on('click', function(){
					// $('#empModal').css('display','none');
					// $('#empModal').modal("hide");
					// var operatorstarttime = $('.downrealtime').val();
					// var operatorendtime = $('.endtimevalue').val();
					// var operatorendtime = '15:00:00';
					// $.ajax({
						// url:'autoopreason/'+operatorstarttime+'/'+operatorendtime,
						// type:'get',
						// data:{startoperator:operatorstarttime},
						// success:function(data){
							// alert(data);
						// }
					// });
				// });				
			});	
			

			$('.no_access').on('click', function(){
				alert('Please select SKU');
			});

			$('.unitModal').on('click', function(){
				//alert($(this).val());
				var unitValue = $(this).val();
				$('.unit_value').val(unitValue);
			});	

			$('.plantModal').on('click', function(){
				//alert($(this).val());
				var plantValue = $(this).val();
				$('.plant_value').val(plantValue);
			});	

			$('.plantpinModal').on('click', function(){
				//alert($(this).val());
				var plantValue = $(this).val();
				$('.plantpin_value').val(plantValue);
			});	

			$('.cascadeModal').on('click', function(){
				//alert($(this).val());
				var cascadeValue = $(this).val();
				$('.cascade_value').val(cascadeValue);
			});	

			$('.machineModal').on('click', function(){
				//alert($(this).val());
				var machineValue = $(this).val();
				$('.machine_value').val(machineValue);
			});	

			$('.productModal').on('click', function(){
				//alert($(this).val());
				var productValue = $(this).val();
				$('.product_value').val(productValue);
			});	

			$('.productmappingModal').on('click', function(){
				//alert($(this).val());
				var productValue = $(this).val();
				$('.plant_map_value').val(productValue);
			});	

			$('.productbctModal').on('click', function(){
				//alert($(this).val());
				var productValue = $(this).val();
				$('.product_bct_value').val(productValue);
			});	

			$('.skuModal').on('click', function(){
				//alert($(this).val());
				var skuValue = $(this).val();
				$('.sku_value').val(skuValue);
			});			

			$('.skuunitModal').on('click', function(){
				//alert($(this).val());
				var skuValue = $(this).val();
				$('.skuunit_value').val(skuValue);
			});

			$('.skutypeModal').on('click', function(){
				//alert($(this).val());
				var skuValue = $(this).val();
				$('.skutype_value').val(skuValue);
			});				

			$('.breakdownModal').on('click', function(){
				//alert($(this).val());
				var breakValue = $(this).val();
				$('.breakdown_value').val(breakValue);
			});	

			$('.subbreakdownModal').on('click', function(){
				//alert($(this).val());
				var subbreakValue = $(this).val();
				$('.subbreakdown_value').val(subbreakValue);
			});	

			$('.userModal').on('click', function(){
				//alert($(this).val());
				var userValue = $(this).val();
				$('.user_value').val(userValue);
			});				

			// $('.sku_row').on('click', function(){
				// var row_val = $(this).val();
			// });	

			$('.final_submit').on('click', function(){
				// alert($('#password').val());
				// alert($('#cpassword').val());
				var pass = $('#password').val();
				var cpass = $('#cpassword').val();
				if(pass != cpass){
					alert('Confirm doesn\'t match');
					return false;
				}
			});	
			
			$('.rpm_show2').on('click', function(){
				$('.add_rpm').toggle();
			});			

			$("select.sku_row").change(function(){
				var skuval = $(this).children("option:selected").val();
				//alert("You have selected the sku row - " + skuval);	
				$('.sku_value_info').val(skuval);
				// if($(skuval).val() != ''){
					// alert('1');
					// $('select.sku_row').on('change', function(){
						// alert('submit');
					// });
				// }

				//$.post("footer.php", {"locationID": skuval});
				//e.preventDefault();
				var id = skuval;
				//alert(id);
				  $.ajax({	
					type: 'get',					
					url: 'skuvalueeeeeee',

					dataType: 'json',

					data: {id : skuval},

					success: function (data) {	
						//alert(data);
						//var data = JSON.stringify(data);
						console.log(data[0].def);
						console.log(data[0].min);
						console.log(data[0].max);
						$('#sku_rpm_default').val(data[0].def);
						$('#sku_rpm_minimum').val(data[0].min);
						$('#sku_rpm_maximum').val(data[0].max);
						$('#sku_rpm_final').val(data[0].finalval);
						// $('#sku_rpm_maximum').val(data[0].max);
						
						// var def_val = data[0].def;
						var def_val = data[0].finalval;
						// alert(def_val);
						var spin = '';
						// spin += '<option value="'+data[i].subbreakdown_id+'">'+data[i].subbreakdown_reason+'</option>';
						// $('#sku_rpm_val').empty().append(options);
						
						$.each(data, function (i, spinner) {
							var spin_val = data[i].spinner;	
							//alert(def_val);
							spin += '<option value="'+data[i].spinner+'">'+data[i].spinner+'</option>';	
						});

						// alert(data[0].def);
						$('#sku_rpm2').val(data[0].def);		
						$('#sku_rpm_val').empty().append(spin);		

						// alert(def_val);
						// $("#sku_rpm_val option[value=def_val]").prop("selected", "selected");
						$('#sku_rpm_val option[value="'+def_val+'"]').attr("selected",true);
						$('.rpm_button').prop('disabled', false);						

						// alert(h);
						// $('#sku_rpm_val option[value=def_val]').attr('selected','selected');
					}
				});	
			});

			$('.rpm_downreason').on('click', function(){
				$('.rpm_downreason_form').toggle();
			});

			$('.breakdown_reasonns').on('click', function(){				
				$('.breakdown_reasonns').change(function(){
					var reason_id = $(this).val();
					// alert(reason_id);
					$.ajax({
						type:'get',
						url:'opreasonid/'+reason_id,
						dataType:'json',
						//data:{id: reason_id},
						success:function(r){
							// alert();
							// alert(r.length);
							//alert(r[0].subbreakdown_id);
							var data =r;
							// alert(data.length);
							var options = '';
							if(data.length==''){
								//alert('0');
								$('.sub_reason').css('display','none');
								$('#sub_breakdown_reason').prop("required", false);
								$('#sub_breakdown_reason').val(0);
								$('#sub_breakdown_reason').empty().append(options);
							}else{
								//alert('1');
								for(var i=0; i<data.length; i++){
									// alert(data[0].subbreakdown_id);
									options += '<option value="'+data[i].subbreakdown_id+'">'+data[i].subbreakdown_reason+'</option>';
								}
								$('.sub_reason').css('display','block');			
								$('#sub_breakdown_reason').prop("required", true);
								$('#sub_breakdown_reason').empty().append(options);
							}
						}						
					});
				});
			});

			// $('#breakdown_reason').on('click', function(){				
				// $('#breakdown_reason').change(function(){
					// var reason_id = $(this).val();
					// alert(reason_id);
					// $.ajax({
						// type:'get',
						// url:'opreasonid/'+reason_id,
						// dataType:'json',
						// data:{id: reason_id},
						// success:function(r){
							// alert();
							// alert(r.length);
							// alert(r[0].subbreakdown_id);
							// var data =r;
							// alert(data.length);
							// var options = '';
							// if(data.length==''){
								// alert('0');
								// $('.sub_reason').css('display','none');
								// $('#sub_breakdown_reason').prop("required", false);
								// $('#sub_breakdown_reason').val(0);
								// $('#sub_breakdown_reason').empty().append(options);
							// }else{
								// alert('1');
								// for(var i=0; i<data.length; i++){
									// alert(data[0].subbreakdown_id);
									// options += '<option value="'+data[i].subbreakdown_id+'">'+data[i].subbreakdown_reason+'</option>';
								// }
								// $('.sub_reason').css('display','block');			
								// $('#sub_breakdown_reason').prop("required", true);
								// $('#sub_breakdown_reason').empty().append(options);
							// }
						// }						
					// });
				// });
			// });

			

			// $('#sku_rpm_val').on('click', function(){
			$('#sku_row').on('click', function(){
				$('#rpm_info').css("display","block");
				$('.standard').css("display","block");
			});			

			$("select.pcu_unit").change(function(){	
				var selectedunit = $(this).children("option:selected").val();
				//alert("You have selected the Unit - " + selectedunit);	
				//alert(selectedunit);
				// alert(plantval);
				$.ajax({
					type:'get',
					url:'pcu_unit/'+selectedunit,
					dataType:'json',
					success:function(data){
						var plant = '';
						plant += '<option value="0" selected="selected"> All </option>';
						$.each(data, function(i, unit_plant_info){
							plant += '<option value="'+data[i].plant_id+'">'+data[i].plant_name+'</option>';
						});	
						var plantval = $('.pcu_plant').val();
						// if(plantval > 0){
							// alert(plantval);							
							// var newplant = plantval.empty();
							// alert(newplant);
						// }						

						$('.pcu_plant').empty().append(plant);	
					}
				});
				$('.pcuunitid').val(selectedunit);				
			});

			
			$('select.pcu_plant').change(function(){				
				var pcuplantid = $(this).children("option:selected").val();
				$('.pcuplantid').val(pcuplantid);
			});			

			$('.pcudatainfo').on('click', function(){
				// alert($('.pcuunitid').val());
				// alert($('.pcuplantid').val());
				// alert($('.pcu_status').val());

				var unitid = $('.pcuunitid').val();
				var plantid = $('.pcuplantid').val();
				var status = $('.pcu_status').val();
				
				if ($("select.pcu_plant")[0].selectedIndex <= 0) {
					// alert("Not selected");
					plantid = '0';
				// }else{
					// alert("Selected");
				}
				if(unitid == ''){
					unitid = 0;
					//alert(unitid);
				}
				if(plantid == ''){
					plantid = 0;
					//alert(unitid);
				}
				// console.log(unitid);
				// console.log(plantid);
				// console.log(status);
				$.ajax({
					type:'get',
					url:'pcudatainfo',
					data:{unitid: unitid, plantid: plantid, status: status},
					dataType:'json',
					success:function(data){
						// alert(data);	
						var connected = '';
						var info = '';	
						var battery = '';						

						$.each(data, function(i, connection){
							var batstatus = data[i].battchgcnt;
							var pktcnt = data[i].pktcnt;
							var busid = data[i].busid;
							var imei = data[i].imei;
							var regno = data[i].regno;
							var satsview = data[i].satsview;
							var gsmrssi = data[i].gsmrssi;
							var battvolt = data[i].battvolt;
							var datetime = data[i].datetime;
							var mainvolt = data[i].mainvolt;

							// alert(batstatus);
							// alert(pktcnt);
							// alert(busid);
							// var i = '';							

							if(busid == '' && imei == ''){
								var battery = '';
								// var count = '';
								connectt = '';
								var countter = i+1;
								// alert('not id');
								// connected += '<tr><td>'+countter+'</td><td>'+busid+'</td><td>'+imei+'</td><td>'+regno+'</td><td>'+pktcnt+'</td><td>'+satsview+'</td><td>'+gsmrssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+battvolt+'</td><td>'+mainvolt+'</td><td>'+connectt+'</td></tr>';

								connected += '<tr><td>'+countter+'</td><td>'+busid+'</td><td>'+imei+'</td><td>'+regno+'</td><td>'+pktcnt+'</td><td>'+gsmrssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+battvolt+'</td><td>'+mainvolt+'</td><td>'+connectt+'</td></tr>';

								i++;

							}else{
								if(batstatus == 1){
									var connectt = '<img src="<?php echo $img_location."green.png"; ?>" width="5%">';
									var info = '';
								}else{
									var connectt = '<img src="<?php echo $img_location."red.png"; ?>" width="5%">'+datetime;
									var info = '<img src="<?php echo $img_location."batt_disconn.ico"; ?>" width="5%">';	
								}

								if(battvolt < 2){
									var battery = '<img src="<?php echo $img_location."batt_3.ico"; ?>">';
								}else{
									if(batstatus==1){
										var battery = '<img src="<?php echo $img_location."batt_2.ico"; ?>">';	
									}else{
										var battery = '<img src="<?php echo $img_location."batt_4.ico"; ?>">';	
									}
								}

								if(pktcnt == -10){
									pktcnt = 0;
								}
								// if(i == 0){
									// i = 1;
								// }
								// alert('yes id');
								var countter = i+1;
								// countter = count+1;
								connected += '<tr><td>'+countter+'</td><td>'+busid+'</td><td>'+imei+'</td><td>'+regno+'</td><td>'+pktcnt+'</td><td>'+gsmrssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+battvolt+'</td><td>'+mainvolt+'</td><td>'+connectt+'</td></tr>';	

								i++;					
							}
							// alert(connected);
						});
						$('.live_health_info').empty().append(connected);
					}
				});
			});		

			$('select.pcu_status').change(function(){
				var pcu_status = $(this).children("option:selected").val();
				// alert(pcu_status);
				if(pcu_status == 'connected'){
					// alert('connected');
					// $.ajax({
						// type:'get',
						// url:'onlyconnectedpcu',
						// dataType:'json',
						// success:function(data){
							// var connected = '';
							// var info = '';	
							// var battery = '';
							// var connectt = '<img src="<?php echo $img_location."green.png"; ?>" width="5%">';
							// $.each(data, function(i, connection){
								// var bat_status = data[i].batt_chg_cnt;
								// var i = '';							
								// if(bat_status == 1){
									// var battery = '<img src="<?php echo $img_location."batt_2.ico"; ?>">';	
								// }else{
									// var battery = '<img src="<?php echo $img_location."lowbattery.png"; ?>" style="width:20%;">';	
								// }

								// if(data[i].pkt_cnt == -10){
									// data[i].pkt_cnt = 0;
								// }							

								// connected += '<tr><td>'+i+'</td><td>'+data[i].bus_id+'</td><td>'+data[i].imei+'</td><td>'+data[i].reg_no+'</td><td>'+data[i].pkt_cnt+'</td><td>'+data[i].sats_view+'</td><td>'+data[i].gsm_rssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+data[i].batt_volt+'</td><td>'+data[i].main_volt+'</td><td>'+connectt+'</td></tr>';

								// i++;
							// });
							// $('.live_health_info').empty().append(connected);
						// }
					// });

				}else if(pcu_status == 'disconnected'){
					// alert('disconnected');
					// $.ajax({
						// type:'get',
						// url:'onlydisconnectedpcu',
						// dataType:'json',
						// success:function(data){
							// var connected = '';
							// var info = '';	
							// var battery = '';
							// var connectt = '<img src="<?php echo $img_location."green.png"; ?>" width="5%">';
							// $.each(data, function(i, connection){
								// var bat_status = data[i].batt_chg_cnt;
								// var i = '';							
								// if(bat_status == 1){
									// var battery = '<img src="<?php echo $img_location."batt_2.ico"; ?>">';	
								// }else{
									// var battery = '<img src="<?php echo $img_location."lowbattery.png"; ?>" style="width:20%;">';	
								// }
								// if(data[i].pkt_cnt == -10){
									// data[i].pkt_cnt = 0;
								// }								

								// connected += '<tr><td>'+i+'</td><td>'+data[i].bus_id+'</td><td>'+data[i].imei+'</td><td>'+data[i].reg_no+'</td><td>'+data[i].pkt_cnt+'</td><td>'+data[i].sats_view+'</td><td>'+data[i].gsm_rssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+data[i].batt_volt+'</td><td>'+data[i].main_volt+'</td><td>'+connectt+'</td></tr>';

								// i++;
							// });
							// $('.live_health_info').empty().append(connected);
						// }
					// });
				}else{
					// alert('all');
					// $.ajax({
					// type:'get',
					// url:'pcuhealthlive',
					// dataType:'json',
					// success:function(data){
						// var live_health = '';
						// var info = '';		
						// var connection = '<img src="<?php echo $img_location."green.png"; ?>" width="5%">';
						// var k=1;					

						// $.each(data, function(i, health){
							// console.log(data[i].pkt_cnt);
							// var battery = '';
							// var bat_status = data[i].batt_chg_cnt;
							// var i = '';							
							// if(bat_status == 1){
								// var battery = '<img src="<?php echo $img_location."batt_2.ico"; ?>">';	
							// }else{
								// var battery = '<img src="<?php echo $img_location."lowbattery.png"; ?>" style="width:20%;">';	
							// }
							// if(data[i].pkt_cnt == -10){
								// data[i].pkt_cnt = 0;
							// }

							// live_health += '<tr><td>'+i+'</td><td>'+data[i].bus_id+'</td><td>'+data[i].imei+'</td><td>'+data[i].reg_no+'</td><td>'+data[i].pkt_cnt+'</td><td>'+data[i].sats_view+'</td><td>'+data[i].gsm_rssi+'</td><td>'+info+'</td><td style="text-align:center;">'+battery+'</td><td>'+data[i].batt_volt+'</td><td>'+data[i].main_volt+'</td><td>'+connection+'</td></tr>';

							// i++;

						// });
							// $('.live_health_info').empty().append(live_health);
						// }
					// });
				}
			});

			// $('select.pcu_plant').change(function(){
				// var plantid = $(this).children("option:selected").val();
				// alert("Pland id"+plant_id);
				// $.ajax({
					// type:'get',
					// url:'pcu_unit/'+plantid,
					// dataType:'json',
					// success:function(data){
						// var machine ='';
						// $.each(data, function(i, plant_machine_info){
							// machine += '<option val="'+data[i]+'">'
						// });
					// }
				// });
			// });		

			// $('.pcu_unit').on('click', function(){
				// $.ajax({
					// type:'get',
					// url:'pcu_unit',
					// dataType:'json',
					// success:function(data){

						// var unit = '';

						// $.each(data, function(i, unit_id){

							// var unit_val = data[i].unit_id;

							// unit += '<option value="'+data[i].unit_id+'">'+data[i].unit_name+'</option>';

							// alert(unit);

						// });						

						// $('#sku_rpm_val').empty().html(unit);	

					// }

				// });

			// });
			$('.showinfo').on('click', function(){
				// alert('heee');
				$('#usersection').toggle();
			});

        });				

    </script>
	<?php
		$user_type_id = Session::get('user_type_id');
		// echo $user_type_id;
		// dd($user_type_id);
		if($user_type_id == 5){
	?>
		<script>
			// dd($user_type_id);
			$('.emailadddiv').css('display','none');
			$('.mobileadddiv').css('display','none');
			$('.passadddiv').css('display','none');
			$('.cpassadddiv').css('display','none');
			$('.emailadd').prop('required',false);
			$('.mobileadd').prop('required',false);
			$('.passadd').prop('required',false);
			$('.cpassadd').prop('required',false);
		</script>
	<?php
		}
	?>
    <?php		
		// echo "<script>console.log(skuval)</script>";
		if(Session::get('admin_unit')){
			echo "<script>
				$('.admin_unit').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.unit_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_plant')){
			echo "<script>
				$('.admin_plant').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.plant_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_bct')){
			echo "<script>
				$('.admin_bct').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.product_mixer_bct_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_plant_pincode')){
			echo "<script>
				$('.admin_plant_pincode').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.plant_pincode_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_product_mapping')){
			echo "<script>
				$('.admin_product_mapping').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.product_plant_mapping_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_cascade')){
			echo "<script>
				$('.admin_cascade').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.cascade_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_machine')){
			echo "<script>
				$('.admin_machine').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.machine_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_products')){
			echo "<script>
				$('.admin_products').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.product_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_users')){
			echo "<script>
				$('.admin_users').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.users_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_skus')){
			echo "<script>
				$('.admin_skus').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.sku_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_machine_product_skus')){
			echo "<script>
				$('.admin_machine_product_skus').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.machineproductskusection_info').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_reasons')){
			echo "<script>
				$('.admin_reasons').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.breakdown_reason').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_subreasons')){
			echo "<script>
				$('.admin_subreasons').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.breakdown_subreason').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_sku_unit')){
			echo "<script>
				$('.admin_sku_unit').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.sku_units').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else if(Session::get('admin_sku_type')){
			echo "<script>
				$('.admin_sku_type').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				$('.sku_types').css('display','block');
				$('.jumbo_panel').css('display','none');
			</script>";
		}else{
			echo "<script>
				$('.jumbo_panel').css('display','block');
			</script>";
		}
		if(Session::get('downtime_reason')){
			echo "<script>
				$('.input_output').css('display','none');
				$('.production_graphics').css('display','none');
				$('.downtime_reason_box').css('display','block', '!important');
				$('.input_panel').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.production_data').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.downtime_reason').css({'box-shadow':'0 4px 4px rgba(255,0,0,0.15)'});	
				$('.jumbo_panel').css('display','none');
			</script>";
		}else{
			echo "<script>
				$('.input_output').css('display','block');
				$('.production_graphics').css('display','none');
				$('.downtime_reason_box').css('display','none');
				$('.input_panel').css({'box-shadow':'0 4px 4px rgba(255,0,0,0.15)'});
				$('.production_data').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.downtime_reason').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.jumbo_panel').css('display','none');
			</script>";
		}		

		if(Session::get('production_graphics')){
	?>
			<script>
				$('.input_output').css('display','none');
				$('.production_graphics').css('display','block', '!important');
				$('.downtime_reason_box').css('display','none');
				$('.input_panel').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.production_data').css({'box-shadow':'0 4px 4px rgba(255,0,0,0.15)'});		
				$('.downtime_reason').css({'box-shadow':'0 0px 0px rgba(0,0,0,0)'});
				$('.jumbo_panel').css('display','none');
				
				var r = $(".unitdata").text();
				// var currentproduction = $('.currentproduction').val();
				// alert(currentproduction);
				// alert(r);
				$.ajax({
					url: 'liveprodction',
					dataType: 'json',
					success: function (data) {
						$('#liveprod').empty().append(data);						
						// $('#liveprod').empty().append(data);
						$('.livedata').empty().append(data);
						$('.currentproduction').empty().append(data);
						var d = data;
						// console.log(d);
						// console.log(r);
						var total = (d*r);
						// alert(d * r);
						//alert(total);
						$('.totaldata').empty().append(total);
						// $("#empModal").modal("show");
					}
				});
			</script>
	<?php
		}
	?>
	<script>
		$(document).ready(function(){
			$("select#unit_name").change(function(){
				var skuval = $(this).children("option:selected").val();
				//alert("You have selected the sku row - " + skuval);
				$.ajax({
					type:'get',
					url:'addplantpinajax/'+skuval,
					dataType:'json',
					success:function(data){
						// alert(data);
						var plant_name = data[0].plant_name;	
						var plantinfo = '';
						// $.each(data, function(i, plantt){
							// var spin_val = data[i].spinner;
							//alert(def_val);
							// plantinfo += '<option value="'+data[i].plant_id+'">'+data[i].plant_name+'</option>';	
						// });
						plantinfo += '<option value="" selected> Choose Plant </option>';
						$.each(data, function(i, unitplantinfo){
							plantinfo += '<option value="'+data[i].plant_id+'">'+data[i].plant_name+'</option>';
						});	
						$('#plant_name').empty().append(plantinfo);
						// plantinfo += '<option value="" selected> Choose Plant </option>';
						// $('#plant_name option[value=""]').attr("selected",true);
					}
				});
			});

			$("select#cascadeajax").change(function(){
				var casval = $(this).children("option:selected").val();
				// alert("You have selected the sku row - " + casval);
				$.ajax({
					type:'get',
					url:'addplantpinajax/'+casval,
					dataType:'json',
					success:function(data){
						// alert(data);
						var plant_name = data[0].plant_name;
						var plantinfo = '';
						// $.each(data, function(i, plantt){
							// var spin_val = data[i].spinner;	
							//alert(def_val);
							// plantinfo += '<option value="'+data[i].plant_id+'">'+data[i].plant_name+'</option>';	
						// });
						plantinfo += '<option value="" selected> Choose Plant </option>';
						$.each(data, function(i, unitplantinfo){
							//var plant_val = data[i].unit_plant_info;
							plantinfo += '<option value="'+data[i].plant_id+'">'+data[i].plant_name+'</option>';
							//alert(plant);
						});		
						$('#plantajax').empty().append(plantinfo);
					}
				});
			});

			$("select#machineplantajax").change(function(){
				var machineval = $(this).children("option:selected").val();
				// alert("You have selected the sku row - " + machineval);	
				$.ajax({
					type:'get',
					url:'addmachineajax/'+machineval,
					dataType:'json',
					success:function(data){
						// alert(data);
						var plant_name = data[0].plant_name;
						//alert(plant_name);
						var machineplantinfo = '';
						// $.each(data, function(i, plantt){
							// var spin_val = data[i].spinner;	
							//alert(def_val);
							// plantinfo += '<option value="'+data[i].plant_id+'">'+data[i].plant_name+'</option>';	
						// });
						machineplantinfo += '<option value="" selected> Choose Plant </option>';
						$.each(data, function(i, plantinfo){
							//machineplantinfo += '<option value=""> Choose Plant </option>';
							machineplantinfo += '<option value="'+data[i].plant_id+'">'+data[i].plant_name+'</option>';
						});	
						$('#plantmachineinfo').empty().append(machineplantinfo);
					}
				});
			});			

			$("select.plantajax").change(function(){
				var plantid = $(this).children("option:selected").val();
				//alert(plantid);
				$.ajax({
					type:'get',
					url:'addmachineplantajax/'+plantid,
					dataType:'json',
					success:function(data){
						// alert(data);
						// var plant_name = data[0].plant_name;
						// alert(plant_name);
						var mcascadeinfo = '';	
						//alert(data);
						mcascadeinfo += '<option value=""> Choose Cascade </option>';
						$.each(data, function(i, cascadeinfo){	
							mcascadeinfo += '<option value="'+data[i].cascadeid+'">'+data[i].cascadename+'</option>';
							// alert(mcascadeinfo);
						});	
						$('#plantcascadeajax').empty().append(mcascadeinfo);
					}
				});
			});			

			$('select#edit_unitpincode').change(function(){
				var unitpincode = $(this).children("option:selected").val();
				// alert(unitpincode);
				$.ajax({
					type:'get',
					url:'editplantpinajax/'+unitpincode,
					dataType:'json',
					success:function(data){
						var plantpin = '';
						plantpin += '<option value="" selected> Choose Plant </option>';
						$.each(data, function(i, editplant){
							// plantpin += '<option value=""> Choose Plant </option>';
							plantpin += '<option value="'+data[i].plantid+'">'+data[i].plantname+'</option>';
						});
						$('#plant_name').empty().append(plantpin);
					}
				});
			});			

			$('select#editcasunit').change(function(){
				var editcasunit = $(this).children("option:selected").val();
				// alert(unitpincode);
				$.ajax({
					type:'get',
					url:'editcascadeajax/'+editcasunit,
					dataType:'json',
					success:function(data){
						var casunit = '';
						$.each(data, function(i, editplant){
							casunit += '<option value="'+data[i].plantid+'">'+data[i].plantname+'</option>';
						});
						$('#editcascade').empty().append(casunit);
					}
				});
			});			

			$('select#machineunitid').change(function(){
				var machineunitid = $(this).children("option:selected").val();
				$.ajax({
					type:'get',
					url:'editproductajax/'+machineunitid,
					dataType:'json',
					success:function(data){
						var machineajax = '';
						$.each(data, function(i, machineinfo){
							machineajax += '<option value="'+data[i].productplantid+'">'+data[i].productplantname+'</option>'
						});
						$('#machineplantid').empty().append(machineajax);
					}
				});
			});			

			$('select#machineplantid').change(function(){
				var machineplantid = $(this).children("option:selected").val();
				// alert(machineplantid);
				$.ajax({
					type:'get',
					url:'editproductplantajax/'+machineplantid,
					dataType:'json',
					success:function(data){
						var machineajax = '';
						$.each(data, function(i, machineinfo){
							machineajax += '<option value="'+data[i].productcasid+'">'+data[i].productcasname+'</option>';
						});
						$('#machinecascadeid').empty().append(machineajax);
					}
				});
			});

			

			$('.production_data').on('click', function(){
				var r = $(".unitdata").text();
				// alert(r);				
				$.ajax({
					type:'get',
					url:'pkt_cnt',
					dataType:'json',
					success:function(data){
						// alert(data);
						// console.log(data);
						// var livedata = '';
						$('.livedata').empty().append(data);
						var d = data;
						// console.log(d);
						// console.log(r);
						var total = (d*r);
						// alert(d * r);
						//alert(total);
						// alert('here');
						$('.totaldata').empty().append(total);						
						$('.currentproduction').val(data);
					}
				});				
			});
			
			$('.plant1').on('click', function(){
				// alert($(this).val());
				// alert($('.unitvaluereflect1').val());
				$('.unitts').prop("checked", true);
				// alert($(".plant1:checked").length);
				if($(".plant1:checked").length < 1){
					$('.unitts').prop("checked", false);
				}
			});
			$('.plant2').on('click', function(){
				// alert($(this).val());
				// alert($('.unitvaluereflect2').val());
				// $('.unit2').prop("checked", true);
				// alert($(".plant2:checked").length);
				$('.unitts2').prop("checked", true);
				if($(".plant2:checked").length < 1){
					$('.unitts2').prop("checked", false);
				}
			});			
			
			if(!$('.plant2').is(':checked').length){
				// alert('Please select unit to assign');
				$('.unit2').prop("checked", false);		
			}
			
			
			$('.submitrole').on('click', function(e){
				var unitp1 = $(".plant1:checked").length;
				var unitp2 = $(".plant2:checked").length;				
			});
			
			

			$('.yesiwant').on('click', function(){
				// alert('here');$
				$('#endproduction').modal('toggle');			
				// $('.rpm_show').css('display','block');
				// $('.add_button2').css('display','none');
				$('.add_rpm').css('display','block');
				$(".rpm_show").text("Start SKU Production");
				$(".rpm_show").prop('disabled',true);
				$(".rpm_show").css('display','none');
				$('#startskuproduction').css('display','block');
				
				var skuname = $('.skuname').val();
				// alert(skuname);
				$.ajax({
					type:'get',
					url:'endsku',
					data:{id:skuname},
					dataType:'json',
					success:function(data){
						alert(data);
					}
				});
			});	
			$('#startskuproduction').on('click', function(){
				// alert('heree');
			});

			$('.admin_unit').on('click', function(){
				// alert('admin_unit');
				// $('.admin_unit').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				// $('.unit_info').css('display','block');
				// $('.jumbo_panel').css('display','none');
			});

			$('.admin_plant').on('click', function(){
				// alert('admin_plant');
				// $('.admin_plant').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				// $('.plant_info').css('display','block');
				// $('.jumbo_panel').css('display','none');
			});

			$('.admin_cascade').on('click', function(){
				// alert('admin_cascade');
				// $('.admin_cascade').css({'box-shadow':'0 4px 4px rgba(0,0,0,0.15)'});
				// $('.cascade_info').css('display','block');
				// $('.jumbo_panel').css('display','none');
			});
						

			$('select.sv_pcu_unit').change(function(){
				var selunit = $(this).children("option:selected").val();
				$('.svunit').val(selunit);
				// alert(selunit);			
				$.ajax({
					type:'get',
					url: 'svunit/'+selunit,
					dataType:'json',
					success:function(data){
						// alert(data);
						var plant = '';
						var cascade = '';
						var machine = '';
						plant += '<option value="0" selected="selected"> All </option>';
						cascade += '<option value="0" selected="selected"> All </option>';
						machine += '<option value="0" selected="selected"> All </option>';
						$.each(data, function(i, unit_plant_info){
							plant += '<option value="'+data[i].plantid+'">'+data[i].plantname+'</option>';
						});	
						// alert(data);
						// var plantval = $('.sv_pcu_plant').val();											
						$('.sv_pcu_plant').empty().append(plant);
						$('.sv_pcu_cascade').empty().append(cascade);
						$('.sv_pcu_machine').empty().append(machine);						
					}
				});
			});
			

			$("select.sv_pcu_plant").change(function(){
				var selplant = $(this).children("option:selected").val();
				$('.svplant').val(selplant);
				// alert(selplant);
				$.ajax({
					type:'get',
					url:'svplant/'+selplant,
					dataType:'json',
					success:function(data){
						// alert(data);
						var cascadee = '';
						var machinee = '';
						cascadee += '<option value="0" selected="selected"> All </option>';
						machinee += '<option value="0" selected="selected"> All </option>';
						$.each(data, function(i, unit_plant_info){
							cascadee += '<option value="'+data[i].cascadeid+'">'+data[i].cascadename+'</option>';
						});	
						// var plantval = $('.sv_pcu_plant').val();	
						// $("option:selected").prop("selected", false)
						$('.sv_pcu_cascade').prop("selected", false)
						$('.sv_pcu_cascade').empty().append(cascadee);						
						$('.sv_pcu_machine').empty().append(machinee);
						$('.svcascade').val(0);
						$('.svmachine').val(0);
					}
				});
			});

			

			$("select.sv_pcu_cascade").change(function(){
				var selcas = $(this).children("option:selected").val();
				$('.svcascade').val(selcas);
				// alert(selcas);
				$.ajax({
					type:'get',
					url:'svcascade/'+selcas,
					dataType:'json',
					success:function(data){
						// alert(data);
						var machine = '';
						machine += '<option value="0" selected="selected"> All </option>';
						$.each(data, function(i, unit_plant_info){
							machine += '<option value="'+data[i].machineid+'">'+data[i].machinename+'</option>';
						});	

						// var plantval = $('.sv_pcu_plant').val();	
						$('.sv_pcu_machine').empty().append(machine);
					}
				});
			});

			

			$("select.sv_pcu_machine").change(function(){
				var selmachine = $(this).children("option:selected").val();
				$('.svmachine').val(selmachine);
				// $('.sv_pcu_machine').empty().append(machine);
				// alert(selmachine);
			});			

			$("select.sv_pcu_shift").change(function(){
				var selshift = $(this).children("option:selected").val();
				$('.svshift').val(selshift);
			});


			// $('.svdatainfo').on('click', function(){
			// 	// alert('datainfo');
			// 	var subunit = $('.svunit').val();
			// 	var subplant = $('.svplant').val();
			// 	var subcas = $('.svcascade').val();
			// 	var submachine = $('.svmachine').val();
			// 	var subshift = $('.svshift').val();	
			// 	var subfrom = $('.txtStartDate').val();
			// 	var subto = $('.txtEndDate').val();				

			// 	if(subunit == ''){
			// 		subunit = 0;
			// 	}

			// 	if(subplant == ''){
			// 		subplant = 0;
			// 	}

			// 	if(subcas == ''){
			// 		subcas = 0;
			// 	}

			// 	if(submachine == ''){
			// 		submachine = 0;
			// 	}

			// 	if(subshift == ''){
			// 		subshift = 0;
			// 	}

			// 	if(subfrom == ''){
			// 		subshift = 0;
			// 	}

			// 	if(subto == ''){
			// 		subshift = 0;
			// 	}
			// 	//alert(subfrom);
			// 	//alert(subto);
			// 	// alert(submachine);
			// 	// alert('111');
			// 	$.ajax({
			// 		type: 'get',
			// 		// url: 'svdata',
			// 		url: 'detail',
			// 		dataType: 'json',
			// 		data:{U: subunit, P:subplant, C:subcas, M:submachine, S:subshift ,FD:subfrom ,TD:subto ,T: 'test'},
			// 		 beforeSend: function() {
			// 		      $("#myDiv").show();
 			// 			$("#hide-div").hide();
			// 		  },

			// 		success:function(data){
			// 			$("#myDiv").hide();
			// 			$("#hide-div").show();

			// 			// console.log(data[0].first_name, data[0].cascade_name, data[0].machine_name, data[0].sku_code, data[0].shift_name, data[0].total_production);
			// 			// console.log(data[1][0].data[1][1].data[1][2].data[1][3]);
			// 			// console.log(data[2][0].data[2][1].data[2][2].data[2][3]);
			// 			// console.log(data[3][0].data[3][1].data[3][2].data[3][3]);
			// 			// console.log(data[4][0].data[4][1].data[4][2].data[4][3]);
			// 			// alert('success');

			// 			$('.datashowhere').css("display", "none");
			// 			var spin='';
			// 			var count = 1;
			// 			console.log('--------------',data);
			// 			$.each(data, function (i, spinner) {
			// 				var spin_val = data[i][i];
			// 				var date = data[i].logintime;							
			// 				//chgdate = date.split(' ')[0];
			// 				// var newDate = date.toString('dd-MM-yy');

			// 				 //console.log('+++++++++++++++++',typeof(data[i].sku_name));	
			// 				if(data[i].sku_name == undefined) {
			// 					var sku_name = '-';	
			// 				} else {
			// 					var sku_name = data[i].sku_name;	
			// 				}

			// 				if(data[i].sku_code == undefined) {
			// 					var sku_code = '';
			// 				} else {
			// 					var sku_code = data[i].sku_code;
			// 				}

			// 				if(data[i].rpm_default == undefined) {
			// 					var rpm_default = '-';
			// 				} else {
			// 					var rpm_default = data[i].rpm_default;
			// 				}

			// 				// var newDate22 = moment(chgdate).format('dd-MM-yyyy');
			// 				// console.log(newDate22);	
			// 				// alert(date);
			// 				// alert(spin_val);
			// 				// count = count;
			// 				spin += '<tr><td>'+count+'</td><td>'+date+'</td><td>'+data[i].first_name+'</td><td>'+data[i].cascade_code+'</td><td>'+data[i].machine_regno+'</td><td>'+sku_code+'</td><td>'+data[i].shift_code+'</td><td>'+rpm_default+'</td><td>'+data[i].cld+'</td><td>'+data[i].total_production+'</td><td>'+data[i].breakdown_part+'</td><td>'+data[i].oee+'</td></tr>';	
			// 				count++;
			// 			});
			// 			// alert(spin);
			// 			$('.reportval').empty().append(spin);	

			// 			//if(JSON.parse(data).length == 0) {
			// 				//console.log(data);
			// 				// console.log('Error----------------',JSON.parse(data).length);
			// 			//} else {
			// 				console.log('Successsss----------------');
			// 			//}
			// 		}
			// 	});
			// });
			

			$("select.sv_pcu_shift").change(function(){
				var selshift = $(this).children("option:selected").val();
				// alert(selshift);
			});
			<?php
			// echo Session::get('designation_id');
			
			if( Session::get('designation_id') == 5){
				$breaklimit = "00:".session::get('breakdown_limit').":00";
				$breaklimit1 = session::get('breakdown_limit');
				// echo $breaklimit1;
				// echo "----------------------------";
				$limitinsec = $breaklimit1 * 60000;	
				$autosubmit = $limitinsec - 120000;
				// echo $breaklimit;
				if(!empty(Session::get('sku_row_id'))){
					
			?>		
			
				function loadProduData10() {	
					var r = $(".unitdata").text();
					var livedata = $('.livedata').val();
					// console.log(livedata);
					var livepromac = $('.livedata').val();
					var currentproduction = $('.currentproduction').val();
					// alert(currentproduction);
					// alert(livepromac);
					
					var dt = new Date();
					var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();	
					
					// var start = '00:35:00';				
					var start = '<?php echo $breaklimit; ?>';
					var end = time;
					s = start.split(':');
					e = end.split(':');
					min = e[1]-s[1];
					hour_carry = 0;
					if(min < 0){
						min += 60;
						hour_carry += 1;
					}
					hour = e[0]-s[0]-hour_carry;
					diff = hour + ":" + min+":"+"00";
					// alert(end);
					// alert(start);
					// alert(diff);
					
					
					$.ajax({
						url: 'liveprodction',
						dataType: 'json',
						success: function (data) {
							$('#liveprod').empty().append(data);
							$('.livedata').empty().append(data);
							var d = data;
							var total = (d*r);
							$('.totaldata').empty().append(total);
							var report = data - currentproduction;											
							if(report == 0){
								// $('.downrealtime').val(subtime);
								$('.downrealtime').val(diff);
								$('.startertime').val(end);
								$('.endtimevalue').prop('type', 'text');
								$('.endtimevalue').prop('disabled', true);
								$('.endtimevalue').val(end);
								$('#timepkr').css('display','none');
								$('.showtime').css('display','none');
								$("#empModal").modal("show");
								$('.timeflag').val(0);
							}
						}
					});


					// setTimeout(function(gettests){
						// $('#empModal').modal('hide');
						// var operatorstarttime = $('.downrealtime').val();
						// var operatorendtime = $('.endtimevalue').val();
						// alert('h12222');
						// $.ajax({
							// url:'autoopreason/'+operatorstarttime+'/'+operatorendtime,
							// type:'get',
							// success:function(data){
								// alert(data);
							// }
						// });
					// },<?php echo $autosubmit; ?>);
					
					$('.breakdownmodalclose').on('click', function(){
						$("#areyousuretoclose").modal("show");
					});
					
					// $('.yesamsuretoclose').on('click', function(){
						// $('#empModal').css('display','none');
						// $('#empModal').modal("hide");
						// var operatorstarttime = $('.downrealtime').val();
						// var operatorendtime = $('.endtimevalue').val();
						// var operatorendtime = '15:00:00';
						// $.ajax({
							// url:'autoopreason/'+operatorstarttime+'/'+operatorendtime,
							// type:'get',
							// data:{startoperator:operatorstarttime},
							// success:function(data){
								// alert(data);
							// }
						// });
					// });					
				}			
				// setInterval(function(){ loadProduData10(); }, 1000000);
				setInterval(function(){ loadProduData10(); }, <?php echo $limitinsec; ?>);
				
				
				function loadProduData() {	
					var r = $(".unitdata").text();	
					// var currentproduction = $('.currentproduction').val();
					$.ajax({
						url: 'liveprodction',
						dataType: 'json',
						success: function (data) {
							$('#liveprod').empty().append(data);
							$('.livedata').empty().append(data);						
							$('.currentproduction').val(data);
							var d = data;
							var total = (d*r);
							$('.totaldata').empty().append(total);
						}
					});
				}			
				setInterval(function(){ loadProduData(); }, 200000);
			<?php
				}
				}
			?>
			$('.machineprodction').on('click', function(){
				var r = $(".unitdata").text();
				// var currentproduction = $('.currentproduction').val();
				// alert(currentproduction);
				// alert(r);
				
				var dt = new Date();
				var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();				
				// var subtime = dt.getHours() + ":" + (dt.getMinutes()-10) + ":" + dt.getSeconds();

				var start = '00:35:00';				
				// var start = <?php //echo $breaklimit; ?>;
				var end = time;
				s = start.split(':');
				e = end.split(':');
				min = e[1]-s[1];
				hour_carry = 0;
				if(min < 0){
					min += 60;
					hour_carry += 1;
				}
				hour = e[0]-s[0]-hour_carry;
				diff = hour + ":" + min+":"+"00";
				// alert(end);
				// alert(start);
				// alert(diff);
				
				
				$.ajax({
					url: 'liveprodction',
					dataType: 'json',
					success: function (data) {
						$('#liveprod').empty().append(data);						
						// $('#liveprod').empty().append(data);
						$('.livedata').empty().append(data);
						$('.currentproduction').empty().append(data);
						var d = data;
						// console.log(d);
						// console.log(r);
						var total = (d*r);
						// alert(d * r);
						//alert(total);
						$('.totaldata').empty().append(total);
						// $("#empModal").modal("show");
					}
				});
			});
			
			$("select.select_designation").change(function(){
				var selectedDesignation = $(this).children("option:selected").val();
				// alert(selectedDesignation)
				
				if(selectedDesignation == 5){
					// alert('operator');
					$('#onlyemail').css("display",'none');
					$('#onlymob').css("display",'none');
					$('#onlypass').css("display",'none');
					$('#onlycpass').css("display",'none');
					$('#opemail').prop('required',false);
					$('#opmob').prop('required',false);
					$('#password').prop('required',false);
					$('#cpassword').prop('required',false);
				}else{
					$('#onlyemail').css("display",'block');
					$('#onlymob').css("display",'block');
					$('#onlypass').css("display",'block');
					$('#onlycpass').css("display",'block');
					$('#opemail').prop('required',true);
					$('#opmob').prop('required',true);
					$('#password').prop('required',true);
					$('#cpassword').prop('required',true);
				}
			});	
			$('.userModal22').on('click', function(){
				var userrole = $(this).val();
				// alert(userrole);
				$('.userroleinfo').val(userrole);
				// alert(userrole);
				// $.ajax({
					// type:'get',
					// url:'getuserroleinfo/'+userrole,
					// dataType:'json',
					// success:function(data){
						// var spin = '';
						// console.log(data[0].unitkey);
						// console.log(data[0].plantkey);
						// alert(data[0].unitkey);
						// alert(data[0].unitvalue);
						// alert(data[0].plantkey);
						// alert(data[0].plantvalue);
						// $.each(data, function (i, spindata) {							
							// console.log(data[0].unitkey);
							// var spin_val = data[i].unitkey;	
							// console.log(spin_val);
							//alert(def_val);
							// spin += '<option value="'+data[i].unitkey+'">'+data[i].unitkey+'</option><option value="'+data[i].plantkey+'">'+data[i].plantkey+'</option>';	
						// });
						// $('.userrolehere').empty().append(spin);	
					// }
				// });				
			});
			
			$('.units, .plants').on('click',function(){
				if ($(this).hasClass("units") && $(this).hasClass("plants")) {
					alert('here');
				}
			});			
			
			$('.submitsubreason').on('click', function(){
				// alert('heree');
				if($('input[type=checkbox]:checked').length == 0){
					alert('Please Assign Sub-Reason to Machine(s)');
					return false;
				}else{   
					// alert('ok');
					// return false;
				}		
				// if($('.submachine').prop('checked') == false){
					// alert('Please Assign Sub-Reason To Machine');
					// return false;
				// }
			});
			
			// $('.activaa').css('background-color','#0f3c26 !important');
			// $('.subbreak').on('click', function(){
				// alert('here');
			// });
		});	
	</script>
 		
	<script>
		$(document).ready(function(){
			$('.breakdown_reasonns').on('click', function(){				
				// $('.breakdown_reasonns').change(function(){
				var reason_id = $(this).val();
				//$('.breakdown_reasonns').addClass('active');
				// alert(reason_id);
				$(this).addClass("activaa").siblings().removeClass("activaa");   			
				$.ajax({
					type:'get',
					url:'opreasonid/'+reason_id,
					dataType:'json',
					//data:{id: reason_id},
					success:function(r){
						// alert();
						// alert(r.length);
						//alert(r[0].subbreakdown_id);
						var data =r;
						// alert(data.length);
						var options = '';
						var options2 = '';
						$('.subreasonnumber').val(data.length);
						if(data.length==''){
							//alert('0');
							$('.sub_reason3').css('display','none');
							$('#sub_breakdown_reason3').prop("required", false);
							
							// $('.sub_reason3').css('display','none');
							$('.sub_breakdown_reason4').prop("required", false);								
							$('.rpm_reason').prop("disabled", false);								
						}else{
							//alert('1');
							for(var i=0; i<data.length; i++){
								// alert(data[0].subbreakdown_id);								
								options2 += '<label name="" class="subbreakkk sub'+data[i].subbreakdown_id+'" style="width:30%; height: 195px; font-size: 16px; border-radius:5px; color:white; float:left; margin-right:1%; margin-bottom:1%; background-color:#5b826f;"><input type="submit" class="subbreak" name="subbreak" style="margin-bottom:10px; margin-right:10px; border-radius:5px; display:none; background-color:#5b826f; color:white; padding:10px; border: 1px solid #5b826f; width:18%; text-align: left !important; font-size:25px;" name="subbreakreason" value="'+data[i].subbreakdown_id+'"><img style="margin-top:20px; background:white; margin-bottom: 8px; margin-right:10px; padding:5px; width:28%; margin-left:5%;" src="'+data[i].subbreakicon+'"><div style="font-size:25px; margin-left:5%; line-height: 29px; padding-bottom: 21px;">'+data[i].subbreakdown_reason+'</div></label>';								
								// options2 += '<label name="" class="subbreakkk" style="width:18%; height: 200px; border-radius:5px; color:white; float:left; margin-right:1%; margin-bottom:1%; background-color:#5b826f;"><input type="submit" class="subbreak" name="subbreak" style="margin-bottom:10px; margin-right:10px; border-radius:5px; display:block; background-color:#5b826f; color:white; padding:10px; border: 1px solid #5b826f; width:18%; text-align: left !important; font-size:25px;" name="subbreakreason" value="'+data[i].subbreakdown_id+'"><img style="margin-top:20px; background:white; margin-bottom: 8px; margin-right:10px; padding:5px; width:30%; margin-left:5%;" src="'+data[i].subbreakicon+'"><div style="font-size:25px; margin-left:5%; line-height: 29px; padding-bottom: 21px;">'+data[i].subbreakdown_reason+'</div></label>';								
							}								
							$('.sub_breakdown_reason3').css('display','block');			
							$('.sub_breakdown_reason4').prop("required", false);
							$('.sub_reason3').prop("required", false);
							$('.subbreakbutton').empty().append(options2);
							$('.rpm_reason').prop("disabled", false);		
							// $('.hereeeee').empty().append(options2);
							// $('.subbreak').addClass('hereeeeee');
						}
					}						
				});
				// });
			});
			
			$('.subbreakbutton').on('click', function(){
				var subvalue = $('.subreasonvalue').val();
				$('.sub'+subvalue).addClass('activaa').siblings().removeClass('activaa');
			});
			
			$('.breakdown_reasonns').on('click', function(){
				console.log($(this).val());			
				$('.breakreasonvalue').val($(this).val());
				return false;			
			});
			
			$('.subbreak').on('click', function(){
				// alert($('.subbreak').val());
				
			});
			
					
			
			// $('.subbreak').on('click', function(){
				// alert('heeeeeeeeee');
			// });
			
			$('.rpm_reason').on('click', function(){							
				if($('.breakreasonvalue').val() == '') {
					alert('Please Select Breakdown Reason / कृप्या ब्रेकडाउन का कारण चुनें ');						
					return false;
				}	
					
				// alert($('.end_time').val());
				// if($('.end_time').val() == ''){
					// alert('Please select end time');
					// return false;
				// }
				
				if($('.subreasonnumber').val() > 0){
					// $('.subbreak').prop('required',true);
					// var flag = 0;
					// var subflag = $('.subflag').val();	
					$('.subbreak').on('click', function(){
						// var flag = 1;
						// $('.subflag').val(flag);	
						// alert('heree');						
					});
					var subflag = $('.subreasonvalue').val();
					// alert(subflag);
					if(subflag == ''){
						alert('Please Select Breakdown Sub-Reason / कृप्या ब्रेकडाउन का उप-कारण कारण चुनें ');	
						return false;
					}						
					
				};
			});				
			
			$('.breakdown_reasonns').on('click', function(){
				$('.subbreakremark').css('display','none');
				$('#breakdown_remark').prop('required', false);
			});
			
			$('#break22').on('click',function(){
				$('.subbreakremark').css('display','block');
				$('#breakdown_remark').prop('required', true);
			});	
			
			// $('.selbreakreason').on('click', function(){
				// alert('here');
				// return false;
			// });
			
			$('.checklogoutoperator').on('click', function(){
				var operatorid = $(this).val();
				// alert(operatorid);	
				// alert('.check'+operatorid);
				// return false;
				$.ajax({
					type:'get',
					url:'checklogoutoperator/'+operatorid,
					dataType:'html',
					success:function(data){
						$('.check'+operatorid).html('NO');
						$('.check'+operatorid).removeClass('btn-danger');
						$('.check'+operatorid).addClass('btn-success');
						$('.check'+operatorid).prop('disabled',true);					
					}
				});			
			});	
			
			setTimeout(function() {
				$('.successMessage').fadeOut('fast');
			}, 10000);	
			
			$('.rpm_min').keyup(function(){
				var str = $(".rpm_min").val();
				// alert(str);
				$('#rpm1').val(str);
			});	
			$('.rpm_max').keyup(function(){
				var str2 = $(".rpm_max").val();
				// alert(str);
				$('#rpm2').val(str2);
			});	
			$('.rpm_default').keyup(function(){
				var str3 = $(".rpm_default").val();
				// alert(str);
				$('#rpm3').val(str3);
			});	
			
			
			jQuery(function(){     
				var d = new Date(),        
					h = d.getHours(),
					m = d.getMinutes();
				if(h < 10) h = '0' + h; 
				if(m < 10) m = '0' + m; 
				$('input[type="time"][value="now"]').each(function(){ 
					$(this).attr({'value': h + ':' + m});
				});
			});
			
			$('select.productskuplant').on('change', function(){
				var plantidd = $(this).children("option:selected").val();
				$.ajax({
					url:'productskuplant/'+plantidd,
					type:'get',
					dataType:'json',
					success:function(data){
						// console.log(data.length);						
						$('.promac').val(data.length);
						var machinehere = '';
						machinehere += '<option value="0" selected="selected"> Select Machine </option>';
						$.each(data, function(i, plantinfo){
							machinehere += '<option value="'+data[i].machineid+'">'+data[i].machinename+'</option>';
						});
						$('.productskumachine').empty().append(machinehere);	
					}						
				});				
			});		
			
			
			$('select.productskupro').on('change', function(){
				var productskuid = $(this).children("option:selected").val();
				$.ajax({
					url:'productskupro/'+productskuid,
					type:'get',
					dataType:'json',
					success:function(data){
						// console.log(data);
						$('.prosku').val(data.length);
						var producthere = '';
						producthere += '<option value="0" selected="selected"> Select SKU </option>';
						$.each(data, function(i, plantinfo){
							producthere += '<option value="'+data[i].skuid+'">'+data[i].skuname+'</option>';
						});						
						$('.productskuu').empty().append(producthere);	
					}						
				});
			});
			
			$("select.productskumachine").on('change', function(){
				var selectedCountry = $(this).children("option:selected").val();
				// alert(selectedCountry);
				$('#prodvalue').val(1);
			});
			
			$("select.productskuu").change(function(){
				var selectedCountry22 = $(this).children("option:selected").val();
				$('.prodskuvalue').val(1);
			});
			
			$('.formsubmission').on('click', function(){
				var productidd = $('.plantmachineee').val();
				var promac = $('.promac').val();
				var skuidd = $('.skuidd').val();
				var prosku = $('.prosku').val();
				if(productidd == ''){
					alert('Please select Machine');
					return false;
				}				
				if(promac == 0){
					alert('No Machine Assigned to this Plant');					
					return false;
				}
				
				if($('#prodvalue').val() == 0){
					alert('Please select Machine');
					return false;
				}				
				
				if($('.productskuu').val() == ''){
					alert('Please select SKU');
					return false;
				}
				if(skuidd == ''){
					alert('Please select SKU');
					return false;
				}				
				if(prosku == 0){
					alert('No SKU Assigned to this Product');
					return false;
				}				
				
				if($('.prodskuvalue').val() == 0){
					alert('Please select SKU');
					return false;
				}
				
				if($('.rpmval').val() == ''){
					alert('Please select RPM');
				}				
				// return false;
			});
					
		});	
	</script>
	<script>
		$(document).on("click", ".subbreak", function() {
			// console.log($(this).val());
			// alert('jereeee');
			$('.subreasonvalue').val($(this).val());
			// alert($(this).val());
			$(this).addClass('subactivaa').siblings().removeClass('subactivaa');			
			return false;
		});	
	</script>
	
	<?php	
		if( Session::get('designation_id') == 5){
			$hour = Session::get('shift_duration');	
			$hour = $hour * 3600000;
			// echo $hour;
			// echo "<br/>";
			// $logintime = $operatorlogoutrow['logintime'];
			// echo $logintime;
			// echo "<br/>";		
			
			// $start = '2020-11-07 11:00:00';
			// echo $logintime;
			// echo "<br/>";
			// echo $start;
			// display the converted time
			// $autologoutoperatortime = date('Y-m-d H:i:s',strtotime('+'.$hour.' hour +00 minutes +00 seconds',strtotime($logintime)));
			// $autologoutoperatortime = '2020-11-07 11:50:00';
			// echo "++++++++++";		
			
			// echo "logout time".$autologoutoperatortime;
			// echo "<br/>";
			// echo "Current time".$currenttime;
			// echo "<br/>";
			// if($autologoutoperatortime == true){
				// echo "logout now";
			// }else{
				// echo "keep login";
			// }
			// echo "<br/>";		echo "<br/>";		echo "<br/>";		
	?>
	
		<script>
			setTimeout(function() {			
				$.ajax({
					url:'operatorlogout',
					type:'get',				
					success:function(data){
						// alert('here');
						window.location.href = "{{ url('/operatorlogin') }}";
					}
				});
			}, <?php echo $hour; ?>);			
			
		</script>
		<script>			
			$('.medchange').on('click', function(){
				// alert('dot');
				return false;
			});
		</script>
	<?php } ?>		
        </div>
    </body>
</html>


