<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>RM Chemicals</title>		
		
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- ===== Addition of external codes starts here === -->
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="../resources/assets/images/favicon.png">
        
        <!-- Bootstrap Core CSS -->
        <link href="../resources/css/tpicker.css" rel="stylesheet">
		
		
		
		<!-- ===============================================================Bootstrap Core CSS -->
        <link href="../resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- chartist CSS -->
        <!--link href="../resources/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
        <link href="../resources/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
        <link href="../resources/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
        <link href="../resources/assets/plugins/css-chart/css-chart.css" rel="stylesheet"-->

        <!-- Calendar CSS -->
		<link href="../resources/assets/plugins/calendar/dist/fullcalendar.css" rel="stylesheet" />
       

        <!-- toast CSS -->
        <link href="../resources/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../resources/css/style.css" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="../resources/css/colors/default-dark.css" id="theme" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
         ===== Addition of external codes ends here === -->
        <!-- Styles -->   

		
        <!--script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-85622565-1', 'auto');
        ga('send', 'pageview');
        </script--> 
        
		
		
		
        <style>
                 
            .box
            {
                width:1270px;
                padding:20px;
                background-color:#fff;
                border:1px solid #ccc;
                border-radius:5px;
                margin-top:25px;
            }
            
			.standardsize{
				color: red;
				font-size: 13px;
				margin-left: 10px;
			}
			
			.fa-input {
				font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
				padding-left:25px;
				width: 100% !important;
				background-color: #5b826f;
			}
			
            .topbar .top-navbar .app-search .srh-btn{
                top:20px !important;
            }
            .unit1>a{
                color:white;
            }
            .unit2>a{
                color:white;
            }
            .plant_detail_1>a{
                color:white;
            }
            .dev_style{
                text-decoration: none;
                color: white;
                font-weight: bolder;
            }
            tbody{
               /* background: white;                 
                color: white;*/
                box-shadow: 7px 4px 10px -4px #888;
            }
			
			/** Style Edited start */
			html body .bg-info-2 {
				background-color: #7460ee !important;
			}
			html body .bg-info-3 {
				background-color: #55ce63 !important;
			}
			html body .bg-info-4 {
				background-color: #f62d51 !important;
			}
			html body .bg-info-5 {
				background-color: #ffbc34 !important;
			}
			html body .bg-info-6 {
				background-color: #01c0c8 !important;
			}
			.font-light {
				line-height: 27px;
				font-size: 17px;
			}
			.dark-logo {
				width: 61px !important;
			}
			.span-date {
				float: right;
			}
			.card-title{
				color:#fff;
				font-size:22px;
				font-weight:bolder;
			}
			
			.font-light {
				line-height: 25px;
				font-size: 16px;
			}
			
			.text-themecolor {
				color: #174d33 !important;
			}			
			
			i.icon-arrow-left-circle {
				display: none;
			}
			
			.back_button{
				width:100%; 
				border-radius:20px; 
				padding:12px; 
				margin-top:10px; 
				background: #e8e8e8;
			}
			 <!-- ============================================================== -->
			<!-- Delete Confirmation -->
			.modal-confirm {		
				color: #636363;
				width: 400px;
			}
			.modal-confirm .modal-content {
				padding: 20px;
				border-radius: 5px;
				border: none;
				text-align: center;
				font-size: 14px;
			}
			.modal-confirm .modal-header {
				border-bottom: none;   
				position: relative;
			}
			.modal-confirm h4 {
				text-align: center;
				font-size: 26px;
				margin: 30px 0 -10px;
			}
			.modal-confirm .close {
				position: absolute;
				top: -5px;
				right: -2px;
			}
			.modal-confirm .modal-body {
				color: #999;
			}
			.modal-confirm .modal-footer {
				border: none;
				text-align: center;		
				border-radius: 5px;
				font-size: 13px;
				padding: 10px 15px 25px;
			}
			.modal-confirm .modal-footer a {
				color: #fff;
			}	
			.del_button{
				display:block;
			}	
			.modal-confirm .icon-box {
				width: 80px;
				height: 80px;
				margin: 0 auto;
				border-radius: 50%;
				z-index: 9;
				text-align: center;
				border: 3px solid #f15e5e;
			}
			.modal-confirm .icon-box i {
				color: #f15e5e;
				font-size: 46px;
				display: inline-block;
				margin-top: 13px;
			}
			.modal-confirm .btn, .modal-confirm .btn:active {
				color: #fff;
				border-radius: 4px;
				background: #60c7c1;
				text-decoration: none;
				transition: all 0.4s;
				line-height: normal;
				min-width: 120px;
				border: none;
				min-height: 40px;
				border-radius: 3px;
				margin: 0 5px;
			}
			.modal-confirm .btn-secondary {
				background: #c1c1c1;
			}
			.modal-confirm .btn-secondary:hover, .modal-confirm .btn-secondary:focus {
				background: #a8a8a8;
			}
			.modal-confirm .btn-danger {
				background: #f15e5e;
			}
			.modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
				background: #ee3535;
			}
			.trigger-btn {
				display: inline-block;
				margin: 100px auto;
			}
			.modal-body{
				margin-top: -30px;
			}
			 #footer { 
				position: fixed; 
				padding: 10px 10px 0px 10px; 
				bottom: 0; 
				width: 100%; 
				/* Height of the footer*/  
				height: 7%; 
				text-align: center;
				left:0;
				background: white; 
			} 
			.u-img{
				font-size: 39px;
				margin-left: 18px;
				color:#5b826f;
			}
			.u-text{
				margin-left: -6%;
			}
			.areyousure{
				margin-top: 7px !important;
			}			
			
			.contact100-form-title{
				padding-bottom: 30px !important;
			}
			.u-text{
				margin-left:-10%;
			}
        </style> 
		
        <style>
			.loader {
				position:fixed;
				z-index:99;
				top:0;
				left:0;
				width:100%;
				height:100%;
				background:white;
				opacity:0.3;
				display:flex;
				justify-content:center;
				align-items:center;
			}
			.loader>img{
				width: 100px;
			}
			.loader.hidden{
				animation:fadeOut 1s;
				animation-fill-mode: forwards;
			}
			@keyframes fadeOut{
				100%{
					opacity:0;
					visibility:hidden;
				}
			}
        </style>
        <style>
			.supreport{
				visibility:hidden; 
				margin-bottom:-3%;
			}
			
			@media (max-width: 425px){
				.real_time {
					font-size: 16px !important;
					display:none;
				}
				.real_time_monitoring>h3 {					
					display: none;
				}
			}
			@media (max-width: 768px){
				.real_time_monitoring>h3 {					
					display: none;
				}
				.ti-menu{
					display:none;
				}
				
				.col-sm-4 {
					-ms-flex: 0 0 100%;
					flex: 0 0 100%; 
					max-width: 100%;
					margin-bottom:2%;
				}
				.supreport{
					display:none;
					margin-bottom:0% !important;
				}
				
			}
			@media (max-width: 425px){
				.suprow{					
					margin-top: 20%;
				}
				.svdatainfo{					
					margin-top: 14%;
				}
			}
			
			
			
			<!-- Keyboard Start -->
			<!-- keyboard start -->
			input {
			  width: 100%;
			  height: 100px;
			  padding: 20px;
			  font-size: 20px;
			  border: none;
			  box-sizing: border-box;
			}

			.simple-keyboard {
			  max-width: 850px;
			}

			<!-- keyboard end -->
			<!-- Keyboard End -->
			
			
		</style>
    </head>

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    

    