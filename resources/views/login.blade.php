<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<style>
		  body, html {
			height: 100%;
			font-family: Arial, Helvetica, sans-serif;
		  }

		  * {
			box-sizing: border-box;
		  }

		 

		  .bg-img {
			/* The image used */
			background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
			min-height: 100%;
			/* Center and scale the image nicely */
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			position: relative;
		  }

		  /* Add styles to the form container 
		  .container {
			  position: absolute;
			  right: 34%;
			  opacity: 0.8;
			  top:25%;
			  border-radius: 13px;
			  margin: 20px;
			  max-width: 400px;
			  padding: 16px;
			  background-color: white;
		  }*/

		  /* Full-width input fields */
		  input[type=text], input[type=password] {
			width: 100%;
			padding: 15px;
			margin: 5px 0 22px 0;
			border: none;
			background: #f1f1f1;
		  }

		  input[type=text]:focus, input[type=password]:focus {
			background-color: #ddd;
			outline: none;
		  }

		  /* Set a style for the submit button */
		  .btn {
			background-color: #4CAF50;
			color: white;
			padding: 16px 20px;
			border: none;
			cursor: pointer;
			width: 100%;
			opacity: 0.9;
		  }

		  .btn:hover {
			opacity: 1;
		  }
		  .formcontainer{
			background: white;
			/* margin-top: 13%; */
			width: 40%;		
			padding: 23px;
			margin: 0 auto;
			margin-top: 10%;
			opacity: .9;
			border-radius: 10px;			
		}
		
		.bg-img {
			/* The image used */
			background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
			width: 100%;
			height: 100%;
			/* Center and scale the image nicely */
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			position: relative;
		}
		
		@media (max-width: 768px){
			.real_time_monitoring>h3 {
				font-size: 12px;
				display: none;
			}
			.col-sm-4 {
				-ms-flex: 0 0 100%;
				flex: 0 0 100%;
				max-width: 100%;
				margin-bottom: 0% !important;
				padding-bottom: 0% !important;
			}
			
			.formcontainer{
				background: white;
				/* margin-top: 13%; */
				width: 80% !important;
				padding: 23px;
				margin: 0 auto;
				margin-top: 10%;
				opacity: .9;
				border-radius: 10px;
			}
			
			.bg-img {
				height: 100%;
			}
			
		}
		</style>
	</head>
	<body>
		<div class="bg-img">
			<div class="container">
				<form action="login" method="post" class="formcontainer">			
					{{ csrf_field() }}			
					@if(!empty(Session::get('session_message')))
						<div style="background-color:red; padding: 14px; color:white; border-radius: 10px;">		
							{{ Session::get('session_message') }}
						</div>
					@endif
					<h2 style="text-align:center;">Login Authentication</h2><hr>
					<label for="username"><b>Username</b></label>        
					<input type="text" placeholder="Enter Username" name="username" required>
					<label for="psw"><b>Password</b></label>
					<input type="password" placeholder="Enter Password" name="password" required>
					<button type="submit" class="btn">Go</button>			
				</form>    
			</div>
		</div>
	</body>
</html>
