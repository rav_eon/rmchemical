@extends('header')
<!--link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simple-keyboard@latest/build/css/index.css"-->
<style>
.vtabs {
    width: 100% !important;
}
.vtabs .tabs-vertical {
    width: 204px !important;
}
.plant_detail_1 > a {
    color: #272c33 !important;
}.table td, .table th {
    border-color: #009efb !important;
}
.dev_style {
    color: #272c33 !important;
    font-weight: normal !important;
}
.card-body.heading-block {
    padding: 10px;
    background: #999595;
}
.heading-text span {
    color: white;
}

.vtabs {
width: 100% !important;
}
.vtabs .tabs-vertical {
width: 204px !important;
}
.plant_detail_1 > a {
color: #272c33 !important;
}.table td, .table th {
border-color: #5b826f !important;
}
.dev_style {
color: #272c33 !important;
font-weight: normal !important;
}
/* .card-body.heading-block {
padding: 10px;
background: #999595;
} */
.heading-text span {
color: #174d33;
font-style: italic;
}
.heading-text {
height : 60px;
}

.page-wrapper {
    margin-left: 0px !important;  
}
.footer {
	left: 0px !important;
	text-align:center;   
}
.span-date{
	display:none;
}
.contact-list td {
    vertical-align: middle;
    padding: 10px 10px !important;
}

text[Attributes Style] {
    text-anchor: start;
    font-family: Arial;
    font-size: 35 !important;
    stroke: none;
    stroke-width: 0;
    fill: rgb(255, 255, 255);
	
	padding-right: 20px !important;
}

.admin_display_bar{
	background: #3c6382;
    color: white;
}

.prod td {
    vertical-align: middle;
    padding: 25px 10px;
    background: #efe78f;
    padding: 27px !important;
    font-size: 30px;
    font-weight: bold;
}
.breakdown_reasonns{	
	/*width:48%;*/
	width:18%;
}
.subbreak{
	/*width:48%;*/
	width:18% !important;
}
.modal-dialog {
    /*max-width: 850px !important;*/
	max-width: 96% !important;    
    margin: 2rem auto;
}
.activaa{
	background-color:#0f3c26 !important
}
.subactivaa{
	background-color:#0f3c26 !important
}

	@media only screen and (max-width : 640px) {
		.container{
			width:100% !important;
		}
		.operatorsub{
			font-size: 30px; 
			font-weight: bold;
		}
		
		.bg-img {
			height:auto;
		}
		.real_time {
			font-size: 16px !important;
			DISPLAY: NONE;
		}
		.keyboard-key .keyboard-key-sm{
			width: 40px !important;
		}
		
		/* Set a style for the submit button */
		.btn {
			background-color: #4CAF50;
			color: white;
			padding: 8px 20px;
			border: none;
			cursor: pointer;
			width: 100%;
			font-size:30px;
			opacity: 0.9;
		}
		
		input[type=text], input[type=password] {
			width: 100%;
			padding: 0px 10px !important;
			margin: 5px 0 0 0;
			font-size:18px;
			border: none;
			background: #f1f1f1;
		}
		.operatorsub{
			margin-top: 0px !important;
		}
		
		.keyboard-row, .keyboard-wrapper .keyboard-input-field {
			font-family: inherit;
			font-size: inherit;
			line-height: inherit;
			height: 35px !important;
		}
		
		.formcontainer{
			background: white;
			width: 90% !important;	
			font-size:23px;
			padding: 23px;
			margin: 0 auto;
			margin-top: 1.5%;
			opacity: .9;
			border-radius: 10px;			
		}
		
		label{
			font-size: 16px;
			font-weight:bolder;
		}
		
		<!-- keyboard -->
		.keyboard-blackout-background{
			background-color: rgb(45 78 33 / 90%) !important;
		}
		.keyboard-wrapper{
			margin-left:190px !important;
			font-size:18px !important;
			z-index:99999 !important;
			top: 5% !important;
			bottom: 20px !important;
		}
		/*.keyboard-wrapper .keyboard-input-field{
			height: 0px !important;
		}*/
		
		.keyboard-wrapper .keyboard-action-wrapper {
			width: 56% !important;
		}
		
		button.keyboard-key.keyboard-key-lg{
			width: 70px !important;
		}
		button.keyboard-key.keyboard-key-xl{
			width: 150px !important;
		}
		button.keyboard-key.keyboard-key-sm{
			width: 30px !important;
		}
		<!-- keyboard -->
	}

	@media (max-width: 768px){
		.breakdown_reasonns{	
			/*width:46%;*/
			width:18%;
		}
		.subbreak{
			/*width:46%;*/
			width:18%;
		}
		.modal-content {
			width: 90% !important;
			margin: 0 auto !important;
		}
		.real_time{
			font-size: 16px !important;		
		}
	}
	
	@media only screen and (max-width : 1280px) {
		.row {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
			margin-top: 6px !important;
		}
		.real_time{
			font-size: 20px;
		}
	}
	@media only screen and (max-width : 1024px) {
		.operatorsub{
			font-size: 30px; 
			font-weight: bold;
		}		
		.real_time{
			font-size: 20px;
		}
		.formcontainer{
			background: white;
			width: 70%;	
			font-size:23px;
			padding: 23px;
			margin: 0 auto;
			margin-top: 3%;
			opacity: .9;
			border-radius: 10px;			
		}
		label{
			font-size: 30px;
		}
		.row{
		    margin-top: -8px !important;
		}
		.table thead th, .table th {
			border: 0px;
			padding: 8px 19px;
		}
		input.form-control.operatorcode {
			padding: 31px 20px;
			font-size: 28px;
		}
		.operatorcode {			
			font-size: 30px;
			height:60px;			
		}
		input[type=text], input[type=password] {
			width: 100%;
			padding: 5px;
			border: none;
			font-size: 25px;
			font-weight: bold;
		}
		
		<!-- keyboard -->
		.keyboard-blackout-background{
			background-color: rgb(45 78 33 / 90%) !important;
		}
		.keyboard-wrapper{
			font-size:18px !important;
			z-index:99999 !important;
			top: 5% !important;
			bottom: 20px !important;
			/* margin-left:170px !important;*/
		}
		button.keyboard-key.keyboard-key-xl{
			width: 270px !important;
		}
		button.keyboard-key.keyboard-key-lg{
			width: 100px !important;
		}
		button.keyboard-key.keyboard-key-sm{
			width: 55px !important;
		}
		.keyboard-wrapper .keyboard-input-field{
			height: 35px !important;
		}
		.keyboard-wrapper .keyboard-action-wrapper {
			width: 100% !important;
		}
		<!-- keyboard -->
	}

.contact-list td {
    vertical-align: middle;
    padding: 25px 10px;
    /*background: #1d3244;
	background: #3c6382;
    color: white;*/
}
html body .font-light {
    font-weight: 300;
    font-size: 25px !important;
}
.infomargin{
	margin-bottom:10px;
}
.infomargin2{
	margin-top: 3px;
}
.confirmclosebutton>button{
	font-size: 23px;
}
.rpmheading{
	font-size:30px;
}
.rpm_button{
	font-size:30px !important;
}
.sku_row{
	font-size:30px !important;
}
.sku_row>option{
	font-size:30px !important;
}
.skuuuuu{
	font-size:30px !important;
}
.livecount{
	color:white;
}
.productiontitle {
    font-size: 29px;
    font-weight: bold;
}
.operatordatetime{
	font-size: 25px !important;
	font-weight:300;
}
.float-right{
	font-size:21px !important;
	font-weight: 900 !important;
	margin-right: -3%;
}
#footer{
	font-size:22px;
}

.modal-body .container{
	max-width:100% !important;
}
.bg-info-9{
	background-color: #c23616 !important;	
}

.btn-info:hover, .btn-info.disabled:hover {    
	opacity: 1 !important;
}

.topbar ul.dropdown-user {
    padding: 0px;
    width: 333px !important;
}
.topbar ul.dropdown-user li a {
    padding: 9px 15px;
    display: block;
    color: #54667a;
    font-size: 25px;
}
	
<!-- keyboard -->
.keyboard-blackout-background{
	z-index:99999 !important;
}
.keyboard-wrapper{
	z-index:99999 !important;
	top: 20% !important;
	bottom: 20px !important;
}
<!-- keyboard -->
</style>	
	<?php		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
	?>
	<?php
		// echo $dbname;
		// $dbname == NULL;
		// dd($dbname);
		
		if($dbname == NULL){
			// return view('operatorlogin');
			// dd( url('/operatorlogin') );
			// dd('000');
	?>
			<script>window.location.href = "{{ url('/operatorlogin') }}";</script>
	<?php
			echo "";
			dd('111');
		}
	
	
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		
		
		$operator_name = Session::get('operator_name');
		//dd($operator_name);
		$designation_id = Session::get('designation_id');
		$plant = Session::get('plant');
		$opidd = session::get('operator_id');
		
		if($opidd == ''){
	?>
			<script>window.location.href = "{{ url('/operatorlogin') }}";</script>
	<?php
			echo "";
			dd('111');
			
		}
	
		// dd($opidd);
		$operatorloggedout = pg_query($db, "SELECT * FROM rm_oplogins WHERE operator_id = $opidd ORDER BY logintime DESC");
		$operatorlogoutrow = pg_fetch_array($operatorloggedout);
		$oploggedin = $operatorlogoutrow['loggedin'];
		// echo $oploggedin;
		// if($oploggedin == 0){
		
		// $new_time = date($logintime, strtotime($hour.' hours'));
		// echo $new_time;
		// dd(Session::get('operatorlastentry'));
		
		if(empty(Session::get('operator_name'))){
	?>
			<script>window.location.href = "{{ url('/operatorlogin') }}";</script>
	<?php
		  //return redirect('/login');
		}
		if( Session::get('designation_id') != 5){
	?>
			<script>window.location.href = "{{ url('/message') }}";</script>
	<?php
		}
	?>
    <body class="fix-header fix-sidebar card-no-border" id="container">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
	
    <span>    
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <!--header class="topbar" style="display:none;"-->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/operator') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
				<div class="" style="padding-left:15%; text-align:center;">
					<h2 style="color:#194F35 !important;" class="real_time"><strong>Real Time Production Monitoring System</strong></h2>
				</div>
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a></li>                       
                    </ul>
					
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!--li class="nav-item hidden-sm-down">
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
						</li-->
						<h2 style="font-size:20px;">Welcome <?php echo Session::get('first_name') ?>! </h2>&nbsp;&nbsp;&nbsp;
                        <li class="nav-item dropdown">
                            <!--a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-user" aria-hidden="true"></i>
							</a-->
							
							<button class="btn btn-secondary" data-toggle="modal" data-target=".bs-example-modal-sm">Logout</button>
							
							
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">
                                                <h5>Operator Code:</h5>
                                                <h4>{{ Session::get('operator_name') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
									<!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="operatorlogout">
											{{ csrf_field() }}
											<a href="operatorlogout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>									
                                </ul>
                            </div>
                        </li>                        
                    </ul>
                </div>
            </nav>
        </header>
		<?php 
			// dd(Session::all());
		?>
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
	<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!--div class="row">
			<div class="col-lg-12">
				<div class="col-md-6" style="font-family:'Rubik', sans-serif, text-align:right; font-size: 30px; font-weight: bolder;">
					<h2 class="text-themecolor mb-0 mt-0 float-left" style="font-size:25px; float:left;">Operator Input Control Panel</h2>
				</div>
				<!--div class="col-md-6">
					<button class="btn btn-info float-right" style="font-weight: 100 !important;">Welcome <?php //echo Session::get('first_name') ?>! </button>
				</div-->
			<!--/div>
		</div><br/-->
		<!-- new code -->	
		@if($errors->any())
			<div class="successMessage" style="background-color:red; padding: 14px; font-weight:bolder; color:white; border-radius: 10px;">	
				{{ implode('', $errors->all(':message')) }}
			</div>
		@endif
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="card">
					<div class="card-body">
						<!--h3 class="heading-text" style="font-family:'Rubik', sans-serif"><span>Production History of Machine</span><span class="span-date">Date : <?php //echo $yesterday_date ?><?php // echo $today = date("j-F-Y");   ?></span></h3-->
						<div class="row">
							<div class="col-lg-12">
								<div class="col-md-4 float-left" style="font-family:'Rubik', sans-serif, text-align:right; font-size: 20px;	font-weight: bolder;"><span>
									<!--strong>Plant: <div class="btn btn-default active"><?php //echo Session::get('plant_id') ?></div>
									Cascade: <div class="btn btn-default active"><?php //echo Session::get('cascade_id') ?></div></strong-->
									<strong><div class="btn btn-default active" style="font-size:20px; font-weight:600;">Machine: <?php echo Session::get('machine_name') ?></div></strong>
								</div>								
								<?php	
									// echo "<pre>";
									// echo session::get('breakdown_limit');
									// echo "</pre>";
									$opid = session::get('operator_id');
									// print_r(session::get('operator_id'));
									$operatorsku = pg_query($db, "SELECT * FROM rm_opskudata WHERE operator_id = $opid ORDER BY id DESC LIMIT 1");
									$operatorrow = pg_fetch_array($operatorsku);
									if(Session::get('sku_name')){
										$operatordate = date('d-M-Y', strtotime($operatorrow['sku_start_at']));
										$operatortime = date("h:i:sa", strtotime($operatorrow['sku_start_at']));
								?>
								<div class="col-md-8 float-right" style="font-family:'Rubik', sans-serif, text-align:right; font-size: 20px !important; font-weight: 600 !important; margin-right:0%; margin-top:-12px">
									<!--strong>SKU Information:</strong-->
									<span class="btn btn-default active">
										<button class="btn btn-default operatordatetime" style="color:#7460ee; font-size:20px;">
										<strong style="color:black; font-size:20px; font-weight:600;">Login Time: </strong><?php echo $operatordate; ?> | <?php echo $operatortime; ?></button>
									</span>
								</div>
								<?php
									}
								?>
							</div>	
						</div>							
						<br/>
						<div class="row">
							<!-- col -->
							<div class="col-lg-6 col-md-6 col-sm-6 input_panel">
								<div class="card bg-info sku_info_bg">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<?php
												if(!Session::get('sku_row_id')){
											?>
											<div class="carousel-inner information">
												<div class="carousel-item flex-column active">
													<h3 class="text-white font-light" style="font-size: 25px;">
														<div class="infomargin" style="font-weight:600">SKU Information</div>
														<span class="font-bold">Running SKU </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Not Selected</span>
													</h3>
												</div>		
											</div>													
											<?php
												}else{
											?>
											<div class="information">	
												<h3 class="text-white font-light">
													<div class="infomargin" style="font-weight:600">SKU Information</div>
													<div class="font-bold">Running SKU: &nbsp;{{ Session::get('sku_name') }} | RPM: &nbsp;{{ Session::get('sku_rpm') }}</div>
												</h3>
											</div>
											<?php
												}
											?>	
										</div>
									</div>
								</div>
							</div>
							<!-- col -->
							<?php
								if(!Session::get('sku_row_id')){
							?>
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="card bg-info-3 no_access" style="background: #b9b9b9 !important;">
									<div class="card-body" >
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<h3 class="text-white font-light" style="font-size: 25px;">
														<div class="infomargin" style="font-weight:600">Production Data</div>
														<div class="font-bold infomargin2">Live Data</div>
														<!--div class="font-bold infomargin2" style="visibility:hidden;">Live Data</div-->
													</h3>
												</div>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- col -->
							<!--div class="col-lg-4 col-md-6">
								<div class="card bg-info-4 no_access" style="background: #b9b9b9 !important;">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<!--div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<h3 class="text-white font-light">
														<div class="infomargin" style="font-weight:600">Downtime Reason</div>
														<span class="font-bold infomargin2">Production Down Reasons</span>
														<span class="font-bold infomargin2" style="visibility:hidden;">Production Down Reasons</span>
													</h3>
												</div>												
											</div>
										</div>
									</div>
								</div>
							</div-->
							<?php
								}else{
							?>
							<div class="col-lg-6 col-md-6 col-sm-6 production_data">
								<div class="card bg-info-3">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<h3 class="text-white font-light infomargin">
														<div class="infomargin" style="font-weight:600">Production Data</div>
														<span class="font-bold infomargin2">Live Data</span>
														<!--div class="font-bold infomargin2" style="visibility:hidden;">Live Data</div-->
													</h3>
												</div>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- col -->
							<!--div class="col-lg-4 col-md-6 downtime_reason"-->
							<!--div class="col-lg-4 col-md-6" >
								<div class="card bg-info-9">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<!--div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<h3 class="text-white font-light infomargin">
														<div class="infomargin" style="font-weight:600">Downtime Reason</div>
														<span class="font-bold infomargin2">Production Down Reasons</span>
														<span class="font-bold infomargin2" style="visibility:hidden;">Production Down Reasons</span>
													</h3>
												</div>												
											</div>
										</div>
									</div>
								</div>
							</div-->
							
							<?php
								}
							?>
						</div>						
					</div>
				</div>
			</div>
		</div>			
		<!--/div> <!-- Card end 2-->
		<div class="row production_graphics">
			<div class="col-12">
				<div class="card ">					
					<div class="card-body">	
						<h2 class="productiontitle">Live Production <button class="btn btn-info machineprodction" style="float:right; border-radius:5px; background:#5b826f; color:white; padding:2px 20px; margin-bottom:10px; margin-left:10px; font-size:25px;">
							<i class="fa fa-refresh" aria-hidden="true"></i>&nbsp; Refresh</button></h2>
						<!--div id="piechart_3d" style="width: 900px; height: 500px;"></div-->						
						<!--span style="float:left; border-radius:5px; background:#5b826f; color:white; padding:20px; margin-bottom:10px; font-size:25px;">Live Production<!--span id="liveprod" class="livedata"--></span><?php // echo $cnttotal; ?><!--/span-->
						<input type="hidden" class="currentproduction" value="">
						<!--button class="btn btn-info machineprodction" style="float:right; border-radius:5px; background:#5b826f; color:white; padding:2px 20px; margin-bottom:10px; margin-left:10px; font-size:25px;">
							<i class="fa fa-refresh" aria-hidden="true"></i>&nbsp; Refresh</button-->
						<!--input type="text" class="liverecord livedata" value=""/-->
						<button type="button" class="btn btn-info btn-rounded m-t-10 add_button rpm_show" id="empModal2" data-toggle="modal" data-target="#empModal" style="align-items:center; display:none;"></button>						
						
						<div id="barchart_material">
							<table id="demo-foo-addrow"
								class="table table-bordered m-t-10 table-hover contact-list" data-paging="true"
								data-paging-size="7">								
								<thead>
									<tr class="admin_display_bar">
										<!--th>S. No</th-->
										<th><h2 class="livecount">SKU Name</h2></th>
										<?php
											
											$machine_name = Session::get('machine_regno');
											// dd(Session::all());
											// dd($sku_name);
											$machinetypeid = pg_query($db, "SELECT * FROM rm_machines LEFT JOIN rm_machinetypes ON rm_machines.machine_type_id = rm_machinetypes.id WHERE machine_regno = '$machine_name'");
											$machinetypeidval = pg_fetch_array($machinetypeid);
											$machine_nametype = $machinetypeidval['output_counter_id'];
											// echo "<pre>";
											// print_r($machinetypeidval['output_counter_id']);
											// echo "</pre>";
											// echo "---------";
											if($machine_nametype == 1){												
										?>
												<th><h2 class="livecount">Live Batch Count</h2></th>
												<th><h2 class="livecount">Live Production QTY</h2></th>
												<!--th><h2 class="livecount">Live Production QTY</h2></th-->
										<?php
											}else{
										?>
												<th><h2 class="livecount">Live CLD/Bag</h2></th>
												<!--th>Pack per CLD/Bag</th-->
												<th style="display:none;"><h2 class="livecount">Pack Size</h2></th>
												<th style="display:none;"><h2 class="livecount">Unit</h2></th>
												<th style="display:none;"><h2 class="livecount">Weight/Ltr per CLD/Bag</h2></th>
												<th><h2 class="livecount">Live Production QTY(KG/LTR)</h2></th>
										<?php
											}
										?>
									</tr>
								</thead>
								<tbody>	
									<?php
										// $production_query = pg_query($db, "SELECT * FROM rm_skus WHERE sku_code =$sku_code");
										$sku_name = Session::get('sku_name');
										// dd($sku_code);
										$production_query = pg_query($db, "SELECT * FROM rm_skus WHERE sku_name='$sku_name'");
										$i =1;
										while($production_rows = pg_fetch_array($production_query)){
											// echo "<pre>";
											// print_r($production_rows);
											// echo "</pre>";										
									?>
									<tr class="prod">
										<!--td><?php // echo $i; ?></td-->										
										<td><?php echo $production_rows['sku_name'];?></td>
										<td class="livedata" style="font-size:30px; font-weight:bolder;"></td>
										<!--td>4</td-->
										<?php
											// $sku_name = Session::get('machine_regno');											
											if($machine_nametype == 1){
												
										?>
											<td style="display:none;"><?php echo $production_rows['pack_size'];?></td>
											<td style="display:none;"><?php echo $production_rows['pack_size_unit'];?></td>
											<td class="unitdata" style="display:none;">
												<?php 
													$productioncld = $production_rows['unit_per_cld'];
													echo $productioncld;
												?>
											</td>
											<td class="totaldata"></td>
											<!--td class="totaldata" style="display:none;"></td-->
										<?php
											}else{
										?>
											<td style="display:none;"><?php echo $production_rows['pack_size'];?></td>										
											<td style="display:none;"><?php echo $production_rows['pack_size_unit'];?></td>
											<td class="unitdata" style="display:none;">
												<?php 
													$productioncld = $production_rows['unit_per_cld'];
													echo $productioncld;
												?>
											</td>
											<td class="totaldata"></td>
										<?php
											}
										?>
									</tr>			
									<?php
											$i++;
										}											
									?>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 input_output">
				<div class="card">					
					<div class="card-body">	
						<div class="table-responsive" style="">	
						<?php
							if(Session::get('sku_row_id')){
								// echo session::get('operator_id');
								$lastskuid = session::get('operator_id');
								$skurowquery = pg_query($db, "SELECT * FROM rm_opskudata WHERE operator_id = $lastskuid ORDER BY id DESC LIMIT 2");
								
								$skurowarray = array();
								while($skurowwww = pg_fetch_array($skurowquery)){									
									$skuidd = $skurowwww['sku_id'];							
									$skurowarray[] = array(
										'skuidd' => $skuidd
									);									
								}	
								// echo Session::get('sku_name');
								// end sku edit hereeee
								if(Session::get('sku_name') == ''){
						?>			
									<button type="button" class="btn btn-info btn-rounded m-t-10 add_button2 rpm_show3" data-toggle="modal" data-target="#exampleModalCenter" style="align-items:center;  font-size: 30px;">Start SKU Production</button>&nbsp;&nbsp; <span style="font-size:30px; font-weight:bolder;">Previous SKU: &nbsp;&nbsp;</span>
						<?php
								}else{
						?>
									<button type="button" class="btn btn-info btn-rounded m-t-10 add_button rpm_show" id="endskuproduction" data-toggle="modal" data-target="#endproduction" style="align-items:center;  font-size: 30px;">End SKU Production</button>&nbsp;&nbsp; <span style="font-size:30px; font-weight:bolder;">Previous SKU: &nbsp;&nbsp;</span>
						<?php
								}
								if(count($skurowarray)>1){
									// echo "here";
									$skuiddd = $skurowarray[1]['skuidd'];
									// echo "<br/>";
									$selectskuname = pg_query($db, "SELECT sku_name FROM rm_skus WHERE id=$skuiddd");
									$selectskurow = pg_fetch_array($selectskuname);
						?>
									<button class="btn btn-primary" style="font-size:20px"><?php print_r($selectskurow['sku_name']); ?></button>
						<?php								
								}else{
						?>
									<button class="btn btn-primary">No last sku available</button>						
						<?php
								}
						?>								
							<button type="button" class="btn btn-info btn-rounded m-t-10 add_button2 rpm_show3" id="startskuproduction" data-toggle="modal" data-target="#exampleModalCenter" style="align-items:center;  font-size: 30px; display:none;">Start SKU Production</button><br/><br/>								
								
							<div class="modal fade productionpop" id="endproduction" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document" style="width: 42%; top: 26%; font-size: 30px;">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel" style="font-size: 30px;">SKU End Confirmation</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body areyousure">
											<div>Are you sure you want End SKU Production?</div>
											<div>क्या आप वाकई SKU उत्पादन समाप्त करना चाहते हैं?</div>
										</div>									  
										<div class="modal-footer confirmclosebutton">
											<button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
											<input type="hidden" name="skuname" class="skuname" value="<?php echo Session::get('sku_row_id'); ?>">
											<button type="button" class="btn btn-primary yesiwant" data-toggle="modal" data-target="#exampleModalCenter">Confirm</button>
										</div>
									</div>
								</div>
							</div>						
						<?php
							}else{
						?>
								<button type="button" class="btn btn-info btn-rounded m-t-10 add_button rpm_show3" data-toggle="modal" data-target="#exampleModalCenter" style="align-items:center; font-size: 23px;">Select SKU Information</button><br/><br/>									
						<?php
							}
							// echo Session::get('latestskuid');
						?>							
						</div>
					</div>
				</div>
			</div>	
			
		</div>			
		
		

		<!-- Start and end sku production modal --->
		<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<!--div class="modal-header" style="visibility:hidden;"-->
					<div class="modal-header" style="visibility:hidden;">
						<!--h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5-->
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<!--form method="post" action="opskudata" class="add_rpm" style="display:none;"-->		
						<form method="post" action="opskudata" class="add_rpm">		
							@csrf()
							<!--label>Select SKU</label-->
							<label class="rpmheading">Select SKU</label>
							<?php 
								$pincode = Session::get('plant_pincode');								
							?>
							<select name="sku_row" class="form-control sku_row" id="sku_row" required>
								<option value="">-- Select SKU --</option>
								<?php	
									$rpm_sku = pg_query($db, "SELECT rm_skus.sku_name,rm_skus.id FROM rm_pincodes LEFT JOIN rm_plantproducts ON rm_plantproducts.plant_id = rm_pincodes.plant_id LEFT JOIN rm_skus ON rm_skus.product_id=rm_plantproducts.product_id WHERE rm_pincodes.pin_code = '$pincode'");
																			
									while($rpm_sku_row = pg_fetch_array($rpm_sku)){
										// echo "<pre>";	
										// print_r($rpm_sku_row);
										// echo "</pre>";
								?>
								<option value="<?php echo $rpm_sku_row['id']; ?>" <?php echo($rpm_sku_row['id'] == Session::get('id') ? 'selected' : '' ); ?>><?php echo $rpm_sku_row['sku_name'];   ?></option>
								<?php							
									}
								?>									
							</select><br/>
							<br/>
							<?php	$check = '';$rpm_default = ''; ?>
							<div class="container" style="margin-left: -14px;">
								<div class="row">
									<div class="col-md-2 ">
										<!--label>Select RPM</label-->
										<label class="rpmheading">RPM</label>
									</div>																			
								</div>
							</div>															
							<select name="sku_rpm" class="skuuuuu form-control" id="sku_rpm_val" disabled required></select><br/><br/>
							<input type="hidden" name="sku_rpm2" id="sku_rpm2" value="">
							
							<div id="stage">
								<?php									
									$rpm_val = '';
									echo json_decode($rpm_val);									
								?>									
							</div>			
							
							<label class="rpmheading">Remark</label>
							
							<textarea class="form-control remarks" id="remarks" name="remarks" rows="4" cols="50" ></textarea>
							<br/><br/>
							
							
							
							<input type="submit" value="Confirm & Start Selected SKU Production" class="form-control btn btn-info rpm_button" style="color:white; font-size:18px;" disabled>							
						</form>
					</div>
					
					
					<!--div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div-->
				</div>
			</div>
		</div>
		<!-- Start and end sku production modal --->

		
	<!-- Modal -->
	<div class="modal fade" id="empModal" role="dialog" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog"> 
			<!-- Modal content-->
			<!--=============== Input Breakdown Reason ===================--->
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title">Breakdown Reason</h2>
					<!--button type="button" class="close" data-dismiss="modal">&times;</button-->
				</div>
				<div class="modal-body">					
					<?php
						date_default_timezone_set('Asia/Kolkata');
						$currentTime = date( 'H:i:s', time () );
						// echo $currentTime;								
						$current_date = date('d-m-Y');
						$night_shift = "23:00:00";						
					?>	
					<!--div class="container breakdowncontainer"-->
					<div class="container">
						<div class="row">
							<form method="post" action="opreason" class="rpm_downreason_form">		
								@csrf()
								<?php 
									if($currentTime >= $night_shift){						
								?><br/>							
								<label>Start Date & Time</label>											
								<input type="datetime-local" class="form-control date_time2" name="start_time"  required><br/><br/>
								<label>End Date & Time</label>								
								<input type="hidden" name="timeflag" class="timeflag" value="1">
								<input type="hidden" class="startertime end_time" name="end_timevalue" value="">
								<input type="datetime-local" class="form-control" name="end_time" required><br/><br/>
								<?php
									}else{
										//echo "other shift";
								?><br/>	
								<div>												
									<div style="width:20%; float:left;">
										<h2><label>Start Time</label>								
										<!--input type="time" class="form-control" name="start_time" required-->
										<input type="text" class="form-control downreasontime downrealtime" name="start_time" value="<?php echo $currentTime; ?>" style="font-size:25px !important;" disabled></h2>
									</div>
									<div style="width:400px; float:left; margin-left:5%;">
										<h2><label>End Time</label>	
										<input type="hidden" name="timeflag" class="timeflag" value="1">
										<input type="hidden" class="startertime end_time" name="end_timevalue" value="">
										<input type="hidden" class="form-control endtimevalue" name="end_time" style="font-size:25px !important;" value="" required>
										<!--input type="text" class="form-control endtimevalue" name="end_time" value="" required-->
										
										
										<input id="timepkr" placeholder="hh:mm:ss" name="end_time" style="float:left; font-size:25px !important;">
										<h2 class="btn btn-primary showtime" onclick="showpickers('timepkr',24)" style="width:40px; z-index:999; float:left; padding: 12px 0px; margin-top: -8px;"><i class="fa fa-clock-o"></i></h2>										
									</div>										
									
									
								</div>
								<div class="timepicker"></div>	
								<?php
									}
								?><br/><br/>
								<input type="hidden" name="start_time" value="<?php echo $currentTime;?>"><br/><br/>
								<!--label>Select Breakdown Reason / ब्रेकडाउन का कारण चुनें </label><br/-->
								
								<button class="btn btn-info selbreakreason" style="width:100%; background-color:#7460ee;  margin-bottom:10px; font-size: 25px;">
									<label>Select Breakdown Reason / ब्रेकडाउन का कारण चुनें </label>
								</button>
								<div class="container">					
									<?php							
										$rpm_breakdownreason = pg_query($db, "SELECT * FROM rm_breakdownreasons WHERE status=true");
										while($rpm_breakdown_row = pg_fetch_array($rpm_breakdownreason)){
									?>											
											<!--button type="button" class="btn btn-info breakdown_reasonns" style="margin-bottom:10px;" value="<?php //echo $rpm_breakdown_row['id']; ?>"><?php //echo $rpm_breakdown_row['breakdown_reason']; ?></button-->
											<button name="breakdownreason" class="btn btn-info breakdown_reasonns activa" id="break<?php echo $rpm_breakdown_row['id']; ?>" name="breakdownreason" style="margin-bottom:10px; background-color:#5b826f; text-align: left !important; font-size: 25px; height: 195px; width:30%" value="<?php echo $rpm_breakdown_row['id']; ?>">
												<!--div><img src="<?php //echo url('/images').'/'.$rpm_breakdown_row['breakdown_icon']; ?>" style="background:white; padding:5px;" width="40px"></div-->
												<div><img src="<?php echo url('/images').'/'.$rpm_breakdown_row['breakdown_icon']; ?>" style="background:white; padding:5px;" width="28%"></div>
												<div><?php echo $rpm_breakdown_row['breakdown_reason']; ?></div>
											</button>&nbsp;&nbsp;&nbsp;
									<?php								
										}
									?>	
								</div>
								<input type="hidden" value="" class="subreasonnumber">
								<input type="hidden" value="0" class="subflag">
								<div class="sub_reason3 sub_breakdown_reason3" style="display:none; margin-bottom:15px; margin:12px 0px;">
									<!--button class="btn btn-info subactive selbreakreason" style="width:100%; background-color:#1f4834; margin-bottom:10px;"><label>Select Breakdown Sub-Reason / ब्रेकडाउन  का उप-कारण कारण चुनें </label></button-->
									<button class="btn btn-info subactive selbreakreason" style="width:100%; background-color:#7460ee; margin-bottom:10px; font-size: 25px;">
										<label>Select Breakdown Sub-Reason / ब्रेकडाउन  का उप-कारण कारण चुनें </label>
									</button>
									<!--label>Select Breakdown Sub-Reason</label><br/-->							
									<!--span type="button" name="sub_breakdown_reason3" class="form-control sub_breakdown_reason3" id="sub_breakdown_reason3" style="margin-bottom:30px;"></span-->
									<div class="container">		
										<div class="subbreakbutton sbr" name="subbreakreason" value=""></div>
										<input type="hidden" name="breakreasonvalue" value="" class="breakreasonvalue" required>
										<input type="hidden" name="subreasonvalue" value="" class="subreasonvalue" required>
									</div>
								</div>
								
								<div class="subbreakremark" style="margin:12px 0px; display:none;">
									<button class="btn btn-info subactive selbreakreason"  style="width:100%; background-color:#7460ee; margin-bottom:10px; font-size:25px;">
										<label>Other Reason / अन्य  कारण बताए </label>
									</button>
									<div class="container">											
										<textarea class="form-control otherreason" id="breakdown_remark" name="breakdown_remark" rows="2" cols="20" style="font-size:25px;" placeholder="Please mention other reason here / कृपया अन्य कारणों का उल्लेख यहां करें..." ></textarea>			
										<!--input type="text" class="otherreason input"-->
									</div><br/>
									<!--  Keyboard Start -->
									<!--input class="input" placeholder="Tap on the virtual keyboard to start" /-->
									<!--div class="simplekey simple-keyboard" style="display:block;"></div-->
									<!--  Keyboard End -->
								</div>								
								<div>
									<input type="submit" value="Submit" class="form-control btn btn-info rpm_reason" style="color:white; font-size:25px;" disabled>
									<!--button type="button" style="color:white; font-size:15px;" class="form-control btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Submit</button-->
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="text-align:center;">
					<!--button type="button" class="btn btn-default breakdownmodalclose" data-dismiss="modal" style="width: 100%; padding: 10px 0; font-size: 19px; background:#d8d8d8">Close</button-->
					<!--button type="button" class="btn btn-default breakdownmodalclose" style="width: 100%; padding: 10px 0; font-size: 19px; background:#d8d8d8">Close</button-->
				</div>
			</div>
			<!--=============== Input Breakdown Reason End Here ===================--->
		</div>
	</div>	
	
	<div class="modal fade" id="areyousuretoclose" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog" role="document" style="background: #fff; width:50%; border-radius:10px; top:15%; border: 3px solid #562020;">
			<div class="modal-content" style="background: #fff;">
				<div class="modal-header">
					<h2 class="modal-title" id="exampleModalLabel" style="color:#000;">Confirmation Box / पुष्टीकरण</h2>
					<!--button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button-->
				</div><br/>
				<div class="modal-body" style="text-align:center;">
					<h2 style="color:#000;">Are you sure you want to close? <h2>
					<h2 style="color:#000;">क्या आप वाकई बंद करना चाहते हैं?</h2>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger yesamsuretoclose" style="width:50%; padding:10px; font-size:25px; font-weight:200" data-dismiss="modal">Yes/हाँ</button>
					<button type="button" class="btn btn-success" style="width:50%; padding:10px; font-size:25px; font-weight:200" data-dismiss="modal">Cancel/रद्द करें</button>
				</div>
			</div>
		</div>
	</div>	
	
	
	<!-- Logout Modal Start -->
	<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="max-width: 300px !important;">
			<div class="modal-content">
				<div class="modal-header" style="margin-bottom:5%;"><h4>Logout <i class="fa fa-lock"></i></h4></div>	
				<div class="modal-body" style="margin-top:0%;">
					<h5>Operator Code: {{ Session::get('operator_name') }}</h5>
					<h5>Operator Name: {{ Session::get('first_name') }}</h5>
					<i class="fa fa-question-circle"></i> Are you sure you want to log-off?
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<a href="operatorlogout" class="btn btn-danger btn-block">Logout</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Modal End -->
	
	<script>
		
		// window.open('operator', '', 'fullscreen=yes, scrollbars=auto');
		// window.open('operator','','fullscreen=yes');
		
		
		var myVar = setInterval(myTimer, 50000);
		function myTimer() {
			id = <?php echo session::get('operator_id'); ?>;
			// alert(id);
			$.ajax({
				url:'operatorautologout/'+id,
				type:'get',
				success:function(data){
					// console.log(data);
					if(data == 1){
						// console.log('good job');
					}else{
						// console.log('logout please');
						window.location.href ="{{ url('/operatorlogin') }}";
					}					
				}
			});
			// $("#empModal").modal({
				// backdrop: 'static',
				// keyboard: false
			// })			
		}		
	</script>
	
	
	
@extends('footer')
