<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	
	<!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script-->
    <style>
      body, html {
        height: 100%;
        font-family: Arial, Helvetica, sans-serif;
      }

      * {
        box-sizing: border-box;
      }     

      /* Add styles to the form container 
      .container {
          position: absolute;
          right: 34%;
		  opacity: 0.8;
          top:15%;
          border-radius: 13px;
          margin: 20px;
          max-width: 400px;
          padding: 16px;
          background-color: white;
      }*/
		
      /* Full-width input fields */
      input[type=text], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        border: none;
        background: #f1f1f1;
      }
	  
      input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
      }
	  
	  input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
      }

      /* Set a style for the submit button */
      .btn {
        background-color: #4CAF50;
        color: white;
        padding: 16px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
      }

      .btn:hover {
        opacity: 1;
      }
	.responsive {
	  width: 100%;
	  height: auto;
	}
	.oplogout{
		background-color:red;
		color:white;
		padding:10px;
		text-decoration:none;
		width:100%;
		text-align:center;
	}
	.alreadylogin{
		font-size:20px;
	}

	.formcontainer{
		background: white;
		/* margin-top: 13%; */
		width: 40%;		
		padding: 23px;
		margin: 0 auto;
		margin-top: 10%;
		opacity: .9;
		border-radius: 10px;			
	}
	
	.bg-img {
        /* The image used */
        background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
        width: 100%;
		height: 100%;
        Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
	
	@media (max-width: 768px){
		.real_time_monitoring>h3 {
			font-size: 12px;
			display: none;
		}
		.col-sm-4 {
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
			margin-bottom: 0% !important;
			padding-bottom: 0% !important;
		}
		
		.formcontainer{
			background: white;
			/* margin-top: 13%; */
			width: 80% !important;
			padding: 23px;
			margin: 0 auto;
			margin-top: 10%;
			opacity: .9;
			border-radius: 10px;
		}
		
		.bg-img {
			height: 100%;
		}
		
	}
		
	@media only screen and (max-width : 640px) {
		.container{
			width:100% !important;
		}
		.operatorsub{
			font-size: 30px; 
			font-weight: bold;
		}
		
		.keyboard-key .keyboard-key-sm{
			width: 40px !important;
		}
		
		.bg-img {
			/* The image used */
			background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
			width: 100%;
			height: 700px !important;
			/* Center and scale the image nicely */
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			position: relative;
		}
		
		/* Set a style for the submit button */
		.btn {
			background-color: #4CAF50;
			color: white;
			padding: 8px 20px;
			border: none;
			cursor: pointer;
			width: 100%;
			font-size:30px;
			opacity: 0.9;
		}
		
		input[type=text], input[type=password] {
			width: 100%;
			padding: 5px 25px;
			margin: 5px 0 22px 0;
			font-size:30px;
			border: none;
			background: #f1f1f1;
		}
		
		.formcontainer{
			background: white;
			width: 80% !important;	
			font-size:23px;
			padding: 23px;
			margin: 0 auto;
			margin-top: 3%;
			opacity: .9;
			border-radius: 10px;			
		}
		
		
		
		<!-- keyboard -->
		.keyboard-blackout-background{
			background-color: rgb(45 78 33 / 90%) !important;
		}
		.keyboard-wrapper{
			margin-left:190px !important;
			font-size:18px !important;
			z-index:99999 !important;
			top: 5% !important;
			bottom: 20px !important;
		}
		.keyboard-wrapper .keyboard-input-field{
			height: 0px !important;
		}
		.keyboard-row, .keyboard-wrapper .keyboard-input-field {
			font-family: inherit;
			font-size: inherit;
			line-height: inherit;
			height: 35px !important;
		}
		
		.keyboard-wrapper .keyboard-action-wrapper {
			width: 56% !important;
		}
		
		button.keyboard-key.keyboard-key-lg{
			width: 70px !important;
		}
		button.keyboard-key.keyboard-key-xl{
			width: 150px !important;
		}
		button.keyboard-key.keyboard-key-sm{
			width: 30px !important;
		}
		<!-- keyboard -->
	}
		
	@media only screen and (min-width : 1024px) {
		.operatorsub{
			font-size: 30px; 
			font-weight: bold;
		}
		
		button.keyboard-key.keyboard-key-sm{
			width: 40px !important;
		}
		/* Set a style for the submit button */
		.btn {
			background-color: #4CAF50;
			color: white;
			padding: 16px 20px;
			border: none;
			cursor: pointer;
			width: 100%;
			font-size:30px;
			opacity: 0.9;
		}
		
		input[type=text], input[type=password] {
			width: 100%;
			padding: 30px;
			margin: 5px 0 22px 0;
			font-size:30px;
			border: none;
			background: #f1f1f1;
		}
		
		.formcontainer{
			background: white;
			width: 80% !important;	
			font-size:23px;
			padding: 23px;
			margin: 0 auto;
			margin-top: 3%;
			opacity: .9;
			border-radius: 10px;			
		}
		
		
		
		<!-- keyboard -->
		.keyboard-blackout-background{
			background-color: rgb(45 78 33 / 90%) !important;
		}
		.keyboard-wrapper{
			font-size:18px !important;
			z-index:99999 !important;
			top: 110px !important;
			bottom: 20px !important;
			/* margin-left:170px !important;*/
		}
		button.keyboard-key.keyboard-key-xl{
			width: 270px !important;
		}
		button.keyboard-key.keyboard-key-lg{
			width: 100px !important;
		}
		button.keyboard-key.keyboard-key-sm{
			width: 55px !important;
		}
		.keyboard-wrapper .keyboard-input-field{
			height: 0px !important;
		}
		.keyboard-wrapper .keyboard-action-wrapper {
			width: 100% !important;
		}
		<!-- keyboard -->
	}
    </style>
	
	<!-- ==================== on screen keyboard start ========================================== -->	
	<!--link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simple-keyboard@latest/build/css/index.css"-->
	
	<script src="https://code.jquery.com/jquery.min.js"></script>
	<!--script src="dist/keyboard.js"></script>
	<link rel="stylesheet" href="dist/keyboard.css"-->
	<!-- ==================== on screen keyboard end ========================================== -->
</head>
<body id="videoElement">	
	<div class="bg-img">		
		<div class="container">
			<div id="responsive">
				<form action="operatorcode" method="post" class="formcontainer" autocomplete="off">		
					{{ csrf_field() }}
					@if($errors->any())
						<div style="background-color:red; padding: 14px; color:white; font-weight:bolder; border-radius: 10px;">	
							{{ implode('', $errors->all(':message')) }}
						</div>
					@endif
					<?php 
						if(Session::get('loggedmachineid')){
							// echo Session::get('loggedmachineid');
							// echo "<br/>";
							// echo Session::get('loggedmachinename');
							// echo "<br/>";
							// echo Session::get('loggedopid');
							// echo "<br/>";
							// echo Session::get('loggedusername');
					?>		
						<div style="padding: 14px; color:white; border-radius: 10px;">	
							<div style="background-color:green; margin-bottom:10px; padding:15px;">
								<div>Machine:<span class="alreadylogin">{{ Session::get('loggedmachinename') }}</span></div>
								<div style="margin-bottom:5px;">Operator: <span class="alreadylogin" >[{{ Session::get('loggedusername') }}]</span> Already logged in</div> <div>Kindly logout / कृपया लॉगआउट करें</div>
							</div>
							<div class="oplogout"><a href="operatorlogout" style="text-decoration: none; color: white;font-weight: bolder;">LOGOUT</a></div>
						</div>
					<?php
						}
					?>	
					<h3 style="text-align:center;" class="operatorsub">Operator Authentication</h3><hr>
					<label for="machine_id"><b>Enter Machine Code</b></label>  
					<!--input type="text" name="machine_id" class="form-control test1 input" required-->
					<input type="text" name="machine_id" class="form-control" required>
					<label for="pin_code"><b>Enter PIN Code</b></label>  
					<input type="text" name="pin_code" class="form-control" required>								
					
					
					<!--  Keyboard Start -->
					<!--input class="input" placeholder="Tap on the virtual keyboard to start" /-->
					<div class="simple-keyboard" style="display:none;"></div>
					<!--  Keyboard End -->
					
					
					
					
					<button type="submit" class="btn operatorsub">Next</button>			
				</form>    
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	
	<script>
		// window.open ("operatorlogin","","fullscreen=yes");  
		// window.open('operatorlogin','',"fullscreen=yes");  
		// window.close();
		
		// $(function () {
		  // $("select").select2();
		// });		
	</script>
	<!-- ==================== on screen keyboard start ========================================== -->	
	<!--script src="https://cdn.jsdelivr.net/npm/simple-keyboard@latest/build/index.js"></script-->
	
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../resources/keyboard/main.js"></script>
    <link rel="stylesheet" type="text/css" href="../resources/keyboard/styles.css">
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).keyboard({
                // language: 'us:English, arabic:العَرَبِيَّة, vietnamese:tiếng Việt, hindi:हिन्दी',
                // language: 'us:English, hindi:हिन्दी',
                language: 'us:English',
                enterKey: function () {
                    alert('Hey there! This is a callback function example.');
                },
                keyboardPosition: 'bottom',
                directEnter: false,
                showSelectedLanguage: true
            });
        });
    </script>
	<!-- keyboard form start -->	
	<!-- ==================== on screen keyboard end ========================== -->
</body>	
</html>
