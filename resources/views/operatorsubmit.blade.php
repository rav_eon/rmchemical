
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <style>
      body, html {
        height: 100%;
        font-family: Arial, Helvetica, sans-serif;
      }

      * {
        box-sizing: border-box;
      }

      .bg-img {
        /* The image used */
        background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
        min-height: 100%;
        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
      }

      /* Add styles to the form container 
      .container {
          position: absolute;
          right: 34%;
		  opacity: 0.8;
          top:5%;
          border-radius: 13px;
          margin: 20px;
          max-width: 400px;
          padding: 16px;
          background-color: white;
      }*/

      /* Full-width input fields */
      input[type=text], input[type=password] {
			width: 100%;
			padding: 5px; 
			/* margin: 5px 0 22px 0; background: #f1f1f1;*/
			border: none;
			
			
	  }

      input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
      }
	  
	  input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
      }

      /* Set a style for the submit button */
      .btn {
        background-color: #4CAF50;
        color: white;
        padding: 16px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
      }

      .btn:hover {
        opacity: 1;
      }
	
	.formcontainer{
		background: white;
		/* margin-top: 13%; */
		width: 50%;		
		padding: 23px;
		margin: 0 auto;
		margin-top: 10%;
		opacity: .9;
		border-radius: 10px;			
	}
	
	.bg-img {
		/* The image used */
		background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
		width: 100%;
		height: 100%;
		/* Center and scale the image nicely */
		background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
        position: relative;
    }
	.bg-img {
		height:auto;
	}
	@media (max-width: 768px){
		.real_time_monitoring>h3 {
			font-size: 12px;
			display: none;
		}
		.col-sm-4 {
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
			margin-bottom: 0% !important;
			padding-bottom: 0% !important;
		}
		
		.formcontainer{
			background: white;
			/* margin-top: 13%; */
			width: 80% !important;
			padding: 23px;
			margin: 0 auto;
			margin-top: 10%;
			opacity: .9;
			border-radius: 10px;
		}
		
		.bg-img {
			height:auto;
		}
		
	}
	@media only screen and (min-width : 1024px) {
		.operatorsub{
			font-size: 30px; 
			font-weight: bold;
		}		
		
		.formcontainer{
			background: white;
			width: 70%;	
			font-size:23px;
			padding: 23px;
			margin: 0 auto;
			margin-top: 6%;
			opacity: .9;
			border-radius: 10px;			
		}
		label{
			font-size: 30px;
		}
		input.form-control.operatorcode {
			padding: 31px 20px;
			font-size: 28px;
		}
		.operatorcode {			
			font-size: 30px;
			height:60px;			
		}
		input[type=text], input[type=password] {
			width: 100%;
			padding: 5px;
			border: none;
			font-size: 25px;
			font-weight: bold;
		}
	}
    </style>
</head>
<body>
	<?php
		date_default_timezone_set('Asia/Kolkata');
		$date = new \DateTime();
		$now = date_format($date, 'H:i:s');
		// echo $now;
		// echo "<br/>";
		
		$morning_from = '07:00:00';
		$morning_to = '15:00:00';
		$evening_from = '15:00:00';
		$evening_to = '23:00:00';
		$night_from = '23:00:00';
		$night_to = '07:00:00';
		
		$current_time = date("h:i:s A");
		
		//if($current_time > $morning_from && $current_time < $morning_to ){
		if ($now >= $morning_from && $now <= $morning_to){
			//die('here');
			$shift = 'morning_shift';
			$greeting = 'Machine Information';
			$hello = 'Hello';
			$currentshift_id = 'M';
		}else if($now > $evening_from && $now < $evening_to ){
			//die('here22222');
			$shift = 'evening_shift';
			$greeting = 'Machine Information';
			$hello = 'Hello';
			$currentshift_id = 'E';
		}else if($now > $night_from && $now < $night_to ){
			$shift = 'night_shift';
			$greeting = 'Machine Information';
			$hello = 'Hello';
			$currentshift_id = 'N';
		}
		
	?>
	<div class="bg-img">
		<div class="container">
			<form action="operatorlog" method="post" class="formcontainer">
				{{ csrf_field() }}			
			
				@if($errors->any())
					<div style="background-color:red; padding: 14px; font-weight:bolder; color:white; border-radius: 10px;">	
						{{ implode('', $errors->all(':message')) }}
					</div>
				@endif
				@if(Session::get('op_name'))
				<h2 style="text-align:center; background-color: green; opacity: 1; padding:20px; color:white;" class="operatorsub"><?php echo $hello; ?>   {{ Session::get('first_name') }} !</h2>
				@else
				<h2 style="text-align:center; background-color: green; opacity: 1; padding:20px; color:white;" class="operatorsub"><?php echo $greeting; ?> !  {{ Session::get('op_name') }}</h2>
				@endif
				<input type="hidden" name="currentshift_id" value="<?php echo $currentshift_id; ?>"/>
				
				<div class="row">
					<div class="col-lg-4" style="float:left; width: 40%;">
				<?php
				
					$machine_id = Session::get('machine_id');	
					$machine_regno = Session::get('machine_regno');	
					//dd($machine_id);				
				
					
					$host = Session::get('host');
					$dbname = Session::get('dbname');
					$user = Session::get('user');
					$pass = Session::get('pass');
					// echo $host;
					// echo $dbname;
					// echo $user;
					$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
					
					$machine = pg_query($db, "SELECT * FROM rm_machines WHERE machine_regno='$machine_regno'");
					while($row_machine = pg_fetch_array($machine)){
						// echo "<pre>";
						// print_r($row_machine);
						// echo "</pre>";
						$units = $row_machine['unit_id'];
						$plants = $row_machine['plant_id'];
						$cascades = $row_machine['cascade_id'];
											
						$unit_info = pg_query($db, "SELECT * FROM rm_units WHERE id='$units'");
						while($unit_row = pg_fetch_array($unit_info)){
				?>
							<label for="unit_id"><b>Unit Name</b></label>  
					</div>
					<div class="col-lg-8" style="float:left; width: 60%;">
						<input type="text" name="unit_id" value="<?php echo ucwords($unit_row['unit_name']); ?>"  readonly="readonly" style="background-color: #dedede;"/>
				<?php	
						}
				?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4" style="float:left; width: 40%;">
						<label for="plant_id"><b>Plant Name</b></label> 
					</div>
					<div class="col-lg-8" style="float:left; width: 60%;">
				<?php
						$plant_info = pg_query($db, "SELECT * FROM rm_plants WHERE id='$plants'");
						while($plant_row = pg_fetch_array($plant_info)){
							// echo "<pre>";
							// print_r($plant_row);
							// echo "</pre>";
				?>	
							<input type="text" name="plant_id" value="<?php echo ucwords($plant_row['plant_name']); ?>"  readonly="readonly" style="background-color: #dedede;"/>
				<?php						
						}
						$cascade_info = pg_query($db, "SELECT * FROM rm_cascades WHERE id='$cascades'");
						while($cascade_row = pg_fetch_array($cascade_info)){
				?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4" style="float:left; width: 40%;">
						<label for="cascade_id"><b>Cascade Name</b></label> 
					</div>
					<div class="col-lg-8" style="float:left; width: 60%;">
						<input type="text" name="cascade_id" value="<?php echo ucwords($cascade_row['cascade_name']); ?>"  readonly="readonly" style="background-color: #dedede;"/>
				<?php						
						}
				?>	
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4" style="float:left; width: 40%;">
						<label for="machine_name"><b>Machine Name</b></label> 
					</div>
					<div class="col-lg-8" style="float:left; width: 60%;">
						<input type="text" name="machine_name" value="<?php echo ucwords($row_machine['machine_name']); ?>"  readonly="readonly" style="background-color: #dedede;"/>
				
					</div>
				</div>
				<?php						
						}
					if(Session::get('op_name')){
				?>
				
				<div class="row" >
					<div class="col-lg-4" style="float:left; width: 40%;">
						<label for="operator_name"><b>Operator Code</b></label> 
					</div>
					<div class="col-lg-8" style="float:left; width: 60%;">
						<input type="text" name="operator_name" class="form-control" value="{{ Session::get('op_name') }}"  readonly="readonly" style="background-color: #dedede;"/>
					</div>
				</div>
				<div class="row" style="padding-bottom:5px;">
					<div class="col-lg-4" style="float:left; width: 40%;">
						<label for="shift_duration"><b>Shift Duration</b></label> 
					</div>
					<div class="col-lg-8" style="float:left; width: 60%;">
						<input type="text" name="shift_duration" class="form-control" value="{{ Session::get('shift_duration') }} " readonly="readonly" style="background-color: #dedede;"/>
					</div>
				</div>	
				<?php						
					}
				?>					
				<button type="submit" class="btn operatorsub">Next</button>
			</form>
		</div>
	</div>
</body>
</html>
