
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <style>
      body, html {
        height: 100%;
        font-family: Arial, Helvetica, sans-serif;
      }

      * {
        box-sizing: border-box;
      }

      .bg-img {
        /* The image used */
        background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
        min-height: 100%;
        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
      }

      /* Add styles to the form container */
      .container {
          position: absolute;
          right: 34%;
		  opacity: 0.8;
          top:25%;
          border-radius: 13px;
          margin: 20px;
          max-width: 400px;
          padding: 16px;
          background-color: white;
      }

      /* Full-width input fields */
      input[type=text], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        border: none;
        background: #f1f1f1;
      }

      input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
      }
	  
	  input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
      }

      /* Set a style for the submit button */
      .btn {
        background-color: #4CAF50;
        color: white;
        padding: 16px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
      }

      .btn:hover {
        opacity: 1;
      }
	  
	  .bg-img {
        /* The image used */
        background-image: url("../resources/assets/images/rmchemicals_plant_map.jpg");
        width: 100%;
		height: 100%;
        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
	
	@media (max-width: 768px){
		.real_time_monitoring>h3 {
			font-size: 12px;
			display: none;
		}
		.col-sm-4 {
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
			margin-bottom: 0% !important;
			padding-bottom: 0% !important;
		}
		
		.formcontainer{
			background: white;
			/* margin-top: 13%; */
			width: 80% !important;
			padding: 23px;
			margin: 0 auto;
			margin-top: 10%;
			opacity: .9;
			border-radius: 10px;
		}
		
		.bg-img {
			height:1000px;
		}
		
	}
    </style>
</head>
<body>
	<?php
		date_default_timezone_set('Asia/Kolkata');
		$date = new \DateTime();
		$now = date_format($date, 'H:i:s');
		//echo $now;
		//echo "<br/>";
		
		$morning_from = '07:00:00';
		$morning_to = '15:00:00';
		$evening_from = '15:00:00';
		$evening_to = '23:00:00';
		$night_from = '23:00:00';
		$night_to = '07:00:00';
		
		$current_time = date("h:i:s A");
		
		//if($current_time > $morning_from && $current_time < $morning_to ){
		if ($now >= $morning_from && $now <= $morning_to){
			//die('here');
			$shift = 'morning';
			//echo "morning_shift";
		}else if($now > $evening_from && $now < $evening_to ){
			//die('here22222');
			$shift = 'evening';
			//echo "evening_shift";
		}else if($now > $night_from && $now < $night_to ){
			$shift = 'night';
			//echo "night_shift";
		}
		
	?>
	<div class="bg-img">
		<div class="container">
			<form action="operatorinfo" method="post" class="formcontainer">
				{{ csrf_field() }}
				
				@if(!empty(Session::get('plant')))
					<h2 style="text-align:center;">Plant Title</h2><hr>
					<div style="background-color:green; padding: 14px; color:white; border-radius: 10px; text-align:center;">		
						<?php
							$plant_id = Session::get('plant');
						
							
							$host = Session::get('host');
							$dbname = Session::get('dbname');
							$user = Session::get('user');
							// echo $host;
							// echo $dbname;
							// echo $user;
							$db = pg_connect("host=$host dbname=$dbname user=$user");
		

							$plant_query = pg_query($db, "SELECT * FROM rm_plants WHERE id=$plant_id");
							while($plant_row = pg_fetch_array($plant_query)){
								echo strtoupper($plant_row['plant_name']);					
							}
						?>
					</div>
				@endif
				<h2 style="text-align:center;">Operator Authentication</h2><hr>			
				<label for="operator_name"><b>Enter Your Username</b></label> 			
				<input type="text" name="operator_name" class="form-group">
				<label for="operator_name"><b>Enter Your Password</b></label> 			
				<input type="password" name="operator_pass" class="form-group">			
				<button type="submit" class="btn">Confirm</button>			
			</form>		
		</div>	
	</div>	
</body>
</html>
