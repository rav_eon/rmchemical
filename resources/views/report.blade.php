@extends('header')

<!-- Filter -->
<style>
    .span-class {
        float:right;
    }
    #date {
        width: 10%;
    }
	
.vtabs {
    width: 100% !important;
}
.vtabs .tabs-vertical {
    width: 204px !important;
}
.plant_detail_1 > a {
    color: #272c33 !important;
}.table td, .table th {
    border-color: #009efb !important;
}
.dev_style {
    color: #272c33 !important;
    font-weight: normal !important;
}
.card-body.heading-block {
    padding: 10px;
    background: #999595;
}
.heading-text span {
    color: white;
}
.admin_display_bar{
	background: #3c6382;
    color: white;
}
.vtabs {
width: 100% !important;
}
.vtabs .tabs-vertical {
width: 204px !important;
}
.plant_detail_1 > a {
color: #272c33 !important;
}.table td, .table th {
border-color: #5b826f !important;
}
.dev_style {
color: #272c33 !important;
font-weight: normal !important;
}
/* .card-body.heading-block {
padding: 10px;
background: #999595;
} */
.heading-text span {
color: #174d33;
font-style: italic;
}
.heading-text {
height : 60px;
}
.left-sidebar {
    background: #272c33 !important;
}
.select-date {
    width: auto;
    height: 100%;
}

</style>
<head>
 
  <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
	<div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/shift_officer') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">
						</b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!--li class="nav-item hidden-sm-down">
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                        </li-->Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><!--img src="{{ URL::asset($logo) }}" alt="user"--><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
       
           
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <!--div class="page-wrapper"-->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Dashboard</h3>                       
                    </div>					
                </div>               
                <?php
					$host = Session::get('host');
					$dbname = Session::get('dbname');
					$user = Session::get('user');
					$pass = Session::get('pass');
					
					// echo $user;
					$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
				?>
	<h1>Supervisor
	<button class="span-class btn btn-primary" style="background-color:#174d33;color:white; width:20%; float:right;"><?php  echo $time; ?></button></h1>
	
		<div class="row table-class">
			<div class="col-lg-12">
				<form method="post" action="javascript:void(0)">
					<div class="card">
						<div class="card-body" >	
							<div class="col-lg-12">	
								<div class="row" >
									<div class="col-sm-6">
										<label>From Date:</label>&nbsp;&nbsp;
										<input type="date" class="select-date txtStartDate"  placeholder="<?php  echo $date; ?>"
							onfocus="(this.type='date')" value="<?php  echo $date; ?>" name="txtStartDate">		
									</div>		
									<div class="col-sm-6">
										<label>End Date:</label>&nbsp;&nbsp;
										<input type="date" class="select-date txtEndDate"  placeholder="<?php  echo $date; ?>"
							onfocus="(this.type='date')" value="<?php  echo $date; ?>" name="txtEndDate">		
									</div>			
								</div>&nbsp;&nbsp;&nbsp;&nbsp;
								<div class="row" >							
									@csrf	                               		
									<div class="col-sm-2">
										<label>Unit:</label>&nbsp;&nbsp;
		<?php //echo '**^'.$send_u.'------'.$send_u_n.'-----'.$send_p.'-----'.$send_p_n.'-----'.$send_m.'-----'.$send_m_n.'----'.$send_c.'----'.$send_c_n.'**'; ?>									
										<select class="form-control sv_pcu_unit" name="unit">										
											<?php
												$pcu_unit = pg_query($db, "SELECT * FROM rm_units ORDER BY id ASC");
												while($pcu_unit_row = pg_fetch_array($pcu_unit)){ 
												echo $pcu_unit_row['id'];
											?>

												<option value="<?php echo $pcu_unit_row['id']?>" <?php echo ($pcu_unit_row['id'] == $send_u) ? 'selected':'' ; ?> ><?php echo $pcu_unit_row['unit_name']; ?></option>		
											<?php
												}
											?>
										</select>
										<input type="hidden" class="svunit" name="unit">
									</div>
									<div class="col-sm-2">		
										<label>Plant:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_plant" name="Plant">
											<!--option value="0" selected="selected">All</option-->	
											 <?php
												$dropdown_plant = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id = $send_u");
												while($dropdown_plant_row = pg_fetch_array($dropdown_plant)){ 
												// echo $pcu_unit_row['id'];
											?>
											<option value="<?php echo $dropdown_plant_row['id']?>" <?php echo ($dropdown_plant_row['id'] == $send_p) ? 'selected':'' ; ?> ><?php echo $dropdown_plant_row['plant_name']; ?></option>		
											<?php
												}
											?>
											<!--option value="<?php // echo $send_p; ?>"><?php // echo $send_p_n; ?></option-->		
										</select>
										<input type="hidden" class="svplant" name="plantid">
									</div>	
									<?php //echo $send_c; ?>
									<div class="col-sm-2">
										<label>Cascade:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_cascade" > 										
											<!--option value="0" selected="selected">All</option-->	
											<?php
												$dropdown_cascade = pg_query($db, "SELECT * FROM rm_cascades WHERE plant_id = $send_p");
												while($dropdown_cascade_row = pg_fetch_array($dropdown_cascade)){ 
												// echo $pcu_unit_row['id'];
											?>
											<option class="old" value="<?php echo $dropdown_cascade_row['id']?>" <?php echo ($dropdown_cascade_row['id'] == $send_c) ? 'selected':'' ; ?> ><?php echo $dropdown_cascade_row['cascade_name']; ?></option>		
											<?php
												}
											?>
										</select>									
										<!--option value="<?php // echo $send_c; ?>"><?php // echo $send_c_n; ?></option-->
										<input type="hidden" class="svcascade" name="cascadeid">
									</div>
									<div class="col-sm-2">
										<label>Machine:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_machine">
											<?php
												$dropdown_machine = pg_query($db, "SELECT * FROM rm_machines WHERE cascade_id = $send_c");
												while($dropdown_machine_row = pg_fetch_array($dropdown_machine)){ 
												// echo $pcu_unit_row['id'];
											?>
											<option value="<?php echo $dropdown_machine_row['id']?>" <?php echo ($dropdown_machine_row['id'] == $send_m) ? 'selected':'' ; ?> ><?php echo $dropdown_machine_row['machine_name']; ?></option>		
											<?php
												}
											?>										
											<!--option value="<?php // echo $send_m; ?>"><?php // echo $send_m_n; ?></option-->
										</select>
										<input type="hidden" class="svmachine" name="machineid">
									</div>
									<div class="col-sm-2">
										<label>Shift:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_shift" > 
											<option value="0" selected="selected">All</option>	
											<?php
												$pcu_shift = pg_query($db, "SELECT * FROM rm_shifts");
												while($pcu_shift_row = pg_fetch_array($pcu_shift)){
											?>
												<option value="<?php echo $pcu_shift_row['id']; ?>"><?php echo $pcu_shift_row['shift_name']; ?></option>		
											<?php
												}
											?>
										</select>
										<input type="hidden" class="svshift" name="shiftid">
									</div>
									<div class="col-sm-2">
										<label style="visibility:hidden;">Shift:</label>&nbsp;&nbsp;
										<input type="submit" name="Display" value="Display Data" class="svdatainfo form-control" style=" background-color: #4CAF50; color:white;">	
									</div>
								</div>                        
							</div><!--   -->
						</div>
					</div>    
				</form>
		
		
		
				<div class="card">
					
				<!--body -->
				   <div id="myDiv" style="display:none;width:300px;height:240px;border:0px solid #5b826f;position:absolute;top:50%;left:40%;padding:2px;"><img src='../resources/assets/images/Spinner-1s-200px.gif' width="290px" height="220" /></div>
				<!--/body-->
					<div class="card-body" id="hide-div">						
						<div class="table-responsive">
							<table class="table">
								<thead class="bg-info text-white">
									<tr>
										<th>#</th>
										<th id ="date">DATE</th>
										<th>OPERATOR</th>
										<th>CASCADE</th>
										<th>MACHINE</th>
										<th>SKU</th>
										<th>SHIFT</th>
										<th>RPM</th>
										<th>CLD</th>
										<th>PRODUCTION</th>
										<th>DOWNTIME</th>
										<th>OEE </br>%</th>
										
									</tr>
								</thead>
									<form method="post" action="export">
										@csrf
										<input class="tdvalue" type="hidden" name="resulttt" value="">
										<!--a href="export">
											<button class="btn btn-primary float-right export" style="width:10%; margin-bottom:1%;">Export to Excel</button>
										</a-->
										<input type="submit" name="submit" class="btn btn-primary float-right export" style="width:10%; margin-bottom:1%;" value="Export to Excel">
									</form>
									<?php $i = 1; ?>
                                    <?php 
										$array = array();
										if(count($data) > 0) {  
                                            foreach($data as $record) {  
												$array[] = array($record);
									?>
                                        <tbody class="datashowhere">
										<tr>
											<td><?php echo $i ?></td>
											<td>{{ $record['logintime']}} </td>
											<td>{{ ucfirst($record['first_name'])}} {{ $record['last_name']}}</td>
											<td>{{ $record['cascade_code']}} </td>
											<td>{{ $record['machine_regno']}} </td>
											<td><?php if(array_key_exists('sku_code', $record)){
                                                echo $record['sku_code'];
                                            } else{
                                                echo '-';
                                            } ?> </td>
											<td>{{ ucfirst($record['shift_code'])}}</td>
											<td><?php if(array_key_exists('rpm_default', $record)){
													echo $record['rpm_default'];
												} else{
													echo '-';
												}  ?> 
											</td>
											<td>{{ $record['cld']}} </td>
											<td><?php  echo !empty($record['total_production']) ? $record['total_production'] : '-' ?> </td>
											<td>{{ $record['breakdown_part']}} </td>
											<td>{{ $record['oee']}} </td>
											<td> </td>
										<?php $i++ ; }  } else { //echo "No Record Found";
											}											
											// Session::put('reportdata',$data);
										?>
								</tbody>
								<tbody class="reportval"></tbody>	
							</table>
						</div>
					</div>					
				</div>
			</div>		   
		</div>                
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous">
		</script>
		
	<script>	
		
		$('.export').on('click', function(){
			var customerId = 0;
			$('.reportval tr').each(function() {
				customerId = $(this).find("td").html();   
				// alert(customerId);
			});
			// alert(customerId);
			if(customerId == 0){
				// alert('000');				
				// return false;				
				<?php Session::put('reportdata',$data); ?>
			}else{
				// alert('111');				
				// alert($_SESSION['loginData']);
				// return false;
			}
		});
		
		$('.svdatainfo').on('click', function(){				
			var check_u = <?php echo $send_u;  ?>;
			var check_p = <?php echo $send_p;  ?>;
			var check_m = <?php echo $send_m;  ?>;
			var check_c = <?php echo $send_c;  ?>;			

			var subunit = $('.svunit').val();
			var subplant = $('.svplant').val();
			var subcas = $('.svcascade').val();
			var submachine = $('.svmachine').val();
			var subshift = $('.svshift').val();	
			var subfrom = $('.txtStartDate').val();
			var subto = $('.txtEndDate').val();

			// alert(subcas);

			if(subunit == ''){
				subunit = check_u;
			}

			if(subplant == ''){
				subplant = check_p;
			}

			if(subcas == ''){
				subcas = check_c;
			}

			if(submachine == ''){
				submachine = check_m;
			}

			if(subshift == ''){
				subshift = 0;
			}

			if(subfrom == ''){
				subshift = 0;
			}

			if(subto == ''){
				subshift = 0;
			}

			//alert(subfrom);
			//alert(subto);
			// alert(submachine);
			// alert('111');
			$.ajax({
				type: 'get',
				// url: 'svdata',
				url: 'detail',
				dataType: 'json',
				data:{U: subunit, P:subplant, C:subcas, M:submachine, S:subshift ,FD:subfrom ,TD:subto ,T: 'test'},
				beforeSend: function() {
					$("#myDiv").show();
					$("#hide-div").hide();
				},

				success:function(data){
					$("#myDiv").hide();
					$("#hide-div").show();

					// console.log(data[0].first_name, data[0].cascade_name, data[0].machine_name, data[0].sku_code, data[0].shift_name, data[0].total_production);

					// console.log(data[1][0].data[1][1].data[1][2].data[1][3]);
					// console.log(data[2][0].data[2][1].data[2][2].data[2][3]);
					// console.log(data[3][0].data[3][1].data[3][2].data[3][3]);
					// console.log(data[4][0].data[4][1].data[4][2].data[4][3]);
					// alert('success');

					$('.datashowhere').css("display", "none");
					var spin='';
					var count = 1;
					// console.log('--------------',data);
					$.each(data, function (i, spinner) {
						var spin_val = data[i][i];
						var date = data[i].logintime;							
						//chgdate = date.split(' ')[0];
						// var newDate = date.toString('dd-MM-yy');

						//console.log('+++++++++++++++++',typeof(data[i].sku_name));	
						if(data[i].sku_name == undefined) {
							var sku_name = '-';	
						} else {
							var sku_name = data[i].sku_name;	
						}

						if(data[i].sku_code == undefined) {
							var sku_code = '';
						} else {
							var sku_code = data[i].sku_code;
						}

						if(data[i].rpm_default == undefined) {
							var rpm_default = '-';
						} else {
							var rpm_default = data[i].rpm_default;
						}

						if(data[i].total_production == undefined) {
							var total_production = '-';
						} else {
							var total_production = data[i].total_production;
						}

						// var newDate22 = moment(chgdate).format('dd-MM-yyyy');
						// console.log(newDate22);	
						// alert(date);
						// alert(spin_val);
						// count = count;
						spin += '<tr><td>'+count+'</td><td>'+date+'</td><td>'+data[i].first_name+'</td><td>'+data[i].cascade_code+'</td><td>'+data[i].machine_regno+'</td><td>'+sku_code+'</td><td>'+data[i].shift_code+'</td><td>'+rpm_default+'</td><td>'+data[i].cld+'</td><td>'+total_production+'</td><td>'+data[i].breakdown_part+'</td><td>'+data[i].oee+'</td></tr>';	
						count++;
					});
					// alert(spin);
					$('.reportval').empty().append(spin);
					// alert(spin);					
					sessionStorage.setItem("loginData",spin);
					$('.tdvalue').val(spin)
					// $.session.set("compareLeftContent", spin);
					// alert($.session.get("compareLeftContent"));
					

					//if(JSON.parse(data).length == 0) {
						//console.log(data);
						// console.log('Error----------------',JSON.parse(data).length);
					//} else {
						// console.log('Successsss----------------');
					//}
				}
			});
		});					
		
			
	</script>
	
@extends('footer')

