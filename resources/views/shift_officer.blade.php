@extends('header')
<style>
    .vtabs {
    width: 100% !important;
}
.vtabs .tabs-vertical {
    width: 204px !important;
}
.plant_detail_1 > a {
    color: #272c33 !important;
}.table td, .table th {
    border-color: #009efb !important;
}
.dev_style {
    color: #272c33 !important;
    font-weight: normal !important;
}
.card-body.heading-block {
    padding: 10px;
    background: #999595;
}
.heading-text span {
    color: white;
}
.admin_display_bar{
	background: #3c6382;
    color: white;
}
.vtabs {
width: 100% !important;
}
.vtabs .tabs-vertical {
width: 204px !important;
}
.plant_detail_1 > a {
color: #272c33 !important;
}.table td, .table th {
border-color: #5b826f !important;
}
.dev_style {
color: #272c33 !important;
font-weight: normal !important;
}
/* .card-body.heading-block {
padding: 10px;
background: #999595;
} */
.heading-text span {
color: #174d33;
font-style: italic;
}
.heading-text {
height : 60px;
}


</style>
	<?php
		//$find_user = Session::get('user_id');
		//die($find_user."hereeee");
		
		if(empty(Session::get('email')))
		{
	?>
			<script>window.location.href = "{{ url('/login') }}";</script>
	<?php
		  //return redirect('/login');
		}
		if( Session::get('designation_id') != 4)
		{
	?>
			<script>window.location.href = "{{ url('/message') }}";</script>
	<?php
		}
	?>
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/shift_officer') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!--li class="nav-item hidden-sm-down">
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                        </li-->Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><!--img src="{{ URL::asset($logo) }}" alt="user"--><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
       
        @extends('sidebar')       
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Dashboard</h3>                       
                    </div>					
                </div>               
                <?php
					$host = Session::get('host');
					$dbname = Session::get('dbname');
					$user = Session::get('user');
					$pass = Session::get('pass');
					// echo $host;
					// echo $dbname;
					// echo $user;
					$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
				?>
		<h1>Supervisor</h1>
		@if ($message = Session::get('message'))
			<div class="alert alert-info alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>	
				<strong>{{ $message }}</strong>
			</div>
		@endif
				
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="card">
					<div class="card-body">
						<h3 class="heading-text" style="font-family:'Rubik', sans-serif"><span>Unit & Plant Information </span></h3>
						<!-- //#################### Plant Section ############################# -->
						<div class="table-responsive plant_info" id="plantsection">							
							<form method="post" action="javascript:void(0)">
							<!--form method="post" action="pcudatainfo"-->
								<div class="row">							
									@csrf
									<div class="col-sm-2">
										<label>Unit:</label>&nbsp;&nbsp;
										
										<select class="form-control sv_pcu_unit" name="unit">										
											<?php
												$pcu_unit = pg_query($db, "SELECT * FROM rm_units ORDER BY id ASC");
											?>
												<option value="0" selected="selected">All</option>
											<?php
												while($pcu_unit_row = pg_fetch_array($pcu_unit)){
											?>
												<option value="<?php echo $pcu_unit_row['id']; ?>"><?php echo $pcu_unit_row['unit_name']; ?></option>		
											<?php
												}
											?>
										</select>
										<input type="hidden" class="svunit" name="unit">
									</div>
									<div class="col-sm-2">		
										<label>Plant:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_plant" > 
											<option value="0" selected="selected">All</option>												
										 </select>
										 <input type="hidden" class="svplant" name="plantid">
									</div>							
									<div class="col-sm-2">
										<label>Cascade:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_cascade" > 
											<option value="0" selected="selected">All</option>	
										 </select>
										 <input type="hidden" class="svcascade" name="cascadeid">
									</div>
									<div class="col-sm-2">
										<label>Machine:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_machine"> 
											<option value="0" selected="selected">All</option>
										</select>
										<input type="hidden" class="svmachine" name="machineid">
									</div>
									<div class="col-sm-2">
										<label>Shift:</label>&nbsp;&nbsp;
										<select class="form-control sv_pcu_shift" > 
											<option value="0" selected="selected">All</option>	
											<?php
												$pcu_shift = pg_query($db, "SELECT * FROM rm_shifts");
												while($pcu_shift_row = pg_fetch_array($pcu_shift)){
											?>
												<option value="<?php echo $pcu_shift_row['id']; ?>"><?php echo $pcu_shift_row['shift_name']; ?></option>		
											<?php
												}
											?>
										</select>
										<input type="hidden" class="svshift" name="shiftid">
									</div>
									<div class="col-sm-2">
										<label style="visibility:hidden;">Shift:</label>&nbsp;&nbsp;
										<input type="submit" name="Display" value="Display Data" class="svdatainfo form-control" style=" background-color: #4CAF50; color:white;">	
									</div>
								</div>
							</form>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	
	
            
@extends('footer')