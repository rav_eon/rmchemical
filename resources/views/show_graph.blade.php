@extends('header')
<!-- Filter -->
<style>
    .span-class {
        float:right;
    }
    #date {
        width: 10%;
    }
	
.vtabs {
    width: 100% !important;
}
.vtabs .tabs-vertical {
    width: 204px !important;
}
.plant_detail_1 > a {
    color: #272c33 !important;
}.table td, .table th {
    border-color: #009efb !important;
}
.dev_style {
    color: #272c33 !important;
    font-weight: normal !important;
}
.card-body.heading-block {
    padding: 10px;
    background: #999595;
}
.heading-text span {
    color: white;
}
.admin_display_bar{
	background: #3c6382;
    color: white;
}
.vtabs {
	width: 100% !important;
}
.vtabs .tabs-vertical {
	width: 204px !important;
}
.plant_detail_1 > a {
	color: #272c33 !important;
}.table td, .table th {
	border-color: #5b826f !important;
}
.dev_style {
	color: #272c33 !important;
	font-weight: normal !important;
}
/* .card-body.heading-block {
	padding: 10px;
	background: #999595;
} */
.heading-text span {
	color: #174d33;
	font-style: italic;
}
.heading-text {
	height : 60px;
}
.left-sidebar {
    background: #272c33 !important;
}
.select-date {
    width: auto;
    height: 100%;
}
#span-text {
    font-size: 20px;
}
p {
    margin-bottom: 0rem !important;
}
.text-white {
    font-size: 23px !important;
}
#full-graph {
    padding-bottom: 0%;
    padding-top: 0%;
    padding-left: 0%;	
}
.card{
	margin-bottom:0px !important;
}
.real_time{
	font-size: 25px !important;
    padding-left: 0%;
    margin-right: 291px;
}
@media (max-width: 768px){	
	.real_time{
		font-size: 16px !important;
		padding-left: 0%;
		margin-right: 60px;
	}
	.datebox{
		margin-right:0% !important;
	}
	.dashboard2{
		display:block !important;
	}
	.dashboard2>a.btn.btn-primary {
		font-size: 13px;
	}
	.productionreport2{
		display:block !important;
	}
	.col-sm-6.prodreport{
		max-width: 34%;
	}	
	.productionreport{
		display:none;
	}
	.dashboard{
		display:none;
	}
}
.country-state li {
    margin-top: 0px !important;
    margin-bottom: 10px;
}

</style>
<style>
.carousel-control-prev,
    .carousel-control-next{
        height: 50px;
        width: 50px;
        outline: $color-white;
        background-size: 100%, 100%;
        border-radius: 50%;
        border: 1px solid $color-white;
        background-color: $color-white;
    }

    .carousel-control-prev-icon { 
        background-image:url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E"); 
        width: 30px;
        height: 48px;		
    }
    .carousel-control-next-icon { 
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
        width: 30px;
        height: 48px;
		
    }	
</style>
 
<head>

 <?php

//  echo "<pre>";
//  print_r($again_array); 

// $check_new_array = array(
// 	array ('Y','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X'),
// 	array ('28/7-8',10,0,0,0,0,0,0,10,0,10,0,0,10,0,0,0,0,0,0,10,0,0,10,0),
// 	array ('28/8-9',0,0,10,0,0,0,0,10,10,0,0,0,0,10,0,0,0,0,0,10,0,10,0,0)
// );
// echo "<pre>";
// print_r($check_new_array);

//  die;
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');	
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
    
	
?>
	<span><?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?></span>
	<div id="main-wrapper" >
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/shift_officer') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">
						</b>                        
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>                       
                    </ul>
                    <div class="" style="text-align:center;">
						<h2 style="color:#194F35 !important;" class="real_time"><strong>Real Time Production Monitoring System</strong></h2>
					</div>
                    <ul class="navbar-nav my-lg-0">
						Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><!--img src="{{ URL::asset($logo) }}" alt="user"--><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p><a href="javascript:void(0)" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li-->
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>                        
                    </ul>
                </div>
            </nav>
        </header>

<?php
// echo $check."------------------------";
// echo "<pre>";
// print_r($current_array);

// echo "<pre>";
// print_r($prev_array);

// die;

	if($flag == 'string') { ?>
  
        <style>
            body.overlay {
                position: absolute;
                width: 100%;
                top: 0;
                left: 0;
                background: #000;
                opacity: 0.5;
                filter: alpha(opacity = 50); /* required for opacity to work in IE */
            }
        </style>

	<?php }

		if($flag == 'array') {
			// if($check == 'curr' && $current_array['output_counter_type'] == 'CLD' ) { 
			// 	$OEE = $check == 'curr' && count($current_array) > 0 ? $current_array['oee'].'%;' : $prev_array[0]['oee'].'%;'; 
			// } else if( $check != 'curr') { 
			// 	$OEE = $prev_array[0]['oee'].'%;'; 
			// } else {
				$OEE = $f_oee.'%;'; 
			// }	
	?>
	<html>
	<head>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
			var arrayFromPHP = <?php echo json_encode($data); ?>;
			// console.log('arrayFromPHP-------------',arrayFromPHP);
			var mainArray = [["TIME INTERVALS","",{ role: 'style' }]];
			var mReg = '<?php echo $m_reg; ?>';        
			
			$.each( arrayFromPHP, function( key, value ) {
				mainArray.push([value.interval,value.oee,value.color]);
			});
			google.charts.load('current', {'packages':['bar']});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {
				var data = google.visualization.arrayToDataTable(mainArray);

				var options = {
					axisFontSize: 14,
					title: 'OEE% of Machine '+mReg,
					legend:{position:'none'},
					// colors: ['green'],
					vAxis: {
						title: 'OEE%',
						textStyle: {
							fontName: 'Arial',
							fontSize: 10
						},
						titleTextStyle: {
							fontSize: 12,
							italic: false,
							bold:true
						}
						},
					hAxis: {
						title: 'TIME INTERVALS',
						textStyle: {
							fontName: 'Arial',
							fontSize: 10
						},
						titleTextStyle: {
							fontSize: 12,
							italic: false,
							bold:true
						}
					},
					// tooltip: {isHtml: true},
					bars: 'vertical' // Required for Material Bar Charts.
				};
				var chart = new google.visualization.ColumnChart(document.getElementById('barchart_material'));
				chart.draw(data, google.charts.Bar.convertOptions(options));
			}
			 //$('.g').not(':hidden').last().addClass("red");
		</script>
	</head>
    <?php  } ?>	
    <!--<div class="row"-->
	<h1 class="supreport" >Supervisor Report</h1>
	<div class="col-lg-12">
		 <div class="card-body">
            <div class="row suprow">
			<?php if($flag == 'array') { ?>       
				<div class="col-sm-4">
					<div class="card" style="padding-bottom:4%;">
						<div class="card-body" id='choose' style="padding-bottom:0%">							
							<div id='report'>
								<form method='POST' action='current_shift?u=<?php echo $unit_id; ?>&p=<?php echo $plant_id; ?>&m=<?php echo $machine_id; ?>&c=<?php echo $cascade_id; ?>'>	
									{{ csrf_field() }}
									<div class="row" >
										<div class="col-sm-6 prodreport">
											<h3 class="float:left">Production Report</h3>
										</div>
										<div class="col-sm-6 productionreport">
											<input class="btn btn-success float:left" type="submit" name="submit" value="VIEW REPORT" style="background-color:green !important;border:red !important">
										</div>
										<div class="col-sm-3 productionreport2" style="display:none;">
											<input class="btn btn-success float:left" type="submit" name="submit" value="VIEW REPORT" style="background-color:green !important;border:red !important">
										</div>
										<div class="col-sm-3 dashboard2" style="display:none;">
											 <a href="{{ url('/shift_officer') }}" class="btn btn-primary" style="float:left;">
												Back to Dashboard
											</a>			
										</div>	
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>	
				<?php  $date = date('d/m/Y',strtotime($working_time)); ?>
				<?php }else{ ?>
				<div class="col-sm-4" >
					<div class="card" style="padding-bottom:4%;">
						<div class="card-body" id='choose' style="padding-bottom:0%">							
							<div id='report'>
								<div class="col-sm-12">	
									<form method='POST'>	
										{{ csrf_field() }}
										<div class="row" >	
											<div class="col-sm-6">		
												<h3 class="float:left">Production Report</h3>
											</div>
											<div class="col-sm-6">
												<input class="btn btn-success float:left" type="submit" name="submit" value="NO REPORT AVAILABLE" style="background-color:green !important;border:red !important" disabled>
											</div>											
											
											<div class="col-sm-3 dashboard2" style="display:none;">
												 <a href="{{ url('/shift_officer') }}" class="btn btn-primary">
													Back to Dashboard
												</a>			
											</div>	
										</div>										
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="col-lg-8">
					<div class="card">
						<form method="post" action="show_graph?p=<?php echo $_GET['p']; ?>&m=<?php  echo $_GET['m']; ?>&c=<?php echo $_GET['c']; ?>&u=<?php echo $_GET['u']; ?>&d=<?php echo isset($_POST['txtStartDate']) && !empty($_POST['txtStartDate']) ? $_POST['txtStartDate'] : '2020-10-28' ?>">
							{{ csrf_field() }}
							<div class="card-body" >
								<div class="row" >
									<div class="col-sm-6 datebox" style="margin-right:-15%;">
										<label>Choose Date:</label>&nbsp;&nbsp;
										<input type="date" class="select-date txtStartDate"  placeholder="" onfocus="(this.type='date')" value="" name="txtStartDate" >		
									</div>		
									<div class="col-sm-3">
										<input type="submit" name="Display" value="Display Graph" class="svdatainfo form-control" style=" background-color: #4CAF50; color:white;">	
									</div>
									<div class="col-sm-3 dashboard">
										 <a href="{{ url('/shift_officer') }}" class="btn btn-primary">
											Back to Dashboard
										</a>			
									</div>	
								</div>
							</div>
						</form>            
					</div>
				</div>
			</div> 
        </div> 
    </div>
    <!--/div-->    

    	<?php if($flag == 'array') {  ?>
		<!--div class="row"-->
		<!--   =============== Carousel Start =================== -->
		<div class="col-lg-12">
			<div class="card-body" >
				<div class="row" >
					<div class="col-lg-4">
						<div class="card">
							<div class="card-body" style="height: 388px !important;">
								<h4 class="card-title" style="color: #6f6f8c !important;font-size: 16px !important;float: right !important;"><?php echo $working_time; ?></h4>
								<p style="margin-left: 11px;"><strong>Machine: <?php echo $m_reg; ?></strong></p>
								
								<?php
									$output = ($check != 'curr') ?  $prev_array[0]['output_counter_type'] : (count($current_array) > 0 ? $current_array['output_counter_type'] : $prev_array[0]['output_counter_type']);
								if($output == 'CLD') { ?>
								
								<div id="carouselExampleControls" class="carousel" data-ride="">
									<div class="carousel-inner">
										<div class="carousel-item active">
											<ul class="country-state">
												<li>
												<?php 
												// if($check != 'curr') { 
													if($f_oee >= 85) {
														$get_style = "style='color:green;font-size:38px;'";
														$get_again = "background-color:green !important;";
													}else if($f_oee < 85 && $f_oee >= 75) {
														$get_style = "style='color:#b5b503;font-size:38px;'";
														$get_again = "background-color:#b5b503 !important;";
													}else {
														$get_style = "style='color:red;font-size:38px;'";
														$get_again = "background-color:red !important;";
													} 
												// } else {
												// 	if($current_array['oee'] >= 85) {
												// 		$get_style = "style='color:green;font-size:38px;'";
												// 		$get_again = "background-color:green !important;";
												// 	}else if($current_array['oee'] < 85 && $current_array['oee'] >= 75) {
												// 		$get_style = "style='color:#b5b503;font-size:38px;'";
												// 		$get_again = "background-color:#b5b503 !important;";
												// 	}else {
												// 		$get_style = "style='color:red;font-size:38px;'";
												// 		$get_again = "background-color:red !important;";
												// 	} 
												// }
												?>
													<p>Production: <?php echo $check == 'curr' && count($current_array) > 0 ? $current_array['total_prod'].' Tons' : $prev_array[0]['total_prod'].' Tons' ; ?></p>
													<small>OEE%</small>
													<div class="float-right" <?php echo $get_style ?> ><?php echo $f_oee.'%'; ?> <i class="fa fa-level-up text-success"></i></div>
													<div class="progress">
														<div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $OEE; ?> height: 15px;<?php echo $get_again; ?>" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</li>
											</ul>
											&nbsp;&nbsp;&nbsp;
											<?php if($check == 'curr' && count($current_array) > 0) {  ?>
											<p style="margin-left: 11px;color: blue;"><b>Current SKU Details :</b></p>
											<table class="table no-border">
											
												<tbody>
													<tr>
														<td>CLD</td>
														<td class="font-medium"><?php echo $current_array['cld']; ?></td>
													</tr>
													<tr>
														<td>SKU</td>
														<td class="font-medium"><?php echo $current_array['sku_code']; ?></td>
													</tr>
													<tr>
														<td>Pack Size</td>
														<td class="font-medium"><?php echo $current_array['pack_size']; ?></td>
													</tr>
													<tr>
														<td>Per Unit CLD</td>
														<td class="font-medium"><?php echo $current_array['unit_per_cld']; ?></td>
													</tr>
												</tbody>
											</table>
											<?php } else  { ?>	
												<p style="margin-left: 11px;color: blue;"><b></b></p>
											<table class="table no-border">
												<p style="margin-left: 11px;color: blue;"><b>Last running SKU Detail :</b></p>
												<tbody>
													
													<tr>
														<td>SKU</td>
														<td class="font-medium"><?php echo $prev_array[0]['sku_code']; ?></td>
													</tr>
													<tr>
														<td>Pack Size</td>
														<td class="font-medium"><?php echo $prev_array[0]['pack_size']; ?></td>
													</tr>
													<tr>
														<td>Per Unit CLD</td>
														<td class="font-medium"><?php echo $prev_array[0]['unit_per_cld']; ?></td>
													</tr>
												</tbody>
											</table>
											<?php } ?>										
										</div>
										<?php if($check != 'curr' && count($prev_array) > 1) { foreach($prev_array as $pre_val) { ?>
										<div class="carousel-item">
											<ul class="country-state">
												<li>
													<h5>Time Interval : &nbsp;<?php echo date('H:i:s',strtotime($pre_val['from_date'])).' - '.date('H:i:s',strtotime($pre_val['to_date'])); ?></h5>
												</li>
											</ul>
											&nbsp;&nbsp;
											<table class="table no-border">
											<p style="margin-left: 11px;color: blue;"><b>Previous SKU Details :</b></p> &nbsp;&nbsp;
												<tbody>
													<tr>
														<td>SKU</td>
														<td class="font-medium"><?php echo $pre_val['sku_code']; ?></td>
													</tr>
													<tr>
														<td>Pack Size</td>
														<td class="font-medium"><?php echo $pre_val['pack_size']; ?></td>
													</tr>
													<tr>
														<td>Per Unit CLD</td>
														<td class="font-medium"><?php echo $pre_val['unit_per_cld']; ?></td>
													</tr>
												</tbody>
											</table>											
										</div>
										<?php } } else if ($check == 'curr') { foreach($prev_array as $pre_val) { ?> 
											<div class="carousel-item">
											<ul class="country-state">
												<li>
													<h5>Time Interval : &nbsp;<?php echo date('H:i:s',strtotime($pre_val['from_date'])).' - '.date('H:i:s',strtotime($pre_val['to_date'])); ?></h5>
												</li>
											</ul>
											&nbsp;&nbsp;
											<table class="table no-border">
											<p style="margin-left: 11px;color: blue;"><b>Previous SKU Details :</b></p> &nbsp;&nbsp;
												<tbody>
													<tr>
														<td>SKU</td>
														<td class="font-medium"><?php echo $pre_val['sku_code']; ?></td>
													</tr>
													<tr>
														<td>Pack Size</td>
														<td class="font-medium"><?php echo $pre_val['pack_size']; ?></td>
													</tr>
													<tr>
														<td>Per Unit CLD</td>
														<td class="font-medium"><?php echo $pre_val['unit_per_cld']; ?></td>
													</tr>
												</tbody>
											</table>											
										</div>
										<?php } } ?>
									</div>
									<?php if( (count($prev_array) > 0 && $check == 'curr') || (count($prev_array) > 1 && $check != 'curr') ) {  ?>
									
									<a class="carousel-control-prev" href="#carouselExampleControls" style="background-color:#55ce63; margin-left: -4%; top:40%; width:6%;" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselExampleControls" style="background-color:#55ce63; margin-right: -4%; top:40%; width:6%;" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
									<?php } ?>
								</div>

								<?php } else {  ?>
								<div id="carouselExampleControls" class="carousel" data-ride="">
									<div class="carousel-inner">
										<div class="carousel-item active">
											<ul class="country-state">
												<li>
												<?php
													if($check == 'curr' ) { 
													
														if(count($current_array) > 0) {
															if($current_array['oee'] >= 85) {
																$get_style = "style='color:green;font-size:38px;'";
																$get_again = "background-color:green !important;";
															}else if($current_array['oee'] < 85 && $current_array['oee'] >= 75) {
																$get_style = "style='color:#b5b503;font-size:38px;'";
																$get_again = "background-color:#b5b503 !important;";
															}else {
																$get_style = "style='color:red;font-size:38px;'";
																$get_again = "background-color:red !important;";
															} 
														} else {
															if($prev_array[0]['oee'] >= 85) {
																$get_style = "style='color:green;font-size:38px;'";
																$get_again = "background-color:green !important;";
															}else if($prev_array[0]['oee'] < 85 && $prev_array[0]['oee'] >= 75) {
																$get_style = "style='color:#b5b503;font-size:38px;'";
																$get_again = "background-color:#b5b503 !important;";
															}else {
																$get_style = "style='color:red;font-size:38px;'";
																$get_again = "background-color:red !important;";
															} 
														}
													} else {
														if($prev_array[0]['oee'] >= 85) {
															$get_style = "style='color:green;font-size:38px;'";
															$get_again = "background-color:green !important;";
														}else if($prev_array[0]['oee'] < 85 && $prev_array[0]['oee'] >= 75) {
															$get_style = "style='color:#b5b503;font-size:38px;'";
															$get_again = "background-color:#b5b503 !important;";
														}else {
															$get_style = "style='color:red;font-size:38px;'";
															$get_again = "background-color:red !important;";
														} 
													}
												?>
													<h3>Batches: <?php echo $check != 'curr' ? $prev_array[0]['total_prod'].' Tons' : (count($current_array) > 0 ? $current_array['total_prod'].' Tons' : $prev_array[0]['total_prod']) ; ?></h3>
													<small>OEE%</small>
													<div class="float-right" <?php echo $get_style ?> ><?php echo $check != 'curr' ?  $prev_array[0]['oee'].'%' : (count($current_array) > 0 ? $current_array['oee'].'%' : $prev_array[0]['oee'].'%' ); ?> <i class="fa fa-level-up text-success"></i></div>
													<div class="progress">
														<div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $OEE; ?> height: 15px;<?php echo $get_again ?>" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</li>
											</ul>
											&nbsp;&nbsp;&nbsp;
											<?php if($check == 'curr' && count($current_array) > 0){  ?>
												<table class="table no-border">
													<p style="margin-left: 11px;color: blue;"><b>Current SKU Details :</b></p>
													<tbody>
														<tr>
															<td>No. of batches</td>
															<td class="font-medium"><?php echo $current_array['cld']; ?></td>
														</tr>
														<tr>
															<td>Product Type</td>
															<td class="font-medium"><?php echo $current_array['sku_code']; ?></td>
														</tr>
														<tr>
															<td>BCT</td>
															<td class="font-medium"><?php echo $current_array['bct']; ?></td>
														</tr>
														
													</tbody>
												</table>
											<?php } else { ?>		
												<table class="table no-border">
												<p style="margin-left: 11px;color: blue;"><b>Last running SKU Details :</b></p>
													<tbody>
														
														<tr>
															<td>Product Type</td>
															<td class="font-medium"><?php echo $prev_array[0]['sku_code']; ?></td>
														</tr>
														<tr>
															<td>BCT</td>
															<td class="font-medium"><?php echo $prev_array[0]['bct']; ?></td>
														</tr>
														
													</tbody>
											</table>		
											<?php } ?>									
										</div>


									<?php if(count($prev_array) > 0) { foreach($prev_array as $pre_val) { ?> 
										<div class="carousel-item ">
											<ul class="country-state">
											<li>
												<h5>Time Interval : &nbsp;<?php echo date('H:i:s',strtotime($pre_val['from_date'])).' - '.date('H:i:s',strtotime($pre_val['to_date'])); ?></h5>
												</li>
											</ul>
											&nbsp;&nbsp;&nbsp;
											<table class="table no-border">
											<p style="margin-left: 11px;color: blue;"><b>Previous SKU Details :</b></p>
												<tbody>
													
													<tr>
														<td>Product Type</td>
														<td class="font-medium"><?php echo $pre_val['sku_code']; ?></td>
													</tr>
													<tr>
														<td>BCT</td>
														<td class="font-medium"><?php echo $pre_val['bct']; ?></td>
													</tr>
													
												</tbody>
											</table>											
										</div>


									<?php } } ?>


									</div>
									<?php if(count($prev_array) > 0) {  ?>
										<a class="carousel-control-prev" href="#carouselExampleControls" style="background-color:#55ce63; margin-left: -4%; top:40%; width:6%;" role="button" data-slide="prev">
											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="carousel-control-next" href="#carouselExampleControls" style="background-color:#55ce63; margin-right: -4%; top:40%; width:6%;" role="button" data-slide="next">
											<span class="carousel-control-next-icon" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									<?php } ?>
								</div>
								<?php } ?>	

							</div>
						</div>
					</div>
					<div class="col-lg-8"> 
						<div class="card">
							<div class="card-body">
								<div id="barchart_material" style="width:auto; height: 350px;"></div>
							</div>   
						</div>
					</div>	   
				</div>
			</div> 
        </div> 
		<!--   =============== Carousel End =================== -->
		
		

			<!-- NEW  -->

				<script type = "text/javascript">
						google.charts.load('current', {packages: ['corechart']});     
				</script>
				<?php if($graph_show == 'cld')  { ?>
				<script language = "JavaScript">
					var AgainArray = <?php echo json_encode($again_array); ?>;
					console.log(AgainArray);
					//var AgainArray = <?php //echo json_encode($check_new_array); ?>;
						function drawChart() {
							var data = google.visualization.arrayToDataTable(AgainArray);
							var options = {
								// title: 'Hourly Production & Breakdown Data',colors:['Green','Red','Green'],legend:{position:'none'}, isStacked:true,
								title: 'Hourly Production & Breakdown Data',colors:['Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey'],
								legend:{position:'none'}, 
								isStacked:true,
								width:'100%',
								height:500,
								tooltip: { trigger: 'both' },
								
								vAxis: {
									title: 'TIME INTERVALS',
									textStyle: {
										fontName: 'Arial',
										fontSize: 10
									},
									titleTextStyle: {
										fontSize: 12,
										italic: false,
										bold:true
									}
								},
								hAxis: {
									title: 'MINUTES',
									textStyle: {
										fontName: 'Arial',
										fontSize: 10
									},
									titleTextStyle: {
										fontSize: 12,
										italic: false,
										bold:true
									}
								}
								
							};  
							// Instantiate and draw the chart.
							var chart = new google.visualization.BarChart(document.getElementById('container222'));
							chart.draw(data, options);
						}
					google.charts.setOnLoadCallback(drawChart);
				</script>
			<?php  } else if($graph_show == 'vim') { ?>

				<script language = "JavaScript">
					var AgainArray = <?php echo json_encode($again_array); ?>;
					console.log(AgainArray);
					//var AgainArray = <?php //echo json_encode($check_new_array); ?>;
						function drawChart() {
							var data = google.visualization.arrayToDataTable(AgainArray);
							var options = {
								// title: 'Hourly Production & Breakdown Data',colors:['Green','Red','Green'],legend:{position:'none'}, isStacked:true,
								title: 'Hourly Production & Breakdown Data',colors:['Red','#b5b503','Green','Grey','Red','#b5b503','Green'],legend:{position:'none'}, isStacked:true,
								width:'100%',
								height:500,
								
								tooltip: {isHtml: true},

								vAxis: {
									title: 'TIME INTERVALS',
									textStyle: {
										fontName: 'Arial',
										fontSize: 10
									},
									titleTextStyle: {
										fontSize: 12,
										italic: false,
										bold:true
									}
								},
								hAxis: {
									title: 'MINUTES',
									textStyle: {
										fontName: 'Arial',
										fontSize: 10
									},
									titleTextStyle: {
										fontSize: 12,
										italic: false,
										bold:true
									}
								}
								
							};  
							// Instantiate and draw the chart.
							var chart = new google.visualization.BarChart(document.getElementById('container222'));
							chart.draw(data, options);
						}
					google.charts.setOnLoadCallback(drawChart);
				</script>
			<?php } else if($graph_show == 'batch') { ?>
				<script language = "JavaScript">	
					var AgainArray = <?php echo json_encode($again_array); ?>;
					console.log(AgainArray);
					//var AgainArray = <?php //echo json_encode($check_new_array); ?>;
						function drawChart() {
							var data = google.visualization.arrayToDataTable(AgainArray);
							var options = {
								// title: 'Hourly Production & Breakdown Data',colors:['Green','Red','Green'],legend:{position:'none'}, isStacked:true,
								title: 'Hourly Production & Breakdown Data',colors:['Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey'],legend:{position:'none'}, isStacked:true,
								width:'100%',
								height:500,
								tooltip: { trigger: 'selection' },
								vAxis: {
									title: 'TIME INTERVALS',
									textStyle: {
										fontName: 'Arial',
										fontSize: 10
									},
									titleTextStyle: {
										fontSize: 12,
										italic: false,
										bold:true
									}
								},
								hAxis: {
									title: 'MINUTES',
									textStyle: {
										fontName: 'Arial',
										fontSize: 10
									},
									titleTextStyle: {
										fontSize: 12,
										italic: false,
										bold:true
									}
								}
								
							};  
							// Instantiate and draw the chart.
							var chart = new google.visualization.BarChart(document.getElementById('container222'));
							chart.draw(data, options);
						}
					google.charts.setOnLoadCallback(drawChart);
				</script>

			<?php } ?>
			<!--NEW END   -->



			<!--div class="row"-->
			<?php if(count($again_array) > 1) {  ?>
				<div class="col-lg-12" style="margin-bottom:10px;">
					<div class="card-body" >
						<div class="row" >
							<div class="col-lg-12"> 
								<div class="card" id="full-graph">
									<div class="card-body">	
										<div style="margin-bottom:20px; width:100%;">
											Machine: <strong><?php echo $m_reg; ?></strong>
											<span style="float:right;"><strong>Date: <?php echo $working_time; ?></strong>
												<div>
													<button class="btn btn-success" style="background-color:rgb(0, 128, 0); width:5%"></button> Above 85% 
													<button class="btn btn-warning" style="width:5%;background-color:#b5b503 !important"></button> Between 75-85% 
													<button class="btn btn-danger" style="width:5%"></button> Below 75% 
													<button class="btn btn-secondary" style="width:5%"></button> BreakDown
												</div>									
											</span>
										</div>	<br/>				
										
										<!-- <div class="col-lg-8"> 
											<div class="card"> -->
												<!-- <div class="card-body"> -->
												<div id="container222"></div>
												<!-- </div>    -->
											<!-- </div> -->
										<!-- </div>	   -->

										<!-- <div class="col-lg-4">  -->
											<!-- <div class="card"> -->
												<!-- <div class="card-body"> -->
												<!-- <p>hello </p> -->
												<!-- </div>    -->
											<!-- </div> -->
										<!-- </div> -->																	
									</div>    
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php }  ?>
			<!--/div-->

    	<?php } else {  ?> 
 
       <!--div class="row"-->
		<div class="col-lg-12" style="margin-bottom:10px;">
			<div class="card-body" >
				<div class="row" >
					<div class="col-lg-12"> 
						<div class="card">
							<div class="card-body">
								<div class="alert alert-info">
									<h2 class="text-danger"><i class="fa fa-exclamation-circle"></i>  <?php echo $machine; ?></h2> 
									<h1><?php echo $message; ?></h1> &nbsp;&nbsp;
									<h3><code>Choose another date to get result!</code></h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
      <?php  }
	  
	  
	  
	  
	  
	  
	  /////// ============================== Start
	  
		if($flag == 'array') {
	  ?>	
		
		<script language = "JavaScript">
			var AgainArray = <?php echo json_encode($again_array); ?>;
			// console.log(AgainArray);
			//var AgainArray = <?php //echo json_encode($check_new_array); ?>;
				function drawChart() {
					var data = google.visualization.arrayToDataTable(AgainArray);
					var options = {
						// title: 'Hourly Production & Breakdown Data',colors:['Green','Red','Green'],legend:{position:'none'}, isStacked:true,
						title: 'Hourly Production & Breakdown Data',colors:['Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey','Red','#b5b503','Green','Grey'],legend:{position:'none'}, isStacked:true,
						width:'100%',
						height:500,
						tooltip: { trigger: 'both' },
						
						vAxis: {
							title: 'TIME INTERVALS',
							textStyle: {
								fontName: 'Arial',
								fontSize: 10
							},
							titleTextStyle: {
								fontSize: 12,
								italic: false,
								bold:true
							}
						},
						hAxis: {
							title: 'MINUTES',
							textStyle: {
								fontName: 'Arial',
								fontSize: 10
							},
							titleTextStyle: {
								fontSize: 12,
								italic: false,
								bold:true
							}
						}
						
					};  
					// Instantiate and draw the chart.
					var chart = new google.visualization.BarChart(document.getElementById('container222'));
					chart.draw(data, options);
				}
			google.charts.setOnLoadCallback(drawChart);
		</script>
	  
	  
		<div class="col-lg-12" style="margin-bottom:10px;">
			<div class="card-body" >
				<div class="row" >
					<div class="col-lg-12"> 
						<div class="card" id="full-graph">
							<div class="card-body">	
								<div style="margin-bottom:20px; width:100%;">
									Machine: <strong><?php echo $m_reg; ?></strong>
									<span style="float:right;"><strong>Date: <?php echo $working_time; ?></strong>
										<div>
											<button class="btn btn-success" style="background-color:rgb(0, 128, 0); width:5%"></button> Above 85% 
											<button class="btn btn-warning" style="width:5%;background-color:#b5b503 !important"></button> Between 75-85% 
											<button class="btn btn-danger" style="width:5%"></button> Below 75% 
											<button class="btn btn-secondary" style="width:5%"></button> BreakDown
										</div>									
									</span>
								</div><br/>				
								
								<!-- <div class="col-lg-8"> 
									<div class="card"> -->
										<!-- <div class="card-body"> -->
										<h3>Hourly Production & Breakdown Data</h3>
										<div>TIME INTERVALS</div>
										<div id="example" style=" top:0px; bottom:0px; width:100%; height:auto; position:relative;"></div>										
										<!-- </div>    -->
									<!-- </div> -->
								<!-- </div>	   -->

								<!-- <div class="col-lg-4">  -->
									<!-- <div class="card"> -->
										<!-- <div class="card-body"> -->
										<!-- <p>hello </p> -->
										<!-- </div>    -->
									<!-- </div> -->
								<!-- </div> -->																	
							</div>    
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
			google.charts.load("current", {packages:["timeline"]});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {
				var container = document.getElementById('example');
				var chart = new google.visualization.Timeline(container);
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn({ type: 'string', id: 'Position' });
				dataTable.addColumn({ type: 'string', id: 'Name' });
				dataTable.addColumn({ type: 'date', id: 'Start' });
				dataTable.addColumn({ type: 'date', id: 'End' });
				dataTable.addRows([				 
				  [ '15/09-10', 'Breakdown 01', new Date(2020,12,15,0,0,0),  new Date(2020,12,15,0,10,0) ],
				  [ '15/09-10', 'Breakdown 02', new Date(2020,12,15,0,10,0),  new Date(2020,12,15,0,20,0) ],
				  [ '15/09-10', 'Breakdown 03', new Date(2020,12,15,0,20,0),  new Date(2020,12,15,0,30,0) ],
				  [ '15/09-10', 'John Adams', new Date(2020,12,15,0,30,0),  new Date(2020,12,15,0,40,0) ],
				  [ '15/09-10', 'Thomas Jefferson', new Date(2020,12,15,0,40,0),  new Date(2020,12,15,0,60,0) ],
				  [ '15/10-11', 'John Adams', new Date(2020,12,15,0,0,0),  new Date(2020,12,15,0,10,0) ],
				  [ '15/10-11', 'Thomas Jefferson', new Date(2020,12,15,0,10,0),  new Date(2020,12,15,0,20,0) ],
				  [ '15/10-11', 'Aaron Burr', new Date(2020,12,15,0,20,0),  new Date(2020,12,15,0,30,0) ],
				  [ '15/11-12', 'John Marshall', new Date(2020,12,15,0,0,0),  new Date(2020,12,15,0,10,0) ],
				  [ '15/12-13', 'Breakdown reason01', new Date(2020,12,15,0,00,0),  new Date(2020,12,15,0,10,0) ],
				  [ '15/12-13', 'Breakdown reason02', new Date(2020,12,15,0,10,0),  new Date(2020,12,15,0,20,0) ],
				  [ '15/12-13', 'Breakdown reason03', new Date(2020,12,15,0,20,0),  new Date(2020,12,15,0,30,0) ],
				  [ '15/12-13', 'Breakdown reason04', new Date(2020,12,15,0,30,0),  new Date(2020,12,15,0,40,0) ],
				  [ '15/12-13', 'Breakdown reason05', new Date(2020,12,15,0,40,0),  new Date(2020,12,15,0,50,0) ]
				]);					
				var options = {
					colors: ['green', 'red', 'grey','blue','purple']					
				}
				chart.draw(dataTable, options);		 
			}
		</script>
		
		
		<div class="col-lg-12" style="margin-bottom:50px;">			
			<div class="card-body">
				<div class="row">
					<?php //echo $working_time2; ?>
					<button class="showinfo btn btn-success">
						Show Information
						<!--a href="onlybreakdown" style="text-decoration:none; color:white;"></a-->
					</button>
					<div class="table-responsive users_info infograph" id="usersection" style="display:none;">	  
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true" data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S.No.</th>
									<th>Duration</th>
									<th>Breakdown Reason</th>
								</tr>
							</thead>
							<tbody>
								<?php
									date_default_timezone_set('Asia/Kolkata');
									// echo date('Y-m-d H:i:s');
									$currentdate = date('Y-m-d');
									$sno = 1;	
									if($working_time2 < $currentdate){
										// echo "smaller";
										$finalstarttime = $working_time2." 00:00:00";
										$currenthour = date('23:59:59');
										$finalendtime = $working_time2." ".$currenthour;
									}else{
										// echo "equal";
										$finalstarttime = $working_time2." 00:00:00";
										$currenthour = date('H:00:00');
										$finalendtime = $working_time2." ".$currenthour;
									}
								
									$breakdownreasons = pg_query($db, "SELECT * FROM rm_opdownreasons WHERE machine_id = $machine_id AND (date_start_at >= '".$finalstarttime."' AND date_end_at <= '".$finalendtime."')");
									
									while($bdownrow = pg_fetch_array($breakdownreasons)){								
								?>
										<tr>		
											<td><?php echo $sno; $sno++; ?></td>										
											<td>
												<?php
													print_r("Start Time: <button class='btn btn-secondary'>".$bdownrow['date_start_at']."</button>"); 
													echo "&nbsp;&nbsp;";
													print_r("End Time: <button class='btn btn-secondary'>".$bdownrow['date_end_at']."</button>");												
												?>
											</td>
											<?php				
												$biddddd = $bdownrow['downreason_id'];
												$breakdownreasonsid = pg_query($db, "SELECT * FROM rm_breakdownreasons WHERE id=$biddddd");
												$breakdownrowww = pg_fetch_array($breakdownreasonsid);
											?>
											<td>
												<?php 
													print_r($breakdownrowww['breakdown_reason']);
												?>
											</td>										
											<?php								
											?>
										</tr>
								<?php
									}												
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	<?php  
		}
		/////// ============================== End
	?>		
    <!-- end  -->
	
	
	










			
	<!-- ############################### GOOGLE STACKED START ###############################  --> 
	
    <script type="text/javascript">
		
    </script>  
	<!-- ############################### GOOGLE STACKED END ###############################  --> 
				
    <!-- new code -->
    <script>
		$(document).ready(function () {
			// setTimeout(function () {
				// console.log('Reloading Page');
				// location.reload(true);
			// }, 200000);
			
			
			// alert($('.breakk').val());
			
			
		});
	</script>



	
@extends('footer')