		 <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
		<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="<?php echo $logo; ?>" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <!--a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"-->RM Chemicals <!--/a-->
                        
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>                            
                        </li>
                        
                        <li style="display:none;">
                            <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-nfc-tap"></i><span class="hide-menu">Select Unit</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li class="unit1_live_data"><a href="javaScript:void(0)">Unit 1</a></li>
                                <li class="unit2_live_data"><a href="javaScript:void(0)">Unit 2</a></li>
                            </ul>
                        </li> 
						
                        <li class="unit_plant_live_data" style="display:block;">
                            <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-pillar"></i><span class="hide-menu">Select Plant</span></a>							
                            <ul aria-expanded="false" class="collapse">								
								<?php
									// echo "<pre>".'-----------------------------------------------------';
									$pairs = $get_plant_list;
									// print_r(json_decode($pairs));
									// $pairs = explode(',', $get_plant_list);
									// $a = array();
									// print json_encode($pairs);
									$join = 0;
									foreach ($get_plant_list as $k=>$v) {
										$pair = explode(',', $v);
										foreach($pair as $p){
											// echo $pair[$join];
											// echo $pair[0];
											// echo $pair[1];
											// echo $pair[$join];
								?>									
									<li class="plant_data" value="<?php echo $pair[$join]; ?>"><a href="javascript:void(0)" class="currentshiftinfo"><?php echo $pair[$join]; ?></a></li>
								<?php	
											$join++;
										}
									}
								?>
							</ul>
                        </li> 
					
                        <!--li class="machines_live_data" >
                            <a class="has-arrow " href="javaScript:void(0)" aria-expanded="false"><i class="mdi mdi-snowflake"></i><span class="hide-menu">Live Data</span></a>
                            <ul aria-expanded="false" class="collapse">
								<?php 
									// echo "here";
									// $machine_info = pg_query($db, "SELECT * FROM rm_machines");
									// while($machine_row = pg_fetch_array($machine_info)){
										// echo "<pre>";
										// print_r($machine_row['machine_regno']);
										// echo "</pre>";
									
								?>
                                <li><a href="display_info?regno=<?php // echo $machine_row['machine_regno']; ?>"><?php // echo $machine_row['machine_name']; ?></a></li>
								<?php
									// }
								?>
                            </ul>
                        </li--> 
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
        </aside>