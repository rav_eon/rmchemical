@extends('header')
<style>
    .vtabs {
    width: 100% !important;
}
.vtabs .tabs-vertical {
    width: 204px !important;
}
.plant_detail_1 > a {
    color: #272c33 !important;
}.table td, .table th {
    border-color: #009efb !important;
}
.dev_style {
    color: #272c33 !important;
    font-weight: normal !important;
}
.card-body.heading-block {
    padding: 10px;
    background: #999595;
}
.heading-text span {
    color: white;
}

.vtabs {
width: 100% !important;
}
.vtabs .tabs-vertical {
width: 204px !important;
}
.plant_detail_1 > a {
color: #272c33 !important;
}.table td, .table th {
border-color: #5b826f !important;
}
.dev_style {
color: #272c33 !important;
font-weight: normal !important;
}
/* .card-body.heading-block {
padding: 10px;
background: #999595;
} */
.heading-text span {
color: #174d33;
font-style: italic;
}
.heading-text {
height : 60px;
}


</style>
	<?php
		//$find_user = Session::get('user_id');
		//die($find_user."hereeee");
		
		if(empty(Session::get('email')))
		{
	?>
			<script>window.location.href = "{{ url('/login') }}";</script>
	<?php
		  //return redirect('/login');
		}
		if( Session::get('designation_id') != 3)
		{
	?>
			<script>window.location.href = "{{ url('/message') }}";</script>
	<?php
		}
	?>
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <span>
    <?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/supervisor') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!--li class="nav-item hidden-sm-down">
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                        </li-->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ URL::asset($logo) }}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{ URL::asset($logo) }}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li>    
                                    <li role="separator" class="divider"></li-->    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
       
        @extends('sidebar')
		
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Dashboard</h3>
                        <!-- <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol> -->
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <!--button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm float-right ml-2"><i class="ti-settings text-white"></i></button-->
                        <div class="dropdown float-right mr-2 hidden-sm-down">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> View Production </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> 
                                <a class="dropdown-item" href="display_info?regno=SRF_BOON">Boon Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_PSM">PSM Machine</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L1">Packing Machine01</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L2">Packing Machine02</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_L3">Packing Machine03</a>
                                <a class="dropdown-item" href="display_info?regno=SRF_FINAL">Combiner</a>
                            </div>
                        </div>                        
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                              
                <!-- =============================================================== -->
                <?php
					$yesterday_date = date('Y-m-d', strtotime("-1 days"));
					//echo $yesterday_date;
					$yesterday_start_time = "00:00:00";
					$yesterday_end_time = "24:00:00";

					$start_date_time_mix = $yesterday_date . " " . $yesterday_start_time;
					$end_date_time_mix = $yesterday_date . " " . $yesterday_end_time;
					
					$host = Session::get('host');
					$dbname = Session::get('dbname');
					$user = Session::get('user');
					$pass = Session::get('pass');
					// echo $host;
					// echo $dbname;
					// echo $user;
					$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
					

					$query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_BOON' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
					$srf_boon = pg_result($query, 0);
					//echo $srf_boon;
					//echo "<br/>";
					$query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_PSM' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
					$srf_psm = pg_result($query, 0);
					//echo $srf_psm;
					//echo "<br/>";
					$query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_L1' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
					$srf_l1 = pg_result($query, 0);
					//echo $srf_l1;
					//echo "<br/>";
					$query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_L2' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
					$srf_l2 = pg_result($query, 0);
					//echo $srf_l2;
					//echo "<br/>";
					$query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_L3' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
					$srf_l3 = pg_result($query, 0);
					//echo $srf_l3;
					//echo "<br/>";
					$query = pg_query($db, "SELECT count(*) FROM alerts WHERE regno = 'SRF_FINAL' and devicetime BETWEEN '$start_date_time_mix' and '$end_date_time_mix'");
					$srf_final = pg_result($query, 0);
					//echo $srf_final;

					?>

		<!-- Row -->
		<!--div class="row">
			<div class="col-lg-12 col-md-6">
				<div class="card">
					<div class="card-body heading-block">
						<h3 class="heading-text" style="font-family:'Rubik', sans-serif"><span>Production History of Machine</span><span class="span-date">Date : <?php //echo $yesterday_date; ?></span></h3>
					</div>
				</div>
			</div>
		</div-->
		<h1>Supervisor</h1>
		@if ($message = Session::get('message'))
			<div class="alert alert-info alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>	
				<strong>{{ $message }}</strong>
			</div>
		@endif
		<!-- new code -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="card">
					<div class="card-body">
						<h3 class="heading-text" style="font-family:'Rubik', sans-serif"><span>Production History of Machine</span><span class="span-date">Date : <?php //echo $yesterday_date ?><?php echo $today = date("j-F-Y");   ?></span></h3>
						<div class="row">
							<!-- col -->
							<div class="col-lg-4 col-md-6">
								<div class="card bg-info">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<p class="text-white"><span class="font-bold"><?php echo $srf_boon; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF BOON PRODUCTION</span></h3>
												</div>
												<div class="carousel-item flex-column">
													<p class="text-white"><span class="font-bold"><?php echo $srf_boon; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF BOON PRODUCTION</span></h3>
												</div>
												<div class="carousel-item flex-column">
													<p class="text-white"><span class="font-bold"><?php echo $srf_boon; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF BOON PRODUCTION</span></h3>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- col -->
							<div class="col-lg-4 col-md-6">
								<div class="card bg-info-2">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_psm; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF PSM PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_psm; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF PSM PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_psm; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF PSM PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- col -->
							<div class="col-lg-4 col-md-6">
								<div class="card bg-info-3">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l1; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L1 PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l1; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L1 PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l1; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L1 PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<!-- col -->
							<div class="col-lg-4 col-md-6">
								<div class="card bg-info-4">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l2; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L2 Machine Production</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l2; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L2 Machine Production</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l2; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L2 Machine Production</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- col -->
							<div class="col-lg-4 col-md-6">
								<div class="card bg-info-5">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L3 PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L3 PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF L3 PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- col -->
							<div class="col-lg-4 col-md-6">
								<div class="card bg-info-6">
									<div class="card-body">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="carousel-item flex-column active">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_final; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF FINAL PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_final; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF FINAL PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
												<div class="carousel-item flex-column">
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php echo $srf_final; ?></span></p>
													<h3 class="text-white font-light">Total Production<br><span class="font-bold">SRF FINAL PRODUCTION</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
                
		<!-- ############################################# FORM STARTS ####################################################### -->
		<!-- Row -->
		
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="card">
					<div class="card-body">
						<div>						   
							<form method="post" action="fetch">
								@csrf
								<input type="date" name="txtStartDate" required>
								<!--input type="date" name="txtEndDate" required-->
								<!--input type="time" name="txtStartTime" required>
								<input type="time" name="txtEndTime" required-->
								<select name="shift_hours" id="shift_hours">
									<?php
									//while($shift_row = pg_fetch_array($shift_query)){
									// echo "<pre>";
									// print_r($shift_row['shift_name']);
									// echo "</pre>";

									?>
										<option value="morning_shift" name="morning_shift">Morning Shift</option>
										<option value="evening_shift" name="evening_shift">Evening Shift</option>
										<option value="night_shift" name="night_shift">Night Shift</option>
									<?php
										//}

									?>                            
								</select>
								<select name="machine_select" id="machine_select">
									<option name="all_machine" value="ALL">ALL Machine</option>
									<option name="boon_machine" value="SRF_BOON">BOON Machine</option>
									<option name="psm_machine" value="SRF_PSM">PSM Machine</option>
									<option name="packing_machine1" value="SRF_L1">Packing Machine 1</option>
									<option name="packing_machine2" value="SRF_L2">Packing Machine 2</option>
									<option name="packing_machine3" value="SRF_L3">Packing Machine 3</option>
									<option name="final_machine" value="SRF_FINAL">Final Machine</option>
								</select>
								<input type="submit" name="search" value="Search">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
                <!-- ############################################# FORM ENDS ####################################################### -->
                

            <!--  NEW CODE FOR UNIT Listing with plants  -->
              
				
		<div class="row">
		<div class="col-lg-12 col-md-12">
		<div class="card">
		<div class="card-body">
			<h3 class="heading-text" style="font-family:'Rubik', sans-serif"><span>Unit & Plant Information </span></h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Nav tabs -->
                                <div class="vtabs">
                                    <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home4" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down unit1">UNIT1 (U1)</span> </a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile4" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down unit2">UNIT2 (U2)</span></a> </li>
                                    </ul>
                                    <!-- Tab panes -->
									<div class="tab-content">
                                        <?php
											
											$plant = pg_query($db, "SELECT * FROM plants");
											if (!$plant)
											{
												echo "An error occurred.\n";
												exit;
											}
										?>
                                        <!-- Tab 1 -->
										<div class="table-responsive tab-pane active" id="home4" role="tabpanel">
											<h4 class="card-title">Plant Details</h4>
											<h6 class="card-subtitle"><code>UNIT1</code></h6>
											<table class="table table-hover no-wrap">
												<thead>
													<tr>
														<th>#</th>
														<th>PLANT NAME</th>
														<th>PLANT CODE</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$i = 1;
														while ($row3 = pg_fetch_array($plant))
														{
													?>    
														<tr>
															<td><a href="javascript:void(0);" class="<?php echo strtoupper($row3[0]).'_plant';?>"><?php echo $row3[0]; ?></a></td>
															<td class="plant_detail_1 <?php echo strtoupper($row3[0]).'_plant';?>"><a href="javascript:void(0);" id="<?php echo strtoupper($row3[0]); ?>_plant_link"><?php echo strtoupper($row3[1]); ?></a></td>
															<td class="plant_detail_1 <?php echo strtoupper($row3[0])."_plant"; ?>"><a href="javascript:void(0);" class="<?php echo strtoupper($row3[0])."_plant"; ?>"><?php echo ucwords($row3[2]); ?></a></td>   
														</tr>														
													<?php
															$i++;
														}
													?>
												</tbody>
											</table>
										</div>
										<!-- Tab 2 -->
										<div class="tab-pane p-3" id="profile4" role="tabpanel">
											<h4 class="card-title">Plant Details</h4>
											<h6 class="card-subtitle"><code>UNIT2</code></h6>
											<p>No Plant available on <code>UNIT2</code></p>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
		</div>
		</div>
		</div>
		</div>
			<!-- Surf Excel Packet counter -->
         
				
		<div class="row pcu_info" style="display:none;">
		<div class="col-lg-12 col-md-12">
		<div class="card">
			<div class="card-body">
				<h3 class="heading-text" style="font-family:'Rubik', sans-serif"><span>Plant & Packet Counter Information </span></h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                    <!-- Card Plant 1 -->
                        <div class="card">
                            <div class="card-body">
                                <!-- Nav tabs -->
                                <div class="vtabs">
                                    <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home4" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">SURF EXCEL</span> </a> </li>
                                        <!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile4" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">UNIT2 (U2)</span></a> </li> -->
                                    </ul>
                                    <!-- Tab panes -->
									<div class="tab-content">
										<?php
											
											$packet_counter_unit = pg_query($db, "SELECT * FROM alerts");
											if (!$packet_counter_unit)
											{
												echo "An error occurred.\n";
												exit;
											}
										?>
										<!-- Tab 1 -->
										<div class="table-responsive tab-pane active" id="home4" role="tabpanel">
											<h4 class="card-title">Packet Counter Unit</h4>
											<h6 class="card-subtitle"><code>SURF EXCEL</code></h6>
											<table class="table table-hover no-wrap">
												<thead>
													<tr>
														<th>#</th>
														<th>MACHINE NAME</th>
														<!-- <th>PLANT CODE</th> -->
													</tr>
												</thead>
												<tbody>
													<tr> 
														<td>1</td>
														<td><a href="display_info?regno=SRF_BOON" class="dev_style">SRF BOON</a></td>
													</tr>  
													<tr>
														<td>2</td>
														<td><a href="display_info?regno=SRF_PSM" class="dev_style">SRF PSM</a></td>  
													</tr>  
													<tr>
														<td>3</td>
														<td><a href="display_info?regno=SRF_L1" class="dev_style">SRF L1</a></td>  
													</tr>  
													<tr>
														<td>4</td>
														<td><a href="display_info?regno=SRF_L2" class="dev_style">SRF L2</a></td>
													</tr>  
													<tr>
														<td>5</td>
														<td><a href="display_info?regno=SRF_L3" class="dev_style">SRF L3</a></td>  
													</tr>  
													<tr>
														<td>6</td>
														<td><a href="display_info?regno=SRF_FINAL" class="dev_style">SRF FINAL</a></td>  
													</tr>      

												</tbody>
											</table>
										</div>
									</div>
                                </div>
                            </div>
                        </div> <!-- Card end -->                        
                    </div>
                </div>
            </div>
            <!-- ENDssss -->
		</div>
		</div>
		</div>
		
		
		<!-- Rin Packet counter -->
		<div class="row pcu_info2" style="display:none;">
		<div class="col-lg-12 col-md-12">
		<div class="card">
		<div class="card-body">
			 <h3 class="heading-text" style="font-family:'Rubik',sans-serif"><span>Plant & Packet Counter Information </span></h3>
				<div class="row">
                    <div class="col-lg-12 col-md-12">
                    <!-- Card Plant 1 -->
                        <div class="card">
                            <div class="card-body">
                                <!-- Nav tabs -->
                                <div class="vtabs">
                                    <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home4" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">RIN</span> </a> </li>
                                        <!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile4" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">UNIT2 (U2)</span></a> </li> -->
                                    </ul>
                                    <!-- Tab panes -->
									<div class="tab-content">
										
										<!-- Tab 1 -->
										<div class="table-responsive tab-pane active" id="home4" role="tabpanel">
											<h4 class="card-title">Packet Counter Unit</h4>
											<h6 class="card-subtitle"><code>No Packet Counter Unit available for RIN</code></h6>
											
										</div>
									</div>
                                </div>
                            </div>
                        </div> <!-- Card end -->                        
                    </div>
                </div>	
		</div>
		</div>
		</div>
		</div>					
				<!--/div> <!-- Card end 2-->
			</div>

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            
@extends('footer')