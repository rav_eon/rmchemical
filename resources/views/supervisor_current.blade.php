@extends('header')
<!-- Filter -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .span-class {
        float:right;
    }
    #date {
        width: 10%;
    }
	
.vtabs {
    width: 100% !important;
}
.vtabs .tabs-vertical {
    width: 204px !important;
}
.plant_detail_1 > a {
    color: #272c33 !important;
}.table td, .table th {
    border-color: #009efb !important;
}
.dev_style {
    color: #272c33 !important;
    font-weight: normal !important;
}
.card-body.heading-block {
    padding: 10px;
    background: #999595;
}
.heading-text span {
    color: white;
}
.admin_display_bar{
	background: #3c6382;
    color: white;
}
.vtabs {
width: 100% !important;
}
.vtabs .tabs-vertical {
width: 204px !important;
}
.plant_detail_1 > a {
color: #272c33 !important;
}.table td, .table th {
border-color: #5b826f !important;
}
.dev_style {
color: #272c33 !important;
font-weight: normal !important;
}
/* .card-body.heading-block {
padding: 10px;
background: #999595;
} */
.heading-text span {
color: #174d33;
font-style: italic;
}
.heading-text {
height : 60px;
}
.left-sidebar {
    background: #272c33 !important;
}
.select-date {
    width: auto;
    height: 100%;
}
#span-text {
    font-size: 20px;
}
p {
    margin-bottom: 0rem !important;
}
.text-white {
    font-size: 23px !important;
}
.btn {
  background-color: DodgerBlue;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
}

/* Darker background on mouse-over */
.btn:hover {
  background-color: black;
}


</style>
 
<head>
   

 <?php
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');	
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	//echo $return_plant; 
?>
	<span><?php $logo = "../resources/assets/images/rmchemicals_logo.png"; ?></span>
	<div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/shift_officer') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo" style="width: 90px;">
						</b>                        
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>                       
                    </ul>
                    <div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <ul class="navbar-nav my-lg-0">
						Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><!--img src="{{ URL::asset($logo) }}" alt="user"--><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p><a href="javascript:void(0)" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li-->
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>


		<!-- ######################### SIDEBAR ###########################-->
		<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="<?php echo $logo; ?>" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <!--a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"-->RM Chemicals <!--/a-->
                        
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>                            
                        </li>
                        
                        <li style="display:none;">
                            <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-nfc-tap"></i><span class="hide-menu">Select Unit</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li class="unit1_live_data"><a href="javaScript:void(0)">Unit 1</a></li>
                                <li class="unit2_live_data"><a href="javaScript:void(0)">Unit 2</a></li>
                            </ul>
                        </li> 
						
                        <li class="unit_plant_live_data" style="display:block;">
                            <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-pillar"></i><span class="hide-menu">Select Plant</span></a>							
                            <ul aria-expanded="false" class="collapse">								
								<?php
									// echo "<pre>".'-----------------------------------------------------';
									//$pairs = $get_plant_list;
									// print_r($pairs);
									// $pairs = explode(',', $get_plant_list);
									// $a = array();
									// print json_encode($pairs);
									$join = 0;
									//foreach ($get_plant_list as $k=>$v) {
										//$pair = explode(',', $v);
										foreach($get_plant_list as $p){
											//echo $p;
											// echo $pair[0];
											// echo $pair[1];
											// echo $pair[$join];
								
											// echo $pair[$join]; 
											$plantid = $p; 
											$plantfetch = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id = $plantid");
											while($plantfetchrow = pg_fetch_array($plantfetch)){
												// echo "<pre>";
												// print_r($plantfetchrow['plant_name']);
												// echo "</pre>";
									
								?>
								
								<form method="post" action="shift_officer">
									@csrf
									<li class="plant_data" value="<?php echo $p; ?>"><a href="javascript:void(0)" class="currentshiftinfo" style="display:none;"><?php echo $p; ?></a></li>
									<input type="hidden" name="plantid" value="<?php echo $p; ?>">
									<input class="btn btn-success" style="width:90%;" type="submit" name="submit" value="<?php echo $plantfetchrow['plant_name']; ?>">
								</form>
								<?php	
											$join++;
											}
										}
									//}
								?>
							</ul>
                        </li> 	


                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
        </aside>
		<!-- ######################### SIDEBAR ###########################-->
	
	<!-- Body -->
	<div class="page-wrapper" style="min-height: 432px;">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                          
		<h1>Supervisor</h1>
		<?php
			$idofplant = $return_plant;
		// 	echo "<pre>";
		//  print_r($data); 
			// echo "</pre>";
			// echo "111111";
			if(empty($idofplant)){
                if(count($data) > 0) {
                    $idofplant = $data[0]['plant_id'];
                } else {
                    $idofplant = '';
                }
				
            } else {
                $idofplant = $return_plant;
            }
            if(!empty($idofplant)) { 		
		?>

        
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
                            <div class="col-lg-12">
                                <div class="col-md-12 float-left" style="font-family:'Rubik', sans-serif, text-align:right; font-size: 16px;margin-bottom:5%;	font-weight: bolder;">
                                    <span>&nbsp;&nbsp;&nbsp;
										<?php
											$plantdisplayid = $idofplant; 
											$plantdisplay = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id = $plantdisplayid");
											while($plantdisplayrow = pg_fetch_array($plantdisplay)){
										?>
											<strong>Plant: <div class="btn btn-default active"><?php echo $plantdisplayrow['plant_name'];} ?> </div></strong><strong>Shift:<div class="btn btn-default active"><?php echo $shift; ?></div></strong><strong>Date:<div class="btn btn-default active"><?php echo $time; ?></div></strong>
                                    </span>
                                </div>
                            </div>                        
						
                        <?php 						
							
							foreach($data as $val) { $check_plant = $idofplant; $get_plant = $val['plant_id'];
							
							// echo "</pre>";
						?> 
                        <?php 
							if($check_plant == $get_plant){							
						?> 
							<!-- col -->
							<div class="col-lg-4 col-md-6">
                                <!--a href=""-->
								<form method="post" action="show_graph?p=<?php echo $val['plant_id']; ?>&m=<?php  echo $val['mach_id']; ?>&c=<?php echo $val['cascade_id']; ?>&u=<?php echo $val['unit_id']; ?>">
								
									{{ csrf_field() }}
                                    <div class="card bg-info" <?php 
                                    
                                    if($val['oee'] >= 85) {
                                        echo "style='background-color:green !important;'";
                                    }else if($val['oee'] < 85 && $val['oee'] >= 75) {
                                        echo "style='background-color:#b5b503 !important;'";
                                    }else {
                                        echo "style='background-color:red !important;'";
                                    }  ?> >
                                        <div class="card-body">
                                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">
                                                    <div class="carousel-item flex-column active">
                                                        <p class="text-white">OEE% : <span class="font-bold"><?php echo !empty($val['oee']) ? $val['oee']: '0%' ;?></span></p>
                                                        <h3 class="text-white font-light">Production : <span class=""><?php echo $val['total_production'] ? $val['total_production']: '0' ;?><?php echo $val['total_production'] > 0 ? 'Tons' : '' ?> </span><br><br><span class="font-bold" id="span-text"><?php echo $val['machine_name']; ?></span></h3>
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="inner-addon left-addon">
                                            <input type="hidden" name="mach_id" value="<?php echo $val['mach_id']; ?>">
                                            <?php if($val['oee'] > '0%' && $shift == 'M' && $val['first_flag'] == 'Y') {  ?>
                                                <button class="btn" style="font-size: 24px; color: white;"><i class="fa fa-bar-chart" aria-hidden="true"></i></button>
                                        <?php } else if($shift != 'M'  && $val['first_flag'] == 'Y') { ?>
                                            <button class="btn" style="font-size: 24px; color: white;"><i class="fa fa-bar-chart" aria-hidden="true"></i> </button>
                                    <?php  }?>
                                    </div>
                                    </div>									
								</form>	
                                <!--/a-->
							</div>								
							<?php }  ?>
                        <?php } ?>
						</div>
					</div>
				</div>
			</div>
        </div>
        
                                <?php }  else {  echo ""; }?>


                            


                             
@extends('footer')
