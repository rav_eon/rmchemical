<?php 
error_reporting(0);
include "header.php";

// Depo Id
if (isset($_POST['depo_id'])) {
    $depo_id = $_POST['depo_id'];
    
} else {
    if ($_SESSION['depo_id']!=0) {
        $depo_id = $_SESSION['depo_id'];
    }
}

$status   = $_POST['status'];
$veh_type = $_POST['veh_type'];
$dev_type = $_POST['dev_type'];

// Time Duration
function timeDuration($time){
    global $db_handle;

    // Time Difference
    $timeQry = "select age(now(),'$time') as diff";
    $timeExc = pg_exec($db_handle,$timeQry);
    $timeDur = pg_result($timeExc,'0','diff');

    $secQry  = "SELECT EXTRACT(EPOCH FROM INTERVAL '".$timeDur."') as time";
    $secExc  = pg_exec($db_handle,$secQry);
    $seconds = pg_result($secExc,'0','time');

    // Time Difference
    $time = $seconds/60;
    return $time;
}
?>

<!-- ============================================================== -->
<!-- Modal -->
<!-- ============================================================== -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog" id="eta-content"></div>
</div>
<!-- ============================================================== -->
<!-- End Modal -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <?php if ($msg) { ?>
                <div class="col-lg-12 col-md-12">
                    <div class="alert alert-success"> <i class="ti-user"></i> <?php echo $msg; ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            <?php } ?>

            <?php if ($errormsg) { ?>
                <div class="col-lg-12 col-md-12">
                    <div class="alert alert-danger"> <i class="ti-user"></i> <?php echo $errormsg; ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            <?php } ?>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header up-down-header">
                        <a id="heading22">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#srch_form" aria-expanded="false" aria-controls="srch_form">
                                <h4 class="mb-0"><img class="up_down_icon" src="assets/images/filter.png" />&nbsp;Vehicle Status :</h4>
                            </button>
                        </a>

                        <h6 class="sub_header"><img class="img_icons" src="assets/images/connected.png" />&nbsp;Connected&nbsp;&nbsp;&nbsp;<img class="img_icons" src="assets/images/disconnected.png" />&nbsp;Disconnected&nbsp;&nbsp;&nbsp;<img class="img_icons" src="assets/images/no-parking.png" />&nbsp;Detained&nbsp;&nbsp;<img class="img_icons" src="assets/images/idlebus.png" />&nbsp;Idle Buses</h6>
                    </div>

                    <div id="srch_form" class="collapse show" aria-labelledby="heading22">
                        <div class="card-body">
                            <form action="" method="post" data-parsley-validate>
                               <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Depot Name</label>
                                                <select class="form-control" name="depo_id" required>
                                                    <?php
                                                    $and = "";
                                                    if ($_SESSION['user_name']=='admin@punbus' || $_SESSION['user_name']=='dba@punbus') {
                                                        $and .= " and id_dist!='".$_SESSION['eon_id']."'";
                                                        echo "<option value=''>Select</option>";
                                                    } else if ($_SESSION['user_name']=='eon@punbus' || $_SESSION['user_name']=='tech@punbus') {
                                                        $and .= "";
                                                        echo "<option value=''>Select</option>";
                                                    } else {
                                                        $and .= " and id_no='".$_SESSION['depo_id']."'";
                                                    }

                                                    $recQry = "select id_no, name from hosp_master where 1=1 ".$and." order by name";
                                                    $recExc = pg_exec($db_handle,$recQry);
                                                    while ($recData = pg_fetch_array($recExc)) {
                                                        if ($depo_id==$recData['id_no']) {
                                                            echo '<option value="'.$recData['id_no'].'" selected>'.$recData['name'].'</option>';
                                                        } else {
                                                            echo '<option value="'.$recData['id_no'].'">'.$recData['name'].'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                         <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Status</label>
                                                <select class="form-control" name="status">
                                                    <option value="">All</option>
                                                    <?php if ($status==1) { ?>
                                                        <option value="1" selected>Connected</option>
                                                    <?php } else { ?>
                                                        <option value="1">Connected</option>
                                                    <?php } ?>

                                                    <?php if ($status==2) { ?>
                                                        <option value="2" selected>Disconnected</option>
                                                    <?php } else { ?>
                                                        <option value="2">Disconnected</option>
                                                    <?php } ?>

                                                    <?php if ($status==3) { ?>
                                                        <option value="3" selected>Detained</option>
                                                    <?php } else { ?>
                                                        <option value="3">Detained</option>
                                                    <?php } ?>

                                                    <?php if ($status==5) { ?>
                                                        <option value="5" selected>Idle</option>
                                                    <?php } else { ?>
                                                        <option value="5">Idle</option>
                                                    <?php } ?>

                                                    <?php if ($status==4) { ?>
                                                        <option value="4" selected>Power Remove</option>
                                                    <?php } else { ?>
                                                        <option value="4">Power Remove</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Device Type</label>
                                                <select class="form-control" name="dev_type">
                                                    <option value="">All</option>
                                                    <?php
							if($dev_type=='1'){$a="selected";}
							if($dev_type=='2'){$b="selected";}
							print "<option value='1' $a>AIS 140</option>";
							print "<option value='2' $b>E124</option>";
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Vehicle Type</label>
                                                <select class="form-control" name="veh_type">
                                                    <option value="">All</option>
                                                    <?php
                                                    $recQry = "select id_no, name from veh_type order by name";
                                                    $recExc = pg_exec($db_handle,$recQry);
                                                    while ($recData = pg_fetch_array($recExc)) {
                                                        if ($veh_type==$recData['id_no']) {
                                                            echo '<option value="'.$recData['id_no'].'" selected>'.$recData['name'].'</option>';
                                                        } else {
                                                            echo '<option value="'.$recData['id_no'].'">'.$recData['name'].'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">&nbsp;</label><br/>
                                                <input type="submit" class="btn btn-success" name="submit" value="Search" title="Search" />
                                                <a href="vehicle-status.php" class="btn btn-inverse" title="Reset">Reset</a>
                                            </div>
                                        </div>

                                        <!--<div class="col-md-2 text-right">
                                            <div class="form-group">
                                                <label class="control-label">&nbsp;</label><br/>
                                                <a href='vehicle-status-excel.php?gt=<?php echo $district_id; ?>:<?php echo $depo_id; ?>:<?php echo $status; ?>:<?php echo $veh_type; ?>' title='Export to Excel'><img src='assets/images/excel.png' width='25px' height='25px'></a>
                                            </div>
                                        </div> -->
                                    </div>
                                    <!--/row-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        // Record
	#if($depo_id)
	#{
        $and = " and VM.id_hosp='$depo_id'";
	#}
	#else
	#{
        #$and = "";
	#}

        if ($status == 1) {
            $and .= "and age(now(),date_time_entry) < '23:59:00'";
        }

        if ($status == 2) {
            $and .= "and age(now(),date_time_entry) > '23:59:00' and current_status='0'";
        }

        if ($status == 5) {
            $and .= " and VM.current_status='3'";
        }

        if ($status == 3) {
            $and .= " and VM.current_status='1'";
        }

        if ($status == 4) {
            $and .= " and P.sensor2b3 in ('y', '0', 'f') ";
        }

        if ($veh_type) {
            $and .= " and VM.dept_no='".$veh_type."'";
        }

        if ($dev_type) {
            $and .= " and VM.device_id='".$dev_type."'";
        }

        $body_data = "";
        $recQry  = "select P.*, VM.bus_id as vts_id, VM.serial_no, VM.reg_no as regno, VM.d_id, VM.c_id, VM.imei,VM.id_hosp, VM.device_id, VM.current_status, VM.adc, VM.fuel_gauge from tc_position P right join vehicle_master VM on P.reg_no=VM.reg_no where 1=1 ".$and." and VM.id_hosp not in ('1143') order by VM.bus_id";
        $recExc  = pg_exec($db_handle,$recQry);
        $tot_veh = pg_num_rows($recExc);
        if (pg_num_rows($recExc) > 0) {
            $sr = 1;
            $con_veh = 0;
            while ($recData = pg_fetch_array($recExc)) {

                // Device Details
                $othQry    = "select * from device_details where vts_id='".$recData['vts_id']."'";
                $othExc    = pg_exec($db_handle,$othQry);
                $serial_no = pg_result($othExc,'0','serial_no');
                $imei_no   = pg_result($othExc,'0','imei_no');

                // Battery Voltage
                $deviceid = $recData['device_id'];
                if ($deviceid=='1') {
                    	$bat_volt = $recData['batt_volt'];
                	$odo_rd   = $recData['distance_covered'];
                } else {
                	$bat_volt = round($recData['batt_volt']/100,2);
			$ss     = "select * from hour_data where reg_no ='$recData[regno]' order by time_stamp desc limit '1'";
			$ss1    = pg_exec($db_handle,$ss);
			$odo_rd = pg_result($ss1,'0','distance');
	
#                	$odo_rd  = '0';
                }

                $veh_batt_volt = $recData['veh_batt_volt'];
                $fuel_rd = $recData['fuel_status'];

                // Network Provider
                if ($recData['ntw_prv']=='Idea P' || $recData['ntw_prv']=='Idea B') {
                    $ntw_opr = "<img class='img_icons' src='assets/images/idea.png' title='".strtoupper($recData['ntw_prv'])."' />";

                } else if ($recData['ntw_prv']=='BSNL F'){
                    $ntw_opr = "<img class='img_icons' src='assets/images/bsnl.png' title='".strtoupper($recData['ntw_prv'])."' />";

                } else if ($recData['ntw_prv']=='airtel'){
                    $ntw_opr = "<img class='img_icons' src='assets/images/airtel.jpg' title='".strtoupper($recData['ntw_prv'])."' />";

                } else {
                    $ntw_opr = "N/A";
                }

                // Ignition Sensor
                if ($deviceid!='1') {
                    $ign_sensor = "N/A";

                } else {
	                $sensor3 = $recData['sensor3'];
        	        if ($sensor3=='1') {
                	    $ign_sensor = "<font color='green'><b>ON</b></font>";
	                } else {
        	            $ign_sensor = "<font color='red'><b>OFF</b></font>";
                	}
		}

                // other parameters 
                if ($deviceid!='1') {
                    $fuel_rd = "N/A";
                }

                $batt_volt = "$bat_volt";

                // Depo Name
                $depoQry   = "select name from hosp_master where id_no='".$recData['id_hosp']."'";
                $depoExc   = pg_exec($db_handle,$depoQry);
                $depo_name = pg_result($depoExc,'0','name');

                // Route Name
                if (!$recData['route_id']) {
                    $recData['route_id'] = 0;
                }
                $routeQry   = "select name,full_name from route_master where id_no='".$recData['route_id']."'";
                $routeExc   = pg_exec($db_handle,$routeQry);
                $route_code = pg_result($routeExc,'0','name');
                $route_name = pg_result($routeExc,'0','full_name');

                // Driver Name
                if (!$recData['d_id']) {
                    $recData['d_id'] = 0;
                }
	$se="select * from driver_assignment where reg_no='".$recData['regno']."' and (now()-interval '5minutes',now()) overlaps (from_dt,to_dt) and id_depo='".$recData['id_hosp']."' order by time_stamp desc limit '1'";
			$see=pg_exec($db_handle,$se);
			$d_id=pg_result($see,'0','driver_id');
			$c_id=pg_result($see,'0','cond_id');

                $curDrvQry   = "select d_code,telephone as contact_no from driver_master where id_no='".$d_id."'";
                $curDrvExc   = pg_exec($db_handle,$curDrvQry);
                $curDrv_code = pg_result($curDrvExc,'0','d_code');
                $curDrv_no = pg_result($curDrvExc,'0','contact_no');

                // Conductor Name
                if (!$recData['c_id']) {
                    $recData['c_id'] = 0;
                }
                $curConQry   = "select c_code,telephone as contact_no from conductor_master where id_no='".$c_id."'";
                $curConExc   = pg_exec($db_handle,$curConQry);
                $curCon_code = pg_result($curConExc,'0','c_code');
                $curCon_no = pg_result($curConExc,'0','contact_no');

                // Driver Conductor
                if ($recData['c_id']) {
                    $drv_con = strtoupper($curDrv_code)."/".strtoupper($curCon_code);
                } else {
                    $drv_con = strtoupper($curDrv_code);
                }

                // Power
                if ($recData['sensor2b3']=='y' || $recData['sensor2b3']=='0' || $recData['sensor2b0']=='f') {
                    $power = "<img class='img_icons' src='assets/images/power_dis.ico' title='Disconnected' />";
                } else {
                    $power = "";
                }

                // Connectivity
                $time = $recData['date_time_entry'];
                if (!$time) {
                    $time = '1970-01-01 05:30:00';
                }
                if ($recData['current_status']=='1') {
                    $connectivity = "<img class='img_icons' src='assets/images/no-parking.png' title='".date('d/m/Y H:i:s', strtotime($time))."' />";

                } else if ($recData['current_status']=='3') {
                    $connectivity = "<img class='img_icons' src='assets/images/idlebus.png' title='".date('d/m/Y H:i:s', strtotime($time))."' />";

                } else {
                    $conQry = "select age(now(),'$time') > '23:59:00' as st";
                    $conExc = pg_exec($db_handle,$conQry);
                    $con_status = pg_result($conExc,'0','st');
                    if ($con_status == 't') {
                        $connectivity = "<img class='img_icons' src='assets/images/disconnected.png' title='".date('d/m/Y H:i:s', strtotime($time))."' />";

                    } else {
                        $con_veh ++;
                        $connectivity = "<img class='img_icons' src='assets/images/connected.png' title='".date('d/m/Y H:i:s', strtotime($time))."' />";
                    }
                }

                // Location Name
		if($recData['latitude'])
		{
               # $loc_name = locationName($recData['latitude'],$recData['longitude']);
		}

                // Location
                $loc = "<img style='cursor:pointer;' class='img_icons' onclick='showAddr(".$recData['latitude'].",".$recData['longitude'].")' src='assets/images/location.png' />";

                // Map
                $map = "<a href=javascript:winopen('google-monitor.php?b_id=".$recData['vts_id']."&lat=".$recData['latitude']."&long=".$recData['longitude']."') style={text-decoration:none;}><img class='img_icons' src='assets/images/map.png' /></a>";

                // Idle Time
                if ($recData['speed']==0) {
                    $idleQry  = "select date_time, age(now(),date_time) as idletm from idle_time_status where reg_no='".$recData['regno']."'";
                    $idleExc  = pg_exec($db_handle,$idleQry);
                    $idle_tm  = pg_result($idleExc,'0','date_time');
                    $idle_tm1 = pg_result($idleExc,'0','idletm');

                    $idl_mins = timeDuration($idle_tm);
                    $idl_hrs  = floor($idl_mins/60);
                    if ($idl_hrs>0) {
                            $idl_min  = $idl_mins%60;

                            if ($idl_hrs <= 9) {
                                    $idl_hrs = "0".$idl_hrs;
                            }

                            if ($idl_min <= 9) {
                                    $idl_min = "0".$idl_min;
                            }

                            $idl_time = $idl_hrs.":".$idl_min;
                    } else {
                            $idl_time = "00:00";
                    }

                    $idl_tim  = explode(".",$idle_tm1);
                    $idl_tm   = $idl_tim[0];

                } else {
                    $idl_tm   = "";
                    $idl_time = "";
                }

                // Fuel Status
                if ($recData['fuel_gauge']==0) {
                    $fuel_status = "";
                } else {
                    $fuel_per = round(($recData['fuel_status']/$recData['adc'])*100);
                    if ($fuel_per<=13) {
                        $fuel_status = "<img class='img_icons' src='assets/images/percent0.png' />";
                    } else if ($fuel_per>13 && $fuel_per<=38) {
                        $fuel_status = "<img class='img_icons' src='assets/images/percent25.png' />";
                    } else if ($fuel_per>38 && $fuel_per<=63) {
                        $fuel_status = "<img class='img_icons' src='assets/images/percent50.png' />";
                    } else if ($fuel_per>63 && $fuel_per<=88) {
                        $fuel_status = "<img class='img_icons' src='assets/images/percent75.png' />";
                    } else if ($fuel_per>88 && $fuel_per<101) {
                        $fuel_status = "<img class='img_icons' src='assets/images/percent100.png' />";
                    } else {
                        $fuel_status = "<img class='img_icons' src='assets/images/percent0.png' />";
                    }
                }
                
                $body_data .= "<tr>
                    <td>".$sr."</td>";
		    if ($recData['device_id']=='1') {
			if (!$serial_no) {
			    $body_data .=" <td title='".$imei_no."'>".$recData['vts_id']."</td>";
		        } else {
			    $body_data .=" <td title='".$imei_no."'>".$serial_no."</td>";
		        }

		    } else {
                	$body_data .=" <td title='".$recData['imei']."'>".$recData['vts_id']."</td>";
		    }

                    $body_data .="<td><a class='show-modal' href='javascript:void(0);' onclick='viewETA(".$recData['vts_id'].",".$recData['route_id'].");' data-toggle='modal' data-target='#modal-default'>".strtoupper($recData['regno'])."</a></td>
                    <td title='Drv: ".$curDrv_no." / Cnd: ".$curCon_no."'>".$drv_con."</td>
                    <td>".strtoupper($depo_name)."</td>
                    <td title='".$route_name."'>".strtoupper($route_code)."</td>
                    <td>".$loc."</td>
                    <td>".$map."</td>
                    <td>".$ign_sensor."</td>
                    <td>".$recData['speed']."</td>
                    <td>".$recData['sats_view']."</td>
                    <td>".$recData['gsm_rssi']."</td>
                    <td>".$power."</td>
                    <td>".$batt_volt."</td>
                    <td>".$veh_batt_volt."</td>
                    <td>".$ntw_opr."</td>
                    <td>".$odo_rd."</td>
                    <td>".$fuel_status."</td>
                    <td>".$connectivity."</td>
                    <td>".$idl_tm."</td>
                </tr>";
                $sr++;
            }

        } else {
            $body_data .= "<tr><td colspan='20' align='center'>No record found</td></tr>";
        }
        ?>

        <!-- Table -->
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                        <tr valign="top">
                            <th width="2%">#</th>
                            <th width="5%">ID</th>
                            <th width="8%">REG NO.</th>
                            <th width="9%">DRV/CND</th>
                            <th width="8%">DEPOT</th>
                            <th width="8%">ROUTE</th>
                            <th width="5%"><img class="img_icons" src="assets/images/location.png" title="Location" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/map.png" title="View On Map" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/ignition.png" title="Ignition Sensor" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/speed.png" title="Speed" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/satellite.png" title="Satellite" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/gsm.png" title="GSM" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/power.ico" title="Power Connection" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/device_battery.png" title="Device Battery" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/veh_battery.png" title="Vehicle Battery" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/isp.png" title="ISP" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/kms.png" title="Today's KMS" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/fuel.png" title="Fuel Status" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/connectivity.png" title="Vehicle Status (<?php echo $con_veh; ?> / <?php echo $tot_veh; ?>)" /></th>
                            <th width="5%"><img class="img_icons" src="assets/images/clock.png" title="Idle Time (HH:MM)" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $body_data; ?>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container Fluid -->
    <!-- ============================================================== -->

    <script type="text/javascript">
        // Auto Refresh
        window.onload = timedRefresh(120000);
        function timedRefresh(timeoutPeriod) {
            setTimeout("location.reload(true);",timeoutPeriod);
        }
        
        // Function for Select Depo's
        function chkDistricts(id){ 
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: 'action=srchDepos&id='+id,
                success: function(data){
                    $('#depos').html(data);
                }
            });
        }
	
	// Function for Show Location
        function showAddr(lat,lng){ 
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: 'action=showAddr&lat='+lat+'&long='+lng,
                success: function(data){
                    alert(data);
                }
            });
        }

        // Function for ETA Details
        function viewETA(vid,rid){ 
            $.ajax({
                type: "POST",
                url: "ajax-eta.php",
                data: 'action=eta&vid='+vid+'&rid='+rid,
                success: function(data){
                    $('#eta-content').html(data);
                }
            });
        }
    </script>
</div>
<!-- ============================================================== -->
<!-- End Page Wrapper -->
<!-- ============================================================== -->

<?php include "footer.php"?>
