@extends('header')
<style>
    .vtabs {
    width: 100% !important;
}
.vtabs .tabs-vertical {
    width: 204px !important;
}
.plant_detail_1 > a {
    color: #272c33 !important;
}.table td, .table th {
    border-color: #009efb !important;
}
.dev_style {
    color: #272c33 !important;
    font-weight: normal !important;
}
.card-body.heading-block {
    padding: 10px;
    background: #999595;
}
.heading-text span {
    color: white;
}

.vtabs {
width: 100% !important;
}
.vtabs .tabs-vertical {
width: 204px !important;
}
.plant_detail_1 > a {
color: #272c33 !important;
}.table td, .table th {
border-color: #5b826f !important;
}
.dev_style {
color: #272c33 !important;
font-weight: normal !important;
}
/* .card-body.heading-block {
padding: 10px;
background: #999595;
} */
.heading-text span {
color: #174d33;
font-style: italic;
}
.heading-text {
height : 60px;
}

.admin_display_bar{
	background: #3c6382;
    color: white;
}

.new_unit {
    width: 100% !important;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

.contact-list td {
    vertical-align: middle;
    padding: 10px 10px !important;
}
.userModal22{
	background: #01c0c8 !important;
    border: 1px solid #01c0c8 !important;
}



.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}


</style>
	<?php
		use Illuminate\Support\Facades\DB as db;
		//$find_user = Session::get('user_id');
		//die($find_user."hereeee");
		
		if(empty(Session::get('email')) || empty(Session::get('id')))
		{
	?>
			<script>window.location.href = "{{ url('/login') }}";</script>
	<?php
		  //return redirect('/login');
		}
		if( Session::get('designation_id') != 1 )
		{
	?>
			<script>window.location.href = "{{ url('/message') }}";</script>
	<?php
		}
		
		$host = Session::get('host');
		$dbname = Session::get('dbname');
		$user = Session::get('user');
		$pass = Session::get('pass');
		// echo $host;
		// echo $dbname;
		// echo $user;
		$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
		//$db = Session::get('db');
		//dd(Session::get('db'));
		//dd(session()->all());
	?>
		
    <body class="fix-header fix-sidebar card-no-border">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    </div>

    <span>
    <?php 
		$logo = "../resources/assets/images/rmchemicals_logo.png"; 
		$img_location = "../resources/assets/images/"; 		
	?>
    </span>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                            <img src="{{ URL::asset($logo) }}" alt="homepage" class="dark-logo fa fa-user" style="width: 90px;">                           
                        </b>                        
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->

                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Mega Menu End Messages -->
                        <!-- ============================================================== -->
                    </ul>
					<div class="real_time_monitoring"><h3>Real Time Production Monitoring System</h3></div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!--li class="nav-item hidden-sm-down">
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                        </li-->Welcome {{ Session::get('firstname') }}! 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><i class="fa fa-user" aria-hidden="true"></i></div>
                                            <div class="u-text">											
                                                <h4>{{ Session::get('firstname') }}</h4>
                                                <p class="text-muted">{{ Session::get('email') }}</p>
												<!--a href="javascript:void(0)" class="btn btn-rounded btn-danger btn-sm">View Profile</a-->
											</div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--li><a href="#"><i class="ti-user"></i> My Profile</a></li-->                                   
                                    <li role="separator" class="divider"></li>
                                    
									<li>
										<form method="post" action="logout">
											{{ csrf_field() }}
											<a href="logout"><i class="fa fa-power-off"></i> Logout</a>
										</form>
									</li>
									
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
       
		@if(session('Status'))
			<p>{{ session('Status') }}</p>
		@endif
        <!--div class="page-wrapper"-->
        <div class="">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
               				
				@if ($message = Session::get('message'))
					<!--div class="alert alert-info alert-block">
						<button type="button" class="close" data-dismiss="alert">×</button>	
						<strong></strong>
					</div-->
				@endif				
								
            <br/> 

			<!-- new code -->
			<div class="row admin_box">
				<div class="col-lg-12 col-md-12">
					<div class="card">
						<div class="card-body">
							<h3 class="heading-text" style="font-family:'Rubik', sans-serif"><span style="font-size:30px;" class="admin_control">Administrator Control Panel</span><span class="span-date admin_control2">Today : <?php //echo $yesterday_date ?><?php echo $today = date("j-F-Y");   ?></span></h3>
							<div class="row">
								<!-- UNIT -->
								
								<div class="col-lg-2 col-md-3 admin_unit" id="1">
									<a href="#pointsection">
										<div class="card bg-info">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">												
													<p class="text-white"><span class="font-bold"><?php //echo $srf_boon; ?></span></p>
													<h3 class="text-white font-light">Unit<br><span class="font-bold">Information</span></h3>
												</div>	
											</div>
										</div>
									</a>
								</div>
								
								<!-- PLANT -->
								<div class="col-lg-2 col-md-3 admin_plant" id="2">
									<a href="#pointsection">
										<div class="card bg-info-2">
											<div class="card-body">
												<div id="myCarousel" class="carousel slide" data-ride="carousel">
												<!-- Carousel items -->
													<div class="carousel-inner">											
														<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
														<p class="text-white"><span class="font-bold"><?php //echo $srf_psm; ?></span></p>
														<h3 class="text-white font-light">Plant<br><span class="font-bold">Information</span></h3>
													</div>												
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- PLANT PINCODE -->	
								<div class="col-lg-2 col-md-3 admin_plant_pincode" id="5">
									<a href="#pointsection">
										<div class="card bg-info-3">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Plant<br><span class="font-bold">Plant Pincode</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- CASCADE -->
								<div class="col-lg-2 col-md-3 admin_cascade" id="3">
									<a href="#pointsection">
										<div class="card bg-info-4">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l1; ?></span></p>
													<h3 class="text-white font-light">Cascade<br><span class="font-bold">Information</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>	
											</div>
										</div>
									</a>
								</div>
								<!-- MACHINE -->
								<div class="col-lg-2 col-md-3 admin_machine" id="4">
									<a href="#pointsection">
										<div class="card bg-info-5">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l2; ?></span></p>
													<h3 class="text-white font-light">Machine<br><span class="font-bold">Information</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->								
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- PRODUCTS -->
								<div class="col-lg-2 col-md-3 admin_products" id="5">
									<a href="#pointsection">
										<div class="card bg-info-6">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Products<br><span class="font-bold">Information</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- MIXER BCT -->	
								<div class="col-lg-2 col-md-3 admin_bct" id="5">
									<a href="#pointsection">
										<div class="card bg-info-6">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Products<br><span class="font-bold">Mixer BCT</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- PRODUCT MAPPING -->	
								<div class="col-lg-2 col-md-3 admin_product_mapping" id="5">
									<a href="#pointsection">
										<div class="card bg-info-5">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Products<br><span class="font-bold">Plant Mapping</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>								
								<!-- PRODUCT SKU's -->	
								<div class="col-lg-2 col-md-3 admin_skus" id="7">
									<a href="#pointsection">
										<div class="card bg-info-4">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Products<br><span class="font-bold">SKU</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- MACHINE PRODUCT SKU's RPM -->	
								<div class="col-lg-2 col-md-3 admin_machine_product_skus" id="2">
									<a href="#pointsection">
										<div class="card bg-info-6">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Products<br><span class="font-bold">Machine Product SKU</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								
								<!-- SKU PACKING UNIT -->
								<div class="col-lg-2 col-md-3 admin_sku_unit" id="8">
									<a href="#pointsection">
										<div class="card bg-info-3">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">SKU<br><span class="font-bold">Packing UNIT</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- SKU PACKING TYPE -->
								<div class="col-lg-2 col-md-3 admin_sku_type" id="8">
									<a href="#pointsection">
										<div class="card bg-info-2">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">SKU<br><span class="font-bold">Packing Type</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- BREAKDOWN REASONS -->
								<div class="col-lg-2 col-md-3 admin_reasons" id="10">
									<a href="#pointsection">
										<div class="card bg-info-3">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Breakdown<br><span class="font-bold">Reasons</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- BREAKDOWN SUB REASONS -->
								<div class="col-lg-2 col-md-3 admin_subreasons" id="11">
									<a href="#pointsection">
										<div class="card bg-info-4">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Breakdown<br><span class="font-bold">Sub Reasons</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- SHIFT INFORMATION -->
								<div class="col-lg-2 col-md-3 admin_shifts" id="6">
									<a href="#pointsection">
										<div class="card bg-info-6">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">Shift<br><span class="font-bold">Information</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								<!-- PCU HEALTH INFORMATION -->
								<!--div class="col-lg-2 col-md-3 admin_pcu_health pcudatainfo" id="6"-->
								<div class="col-lg-2 col-md-3 admin_pcu_health" id="6">
									<a href="#pointsection">
										<div class="card bg-info-3">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">											
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_l3; ?></span></p>
													<h3 class="text-white font-light">PCU<br><span class="font-bold">Health Monitor</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->										
												</div>
											</div>
										</div>
									</a>
								</div>
								
								<!-- USER INFORMATION -->
								<div class="col-lg-2 col-md-3 admin_users" id="12">
									<a href="#pointsection">
										<div class="card bg-info-2">
											<div class="card-body">
												<!-- Carousel items -->
												<div class="carousel-inner">										
													<!-- <i class="fab fa-twitter fa-2x text-white"></i> -->
													<p class="text-white"><span class="font-bold"><?php //echo $srf_final; ?></span></p>
													<h3 class="text-white font-light">Users<br><span class="font-bold">Information</span></h3>
													<!-- <div class="text-white mt-3">
														<i>- john doe</i>
													</div> -->
												</div>
											</div>
										</div>
									</a>
								</div>							
							</div>						
						</div>
					</div>
				</div>
			</div>	
		
				
		
						
			<div class="jumbotron jumbo_panel">							
				<h3 style="text-align:center">Administrator Panel</h3>							
			</div>
			<div id="pointsection">
					<!-- //############################### Unit Section ######################################## -->
					<div class="table-responsive unit_info" id="unitsection" style="display:none;">												
						<a href="{{ url('/add-unit') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Unit</button>
						</a>						
						<h3>UNIT</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Unit Code</th>
									<th>Unit Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_unit_table = pg_query($db, "SELECT * FROM rm_units");	
									$i = 1;
									$rm_data = array();
									while($rm_unit = pg_fetch_array($rm_unit_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $rm_unit['unit_code']; ?></td>
									<td><?php echo $rm_unit['unit_name']; ?></td>
									<td>
										<a href="unitedit/<?php echo $rm_unit['id']; ?>" class="open_edit_unit btn btn-info"><i class="fa fa-edit"></i> </a> | 
										<!--a href="unitdelete/<?php //echo $rm_unit['id']; ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Delete Unit</a-->											
										<!-- Button trigger modal -->
										<?php //$unittt = $rm_unit['id']; ?>
										<button type="button" class="btn btn-danger unitModal" data-toggle="modal" data-target="#unitModal" value="<?php echo $unittt = $rm_unit['id']; ?> " disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="unitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="unitdelete">
														@csrf
														<input type="hidden" class="unit_value" name="unit_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>UNIT?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>									
								</tr>
								<?php
										$i++;
									}
									// echo "<pre>";
									// print_r($rm_data);
									// echo "</pre>";
								?>										
							</tbody>
						</table>
					</div>
					
					<!-- //#################### Plant Section ############################# -->
					<div class="table-responsive plant_info" id="plantsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-plant">Add New Plant</button-->
						<a href="{{ url('/add-plant') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Plant</button>
						</a>
						
						<h3>PLANT</h3>
						@if ($message = Session::get('message'))
							<!--div class="alert alert-info alert-block"  style="text-align:center;">
								<button type="button" class="close" data-dismiss="alert">×</button>	
								<strong></strong>
							</div-->
						@endif
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Unit Name</th>
									<th>Plant Code</th>
									<th>Plant Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_plant_table = pg_query($db, "SELECT * FROM rm_plants ORDER BY unit_id ASC");	
									$i = 1;
									while($rm_plant = pg_fetch_array($rm_plant_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>									
									<?php 
										$unit_id_plant = $rm_plant['unit_id'];					
										$unittt = pg_query($db, "SELECT unit_name FROM rm_units WHERE id = $unit_id_plant");
										while($unit_row = pg_fetch_array($unittt)){
											// echo "<pre>";
											// print_r($unit_row);
											// echo "</pre>";
									?>
										<td><?php echo $unit_row['unit_name']; ?></td>
									<?php	
										}
									?>
									</td>
									<td><?php echo $rm_plant['plant_code']; ?></td>
									<td><?php echo $rm_plant['plant_name']; ?></td>
									<td>
										<a href="plantedit/<?php echo $rm_plant['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger plantModal" data-toggle="modal" data-target="#plantModal" value="<?php echo $plantt = $rm_plant['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="plantModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="plantdelete">
														@csrf
														<input type="hidden" class="plant_value" name="plant_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>PLANT?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>	
							</tbody>								
						</table>
					</div>
					
					<!-- //############################### Plant Pincode Section ######################################## -->
					<div class="table-responsive plant_pincode_info" id="plantpinsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-plant">Add New Plant</button-->
						<a href="{{ url('/add-plant-pincode') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Plant Pincode</button>
						</a>
						
						<h3>PLANT PIN CODE</h3>
						@if ($message = Session::get('message'))
							<!--div class="alert alert-info alert-block"  style="text-align:center;">
								<button type="button" class="close" data-dismiss="alert">×</button>	
								<strong></strong>
							</div-->
						@endif
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Unit Name</th>
									<th>Plant Name</th>
									<th>Pincode</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_plant_pincode_table = pg_query($db, "SELECT * FROM rm_pincodes ORDER BY unit_id ASC");	
									$i = 1;
									while($rm_plant_pincode = pg_fetch_array($rm_plant_pincode_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<?php 
										$unit_id = $rm_plant_pincode['unit_id']; 
										$pin_unit = pg_query($db, "SELECT unit_name FROM rm_units WHERE id=$unit_id");
										while($pin_row_unit = pg_fetch_array($pin_unit)){
											
									?>																			
									<td><?php echo $pin_row_unit['unit_name']; ?></td>
									<?php
										}
									?>
									<?php 
										$plant_id = $rm_plant_pincode['plant_id']; 
										$pin_plant = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id=$plant_id");
										while($pin_row_plant = pg_fetch_array($pin_plant)){
											
									?>																			
									<td><?php echo $pin_row_plant['plant_name']; ?></td>
									<?php
										}
									?>									
									<td><?php echo $rm_plant_pincode['pin_code']; ?></td>
									<td>
										<a href="plant-pincodeedit/<?php echo $rm_plant_pincode['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger plantpinModal" data-toggle="modal" data-target="#plantpinModal" value="<?php echo $plant_pin = $rm_plant_pincode['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="plantpinModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="plant-pincodedelete">
														@csrf
														<input type="hidden" class="plantpin_value" name="plantpin_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>PLANT PINCODE?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>	
							</tbody>								
						</table>
					</div>
					
					<!-- //############################### Cascade Section ################################### -->
					<div class="table-responsive cascade_info" id="cascadesection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-cascade">Add New Cascade</button-->
						<a href="{{ url('/add-cascade') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Cascade</button>
						</a>
						<h3>CASCADE</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Unit Name</th>
									<th>Plant Name</th>
									<th>Cascade Code</th>
									<th>Cascade Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_cascade_table = pg_query($db, "SELECT * FROM rm_cascades ORDER BY unit_id ASC");	
									$i = 1;
									while($rm_cascade = pg_fetch_array($rm_cascade_table)){
										// echo "<pre>";
										// print_r($rm_cascade);
										// echo "</pre>";
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<?php 
										$cas_unit_id = $rm_cascade['unit_id'];
										$cass_unit = pg_query($db, "SELECT unit_name FROM rm_units WHERE id = $cas_unit_id");
										while($cas_unit_row = pg_fetch_array($cass_unit)){
											// echo "<pre>";
											// print_r($unit_row);
											// echo "</pre>";
									?>
										<td><?php echo $cas_unit_row['unit_name']; ?></td>
									<?php	
										}
									 
										$cas_plant_id = $rm_cascade['plant_id'];
										$cass_plant = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id = $cas_plant_id");
										while($cass_plant_row = pg_fetch_array($cass_plant)){
											// echo "<pre>";
											// print_r($unit_row);
											// echo "</pre>";
									?>
										<td><?php echo $cass_plant_row['plant_name']; ?></td>
									<?php	
										}
									?>
									<td><?php echo $rm_cascade['cascade_code']; ?></td>	
									<td><?php echo $rm_cascade['cascade_name']; ?></td>
									<td>
										<a href="cascadeedit/<?php echo $rm_cascade['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger cascadeModal" data-toggle="modal" data-target="#cascadeModal" value="<?php echo $cascade = $rm_cascade['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="cascadeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="cascadedelete">
														@csrf
														<input type="hidden" class="cascade_value" name="cascade_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>CASCADE?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>	
								</tr>
								<?php
										$i++;
									}
								?>										
							</tbody>
						</table>
					</div>					
					<!-- //############################### Machine Section ################################### -->
					<div class="table-responsive machine_info" id="machinesection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-machine">Add New Machine</button-->
						<a href="{{ url('/add-machine') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Machine</button>
						</a>
						<h3>MACHINE</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Unit Name</th>
									<th>Plant Name</th>
									<th>Cascade Name</th>
									<th>Machine Type</th>
									<th>Machine Name</th>
									<th>Breakdown Time</th>
									<!--th>BCT/CLD</th-->
									<th>Machine Reg No</th>
									<th>IMEI No</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_machine_table = pg_query($db, "SELECT * FROM rm_machines ORDER BY unit_id ASC");	
									$i = 1;
									while($rm_machine = pg_fetch_array($rm_machine_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<?php 
										$rm_machine_unit = $rm_machine['unit_id'];
										$machine_unit = pg_query($db, "SELECT unit_name FROM rm_units WHERE id = $rm_machine_unit");
										while($machine_unit_row = pg_fetch_array($machine_unit)){
											// echo "<pre>";
											// print_r($unit_row);
											// echo "</pre>";
									?>
										<td><?php echo $machine_unit_row['unit_name']; ?></td>
									<?php	
										}
									?>
									<?php 
										$rm_machine_plant = $rm_machine['plant_id'];
										$machine_plant = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id = $rm_machine_plant ");
										while($machine_plant_row = pg_fetch_array($machine_plant)){
											// echo "<pre>";
											// print_r($unit_row);
											// echo "</pre>";
									?>
										<td><?php echo $machine_plant_row['plant_name']; ?></td>
									<?php	
										}
									?>
									<?php 
										$rm_machine_cascade = $rm_machine['cascade_id'];
										$machine_cascade = pg_query($db, "SELECT cascade_name FROM rm_cascades WHERE id = $rm_machine_cascade");
										while($machine_cascade_row = pg_fetch_array($machine_cascade)){
											// echo "<pre>";
											// print_r($unit_row);
											// echo "</pre>";
									?>
										<td><?php echo $machine_cascade_row['cascade_name']; ?></td>
									<?php	
										}
									?>
									<?php 
										$rm_machine_type = $rm_machine['machine_type_id'];
										$machine_type_info = pg_query($db, "SELECT machine_type_name FROM rm_machinetypes WHERE id = $rm_machine_type");
										while($machine_type_row = pg_fetch_array($machine_type_info)){
											// echo "<pre>";
											// print_r($unit_row);
											// echo "</pre>";
									?>
										<td><?php echo $machine_type_row['machine_type_name']; ?></td>
									<?php	
										}
									?>
									<td><?php echo $rm_machine['machine_name']; ?></td>
									<td><?php echo $rm_machine['breakdown_limit']; ?></td>
									<!--td><?php // echo $rm_machine['bct_cld']; ?></td-->
									<td><?php echo $rm_machine['machine_regno']; ?></td>
									<td><?php echo $rm_machine['imei']; ?></td>
									<td><a href="machineedit/<?php echo $rm_machine['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger machineModal" data-toggle="modal" data-target="#machineModal" value="<?php echo $machine = $rm_machine['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
									</button>
										
										<!-- Modal -->
										<div class="modal fade" id="machineModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="machinedelete">
														@csrf
														<input type="hidden" class="machine_value" name="machine_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>MACHINE?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>									
								</tr>
								<?php
										$i++;
									}
								?>										
							</tbody>
						</table>
					</div>
					
					<!-- //############################### Product Section ###################################### -->
					<div class="table-responsive product_info" id="productsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-product') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Product</button>
						</a>
						<h3>PRODUCTS</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_product_table = pg_query($db, "SELECT * FROM rm_products ORDER BY id ASC");	
									$i = 1;
									while($rm_product = pg_fetch_array($rm_product_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $rm_product['product_code']; ?></td>	
									<td><?php echo $rm_product['product_name']; ?></td>
									<td><a href="productedit/<?php echo $rm_product['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger productModal" data-toggle="modal" data-target="#productModal" value="<?php echo $product = $rm_product['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="productdelete">
														@csrf
														<input type="hidden" class="product_value" name="product_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>PRODUCT?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					
					<!-- //############################### Mixer BCT Section ###################################### -->
					<div class="table-responsive product_mixer_bct_info" id="mixbctsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-product-mixer-bct') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square fa fa-plus-square">&nbsp; Add New Product Mixer BCT</button>
						</a>
						<h3>PRODUCTS MIXER BCT</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Machine Name</th>
									<th>Product Name</th>
									<th>BCT</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_product_mix_table = pg_query($db, "SELECT * FROM rm_mixerbct ORDER BY id ASC");	
									$i = 1;
									while($rm_product_bct = pg_fetch_array($rm_product_mix_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>									
									<?php 
										$mixer_machine_id = $rm_product_bct['machine_id']; 
										$mixer_machine_query = pg_query($db, "SELECT machine_name FROM rm_machines WHERE id=$mixer_machine_id");
										while($map_product_row = pg_fetch_array($mixer_machine_query)){
									?>
										<td><?php echo $map_product_row['machine_name']; ?></td>	
									<?php
										}
									?>										
									<?php 
										$mixer_product_id = $rm_product_bct['product_id']; 
										$mixer_product_query = pg_query($db, "SELECT product_name FROM rm_products WHERE id=$mixer_product_id");
										while($mixer_product_row = pg_fetch_array($mixer_product_query)){
									?>
										<td><?php echo $mixer_product_row['product_name']; ?></td>	
									<?php
										}
									?>	
									<td><?php echo $rm_product_bct['bct']; ?></td>
									<td><a href="product-mixer-bctedit/<?php echo $rm_product_bct['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger productbctModal" data-toggle="modal" data-target="#productbctModal" value="<?php echo $product = $rm_product_bct['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="productbctModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="product-mixer-bctdelete">
														@csrf
														<input type="hidden" class="product_bct_value" name="product_bct_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>PRODUCT BCT?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					<!-- //################### Product Plant Mapping Info Section ######################## -->
					<div class="table-responsive product_plant_mapping_info" id="plantmapsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-product-plant-mapping') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Product Mapping</button>
						</a>
						<h3>PRODUCTS PLANT MAPPING</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Plant Name</th>
									<th>Product Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_product_map_table = pg_query($db, "SELECT * FROM rm_plantproducts ORDER BY id ASC");	
									$i = 1;
									while($rm_product_map = pg_fetch_array($rm_product_map_table)){										
								?>
								<tr>
									<td><?php echo $i; ?></td>
									
									<?php 
										$map_plant_id = $rm_product_map['plant_id']; 
										$map_plant_query = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id=$map_plant_id");
										while($map_plant_row = pg_fetch_array($map_plant_query)){
									?>
										<td><?php echo $map_plant_row['plant_name']; ?></td>	
									<?php
										}
									?>
									<?php 
										$map_plant_id = $rm_product_map['product_id']; 
										$map_plant_query = pg_query($db, "SELECT product_name FROM rm_products WHERE id=$map_plant_id");
										while($map_product_row = pg_fetch_array($map_plant_query)){
									?>
										<td><?php echo $map_product_row['product_name']; ?></td>	
									<?php
										}
									?>									
									<td><a href="product-plant-mappingedit/<?php echo $rm_product_map['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger productmappingModal" data-toggle="modal" data-target="#productmappingModal" value="<?php echo $rm_product_map = $rm_product_map['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="productmappingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="product-plant-mappingdelete">
														@csrf
														<input type="hidden" class="plant_map_value" name="plant_map_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>PRODUCT PLANT MAPPING?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					
					<!-- //############################### SKU Section ################################## -->
					<div class="table-responsive sku_info" id="skusection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-sku') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New SKU</button>
						</a>
						<h3>SKU</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Product Name</th>
									<th>SKU Code</th>
									<th>SKU Name</th>
									<!--th>RPM Min</th>
									<th>RPM Max</th>
									<th>RPM Default</th-->
									<!--th>1st RPM</th>
									<th>2nd RPM</th>
									<th>3rd RPM</th>
									<th>Default RPM</th-->
									<th>Pack Size</th>
									<th>Pack Unit</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_skus_table = pg_query($db, "SELECT * FROM rm_skus ORDER BY product_id ASC");	
									$i = 1;
									while($rm_skus_row = pg_fetch_array($rm_skus_table)){
										// echo "<pre>";
										// print_r($rm_skus_row);
										// echo "</pre>";
								?>
								<tr>
									<td><?php echo $i; ?></td>
										<?php 
											$pro_id = $rm_skus_row['product_id'];
											$product_select = pg_query($db, "SELECT * FROM rm_products WHERE id= $pro_id");
											while($prod_row = pg_fetch_array($product_select)){
										?>
											<td><?php echo $prod_row['product_name']; ?></td>
										<?php
											}
											// echo $rm_skus_row['product_id']; 
										?>											
									<td><?php echo $rm_skus_row['sku_code']; ?></td>
									<td><?php echo $rm_skus_row['sku_name']; ?></td>
									<!--td><?php //echo $rm_skus_row['rpm_min']; ?></td>
									<td><?php //echo $rm_skus_row['rpm_max']; ?></td>
									<td><?php //echo $rm_skus_row['rpm_final']; ?></td>
									<td><?php //echo $rm_skus_row['rpm_default']; ?></td-->
									<td><?php echo $rm_skus_row['pack_size']; ?></td>										
									<td><?php echo $rm_skus_row['pack_size_unit']; ?></td>
									<td><a href="skuedit/<?php echo $rm_skus_row['id']; ?>"class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger skuModal" data-toggle="modal" data-target="#skuModal" value="<?php echo $product = $rm_skus_row['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="skuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="skudelete">
														@csrf
														<input type="hidden" class="sku_value" name="sku_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>SKU?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					<!-- //############################### MACHINE PRODUCT SKUS Section ################################## -->
					<div class="table-responsive machineproductskusection_info" id="machineproductskusection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/addmachine-product-sku') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Machine Product SKU</button>
						</a>
						<h3>Machine Product SKU</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Plant Name</th>
									<th>Machine Name</th>
									<th>Product Name</th>
									<th>SKU Name</th>
									<th>RPM</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_machine_product_table = pg_query($db, "SELECT * FROM rm_machine_sku_rpm ORDER BY sku_id ASC");	
									$i = 1;
									while($rm_machine_product_skus_row = pg_fetch_array($rm_machine_product_table)){
										// echo "<pre>";
										// print_r($rm_machine_product_skus_row['plant_id']);
										// echo "</pre>";
										$productplantid = $rm_machine_product_skus_row['plant_id'];
										$productmachineid = $rm_machine_product_skus_row['machine_id'];
										$productproid = $rm_machine_product_skus_row['product_id']; 
										$productskuid = $rm_machine_product_skus_row['sku_id']; 
										$productrpm = $rm_machine_product_skus_row['rpm'];
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td>
										<?php 
											$productid = pg_query($db, "SELECT plant_name FROM rm_plants WHERE id=$productplantid");
											$plantidrow = pg_fetch_array($productid);
											echo $plantidrow['plant_name'];
										?>
									</td>
									<td>
										<?php 
											$machineidd = pg_query($db, "SELECT machine_name FROM rm_machines WHERE id=$productmachineid");
											$machineidrow = pg_fetch_array($machineidd);
											echo $machineidrow['machine_name'];
										?>
									</td>
									<td>
										<?php 
											$productidd = pg_query($db, "SELECT product_name FROM rm_products WHERE id=$productproid");
											$productidrow = pg_fetch_array($productidd);
											echo $productidrow['product_name'];
										?>
									</td>
									<td>
										<?php 
											$skuidd = pg_query($db, "SELECT sku_name FROM rm_skus WHERE id=$productskuid");
											$skuidrow = pg_fetch_array($skuidd);
											echo $skuidrow['sku_name'];
										?>
									</td>								
									<td><?php echo $productrpm; ?></td>								
									<td><a href="editmachine-product-sku/<?php echo $rm_machine_product_skus_row['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger skuModal" data-toggle="modal" data-target="#skuModal" value="<?php echo 1 ?> "  disabled><i class="fa fa-trash icon-large"></i></button>
										
										<!-- Modal -->
										<div class="modal fade" id="skuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="skudelete">
														@csrf
														<input type="hidden" class="sku_value" name="sku_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>SKU?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					
					<!-- //############################### SKU PACKING UNIT ################################## -->
					<div class="table-responsive sku_units" id="skupackunitsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-sku-unit') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add SKU Packing Unit</button>
						</a>
						<h3>SKU PACKING UNIT</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Unit Code</th>
									<th>Unit Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>								
								<?php 	
									$i = 1;
									$sku_packing_unit = pg_query($db, "SELECT * FROM rm_skupackingunits");
									while($sku_packing_unit_row = pg_fetch_array($sku_packing_unit)){
								?>
								<tr>	
									<td><?php echo $i; ?></td>
									<td><?php echo $sku_packing_unit_row['unit_code']; ?></td>
									<td><?php echo $sku_packing_unit_row['unit_name']; ?></td>
									<td><a href="skuunitedit/<?php echo $sku_packing_unit_row['id']; ?>"class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger skuunitModal" data-toggle="modal" data-target="#skuunitModal" value="<?php echo $sku_pack_unit = $sku_packing_unit_row['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="skuunitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="skuunitdelete">
														@csrf
														<input type="hidden" class="skuunit_value" name="skuunit_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>SKU Packing Unit?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					<!-- //############################### SKU PACKING TYPE ################################## -->
					<div class="table-responsive sku_types" id="skupacktypesection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-sku-types') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add  SKU Packing Types</button>
						</a>
						<h3>SKU PACKING TYPES</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Type Code</th>
									<th>Type Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>	
								<?php 
									$i = 1;
									$sku_packing_type = pg_query($db, "SELECT * FROM rm_skupackingtypes");
									while($sku_packing_type_row = pg_fetch_array($sku_packing_type)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $sku_packing_type_row['type_code']; ?></td>
									<td><?php echo $sku_packing_type_row['type_name']; ?></td>
									<td><a href="skutypesedit/<?php echo $sku_packing_type_row['id']; ?>"class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger skutypeModal" data-toggle="modal" data-target="#skutypeModal" value="<?php echo $sku_pack_type = $sku_packing_type_row['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="skutypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="skutypesdelete">
														@csrf
														<input type="hidden" class="skutype_value" name="skutype_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>SKU Packing Type?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					
					<!-- //############################### Breakdown Section ################################## -->
					<div class="table-responsive breakdown_reason" id="breakdownsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-breakreason') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Breakdown Reason</button>
						</a>
						<h3>BREAKDOWN REASON</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Breakdown Code</th>
									<th>Breakdown Icon</th>
									<th>Breakdown Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>								
								<?php
									// echo url()->current();
									$rm_breakdown_table = pg_query($db, "SELECT * FROM rm_breakdownreasons ORDER BY breakdown_code ASC");	
									$i = 1;
									while($rm_breakdown = pg_fetch_array($rm_breakdown_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $rm_breakdown['breakdown_code']; ?></td>
									<?php //echo public_path(); ?>
									<td><img src="<?php echo url('/images').'/'.$rm_breakdown['breakdown_icon']; ?>"></td>	
									<td><?php echo $rm_breakdown['breakdown_reason']; ?></td>
									<td><a href="breakreasonedit/<?php echo $rm_breakdown['id']; ?>"class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger breakdownModal" data-toggle="modal" data-target="#breakdownModal" value="<?php echo $breakdown = $rm_breakdown['id']; ?>" disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="breakdownModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="breakreasondelete">
														@csrf
														<input type="hidden" class="breakdown_value" name="breakdown_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>BREAKDOWN REASON?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>
					
					<!-- //##################### Breakdown Subreason Section ########################### -->
					<div class="table-responsive breakdown_subreason" id="breakdownsubreasonsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-product">Add New Product</button-->
						<a href="{{ url('/add-breaksubreason') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New Breakdown Subreason</button>
						</a>
						<h3>BREAKDOWN SUBREASON</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Sub-Reason Code</th>
									<th>Sub-Reason Icon</th>
									<th>Breakdown Reason</th>
									<th>Breakdown Sub-Reason</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_subbreakdown_table = pg_query($db, "SELECT * FROM rm_breakdownsubreasons ORDER BY subbreakdown_code ASC");	
									$i = 1;
									while($subbreakdown = pg_fetch_array($rm_subbreakdown_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $subbreakdown['subbreakdown_code']; ?></td>
									<td><img src="<?php echo url('/images').'/'.$subbreakdown['subbreakicon']; ?>"></td>	
									<?php 
										$breakdown_id = $subbreakdown['breakdown_id'];
										$rm_subbreak = pg_query($db, "SELECT * FROM rm_breakdownreasons WHERE id=$breakdown_id");
										while($breakdownreasons = pg_fetch_array($rm_subbreak)){
									?>	
										<td>
											<?php echo $breakdownreasons['breakdown_reason']; ?>
										</td>	
									<?php
										}
									?>	
									<td><?php echo $subbreakdown['subbreakdown_reason']; ?></td>
									<td><a href="breaksubreasonedit/<?php echo $subbreakdown['id']; ?>"class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger subbreakdownModal" data-toggle="modal" data-target="#subbreakdownModal" value="<?php echo $subbreakdownreason = $subbreakdown['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
										</button>
										
										<!-- Modal -->
										<div class="modal fade" id="subbreakdownModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="breaksubreasondelete">
														@csrf
														<input type="hidden" class="subbreakdown_value" name="subbreakdown_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>SUB-BREAKDOWN REASON?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
										$i++;
									}
								?>									
							</tbody>
						</table>
					</div>					
					
					<!-- //############################### Shift Section ###################################### -->
					<div class="table-responsive shift_info" id="shiftsection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-machine">Add New Machine</button-->
						<a href="{{ url('/add-shift') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square" disabled>&nbsp; Add New Shift</button>
						</a>
						<h3>SHIFT</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Shift Code</th>
									<th>Shift Name</th>
									<th>Shift Starts At</th>
									<th>Shift Ends At</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_shift_table = pg_query($db, "SELECT * FROM rm_shifts");	
									$i = 1;
									while($rm_shift = pg_fetch_array($rm_shift_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $rm_shift['shift_code']; ?></td>
									<td><?php echo $rm_shift['shift_name']; ?></td>
									<td><?php echo $rm_shift['shift_start_at']; ?></td>
									<td><?php echo $rm_shift['shift_end_at']; ?></td>
									<td><a href="shiftedit/<?php echo $rm_shift['id']; ?>" class="btn btn-info fa fa-edit icon-large disabled"> </a> | <button type="button" class="btn btn-danger shiftModal disabled" data-toggle="modal" data-target="#shiftModal" value="<?php echo $shift = $rm_shift['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
									</button>
										
										<!-- Modal -->
										<div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display:none;">
										<!--div class="modal fade" id="shiftModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display:none;"-->
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;" >	
													<form method="post" action="shiftdelete">
													
														@csrf
														<input type="hidden" class="shift_value" name="shift_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>SHIFT?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>									
								</tr>
								<?php
										$i++;
									}
								?>										
							</tbody>
						</table>
					</div>
					
					
					<!-- //####################### PCU Status Section ############################ -->
					<div class="table-responsive pcu_status_info" id="pcustatussection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-machine">Add New Machine</button-->						
						<h3>PCU Health Monitor</h3>
						<!--form method="post" action="pcudatainfo"-->
						<form method="post" action="javascript:void(0)">
							<!--form method="post" action="pcudatainfo"-->
							<div class="row">							
								@csrf
								<div class="col-sm-3">
									<label>Unit:</label>&nbsp;&nbsp;
									
									<select class="form-control pcu_unit" name="unit">										
									<?php
										$pcu_unit = pg_query($db, "SELECT * FROM rm_units ORDER BY id ASC");
									?>
										<option value="0" selected="selected">All</option>
									<?php
										while($pcu_unit_row = pg_fetch_array($pcu_unit)){
									?>
										<option value="<?php echo $pcu_unit_row['id']; ?>"><?php echo $pcu_unit_row['unit_name']; ?></option>		
									<?php
										}
									?>
									</select>
									<input type="hidden" class="pcuunitid" name="unit">
								</div>
								<div class="col-sm-3">		
									<label>Plant:</label>&nbsp;&nbsp;
									<select class="form-control pcu_plant" > 
										<option value="0" selected="selected">All</option>	
										<?php
											$pcu_plant = pg_query($db, "SELECT * FROM rm_plants ORDER BY unit_id ASC");
											while($pcu_plant_row = pg_fetch_array($pcu_plant)){
										?>
											<option value="<?php echo $pcu_plant_row['id']; ?>"><?php echo $pcu_plant_row['plant_name']; ?></option>		
										<?php
											}
										?>
									 </select>
									 <input type="hidden" class="pcuplantid" name="plantid">
								</div>							
								<div class="col-sm-3">
									<label>Status:</label>&nbsp;&nbsp;
									<select class="form-control pcu_status" name="status">
										<option value="0">All</option>
										<option value="1">Connected</option>
										<option value="2">Disconnected</option>
									 </select>
								</div>
								<div class="col-sm-3">
									<label></label><br/>
									<input type="submit" name="Display" value="Display Data" class="pcudatainfo form-control" style="margin-top: 6%; background-color: #4CAF50; color:white;">	
								</div>
							</div>
						</form>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>ID</th>
									<th>IMEI No</th>
									<th>Machine No</th>
									<th>PKT CNT</th>
									<!--th><img src="<?php // echo $img_location."satellite.png"; ?>"></th-->
									<th><img src="<?php echo $img_location."signal.ico"; ?>"></th>
									<th><img src="<?php echo $img_location."batt_conn.ico"; ?>"></th>
									<th><img src="<?php echo $img_location."batt.png"; ?>"></th>
									<th>Device Volt</th>
									<th>Main Volt</th>
									<th>Connectivity</th>
								</tr>
							</thead>
							<tbody class="live_health_info">
								<?php
									$rm_shift_table = pg_query($db, "SELECT * FROM rm_shifts");	
									$i = 1;
									while($rm_shift = pg_fetch_array($rm_shift_table)){
								?>
								<!--tr>
									<td><?php //echo $i; ?></td>
									<td><?php //echo  '1221'; ?></td>
									<td><?php //echo '869247046065907'; ?></td>
									<td><?php //echo 'SRF_BOON' ?></td>
									<td><?php //echo '8'; ?></td>					
									<td><?php //echo '0'; ?></td>					
									<td><?php //echo '21' ?></td>					
									<td></td>					
									<td><img src="<?php //echo $img_location."batt_2.ico"; ?>" style="width:50%"></td>				<td><?php //echo '4'; ?></td>					
									<td><?php //echo '24'; ?></td>
									<td class="data_connection"><img src="<?php //echo $img_location."green.png"; ?>" class="data_connection" style="width:15%"></td>				
								</tr-->
								<?php
										$i++;
									}
								?>										
							</tbody>
						</table>
					</div>
					<!-- //############################### User Section ######################################## -->
					<div class="table-responsive users_info" id="usersection" style="display:none">
						<!--button type="button" class="btn btn-info btn-rounded m-t-10 float-right add_button" data-toggle="modal" data-target="#add-user">Add New User</button-->
						<a href="{{ url('/add-user') }}">
							<button type="button" class="btn btn-info btn-rounded m-t-10 float-right fa fa-plus-square">&nbsp; Add New User</button>
						</a>
						<h3>USERS</h3>
						<table id="demo-foo-addrow"
							class="table table-bordered m-t-30 table-hover contact-list" data-paging="true"
							data-paging-size="7">
							<thead>
								<tr class="admin_display_bar">
									<th>S. No</th>
									<th>Login Name</th>
									<th>First Name</th>
									<th>User Type</th>
									<th>Email</th>
									<th>Password</th>
									<th>Mobile No</th>
									<!--th>Logged</th-->
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$rm_user_table = pg_query($db, "SELECT * FROM rm_users ORDER BY user_type_id ASC");	
									$i = 1;
									while($rm_user = pg_fetch_array($rm_user_table)){
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $rm_user['login_name']; ?></td>
									<td><?php echo $rm_user['first_name']; ?></td>
									
									<?php 
										$user_type_id = $rm_user['user_type_id']; 
										$user_type_query = pg_query($db, "SELECT type_name FROM rm_usertypes WHERE id=$user_type_id");
										while($user_type_row = pg_fetch_array($user_type_query)){
									?>
											<td><?php echo $user_type_row['type_name']; ?></td>
									<?php
										}
										if($rm_user['email_id'] == ''){
									?>
											<td>NA</td>
									<?php }else{ ?>									
											<td><?php echo $rm_user['email_id']; ?></td>
									<?php } 
										if($rm_user['pwd'] == ''){
									?>
											<td>NA</td>
									<?php }else{ ?>	
									<td><?php echo $rm_user['pwd']; ?></td>
									<?php } 
										if($rm_user['mobile_no'] == 0){
									?>
										<td>NA</td>
									<?php }else{ ?>	
									<td><?php echo $rm_user['mobile_no']; ?></td>
									<?php } ?>	
									<!--td>
									
									<!--/td-->	
									<td>	
										<?php
											// echo $user_type_id;
											if($user_type_id == 5 || $user_type_id == 1){
										?>	
												<button type="button" class="btn btn-danger userModal22" data-toggle="modal" data-target="#userRole" value="<?php echo $user = $rm_user['id']; ?>" style="visibility:hidden;"><i class="fa fa-user icon-large"></i></button> 
										<?php
											}else{
										?>
										<form method="post" action="checked">
											@csrf
											<input type="hidden" name="userrrr" value="<?php echo $user = $rm_user['id']; ?>">
											<!--button type="button" class="btn btn-danger userModal22" data-toggle="modal" data-target="#userRole" value="<?php // echo $user = $rm_user['id']; ?>"><i class="fa fa-user icon-large"></i></button-->
											<input type="submit" class="btn btn-success" name="submit" value="R"/>&nbsp;|
									
										<?php } ?>
											<!--a href="userrole/<?php //} ?>" type="button" style="background:#736f67 !important; color:white;" class="btn btn-default fa fa-user icon-large userroleid"></a--><!--a href="userrole/<?php // echo $rm_user['id']; ?>"class="btn btn-info fa fa-user icon-large"> </a-->  <a href="useredit/<?php echo $rm_user['id']; ?>" class="btn btn-info fa fa-edit icon-large"> </a> | <button type="button" class="btn btn-danger userModal" data-toggle="modal" data-target="#userModal" value="<?php echo $user = $rm_user['id']; ?> "  disabled><i class="fa fa-trash icon-large"></i> 
											</button>
										</form>
										<!-- Modal -->
										<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">CONFIRMATION DIALOGUE BOX</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												  </div>
												  <div class="modal-body" style="text-align:center;">	
													<form method="post" action="userdelete">
														@csrf
														<input type="hidden" class="user_value" name="user_value" value="">												
														<h3>Do you really want to <p>DELETE this <strong>USER?</strong></p></h3>
														<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
														<input type="submit" class="btn btn-danger" value="Delete">	
													</form>
												  </div>
												  <div class="modal-footer">
													
												  </div>
												</div>
											</div>
										</div>
									</td>	
								</tr>
								<?php
										$i++;
									}
								?>										
							</tbody>
						</table>
					</div>	
					
					<!-- //########################################################################## -->
				</div>
			</div>
		</div>
	</div>
        <!-- Trigger the modal with a button -->

		<!-- Modal -->
		<!--div id="userRole" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<!--div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">User Role</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>					
					</div>
					<div class="modal-body">	
						<div class="userrolehere"></div>
						<form method="post" action="userrolesubmit">
							@csrf
							<!--p>User Role</p-->
							<!--input type="text" class="userroleinfo" name="userid" value="">	
								
							<?php
								// if (isset($_GET['submit'])) {
									// die($_GET['userrrr']);
									// echo $_GET['userrrr'];
								// }
								// $userrole = pg_query($db, "SELECT * FROM rm_userrights WHERE user_id = 33");
								// while($userrolerow = pg_fetch_array($userrole)){
									// echo "<pre>";
									// $userrow = $userrolerow['plant_id'];
									// print_r (explode(",",$userrow));
									// echo "</pre>";
								// }
								// $unitrole = pg_query($db, "SELECT * FROM rm_units");
								// $i = 1;
								// $j = 1;
								// while($unitrow = pg_fetch_array($unitrole)){
									// echo "<pre>";
									// print_r($unitrow);
									// echo "</pre>";
							?>	
								<hr/>
								<div class="row">
									<div class="col-sm-4">
										<h4>Select Unit</h4>	
										<input type="checkbox" id="unitbox" class="unitbox<?php // echo $i; ?> unitts" value="<?php //echo $unitrow['id']; ?>" name="unitrow[]" disabled>&nbsp; <?php //echo $unitrow['unit_name']; ?>
									</div>
									<div class="col-sm-8">
										<h4>Select Plant</h4>
										<?php
											// $unitrowid = $unitrow['id'];
											// $plantrole = pg_query($db, "SELECT * FROM rm_plants WHERE unit_id = $unitrowid")
										?>	
										<input type="hidden" name="unitttt" class="plantunitid" value="<?php //echo $unitrowid; ?>">
										<?php
											// while($plantrow = pg_fetch_array($plantrole)){
												// echo "<pre>";
												// print_r($unitrow);
												// echo "</pre>";
										?>	
											// <input type="checkbox" id="plantbox" class="plantbox<?php // echo $j; ?> plantts" value="<?php // echo $plantrow['id']; ?>" name="plantrow[]">&nbsp; <?php // echo $plantrow['plant_name']; ?><br/>
										<?php
												// $j++;
											// }
										?>
									</div>								
								</div>								
							<?php
									// $i++;
								// }
							?>	
							<br/>
							<input class="btn btn-success submitrole" type="submit" name="submit" value="Submit">
						</form>					
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div-->     
		
		<!-- ################################### FORM STARTS ######################################## -->
		<!-- Row -->
		<form action="unitupdate" method="post" class="text-center edit_current_unit" style="color: #757575; display:none;">
			@csrf
			<p>Unit Update Section</p>

			<!-- Name -->
			<div class="md-form mt-3">
				<label for="unitcode">Unit Code:</label><br/>
				<input type="hidden" name="unit_id" class="form-control" value="{{ Session::get('unit_id') }}" id="unitid">
				<input type="text" name="unit_code" class="form-control" value="{{ Session::get('unit_code') }}" id="unitcode">	
			</div>

			<!-- E-mai -->
			<div class="md-form">
				<label for="unitname">Unit Name:</label><br/>
				<input type="text" class="form-control" name="unit_name" value="{{ Session::get('unit_name') }}" id="unitname">
			</div>

			<!-- Sign in button -->
			<input type="submit" class="form-control" value="UPDATE UNIT">
		</form>
						
		<!--/div> <!-- Card end 2-->
	</div><br/><br/><br/>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
@extends('footer')
