<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Controller@getData');

Route::get('/', function() {
	if(empty(Session::get('id'))){
		return view('login');
	}
	//echo $unit_code;
	//dd('unit_code');
	
	
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	// echo $host;
	// echo $dbname;
	// echo $user;
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	
	
	if(Session::get('unit_id')){
		//dd(Session::get('unit_id'));
		return view('welcome');
	}
	//dd(Session::get('db'));
	$user_id = Session::get('id');	
	$hod_query = pg_query($db, "SELECT * FROM rm_users WHERE id=$user_id");
	while($hod_row = pg_fetch_array($hod_query)){
		$hod_id = $hod_row['id'];
		$hod_firstname = $hod_row['first_name'];
		$hod_lastname = $hod_row['last_name'];
		$hod_email = $hod_row['email_id'];
		$hod_mobileno = $hod_row['mobile_no'];		
	}
    return view('welcome')->with(['user_id'=>$hod_id,'firstname'=>$hod_firstname, 'lastname'=>$hod_lastname, 'email'=>$hod_email, 'mobileno'=>$hod_mobileno]);
});


// Delete this route start

// Delete this route end

Route::get('/user_register', function () {
	$user_id = Session::get('id');
	
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	// echo $host;
	// echo $dbname;
	// echo $user;
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	
	
	$hod_query = pg_query($db, "SELECT * FROM users WHERE id=$user_id");
	while($hod_row = pg_fetch_array($hod_query)){
		$hod_id = $hod_row['id'];
		$hod_firstname = $hod_row['firstname'];
		$hod_lastname = $hod_row['lastname'];
		$hod_email = $hod_row['email'];
		$hod_mobileno = $hod_row['mobileno'];
		$hod_unit_id = $hod_row['unit_id'];
		$hod_plant_id = $hod_row['plant_id'];
		$hod_shift_id = $hod_row['shift_id'];
		$hod_machine_id = $hod_row['machine_id'];
		$hod_designation_id = $hod_row['designation_id'];				
	}
    return view('welcome')->with(['user_id'=>$hod_id,'firstname'=>$hod_firstname, 'lastname'=>$hod_lastname, 'email'=>$hod_email, 'mobileno'=>$hod_mobileno, 'unit_id'=>$hod_unit_id, 'plant_id'=>$hod_plant_id, 'hod_shift_id'=>$hod_shift_id, 'machine_id'=>$hod_machine_id, 'designation_id'=>$hod_designation_id]);
});

Route::get('/manager', function () {
    return view('manager');
});

Route::get('/operatorlogin', function () {
    return view('/operatorlogin');
});

Route::get('/pcuhealthlive', 'Controller@pcuhealthlive');

// Route::get('/operatorcode', function(){
	// $status = "Incorrect Operator name / ऑपरेटर का नाम गलत है";
	// return view('operatorcode')->withErrors($status);	
// }
Route::post('/operatorcode', function(Request $req){
	//dd($req);
	$host = "voyagereon.com";
	$dbname="eon_rmchemicals";
	// $host = "localhost";
	// $dbname="db_rmchemicals";	
	$user="postgres";
	$pass="eon@c180";		
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");	
	
	Session::put('host',$host);
	Session::put('dbname',$dbname);
	Session::put('user',$user);
	Session::put('pass',$pass);
	
	$machine_id = strtoupper($req->machine_id);
	$machine_regno = $machine_id;
	$plant_pincode = $req->pin_code;	
	//dd($plant_pincode);
	// $loggedarray = array();
	$machine_query2 = pg_query($db, "SELECT * FROM rm_machines WHERE machine_regno= '$machine_regno' AND loggedin = 1");
	// echo "<pre>";
	// print_r($machine_query2);
	// echo "</pre>";
	if(pg_num_rows($machine_query2)>0){
		while($machinerow = pg_fetch_array($machine_query2)){
			// echo "<pre>";
			// print_r($machinerow['id']);
			// echo "<br/>";
			// print_r($machinerow['machine_name']);
			// echo "</pre>";
			$machineid = $machinerow['id'];
			$machinename = $machinerow['machine_name'];
			Session::put('loggedmachineid', $machineid);
			Session::put('loggedmachinename', $machinename);
			$operatorquery = pg_query($db, "SELECT operator_id FROM rm_oplogins WHERE machine_id = $machineid AND loggedin = '1'");
			while($operatorrow = pg_fetch_array($operatorquery)){
				// echo "<pre>";
				// print_r($operatorrow['operator_id']);
				// echo "</pre>";
				$opid = $operatorrow['operator_id'];
				Session::put('loggedopid', $opid);
				$operatorsku = pg_query($db, "SELECT id FROM rm_opskudata WHERE operator_id=$opid AND sku_end_at is NULL");
				echo "<pre>";
				$operatorskurow = pg_fetch_array($operatorsku);
				print_r($operatorskurow['id']);
				echo "</pre>";
				$operatorskuid = $operatorskurow['id'];
				Session::put('operatorskuid', $operatorskuid);
				$username = pg_query($db, "SELECT first_name FROM rm_users WHERE id=$opid");
				while($userrow = pg_fetch_array($username)){
					// echo "<pre>";
					// print_r($userrow['first_name']);
					// echo "</pre>";
					$loggeduser = $userrow['first_name'];
					Session::put('loggedusername', $loggeduser);
				}
			}
		}
		// echo Session::get('loggedmachineid');
		// echo Session::get('loggedmachinename');
		// echo "<br/>";
		// echo Session::get('loggedopid');
		// echo "<br/>";
		// echo Session::get('loggedusername');
		// dd();
		return redirect('operatorlogin')->withErrors('Machine already has been loggedin');		
	}
	
	Session::put('machine_regno', $machine_regno);
	Session::put('plant_pincode', $plant_pincode);	
	
	$machine_query = pg_query($db, "SELECT * FROM rm_machines WHERE machine_regno= '$machine_regno'");
	if(pg_num_rows($machine_query)<=0){
		$oplogerror = "Invalid Machine Code!";
		return Redirect::back()->withErrors($oplogerror);		
	}
		
	// dd($machine_query);	
	while($machine_row = pg_fetch_array($machine_query)){		
		$plant_id = $machine_row['plant_id'];
		// dd($plant_id);
		$machine_id = $machine_row['id'];
		$breakdown_limit = $machine_row['breakdown_limit'];
		
		// dd($machine_id);		
		// echo "<pre>";
		// print_r($machine_row);
		// echo "</pre>";	
		// dd();		
		// dd(Session::get('plant_pincode'));
		$pincode_query = pg_query($db, "SELECT * FROM rm_pincodes WHERE plant_id = $plant_id");
		while($pincode_row = pg_fetch_array($pincode_query)){
			// echo "<pre>";
			// print_r($pincode_row['pin_code']);
			// echo "</pre>";
			// dd($pincode_row['pin_code']);
			$pin = $pincode_row['pin_code'];
			// echo $plant_id;
			// dd($pin);
			// echo $pin;
			// echo $plant_pincode;
			// dd($pin);
			if($pin != $plant_pincode){				
				$plantpincode = "Invalid PIN Code!";
				return Redirect::back()->withErrors($plantpincode);		
			}
		}				
		Session::put('machine_id',$machine_id);		
		Session::put('breakdown_limit',$breakdown_limit);		
		// echo Session::get('machine_id');
		// dd('machine_regno');
	}		
	//dd($req);
	// return view('/operatorsubmit');
	return view('/operatorcode');
});

//Route::post('operatorcodverfication', 'Controller@operatorcodverfication');

// Route::post('/operatorsystem', function(Request $req){
	// $pincode = $req->pin_code;
	// $req->session()->put('pincode', $pincode);
	// return view('/operatorsystem');
//});

Route::get ('pcu_unit/{id}', 'Controller@pcu_unit');
Route::get ('pcu_plant/{id}', 'Controller@pcu_plant');

Route::post('/operatorinfo', 'Controller@operatorinfo');

Route::post('/operatorlog', 'Controller@operatorlog');

Route::get('/operatorexist', function(){
	// session()->forget('op_name');
	$usertype = "Operator Already Loggedin";
	return view('operatorexist')->withErrors($usertype);
	// return view('operatorsubmit')->withErrors($usertype);
});


Route::get('skuvalueeeeeee', function(Request $req){
	//dd('');
	//dd($req);
	//$id = $req->sku_value_info;
	$id = $req->id;	
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	// echo $host;
	// echo $dbname;
	// echo $user;
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	
	$pincode_query = pg_query($db, "SELECT * FROM rm_skus WHERE id = $id");
	
	$rpm_flag = 0;
	if(Session::get('rpm_flag') != 1){
		session()->forget('rpm_default');
		session()->forget('rpm_min');
		session()->forget('rpm_max');
	}	
	$rpm_val = array();
	while($pin_row = pg_fetch_array($pincode_query)){
		// echo "<pre>";
		// print_r($pin_row);
		// echo "</pre>";
		//dd('');
		$rpm_default = $pin_row['rpm_default'];
		$rpm_min = $pin_row['rpm_min'];
		$rpm_max = $pin_row['rpm_max'];
		// $rpm_final = $pin_row['rpm_final'];
		// $str = "min=value, key2=value2";
		//$check = "min=".$rpm_min .", max=".$rpm_max .", default=".$rpm_default ."";
		//echo $check;
		
		$rpm_val[] = array( 
			"def"=> $rpm_default,
			"min"=> $rpm_min, 
			"max"=> $rpm_max);  
		$rpm_number = array();
		for($i=$rpm_min; $i<=$rpm_max; $i++){
			$rpm_number[] = array(
				"spinner" => $i,
				"def"=> $rpm_default,
				"min"=> $rpm_min, 
				"max"=> $rpm_max
				// "finalval"=> $rpm_final
			);
		}
	}	
	// echo "<pre>";
	// print_r();
	// echo "</pre>";
	// echo "<pre>";
	// print_r($rpm_val);
	// echo "</pre>";
	
	//dd();
	//dd($rpm_val[0]);
	// $json = json_encode($rpm_val); 
	$json = json_encode($rpm_number); 
	return $json;	
	
});

Route::post('skuvalue', function(Request $req){
	//dd($req);
	$id = $req->sku_value_info;
	
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	// echo $host;
	// echo $dbname;
	// echo $user;
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$pincode_query = pg_query($db, "SELECT * FROM rm_skus WHERE id = $id");
	while($pin_row = pg_fetch_array($pincode_query)){
		// echo "<pre>";
		// print_r($pin_row);
		// echo "</pre>";
		$rpm_default = $pin_row['rpm_default'];
		$rpm_min = $pin_row['rpm_min'];
		$rpm_max = $pin_row['rpm_max'];
		
		Session::put('sku_id',$id);
		Session::put('rpm_default',$rpm_default);
		Session::put('rpm_min',$rpm_min);
		Session::put('rpm_max',$rpm_max);
		
		return view('operator');
	}
});

Route::post('/opskudata', 'Controller@opskudata');


Route::get('/supervisor', function () {
    return view('supervisor');
});

Route::get('/executive', function () {
    return view('executive');
});

//Route::get('/shift_officer', function () {
    //return view('shift_officer');
    // return view('report');
//});

Route::get('/operator', function () {	
    return view('operator');
});

//Route::post('/operatorlogin', 'Controller@operatorlogin');

Route::get('/message', function(){
	Session::flash('message', 'You DO NOT have permission to access that DASHBOARD'); 
	$designation_id = Session::get('designation_id');
	if($designation_id == 2){
		return redirect('/manager');
	}else if($designation_id == 3){
		return redirect('/executive');		
	}else if($designation_id == 4){
		return redirect('/shift_officer');
	}else if($designation_id == 5){
		return redirect('/operator');
	}else if($designation_id == 1){
		return redirect('/');
	}
});


Route::get('/display_info', 'Controller@display_info');

Route::post('fetch', 'Controller@fetch');

Route::post('display_info_fetch', 'Controller@display_info_fetch');


Route::get('login', function(){
    return view('login');
});

Route::post('login', 'Controller@login');
Route::get('logout', 'Controller@logout');
Route::get('operatorlogout', 'Controller@operatorlogout');
Route::post('opreason', 'Controller@opreason');
Route::get('autoopreason/{operatorstarttime}/{operatorendtime}', 'Controller@opreason');
// Route::get('autoopreason', function(){
	// dd('hereeee');
// });

Route::get('opreasonid/{id}', function($id){
	//dd($id);	
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	
	$breakid = $id;
	// dd($breakid);
		
	$opreasonid_query = pg_query($db, "SELECT * FROM rm_breakdownsubreasons WHERE breakdown_id = $breakid");
	//dd($opreasonid_query);
		
	$rpm_val = array();
	while($opereason_row = pg_fetch_array($opreasonid_query)){
		$subbreakdown_id = $opereason_row['id'];
		$breakdown_id = $opereason_row['breakdown_id'];
		$subbreakdown_reason = $opereason_row['subbreakdown_reason'];
		$subbreakicon2 = $opereason_row['subbreakicon'];		
		
		// ====================================== Start Here...
		$machid = Session::get('machine_id');
		$selmachsubreason = pg_query($db, "SELECT * FROM rm_machine_subreasons WHERE machine_id=$machid");
		$machsubreason = array();
		$i =0;
		while($machinerow = pg_fetch_array($selmachsubreason)){
			// echo "<pre>";
			// print_r($machinerow);
			// echo "</pre>";
			$machsubreason[] = array(
				'subreasonid' => $machinerow['sub_reason_id']
			);
			$i++;
		}
		$machinesub = $machsubreason;
		foreach($machinesub as $k=>$v){
			// echo "<pre>";
			// print_r($v['subreasonid']);
			// echo "</pre>";		
			if($subbreakdown_id == $v['subreasonid']){
				$subbreakicon = url('/images').'/'.$subbreakicon2;
				$rpm_val[] = array( 
					"subbreakdown_id"=> $subbreakdown_id,
					"breakdown_id"=> $breakdown_id,
					"subbreakicon"=> $subbreakicon,
					"subbreakdown_reason"=> $subbreakdown_reason); 
			}//else{
				// echo "no";
			//}
		}
		
		// ====================================== End Here...
		
		
		//dd();
		// $subbreakicon = url('/images').'/'.$subbreakicon2;
		// $rpm_val[] = array( 
			// "subbreakdown_id"=> $subbreakdown_id,
			// "breakdown_id"=> $breakdown_id,
			// "subbreakicon"=> $subbreakicon,
			// "subbreakdown_reason"=> $subbreakdown_reason); 
	}	
	$json = json_encode($rpm_val); 
	return $json;	
});

Route::get('opreason', function(){
	return view('operator');
});

// ############################################### UNIT CRUD
Route::get('add-unit', function(){
	return view('addunit');
});
Route::get('unitedit/{id}', 'Controller@unitedit');
Route::get('editunit', function(){
	return view('editunit');
});
Route::post('/newunit', 'Controller@newunit');
Route::post('unitupdate', 'Controller@unitupdate');
// Route::get('/unitdelete/{id}', 'Controller@unitdelete');
Route::post('/unitdelete', 'Controller@unitdelete');

// ############################################# PLANT CRUD
Route::get('add-plant', function(){
	return view('addplant');
});
Route::get('plantedit/{id}', 'Controller@plantedit');
Route::get('editplant', function(){
	return view('editplant');
});
Route::post('/newplant', 'Controller@newplant');
Route::post('plantupdate', 'Controller@plantupdate');
// Route::get('/plantdelete/{id}', 'Controller@plantdelete');
Route::post('/plantdelete', 'Controller@plantdelete');

// ############################################# PLANT PINCODE CRUD
Route::get('add-plant-pincode', function(){
	return view('addplant-pincode');
});
Route::get('plant-pincodeedit/{id}', 'Controller@plantpincodeedit');
Route::get('editplant-pincode', function(){
	return view('editplant-pincode');
});
Route::post('/newplant-pincode', 'Controller@newplantpincode');
Route::post('plant-pincodeupdate', 'Controller@plantpincodeupdate');
// Route::get('/plantdelete/{id}', 'Controller@plantdelete');
Route::post('/plant-pincodedelete', 'Controller@plantpincodedelete');

// ############################################# CASCADE CRUD
Route::get('add-cascade', function(){
	return view('addcascade');
});
Route::get('cascadeedit/{id}', 'Controller@cascadeedit');
Route::get('editcascade', function(){
	return view('editcascade');
});
Route::post('/newcascade', 'Controller@newcascade');
Route::post('cascadeupdate', 'Controller@cascadeupdate');
// Route::get('/cascadedelete/{id}', 'Controller@cascadedelete');
Route::post('/cascadedelete', 'Controller@cascadedelete');

// ############################################# MACHINE CRUD
Route::get('add-machine', function(){
	return view('addmachine');
});
Route::get('machineedit/{id}', 'Controller@machineedit');
Route::get('editmachine', function(){
	return view('editmachine');
});
Route::post('/newmachine', 'Controller@newmachine');
Route::post('machineupdate', 'Controller@machineupdate');
Route::post('/machinedelete', 'Controller@machinedelete');
// Route::get('/machinedelete/{id}', 'Controller@machinedelete');

// ############################################# PRODUCT CRUD
Route::get('add-product', function(){
	return view('addproduct');
});
Route::get('productedit/{id}', 'Controller@productedit');
Route::get('editproduct', function(){
	return view('editproduct');
});
Route::post('/newproduct', 'Controller@newproduct');
Route::post('productupdate', 'Controller@productupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/productdelete', 'Controller@productdelete');

// ############################################# PRODUCT MIXER BCT CRUD
Route::get('add-product-mixer-bct', function(){
	return view('addproduct-mixer-bct');
});
Route::get('product-mixer-bctedit/{id}', 'Controller@productmixerbctedit');
Route::get('editproductmixerbct', function(){
	return view('editproduct-mixer-bct');
});
Route::post('/newproduct-mixer-bct', 'Controller@newproductmixerbct');
Route::post('product-mixer-bctupdate', 'Controller@productmixerbctupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/product-mixer-bctdelete', 'Controller@productmixerbctdelete');

// ############################################# PRODUCT MAPPING CRUD
Route::get('add-product-plant-mapping', function(){
	return view('addproduct-plant-mapping');
});
Route::get('product-plant-mappingedit/{id}', 'Controller@productplantmapping');
Route::get('editproduct-plant-mapping', function(){
	return view('editproduct-plant-mapping');
});
Route::post('/newproduct-plant-mapping', 'Controller@newproductplantmapping');
Route::post('product-plant-mappingupdate', 'Controller@productplantmappingupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/product-plant-mappingdelete', 'Controller@productplantmappingdelete');

// ############################################# SHIFT CRUD
Route::get('add-shift', function(){
	return view('addshift');
});
Route::get('shiftedit/{id}', 'Controller@shiftedit');
Route::get('editshift', function(){
	return view('editshift');
});
Route::post('/newshift', 'Controller@newshift');
Route::post('shiftupdate', 'Controller@shiftupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/shiftdelete', 'Controller@shiftdelete');

// ############################################# SKU CRUD
Route::get('add-sku', function(){
	return view('addsku');
});
Route::get('skuedit/{id}', 'Controller@skuedit');
Route::get('editsku', function(){
	return view('editsku');
});
Route::post('/newsku', 'Controller@newsku');
Route::post('skuupdate', 'Controller@skuupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/skudelete', 'Controller@skudelete');

// ############################################# MACHINE PRODUCT SKU CRUD
Route::get('addmachine-product-sku', function(){
	return view('addmachine-product-sku');
});
Route::get('editmachine-product-sku/{id}', 'Controller@machineproductskuedit');
Route::get('editmachine-product-sku', function(){
	return view('editmachine-product-sku');
});
Route::post('/newmachineproductsku', 'Controller@newmachineproductsku');
Route::post('machineproductskuupdate', 'Controller@machineproductskuupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/machineproductskudelete', 'Controller@machineproductskudelete');
Route::get('productskuplant/{id}', function($id){
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$plantid = $id;
	// dd($id);
	$plantid_query = pg_query($db, "SELECT * FROM rm_machines WHERE plant_id = $plantid");
	$machineidarray = array();
	while($pcudatarow = pg_fetch_array($plantid_query)){
		// echo "<pre>";
		// print_r($pcudatarow);
		// echo "</pre>";
		$machineidarray[] = array(
			'machineid' => $pcudatarow['id'],
			'machinename' => $pcudatarow['machine_name']
		);	
	}
	$jsonn = json_encode($machineidarray);
	return $jsonn;
});
Route::get('productskupro/{id}', function($id){
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$productskuid = $id;
	// dd($id);
	$plantid_query = pg_query($db, "SELECT * FROM rm_skus WHERE product_id = $productskuid");
	$productidarray = array();
	while($pcudatarow = pg_fetch_array($plantid_query)){
		// echo "<pre>";
		// print_r($pcudatarow);
		// echo "</pre>";
		$productidarray[] = array(
			'skuid' => $pcudatarow['id'],
			'skuname' => $pcudatarow['sku_name']
		);	
	}
	// dd();
	$jsonn2 = json_encode($productidarray);
	return $jsonn2;
});

// ############################################# SKU PACKING UNIT CRUD
Route::get('add-sku-unit', function(){
	return view('addskuunit');
});
Route::get('skuunitedit/{id}', 'Controller@skuunitedit');
Route::get('editskuunit', function(){
	return view('editskuunit');
});
Route::post('/newskuunit', 'Controller@newskuunit');
Route::post('skuunitupdate', 'Controller@skuunitupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/skuunitdelete', 'Controller@skuunitdelete');

// ############################################# SKU PACKING TYPES CRUD
Route::get('add-sku-types', function(){
	return view('addskutypes');
});
Route::get('skutypesedit/{id}', 'Controller@skutypesedit');
Route::get('editskutypes', function(){
	return view('editskutypes');
});
Route::post('/newskutypes', 'Controller@newskutypes');
Route::post('skutypesupdate', 'Controller@skutypesupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/skutypesdelete', 'Controller@skutypesdelete');


// ############################################# BREAKDOWN REASON CRUD
Route::get('add-breakreason', function(){
	return view('addbreakreason');
});
Route::get('breakreasonedit/{id}', 'Controller@breakreasonedit');
Route::get('editbreakreason', function(){
	return view('editbreakreason');
});
Route::post('/newbreakreason', 'Controller@newbreakreason');
Route::post('breakreasonupdate', 'Controller@breakreasonupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/breakreasondelete', 'Controller@breakreasondelete');

// ############################################# BREAKDOWN SUB-REASON CRUD
Route::get('add-breaksubreason', function(){
	return view('addbreaksubreason');
});
Route::get('breaksubreasonedit/{id}', 'Controller@breaksubreasonedit');
Route::get('editbreaksubreason', function(){
	return view('editbreaksubreason');
});
Route::post('/newbreaksubreason', 'Controller@newbreaksubreason');
Route::post('breaksubreasonupdate', 'Controller@breaksubreasonupdate');
// Route::get('/productdelete/{id}', 'Controller@productdelete');
Route::post('/breaksubreasondelete', 'Controller@breaksubreasondelete');


// ############################################# USER CRUD
Route::get('add-user', function(){
	Session::forget('email_existed');
	return view('adduser');
});

Route::get('useremailexist', function(){
	$email_existed = "EMAIL ALREADY EXIST";
	Session::put('email_existed',$email_existed);
	return view('adduser');
});
Route::get('userrolesubmit', 'Controller@userrolesubmit');
Route::post('userrolesubmit', 'Controller@userrolesubmit');

Route::get('getunitboxplant/{id}', 'Controller@getunitboxplant');

Route::get('useredit/{id}', 'Controller@useredit');
Route::get('edituser', function(){
	return view('edituser');
});
Route::post('/newuser', 'Controller@newuser');
Route::post('userupdate', 'Controller@userupdate');
Route::post('/userdelete', 'Controller@userdelete');
// Route::get('/userdelete/{id}', 'Controller@userdelete');



Route::post('register', 'Controller@register');

Route::get('chart', function(){
    return view('chart');
});


/// Filter Section Starts from here
Route::get('addplantpinajax/{id}', 'Controller@addplantpinajax');
Route::get('addcascadeajax/{id}', 'Controller@addcascadeajax');
Route::get('addmachineajax/{id}', 'Controller@addmachineajax');
Route::get('addmachineplantajax/{id}', 'Controller@addmachineplantajax');
Route::get('plantcasmachine/{id}', 'Controller@plantcasmachine');
Route::get('editplantpinajax/{id}', 'Controller@editplantpinajax');
Route::get('editcascadeajax/{id}', 'Controller@editcascadeajax');
Route::get('editproductajax/{id}', 'Controller@editproductajax');
Route::get('editproductplantajax/{id}', 'Controller@editproductplantajax');

// ####################################### Back button
Route::get('unitreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_cascade');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_unit = 'admin_unit';
	Session::put('admin_unit',$admin_unit);			
	return redirect('/');
});

Route::get('/plantreturn', function(){
	Session::forget('admin_unit');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_cascade');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_plant = 'admin_plant';
	Session::put('admin_plant', $admin_plant);
	return view('/welcome');
});

Route::get('cascadereturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_cascade = 'admin_cascade';
	Session::put('admin_cascade',$admin_cascade);			
	return redirect('/');
});

Route::get('breakreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_cascade');
	Session::forget('admin_subreasons');
	$admin_reasons = 'admin_reasons';
	Session::put('admin_reasons',$admin_reasons);			
	return redirect('/');
});


Route::get('breaksubreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_cascade');
	$admin_subreasons = 'admin_subreasons';
	Session::put('admin_subreasons',$admin_subreasons);			
	return redirect('/');
});


Route::get('machinereturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_cascade');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_machine = 'admin_machine';
	Session::put('admin_machine',$admin_machine);			
	return redirect('/');
});


Route::get('plantpinreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_cascade');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_plant_pincode = 'admin_plant_pincode';
	Session::put('admin_plant_pincode',$admin_plant_pincode);			
	return redirect('/');
});

////////
Route::get('productmixerreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_cascade');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_bct = 'admin_bct';
	Session::put('admin_bct',$admin_bct);			
	return redirect('/');
});


Route::get('productmapreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_cascade');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_product_mapping = 'admin_product_mapping';
	Session::put('admin_product_mapping',$admin_product_mapping);			
	return redirect('/');
});


Route::get('productreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_cascade');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_products = 'admin_products';
	Session::put('admin_products',$admin_products);			
	return redirect('/');
});

/////
Route::get('shiftreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_cascade = 'admin_cascade';
	Session::put('admin_cascade',$admin_cascade);			
	return redirect('/');
});


Route::get('skureturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_cascade');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_skus = 'admin_skus';
	Session::put('admin_skus',$admin_skus);			
	return redirect('/');
});

Route::get('machineproductskureturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_cascade');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	Session::forget('admin_skus');
	$admin_machine_product_skus = 'admin_machine_product_skus';
	Session::put('admin_machine_product_skus',$admin_machine_product_skus);			
	return redirect('/');
});


Route::get('skutypereturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_cascade');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_sku_type = 'admin_sku_type';
	Session::put('admin_sku_type',$admin_sku_type);			
	return redirect('/');
});


Route::get('skuunitreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_users');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_cascade');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_sku_unit = 'admin_sku_unit';
	Session::put('admin_sku_unit',$admin_sku_unit);			
	return redirect('/');
});

Route::get('userreturn', function(){
	Session::forget('admin_plant');
	Session::forget('admin_plant_pincode');
	Session::forget('admin_bct');
	Session::forget('admin_product_mapping');
	Session::forget('admin_unit');
	Session::forget('admin_machine');
	Session::forget('admin_products');
	Session::forget('admin_cascade');
	Session::forget('admin_skus');
	Session::forget('admin_sku_type');
	Session::forget('admin_sku_unit');
	Session::forget('admin_reasons');
	Session::forget('admin_subreasons');
	$admin_users = 'admin_users';
	Session::put('admin_users',$admin_users);			
	return redirect('/');
});


Route::get('onlyconnectedpcu', 'Controller@onlyconnectedpcu');
Route::get('onlydisconnectedpcu', 'Controller@onlydisconnectedpcu');
Route::get('pcuplantmachine/{id}', 'Controller@pcuplantmachine');
Route::get('productiondata', 'Controller@productiondata');
Route::get('pkt_cnt', 'Controller@pkt_cnt');


Route::post('pcudatainfo', function(Request $req){
	// dd($req);
	$plantid = $req->plantid;
	$status = $req->status;
	
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$pcudatainfo_query = pg_query($db, "SELECT * FROM rm_machines WHERE plant_id = $plantid");
	$pcuarray = array();
	while($pcudatarow = pg_fetch_array($pcudatainfo_query)){
		// echo "<pre>";
		// print_r($pcudatarow);
		// echo "</pre>";
		$machine_regno = $pcudatarow['machine_regno'];
		$livemachine = pg_query($db, "SELECT * FROM rm_live_data WHERE reg_no = '$machine_regno'");
		// dd($livemachine);
		if(pg_num_rows($livemachine)>0){
			while($machine_row = pg_fetch_array($livemachine)){
				// echo "<pre>";
				// print_r($machine_row);
				// echo "</pre>";
				$pcuarray[] = array(
					'bus_id' => $machine_row['bus_id'],
					'imei' => $pcudatarow['imei'],
					'reg_no' => $machine_row['reg_no'],
					'pkt_cnt' => $machine_row['pkt_cnt'],
					'sats_view' => $machine_row['sats_view'],
					'gsm_rssi' => $machine_row['gsm_rssi'],
					'batt_volt' => $machine_row['batt_volt']
					// 'main_volt' => $machine_row['main_volt']
				);
			}
			echo "<pre>";
			print_r($pcuarray);
			echo "</pre>";
			// echo "Nothing to show";
		}else{
			echo "No record found";
		}
	}
});
// Route::get('pcudatainfo/{plantid}/{status}', 'Controller@pcudatainfo');
Route::get('pcudatainfo', 'Controller@pcudatainfo');
Route::get('endsku',function(Request $req){
	date_default_timezone_set('Asia/Kolkata');	
	$id = Session::get('sku_row_id');
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	// dd(Session::all());
	Session::forget('sku_name');
	Session::forget('sku_rpm');
	$enddate = date("Y-m-d H:i:s");	
	
	$pcudatainfo_query = pg_query($db, "UPDATE rm_opskudata SET sku_end_at='$enddate' WHERE sku_id = $id");
	if($pcudatainfo_query){
		return Redirect::back();
	}
});

Route::get('svunit/{id}', 'Controller@svunit');
// Route::get('svunit/{id}', function(){
	// dd('here');
// });
Route::get('svplant/{id}', 'Controller@svplant');
Route::get('svcascade/{id}', 'Controller@svcascade');
// Route::get('svmachine/{id}', 'Controller@svmachine');
Route::get('svdata', 'Controller@svdata');    

Route::get('shift_officer', 'Controller@showActual');
Route::post('shift_officer', 'Controller@showActual');
//Route::post('shift_officer', function(Request $req){
	//dd($req);
	//$shiftofficerplantid = $req->plantid;
	//return view('supervisor_current')->with('shiftofficerplantid',$shiftofficerplantid);
//});
//Route::get('current_shift', 'Controller@showData');
Route::post('current_shift', 'Controller@showData');
Route::get('detail', 'Controller@showDetail');

Route::post('show_graph', 'Controller@showGraph');

Route::get('liveprodata', 'Controller@liveprodata');
Route::get('liveprodction', 'Controller@liveprodction');

Route::get('getuserroleinfo/{id}', function($id){
	// dd($id);
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$userrole = pg_query($db, "SELECT * FROM rm_userrights WHERE user_id = $id");
	while($userrolerow = pg_fetch_array($userrole)){
		// echo "<pre>";
		// print_r($userrolerow);
		// echo "</pre>";
		
		$userunitrow = $userrolerow['unit_id'];
		$unitexploe = explode(',', $userunitrow);
		$userunitarray = array();
		$userplantarray = array();
		foreach($unitexploe as $uk){
			// echo $uk;
			// echo $uv;
			$userunitarray[] = array(
				'unitkey' => $uk
			);	
			$userplantrow = $userrolerow['plant_id'];
			$plantexploe = explode(',', $userplantrow);
			foreach($plantexploe as $pk){
				// echo $pk;
				// echo $pv;
				// echo "<br/>";
				$userplantarray[] = array(
					'plantkey' => $pk,
				);				
			}
			// echo "<pre>";
			// print_r($userinfoarray);
			// echo "</pre>";
			
		}
		
		$finalarray = json_encode($userunitarray+$userplantarray);
		return $finalarray;
		// echo "<pre>";
		// print_r($userunitarray);
		// echo "<br/>";
		// print_r($userplantarray);
		// print_r($finalarray);		
		// echo "<pre>";		
	}
});

// Route::get('shift_officer/{id}',function(){
	// dd('here');
// });

Route::post('checked', function(Request $req){
	// dd($req);
	$userrole = $req->userrrr;
	return view('addrole')->with('userrole',$userrole);
});

Route::get('casmac/{id}', function($id){
	// dd($id);
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$machinequery = pg_query($db, "SELECT id,machine_name FROM rm_machines WHERE cascade_id = $id");
	$macarray = array();
	while($machinerow = pg_fetch_array($machinequery)){
		$machineid = $machinerow['id'];
		$machinename = $machinerow['machine_name'];
		$macarray[] = array(
			'machineid' => $machineid,
			'machinename' => $machinename
		);
	}
	$machinejson = json_encode($macarray);
	return $machinejson;
});

// Route::post('export', 'Controller@showData');
	
Route::post('export', function(Request $req){	
	// echo "<pre>";
	// print_r(Session::get('reportdata'));
	// echo "</pre>";
	// dd(Session::get('reportdata'));
	
	// dd('export');
	// $host = Session::get('host');
	// $dbname = Session::get('dbname');
	// $user = Session::get('user');
	// $pass = Session::get('pass');
	// $db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	// $reportexport = pg_query($db, "SELECT * FROM rm_units");
	// $reportexport = pg_exec($db, "SELECT EXTRACT(EPOCH FROM (date_end_at::time - date_start_at::time)) as breakdown_part FROM rm_oplogins LEFT JOIN rm_opdownreasons ON rm_opdownreasons.machine_id = rm_oplogins.machine_id");
	$i =1;
	$html = "<table>
		<tr>
			<th>S.No.</th>
			<th>Date</th>
			<th>Operator</th>
			<th>Cascade</th>
			<th>Machine</th>
			<th>SKU</th>
			<th>Shift</th>
			<th>RPM</th>
			<th>CLD</th>
			<th>Production</th>
			<th>Downtime</th>
			<th>OEE%</th>
		</tr>";
	$reportdata = Session::get('reportdata');
	// $html .= ; 
	
	if($req->resulttt){
		// dd('resultt');
		$html .= $req->resulttt;
	}else{
		// dd('no request');
		foreach($reportdata as $k=>$v){
			// echo "<pre>";
			// print_r($v);
			// echo "<pre>";
			// echo $v['logintime'];
			// echo $v['first_name'].$v['last_name'];
			// echo $v['cascade_name'];
			// echo $v['machine_name'];
			// echo $v['sku_name'];
			// echo $v['shift_code'];
			// echo $v['rpm_default'];
			// echo $v['cld'];
			// echo $v['total_production'];
			// echo $v['breakdown_part'];
			// echo $v['oee'];
					
			$html .="<tr>
				<th>".$i."</th>
				<th>".$v['logintime']."</th>
				<th>".$v['first_name']." ".$v['last_name']."</th>
				<th>".$v['cascade_name']."</th>
				<th>".$v['machine_name']."</th>
				<th>".$v['sku_name']."</th>
				<th>".$v['shift_code']."</th>
				<th>".$v['rpm_default']."</th>
				<th>".$v['cld']."</th>
				<th>".$v['total_production']."</th>
				<th>".$v['breakdown_part']."</th>
				<th>".$v['oee']."</th>
			</tr>";
			$i++;
		}
	}
	$html .="</table>";
	header('Content-Type:application/xls');
	header('Content-Disposition:attachment;filename=report.xls');
	header("Pragma: no-cache"); 
	echo $html;
});

Route::get('checklogoutoperator/{id}', function($id){
	// dd($id);
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$operatorsku = pg_query($db, "SELECT * FROM rm_opskudata WHERE operator_id=$id");
	$operatorskurow = pg_fetch_array($operatorsku);		
	$machineid = $operatorskurow['machine_id'];	
	$operatormachine = pg_query($db, "SELECT * FROM rm_machines WHERE id=$machineid");
	$operatormachinerow = pg_fetch_array($operatormachine);
	// $machineid = $operatorskurow['machine_id'];
	
	$date = new \DateTime();
	$now = date_format($date, 'Y-m-d H:i:s');
	// dd($now);
	$operatorskuupdate = pg_query($db, "UPDATE public.rm_opskudata SET sku_end_at='$now' WHERE operator_id=$id");
	$operatorskuupdate = pg_query($db, "UPDATE public.rm_machines SET loggedin=0 WHERE id=$machineid");
	
	// dd('here');
	
	$operatorquery = pg_query($db, "UPDATE public.rm_oplogins SET loggedin=0 WHERE operator_id=$id");	
	if(pg_affected_rows($operatorquery)>0){
		// Session::flush();
		// session_unset();
		echo "UPDATED SUCCESSFULLY";
	}else{
		echo "NOT UPDATED";
	}
});

Route::get('operatorautologout/{id}', function($id){	
	$opidd = $id;
	$host = Session::get('host');
	$dbname = Session::get('dbname');
	$user = Session::get('user');
	$pass = Session::get('pass');
	$db = pg_connect("host=$host dbname=$dbname user=$user password=$pass");
	$operatorskuu = pg_query($db, "SELECT loggedin FROM rm_oplogins WHERE operator_id = $opidd ORDER BY logintime DESC");
	$operatorloggedout = pg_fetch_array($operatorskuu);
	// echo "<pre>";
	// print_r($operatorloggedout['loggedin']);
	// echo "</pre>";
	// dd($operatorloggedout['loggedin']);
	
	if($operatorloggedout['loggedin'] == 0){
		return 0;		
	}else{
		return 1;
	}
});